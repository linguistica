// Driver for stress discovery with Dynamic Computational Networks
// Copyright © 2009 The University of Chicago
#ifndef LEARNING_H
#define LEARNING_H

// See http://linguistica.uchicago.edu/dcnstress.htm for history and
// documentation.

#include "DCNgrammar.h"

namespace linguistica { namespace ui { class status_user_agent; } }
typedef class QString* corpusType;

/**
 *	The learning class uses a simplified version of Gary Larson's
 *  simulated annealing process to find an appropriate grammar
 *  given a corpus of various words with stress. The name is kind
 *  of weird -- it's easiest to think of an instance as a learning
 *  machine. It takes a corpus (an array of QStrings) and returns
 *  a possible grammar. It's also not very well implemented --
 *  nearly everything is public. 
 *
 *  The most interesting function is
 *  runHelper(), which is the essence of the learning algorithm.
 *  It is a first attempt at the learning algorithm, so at the 
 *  moment it's kind of crappy. Luckily, the GUI supports
 *  inputting different values for the learning algorithm.
 */
class learning  
{

public:
	learning();
	virtual ~learning();
	void run(linguistica::ui::status_user_agent& status_display);
	bool					isSuccessful() { return successful; }
	void					setCorpus(corpusType corpus, int numberOfWords);
	grammar*				returnGrammar();

	float					TfromUser;
	float					increaseWhenWrong;
	float					decreaseWhenRight;
	int						numberOfTries;
	unsigned int			cutoffFromUser;

	float					startingAlpha;
	float					startingBeta;
	float					startingI;
	float					startingF;

private:
	void runHelper(class Q3TextStream& logstream,
	               linguistica::ui::status_user_agent& status_display);
	bool					successful;
	corpusType				corpus;
	int						numberOfWords;
	grammar					theGrammar;
	grammar					possibleGrammar;
	int						maxSyl;
};

#endif // LEARNING_H
