// Implementation of learningsyl methods
// Copyright © 2009 The University of Chicago
#include "DCNlearningsyl.h"

#include <cstdlib>
#include <ctime>
#include <fstream>
#include <QString>

#include "DCNgrammarsyl.h"
#include "DCNnetworksyl.h"
#include "DCNcorpussyl.h"
#include "DCNdcnword.h"
#include "ui/Status.h"
#include "random.h"

namespace {
	using linguistica::ui::status_user_agent;
}

learningsyl::learningsyl()
	: successful(false),
	corpus(),
	numberOfWords(0),
	// helpers
	grammar(),
	net(),
	possibleGrammar(),
	possibleNet(),
	rGrammar(),
	// parameters
	TfromUser(1.0),
	increaseWhenWrong(0.02f),
	decreaseWhenRight(0.95f),
	numberOfTries(255),
	cutoffFromUser(255),
	startingAlpha(-0.3f),
	startingBeta(-0.3f) { }

learningsyl::~learningsyl() { }

void learningsyl::runHelper(std::ostream &logstream, status_user_agent& status)
{
	using std::srand;
	using std::time;
	using std::rand;

	srand(static_cast<unsigned>(time(0)));

	successful = false;

	//careful, i'm specifying values
	float				T = TfromUser;
	int					cutoff = cutoffFromUser;
	int					corpusIndex = 0;

	int					maxCorpusIndex = numberOfWords;

//	int					syl = 10;

	float				alpha = 0;
	float 				beta = 0;

	grammar.setValues(startingAlpha,startingBeta);
	net.setGrammar(&grammar);

	status.progress.clear();
	status.progress.set_denominator(cutoffFromUser);
	int updateNumber = 0;
	while (T > 0.01 && cutoff >= 0) {
		status.progress = updateNumber;
		logstream
			<< "T: "		<< T								<< "\t" 
			<< "#: "		<< cutoffFromUser - cutoff			<< "\t"
			<< "a: "		<< grammar.getAlpha()				<< "\t"
			<< "b: "		<< grammar.getBeta()				<< "\t"
			<< "w: "		<< grammar.getSonority(QChar('w'))	<< "\t" 
			<< "a: "		<< grammar.getSonority(QChar('a'))	<< "\t"
			<< "k: "		<< grammar.getSonority(QChar('k'))	<< "\t"
			<< "u: "		<< grammar.getSonority(QChar('u'))	<< "\n";
			

		// 1) take a word from the corpus
		dcnword word = corpus.wordAt(corpusIndex);

		// 2) check all the letters -- if any are new, assign a random float
		QString text = word.getText();
		for (int i = 0; i < text.length(); i++)
		{
			if (!grammar.isInMap(text.at(i)))
			{
				const float randomNum =
					static_cast<float>(rand()) /
					static_cast<float>(RAND_MAX) / 2 +
					float(0.5);
				grammar.setSonority(text.at(i), randomNum);
			}
		}

		// 3) make sonority vector
		net.setWord(text);

		// 4) run network with word & alpha & beta
		net.equilibrium();

		// 5) compare network & word.maxima
		QString expMaxima			= net.getMaxima();	// experimental maxima
		QString targetMaxima		= word.getMaxima();
	
		// 6) make changes to letters associated with word
		bool changes = false;
		for (int j = 0; j < text.length(); j++)
		{
			QChar letter		= text.at(j);
			float s				= grammar.getSonority(letter);
			QChar expChar		= expMaxima.at(j);
			QChar targetChar	= targetMaxima.at(j);

			if (expChar == 'H' && targetChar != 'H')
			{
				// make sure there are no negative letters...
				// I don't know if this is necessary or not...
//				if (s > .1)
				grammar.setSonority(letter, s - 0.1f);
				changes = true;
			}
			else if (expChar != 'H' && targetChar == 'H')
			{
				grammar.setSonority(letter, s + 0.1f);
				changes = true;
			}
		}

		// 7) modify alpha & beta
		if (changes) {
			using linguistica::random_small_float;
			// random float between -1 and 1
			float deltaAlpha = T * random_small_float();
			alpha = grammar.getAlpha() + deltaAlpha;

			float deltaBeta = T * random_small_float();
			beta = grammar.getBeta() + deltaBeta;
			
			possibleGrammar.setValues(alpha, beta);
			possibleNet.setWord(text);
			possibleNet.setGrammar(&possibleGrammar);
			possibleNet.equilibrium();
			
			if (possibleNet.isConverged())
			{
				// alpha * beta > .3 may be another thing to put in here
				/*
				if (alpha > 0)
					alpha *= -1;
				if (beta > 0)
					beta  *= -1;
				*/
				grammar.setValues(alpha, beta);
			}
			T = T + increaseWhenWrong;
			//T = T + sqrt( pow(deltaAlpha, 2.0) + pow(deltaBeta, 2.0)
			//	+ pow(deltaI, 2.0) + pow(deltaF, 2.0) );
			
		}
		else
		{
			T = T * decreaseWhenRight;
		}
		//T = T - .01;
		cutoff--; updateNumber++;
		corpusIndex = (corpusIndex + 1)%maxCorpusIndex;
		if (corpusIndex % 256 == 0)
			logstream.flush();
	}
	status.progress.clear();

	possibleGrammar.setValues(alpha, beta);
	possibleNet.setGrammar(&possibleGrammar);
	possibleNet.equilibrium();
	if (cutoff > 2 && possibleNet.isConverged())
		successful = true;
}

void learningsyl::run(status_user_agent& status_display)
{
	using std::time_t;
	using std::ctime;
	using std::time;

	grammar.clearMap();
	std::ofstream logstream("DCNsyllog.txt");  // declare and open file stream
	time_t rawtime;

	time ( &rawtime );
	logstream << "Learning Algorithm run at " << ctime(&rawtime);
	
	for (int i = 0; i < numberOfTries; i++)
	{
		logstream << "\n\tTRIAL NUMBER " << i+1 << "\n";
		runHelper(logstream, status_display);
		if (this->isSuccessful()) break;
	}
	logstream.close();

	rGrammar = new grammarsyl(grammar); // can do this because of the magic of copying QMaps
}


void learningsyl::setCorpus(corpussyl corpus)
{
	this->corpus = corpus;
	this->numberOfWords = corpus.numberOfWords();
}
