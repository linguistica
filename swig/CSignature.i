%include "CLParse.i"

%ignore CSignatureListViewItem;

%ignore CSignature::IncrementRightNeighborVector;
%ignore CSignature::IncrementLeftNeighborVector;
%ignore CSignature::GetLengthOfPointerToMe;
%ignore CSignature::CheckForFactorability;
%ignore CSignature::AttachWord;
%ignore CSignature::GetNumberOfLetters;
%ignore CSignature::TestFirstLetterOfSuffix;

%ignore CSignature::SetNewStemFrequencies;
%ignore CSignature::SetNewStemPtrList;
%ignore CSignature::GetPrefixPtrList;
%ignore CSignature::GetSuffixPtrList;
%ignore CSignature::SetNewStemCounts;
%ignore CSignature::SetNewWordCounts;
%ignore CSignature::SetNewAffixFrequencies;
%ignore CSignature::CreateNewStemPtrList;
%ignore CSignature::CreateNewPrefixPtrList;
%ignore CSignature::CreateNewSuffixPtrList;
%ignore CSignature::GetNumber;
%ignore CSignature::GetWordCounts;

%{
#include "Signature.h"
%}
%include "Signature.h"

%pythoncode %{
	CSignature.__str__ = lambda self: ".".join([ str(f)  for f in self ])
%}
