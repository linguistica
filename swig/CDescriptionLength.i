%ignore CDescriptionLengthListViewItem;

%ignore CDescriptionLength::GetCompressedLengthOfCorpus;
%ignore CDescriptionLength::GetAffixesTotalPhonologicalInformationContent;
%ignore CDescriptionLength::TotalDL;

%{
#include "DescriptionLength.h"
%}
%include "DescriptionLength.h"
