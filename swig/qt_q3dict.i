/*
This makes Q3Dicts look like Python dict()s, at least for reading.


wrapping C++ templates:
	http://www.swig.org/Doc1.3/SWIGPlus.html#SWIGPlus_nn30

raising Python exceptions in SWIG wrappers:
	http://www.swig.org/Doc1.3/Python.html#Python_nn44

Python iterators:
	http://docs.python.org/library/stdtypes.html#iterator-types

*/

%include "CCorpusWord.i"
%include "CAffix.i"
%include "CSignature.i"
%include "CStem.i"

%{
#include <Qt3Support/Q3Dict>
#include <Qt3Support/Q3DictIterator>
%}

template<class type >
class Q3Dict {
public:
	Q3Dict(int size = 17, bool caseSensitive = true);
	Q3Dict( const Q3Dict<type > & dict );
	~Q3Dict();

	void	clear();
	uint	count() const;
	type	*find( const QString & key ) const;
	void	insert ( const QString & key, const type * item );
	bool	isEmpty () const;
	bool	remove ( const QString & key );
	void	replace ( const QString & key, const type * item );
	void	resize ( uint newsize );
	uint	size () const;
	void	statistics () const;
	type *	take ( const QString & key );
};

%extend Q3Dict {
	bool	__contains__( const QString & key ) const {
		if ($self->find(key) == 0)
			return FALSE;
		else
			return TRUE;
	}
	Q3DictIterator<type > *	__iter__() {
		return new Q3DictIterator<type >(*$self);
	}
	type *	__getitem__(QString key) {
		return $self->find(key);
	}
	int		__len__() const {
		return $self->count();
	}
	void	__setitem__( const QString & key, const type * item ) {
		$self->replace(key,item);
	}
};


template<class type >
class Q3DictIterator {
public:
	Q3DictIterator(const Q3Dict<type > & dict);
	~Q3DictIterator();
	type *	current();
	QString	currentKey();
	type *	toFirst();
	uint	count();
	bool	isEmpty();

	type *	operator() ();
//	type *	operator++ ();
};
%exception next { // termination for Python iterator
	$action
	if (result.isNull()) {
		PyErr_SetString(PyExc_StopIteration, "blahblah");
		return NULL;
	}
}
%extend Q3DictIterator {
	Q3DictIterator<type > *	__iter__() {
		return $self;
	}
	QString	next() {
		if ( ++(*($self)) == 0 )
			return NULL;
		return $self->currentKey();
	}
};


%define Q3DICT_TEMPLATE_WRAP(suffix, T...)
%template(Q3Dict_ ## suffix)	Q3Dict<T >;
%template(Q3DictIterator_ ## suffix)	Q3DictIterator<T >;
%pythoncode %{
(Q3Dict_ ## suffix).items = lambda self: [(k,self[k]) for k in self]
(Q3Dict_ ## suffix).keys = lambda self: [k for k in self]
(Q3Dict_ ## suffix).values = lambda self: [self[k] for k in self]
(Q3Dict_ ## suffix).iteritems = lambda self: ((k,self[k]) for k in self)
(Q3Dict_ ## suffix).iterkeys = lambda self: (k for k in self)
(Q3Dict_ ## suffix).itervalues = lambda self: (self[k] for k in self)
%}
%enddef

Q3DICT_TEMPLATE_WRAP(CCorpusWord, CCorpusWord)
Q3DICT_TEMPLATE_WRAP(PrefixSet, PrefixSet)
Q3DICT_TEMPLATE_WRAP(SignatureSet, SignatureSet)
Q3DICT_TEMPLATE_WRAP(StemSet, StemSet)
Q3DICT_TEMPLATE_WRAP(SuffixSet, SuffixSet)
