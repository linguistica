#!/usr/bin/python

import pprint

filename = "/home/luitien/blkhs12.txt"

import lxa

L = lxa.lexicon
lxa.read_corpus(filename, 100000)
L.FindSuffixes()
stems = L.GetAllStems()
print '%d stems' % len(stems)
words = L.GetAllWords()
print '%d words' % len(words)

for word in words:
	print str(word)		# word is a QString

print '###'

print words[ lxa.QString('gentleness') ].current().GetStem()
# <<< gentle

print words[ lxa.QString('gentleness') ].current().GetSuffix()
# <<< ness

###
## Or, the same thing using results.py:
# import lxa
# lxa.read_corpus(filename, 100000)
# lxa.lexicon.FindSuffixes()
#
# import results
#
# stems = results.all_stems()
# words = results.all_words()
#
# for word in words:
#	print word
#
# print '###'
#
# print results.word('gentleness')
