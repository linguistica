%ignore CStringSurrogate( const CParse& Parse, const int StartIndex = 0, const int Length = -1, bool Backwards = 0 );

// see __getitem__() in %extend below
%ignore CStringSurrogate::operator[];

// yes, three underscores:
%newobject CStringSurrogate___getitem__;

%{
#include "Trie.h"
#include "Parse.h"
#include "StringSurrogate.h"
%}

%include "StringSurrogate.h"
%extend CStringSurrogate {

	std::string __str__() {
		return $self->Display().toStdString();
	}

	QChar __getitem__(const int n) const {
		return $self->Display().at(n);
	}
};
