%{
#include <QBool>
%}

// QBool is defined in qglobal.h, a long, complicated header.
// We extract the QBool definition here to make swig’s job a bit easier.

class QBool
{
    bool b;

public:
    inline explicit QBool(bool B) : b(B) {}
    inline operator const void *() const
    { return b ? static_cast<const void *>(this) : static_cast<const void *>(0); }
};
