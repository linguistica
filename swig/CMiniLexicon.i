%ignore CMiniLexicon::FindAllomorphy2002;
%ignore CMiniLexicon::LogFileRemarkEntropy3;

// can't wrap operator^=
%ignore CMiniLexicon::operator^=;

%ignore MAXIMUM_LENGTH_OF_INPUT_LINE;

%ignore CMiniLexicon::CreateADeletingSignature;
%ignore CMiniLexicon::ClearAllMorphemeCounts;
%ignore CMiniLexicon::HMMCorpusListDisplay;
%ignore CMiniLexicon::HMMLexiconListDisplay;
%ignore CMiniLexicon::AddEntry;
%ignore CMiniLexicon::LookUp;

%ignore CMiniLexicon::GetNumberOfSuffixes;
%ignore CMiniLexicon::GetNumberOfGetNumberOfStems;
%ignore CMiniLexicon::GetNumberOfUnanalyzedWords;
%ignore CMiniLexicon::SetIndex;
%ignore CMiniLexicon::AddWordBoundaries;


// ``Defined in MiniLexicon_DeMarcken.cpp''
%ignore CMiniLexicon::HMMBootStrap;
%ignore CMiniLexicon::ExpectationMaximization;
%ignore CMiniLexicon::Forward;
%ignore CMiniLexicon::Backward;
%ignore CMiniLexicon::ViterbiParse;
%ignore CMiniLexicon::ViterbiParseString;
%ignore CMiniLexicon::RemoveNonSingletonMembersWithLowCount;

%{
#include "MiniLexicon.h"
%}

%include "MiniLexicon.h"

%extend CMiniLexicon {
	int		GetNumberOfUnAnalyzedWords(void) {
		int output = 0;
		int &ref = output;
		$self->GetNumberOfAnalyzedWords(ref);
		return output;
	}
	int		GetNumberOfAnalyzedWords(void) {
		int output = 0;
		int &ref = output;
		return $self->GetNumberOfAnalyzedWords(ref);
	}
};
