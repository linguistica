%include "CLParse.i"

%ignore CWordListViewItem;
%ignore CStemListViewItem;

%ignore CStem::ComputeMyDL;
%ignore CStem::ComputeBoltzmannScore;

%{
#include "Stem.h"
%}

%include "Stem.h"
