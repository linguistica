%include "qt_qstring.i"
%include "CSignature.i"
%include "CStem.i"
%include "CAffix.i"

%{
#include <QList>
#include <QListIterator>
%}


// The methods involving ``iterator'' type are commented out
// because I don't want to bother figuring out what that type
// is if I don't have to.
//
// It was choking on QList::removeOne().  I don't know why.

template<class T >
class QList {
public:
	QList ();
	QList ( const QList<T> & other );
	~QList ();
	void append ( const T & value );
	const T & at ( int i ) const;
	T & back ();
	const T & back () const;
//	iterator begin ();
//	const_iterator begin () const;
	void clear ();
//	const_iterator constBegin () const;
//	const_iterator constEnd () const;
	bool contains ( const T & value ) const;
	int count ( const T & value ) const;
	int count () const;
	bool empty () const;
//	iterator end ();
//	const_iterator end () const;
//	iterator erase ( iterator pos );
//	iterator erase ( iterator begin, iterator end );
	T & first ();
	const T & first () const;
	T & front ();
	const T & front () const;
	int indexOf ( const T & value, int from = 0 ) const;
	void insert ( int i, const T & value );
//	iterator insert ( iterator before, const T & value );
	bool isEmpty () const;
	T & last ();
	const T & last () const;
	int lastIndexOf ( const T & value, int from = -1 ) const;
	QList<T> mid ( int pos, int length = -1 ) const;
	void move ( int from, int to );
	void pop_back ();
	void pop_front ();
	void prepend ( const T & value );
	void push_back ( const T & value );
	void push_front ( const T & value );
	int removeAll ( const T & value );
	void removeAt ( int i );
	void removeFirst ();
	void removeLast ();
//	bool removeOne ( const T & value );
	void replace ( int i, const T & value );
	int size () const;
	void swap ( int i, int j );
	T takeAt ( int i );
	T takeFirst ();
	T takeLast ();
	QSet<T> toSet () const;
	std::list<T> toStdList () const;
	QVector<T> toVector () const;
	T value ( int i ) const;
	T value ( int i, const T & defaultValue ) const;
	bool operator!= ( const QList<T> & other ) const;
	QList<T> operator+ ( const QList<T> & other ) const;
	QList<T> & operator+= ( const QList<T> & other );
	QList<T> & operator+= ( const T & value );
	QList<T> & operator<< ( const QList<T> & other );
	QList<T> & operator<< ( const T & value );
	QList<T> & operator= ( const QList<T> & other );
	bool operator== ( const QList<T> & other ) const;
//	T & operator[] ( int i );
//	const T & operator[] ( int i ) const;
};

%extend QList {
	bool	__contains__(const T &item) {
		return $self->contains(item);
	}
	QListIterator<T > *	__iter__() {
		return new QListIterator<T >(*$self);
	}
	int		__len__() {
		return $self->count();
	}
};


// termination condition for python iterator:
// args[0] is self.
%pythonprepend QListIterator::next() {
	if not args[0].hasNext():
		raise StopIteration
}
template<class T >
class QListIterator {
public:
	QListIterator ( const QList<T> & list );
	bool findNext ( const T & value );
	bool findPrevious ( const T & value );
	bool hasNext () const;
	bool hasPrevious () const;
	const T & next ();
	const T & peekNext () const;
	const T & peekPrevious () const;
	const T & previous ();
	void toBack ();
	void toFront ();
	QListIterator & operator= ( const QList<T> & list );
};

%extend QListIterator {
	QListIterator<T > *	__iter__() {
		return $self;
	}
};

%template(QList_QString)   QList<QString >;
%template(QListIterator_QString)   QListIterator<QString >;

%template(SignatureSet)	QList<class CSignature* >;
%template(QListIterator_CSignature)   QListIterator<CSignature* >;
typedef QList<class CSignature* >	SignatureSet;

%template(StemSet)	QList<class CStem* >;
%template(QListIterator_CStem)   QListIterator<CStem* >;
typedef QList<class CStem* >	StemSet;

%template(SuffixSet)	QList<class CSuffix* >;
%template(QListIterator_CSuffix)   QListIterator<CSuffix* >;
typedef QList<class CSuffix* >	SuffixSet;
