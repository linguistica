%include "CTrie.i"
%include "CStem.i"
%include "CSignature.i"
%include "CCorpusWord.i"
%include "CAffix.i"


%ignore TCollection::operator[];
%ignore TCollection::SeekChain;

// What's up with these?  They use methods that don't seem to be
// defined anywhere.
%ignore TCollection::DumpVisibleToLogFile;
%ignore TCollection::DumpVisibleWords;
%ignore TCollection::FindMemberExtensions;
%ignore TCollection::T_PredecessorFrequency;
%ignore TCollection::T_SuccessorFrequency;
%ignore TCollection::operator<<;

%{
#include "CollectionTemplate.h"
%}
%include "CollectionTemplate.h"

%extend TCollection {
	bool	__contains__( CStringSurrogate& thing) {
		return $self->Contains(thing);
	}
	int		__len__() {
		return $self->GetSize();
	}
	T *		__getitem__(uint n) {
		return $self->GetAt(n);
	}
};

%define TCOLLECTION_TEMPLATE_WRAP(suffix, T...)
%template(TCollection_ ## suffix)    TCollection<T >;
%pythoncode %{
(TCollection_ ## suffix).__iter__ = lambda self: ( self.GetAt(k)  for k in range(len(self)) )
%}
%enddef

// CWordCollection derives from this:
TCOLLECTION_TEMPLATE_WRAP(CStem, CStem)
// CStemCollection derives from this:
TCOLLECTION_TEMPLATE_WRAP(class_CStem, class CStem)
// CSignatureCollection derives from this:
TCOLLECTION_TEMPLATE_WRAP(CSignature, class CSignature)
// CCorpusWordCollection derives from this:
TCOLLECTION_TEMPLATE_WRAP(CCorpusWord, CCorpusWord)
// CSuffixCollection derives from this:
TCOLLECTION_TEMPLATE_WRAP(CSuffix, CSuffix)
