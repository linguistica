%include "qt_qbool.i"
%include "qt_qlist.i"
%include "qt_qstring.i"

%{
#include <QStringList>
%}

// NB: this does not (currently) inherit SWIG /extensions/ from QList.
// See comment in lxa.i about director classes.

#define QT_BEGIN_HEADER
#define QT_BEGIN_NAMESPACE
#define QT_MODULE(x)
#define Q_CORE_EXPORT
#define QT_END_NAMESPACE
#define QT_END_HEADER

%ignore QtPrivate::QStringList_sort;
%ignore QtPrivate::QStringList_join;
%ignore QtPrivate::QStringList_filter;
%ignore QtPrivate::QStringList_contains;
%ignore QtPrivate::QStringList_replaceInStrings;
%ignore QtPrivate::QStringList_indexOf;
%ignore QtPrivate::QStringList_lastIndexOf;
%ignore QStringList::fromLast;

%include "swig/qt_qbool.i"
%include "qstringlist.h"

typedef QList<QString>::iterator string_list_iterator;
typedef QList<QString>::const_iterator string_list_const_iterator;

%extend QStringList {
	string_list_iterator QT3_SUPPORT fromLast();
	string_list_const_iterator QT3_SUPPORT fromLast() const;

// from %extend QList:
	bool __contains__(const QString &item) {
		return $self->contains(item);
	}
	QListIterator<QString>* __iter__() {
		return new QListIterator<QString>(*$self);
	}
	int __len__() {
		return $self->count();
	}
	QString __getitem__(int n) {
		return $self->at(n);
	}
};
