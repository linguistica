%include "TCollection.i"
%include "CSignature.i"

%ignore CSignatureCollection::CalculateFrequencies;
%ignore CSignatureCollection::GetMDL;
%ignore CSignatureCollection::PutAffixesOfRegularSignaturesIntoNewSuffixes;
%ignore CSignatureCollection::PutAffixesOfRegularSignaturesIntoNewPrefixes;
%ignore CSignatureCollection::TryToRemove;

%ignore CSignatureCollection::FindAllomorphy2002;

%{
#include "SignatureCollection.h"
%}
%include "SignatureCollection.h"
