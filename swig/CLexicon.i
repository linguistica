%ignore CLexicon::CreateNewFSA();	// ?
%ignore CLexicon::GetCorpusWord(const CStringSurrogate&);	// ?
%ignore CLexicon::CutMtCorpusWithMorphologyAnalyses(QString, QString, StringToPtrCStem &, int);	// ?

%{
#include "Lexicon.h"
%}

%include "Lexicon.h"
%extend CLexicon {
	int ReadCorpus(char *FileName, int NumberOfWords = 5000) {
		QString qFileName(FileName);
		return $self->ReadCorpus(qFileName, NumberOfWords);
	}
};
