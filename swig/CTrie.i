%ignore CTrieListViewItem;

// commented out in Trie.cpp
%ignore CNode::DumpVisibleWords;


// ?
%ignore CNode::GetLink( const CStringSurrogate );
%ignore CNode::GetLink( CStringSurrogate& );

%{
#include "Trie.h"
%}
%include "Trie.h"
