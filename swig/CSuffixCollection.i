%include "TCollection.i"
%include "CAffix.i"

%ignore CSuffixCollection::FindFactorableSuffixes;
%ignore CSuffixCollection::FindCombinations;
%ignore CSuffixCollection::SortByIndex;

%{
#include "SuffixCollection.h"
%}
%include "SuffixCollection.h"
