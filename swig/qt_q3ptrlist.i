%include "CCorpusWord.i"
%include "CAffix.i"
%include "CSignature.i"
%include "CStem.i"

%{
#include <Qt3Support/Q3PtrList>
#include <Qt3Support/Q3PtrListIterator>
%}

template<class type >
class Q3PtrList {
public:
	Q3PtrList();
	Q3PtrList(const Q3PtrList<type > & list);
	~Q3PtrList();

	void	append ( const type * item );
	type *	at ( uint index );
	int		at () const;
	virtual void	clear ();
	uint	contains ( const type * item ) const;
	uint	containsRef ( const type * item ) const;
	virtual	uint count () const;
	type *	current () const;
	Q3LNode *	currentNode () const;
	int		find ( const type * item );
	int		findNext ( const type * item );
	int		findNextRef ( const type * item );
	int		findRef ( const type * item );
	type *	first ();
	type *	getFirst () const;
	type *	getLast () const;
	void	inSort ( const type * item );
	bool	insert ( uint index, const type * item );
	bool	isEmpty () const;
	type *	last ();
	type *	next ();
	void	prepend ( const type * item );
	type *	prev ();
	bool	remove ( uint index );
	bool	remove ();
	bool	remove ( const type * item );
	bool	removeFirst ();
	bool	removeLast ();
	void	removeNode ( Q3LNode * node );
	bool	removeRef ( const type * item );
	bool	replace ( uint index, const type * item );
	void	sort ();
	type *	take ( uint index );
	type *	take ();
	type *	takeNode ( Q3LNode * node );
	void	toVector ( Q3GVector * vec ) const;
	bool	operator!= ( const Q3PtrList<type> & list ) const;
	Q3PtrList<type> &	operator= ( const Q3PtrList<type> & list );
	bool	operator== ( const Q3PtrList<type> & list ) const;
};

%extend Q3PtrList {
	bool	__contains__(type * item) const {
		if ( $self->contains(item) > 0 )
			return TRUE;
		else
			return FALSE;
	}
	type *	__getitem__(uint index) {
		return $self->at(index);
	}
	Q3PtrListIterator<type > *	__iter__() const {
		return new Q3PtrListIterator<type >(*$self);
	}
	int		__len__() {
		return $self->count();
	}
};

template<class type >
class Q3PtrListIterator {
public:
	Q3PtrListIterator ( const Q3PtrList<type> & list );
	~Q3PtrListIterator ();
	bool	atFirst () const;
	bool	atLast () const;
	uint	count () const;
	type *	current () const;
	bool	isEmpty () const;
	type *	toFirst ();
	type *	toLast ();
	type *	operator() ();
	type *	operator* ();
//	type *	operator+= ( uint jump );
//	type *	operator-= ( uint jump );
};
%exception next { // Termination for python iterator:
	$action
	if (result == NULL) {
		PyErr_SetString(PyExc_StopIteration, "blahblah");
		return NULL;
	}
}
%extend Q3PtrListIterator {
	Q3PtrListIterator<type> *	__iter__() {
		return $self;
	}
	type *	next() {
		type *output;
		output = $self->current();
		if (output == 0)
			return NULL;
		++(*($self));
//		if ( ++(*($self)) == 0 )
//			return NULL;
		return output;
	}
};

%define Q3PTRLIST_TEMPLATE_WRAP(suffix, T...)
%template(Q3PtrList_ ## suffix)    Q3PtrList<T >;
%template(Q3PtrListIterator_ ## suffix)    Q3PtrListIterator<T >;
%enddef

Q3PTRLIST_TEMPLATE_WRAP(CCorpusWord, CCorpusWord)
Q3PTRLIST_TEMPLATE_WRAP(CPrefix, CPrefix)
Q3PTRLIST_TEMPLATE_WRAP(CSignature, CSignature)
Q3PTRLIST_TEMPLATE_WRAP(CStem, CStem)
Q3PTRLIST_TEMPLATE_WRAP(CSuffix, CSuffix)
