%rename(__getitem__) CParse::operator[];

// Commented out in Parse.cpp
%ignore CParse::AppendInAlphabeticalOrder;
%ignore CParse::MergeParse(const CParse&);

%{
#include "Parse.h"
%}

%include "Parse.h"

/*
%inline %{
class CParse_Iterator {
private:
	int			m_index;
	CParse *	m_parse;
public:
	CParse_Iterator(CParse *parse)	{ m_index = 0; m_parse = parse; }

	CStringSurrogate	next()		{ m_index++; return m_parse->GetPiece(m_index); }
	CParse_Iterator *	__iter__()	{ return this; }
};
%}
*/

%extend CParse {
	int __len__() {
		return $self->Size();
	}
};

%pythoncode %{
class CParse_Iterator:
	def __init__(self, parse):
		self.m_index = 0	# index 0 is blank
		self.m_parse = parse
	def next(self):
		self.m_index += 1
		if self.m_index > len(self.m_parse):
			raise StopIteration
		return self.m_parse[self.m_index]
	def __iter__(self):
		return self

CParse.__iter__ = lambda self: CParse_Iterator(self)
%}
