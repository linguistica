// QString

%{
#include <QString>
%}

class QString {
public:
	QString ();
//	QString ( const QChar * unicode, int size );
	QString ( QChar ch );
//	QString ( int size, QChar ch );
//	QString ( const QLatin1String & str );
	QString ( const QString & other );
	QString ( const char * str );
//	QString ( const QByteArray & ba );
	~QString ();

	QString &	append(const QString & str);
	QString &	append(const char * str);
	const QChar	at(int position) const;
	void		chop(int n);
	void		clear();
	int			compare(const QString & other) const;
	int			compare(const QString & other, Qt::CaseSensitivity cs) const;
	bool		contains(const QString & str, Qt::CaseSensitivity cs = Qt::CaseSensitive) const;
	bool		contains(const QChar ch, Qt::CaseSensitivity cs = Qt::CaseSensitive) const;
	bool		contains(const QRegExp & rx) const;
	int			count(const QString & str, Qt::CaseSensitivity cs = Qt::CaseSensitive);
	int			count(const QChar ch, Qt::CaseSensitivity cs = Qt::CaseSensitive);
	int			count(const QRegExp & rx) const;
	int			count() const;

	bool		endsWith( const QString & s, Qt::CaseSensitivity cs = Qt::CaseSensitive) const;
	bool		endsWith(const QChar & ch, Qt::CaseSensitivity cs = Qt::CaseSensitive);
	int			indexOf( const QString & s, int from = 0, Qt::CaseSensitivity cs = Qt::CaseSensitive) const;
	int			lastIndexOf( const QString & s, int from = 0, Qt::CaseSensitivity cs = Qt::CaseSensitive) const;

	QString &	insert(int position, const QString & str);
	QString &	insert(int position, QChar ch);

	bool		isEmpty();
	bool		isNull();
	QString		left(int n) const;
	int			length() const;
	int			localeAwareCompare(const QString & other) const;
	QString		mid(int start, int n = -1) const;

	QString &	prepend(const QString & str);
	QString &	prepend(const char * str);

	QString &	remove(int start, int n);
	QString &	remove(const QString & str, Qt::CaseSensitivity cs = Qt::CaseSensitive);
	QString &	remove(QChar ch, Qt::CaseSensitivity cs = Qt::CaseSensitive);
	QString &	replace(int start, int n, const QString & after);
	void		resize(int size);
	QString		right(int n) const;
//	QString		section(const QString & sep, int start, int end = -1, SectionFlags flags = SectionDefault) const;

	QString &	setNum(int n, int base = 10);
	QString &	setNum(double n, char format = 'g', int precision = 6);
	QString		simplified() const;
	int			size() const;
	QString &	sprintf( const char * cformat, ... );
	
	bool		startsWith(const QString & s, Qt::CaseSensitivity cs = Qt::CaseSensitive) const;

//	QString		toCaseFolded() const;
	double		toDouble(bool * ok = 0) const;
	long		toLong(bool * ok = 0) const;
	std::string	toStdString() const;
	std::wstring	toStdWString() const;
	QString		toLower() const;
	QString		toUpper() const;
	QString		trimmed() const;
	void		truncate(int position);

%extend {
	std::string	__str__() {
		return $self->toStdString();
	}
	QChar		__getitem__(int n) {
		return $self->at(n);
	}
	int			__len__() {
		return $self->length();
	}
	QString		__getslice__(int i, int j) {
		return $self->mid(i, j-i);
	}
}
};
