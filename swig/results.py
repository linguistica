#!/usr/local/bin/python
#
# Direct access to the information that Linguistica puts in its
# saved output.  I.e.: CSignature::OutputSignature(), &c.  Signatures
# are only /suffix/ signatures (for now).
#
# Read in and analyze the corpus first, otherwise the functions here
# won't be very useful:
# >>> lxa.read_corpus( corpus_filename, number_of_words )
# >>> lxa.lexicon.FindSuffixes()	# analyze
#
#
# Also included are a save_project() function, which mimics
# ``File -> Save As'' in the GUI, and signature transforms.

import os

import lxa

def all_signatures( ):
	'''List out all found signatures (as strings).'''
	mini = lxa.lexicon.GetMiniLexicon( lxa.lexicon.GetActiveMiniIndex() )
	return [ str(sig)  for sig in mini.GetSignatures() ]
#	return [ str(sig)  for sig in lxa.lexicon.GetAllSuffixSigs() ]

def signature(sig):
	'''Get information about the specified signature (pass as str or QString).

	Specifically:
		- list of stems
		- number of stems
		- corpus count
		- remark (which step found this signature)
		- robustness
		- pointer to the CSignature object

	The list of stems is sorted alphabetically from the right (CParse::ReverseAlphabetize), as done in the GUI.
	
	If we have no such signature, raise a KeyError.'''
	all_sigs = lxa.lexicon.GetAllSuffixSigs()
	if type(sig) == str:
		sig = lxa.QString(sig)
	try:
		# all_sigs[sig] -> SignatureSet = QList<class CSignature* >
		# I want the first (and only) item:
		x = all_sigs[sig].at(0)
	except AttributeError:	# unknown sig
		raise KeyError, "Signature '%s' not found" % sig
	stemslist = x.GetStems()
	stemslist.ReverseAlphabetize()
	output = dict(
			stems			= [ str(t)  for t in stemslist ],
			stem_count		= x.GetNumberOfStems(),
			corpus_count	= x.GetCorpusCount(),
			remark			= str( x.GetRemark() ),
			robustness		= x.GetRobustness(),
			pointer			= x,
		)
	return output

def all_stems( ):
	'''List out all found stems (as strings).'''
	return [ str(t)  for t in lxa.lexicon.GetAllStems() ]
def stem(t):
	'''Get information about the specified stem (pass as str or QString).
	
	Specifically:
		- confidence
		- corpus count
		- suffixes
		- number of suffixes
		- pointer to the CStem object
	
	If we have no such stem, raise a KeyError.'''
	all_stems = lxa.lexicon.GetAllStems()
	if type(t) == str:
		t = lxa.QString(t)
	try:
		# all_stems[t] -> StemSet = QList<class CStem*>
		# I want the first (and only) item:
		x = all_stems[t].at(0)
	except AttributeError:
		raise KeyError, "Stem '%s' not found." % t
	output = dict(
			confidence		= str( x.GetConfidence() ),
			corpus_count	= x.GetCorpusCount(),
			suffixes		= [ str(f)  for f in x.GetSuffixList() ],
			suffix_count	= None,	# see below
			pointer			= x,
		)
	output['suffix_count'] = len( output['suffixes'] )
	return output

def all_suffixes( ):
	'''List out all found suffixes (as strings).'''
	return [ str(f)  for f in lxa.lexicon.GetAllSuffixes() ]
def suffix(f):
	'''Get information about the specified suffix (pass as str or QString).
	
	Specifically:
		- use count
		- corpus count
		- list of stems
		- index
		- pointer to the CSuffix object
	
	If we have no such suffix, raise a KeyError.'''
	all_suffixes = lxa.lexicon.GetAllSuffixes()
	if type(f) == str:
		f = lxa.QString(f)
	try:
		# all_suffixes[t] -> SuffixSet = QList<class CSuffix*>
		# I want the first (and only) item:
		x = all_suffixes[f].at(0)
	except AttributeError:
		raise KeyError, "Suffix '%s' not found." % f
	stemslist = x.GetStems()
#	stemslist.ReverseAlphabetize()
	output = dict(
			use_count		= x.GetUseCount(),
			index			= x.GetIndex(),
			corpus_count	= x.GetCorpusCount(),
			stems			= [ str(cstem[1])  for cstem in stemslist ],
			pointer			= x,
		)
	return output

def all_words( ):
	'''List out all found words (as strings).'''
	return [ str(w)  for w in lxa.lexicon.GetAllWords() ]
def word(w):
	'''Get information about the specified word (pass as str or QString).
	
	Specifically:
		- pieces (prefix/stem/suffix breaks)
		- size (number of pieces)
		- corpus count
		- location of prefix
		- location of stem
		- location of suffix
		- pointer to the CStem (sic) object
	
	If we have no such word, raise a KeyError.'''
	all_words = lxa.lexicon.GetAllWords()
	if type(w) == str:
		w = lxa.QString(w)
	try:
		# all_words[t] -> StemSet = QList<class CStem*>
		# I want the first (and only) item:
		x = all_words[w].at(0)
	except AttributeError:
		raise KeyError, "Word '%s' not found." % w
	output = dict(
			pieces			= [ str(m)  for m in x ],
			size			= None,	# see below
			corpus_count	= x.GetCorpusCount(),
			prefix_loc		= x.GetPrefixLoc(),
			stem_loc		= x.GetStemLoc(),
			suffix_loc		= x.GetSuffixLoc(),
			signature		= None,	# see below
			pointer			= x,
			suffix			= None, # see below
		)
	# size:
	output['size'] = len( output['pieces'] )
	# signature:
	if x.IsAnalyzed():
		output['signature']	= [ str(f)  for f in x.GetSuffixSignature() ]
	else:
		try:
			output['signature'] = stem(w)['suffixes']
		except KeyError:
			pass	# None
	# suffix:
	if output['signature'] is not None:	# i.e.: w is analyzed
		f = str( x.GetSuffix() )
		if f == "":
			f = "NULL"
		output['suffix'] = f
	return output

project_index = 0
def save_project( dir_name, project_name ):
	'''Mimics ``File -> Save As'' (i.e., LinguisticaMainWindow::saveFileActionSlot()).  Does not handle prefixes (yet).'''
	global project_index
	old_cwd = os.getcwd()
	try:
		os.chdir(dir_name)
		prefix = project_name + '_%d' % project_index
#		prefix = os.path.join(dir_name, project_name + '_%d' % project_index)
		project_index += 1
		projectfile_name = lxa.QString(prefix + '.prj')
		for i in range( lxa.lexicon.GetMiniSize() ):
			mini = lxa.lexicon.GetMiniLexicon(i)
			if not mini:
				continue
			wordsfile_name = lxa.QString(prefix + '_Mini%d_Words.txt' % (i+1))
			prefixfile_name = lxa.QString(prefix + '_Mini%d_Prefixes.txt' % (i+1))
				# not used
			suffixfile_name = lxa.QString(prefix + '_Mini%d_Suffixes.txt' % (i+1))
			signaturefile_name = lxa.QString(prefix + '_Mini%d_Signatures.txt' % (i+1))
			stemfile_name = lxa.QString(prefix + '_Mini%d_Stems.txt' % (i+1))

			mini.GetWords().OutputWords(wordsfile_name)
			mini.GetSuffixes().OutputSuffixes(suffixfile_name)
			if mini.GetSignatures():
				mini.GetSignatures().OutputSignatures( signaturefile_name )
				mini.GetStems().OutputStems( stemfile_name )
		lxa.lexicon.OutputStats( projectfile_name )
	finally:
		os.chdir(old_cwd)


### Signature transforms:
import re

def get_corpus():
	'''Returns the active corpus as a big string.  One-liner.'''
	return "\n".join([ str(line)  for line in lxa.lexicon.GetCorpus() ])

def scrub(w):
	'''Scrubs a word.  Stupid hack until I figure out the real scrubbing code.

	Returns three pieces (pre,word,post), where pre and post are leading and
	trailing non-word characters (punctuation, &c).'''
	m = re.search( '^(?P<pre>[^A-Za-z]*)(?P<w>.*?)(?P<post>[^A-Za-z]*)$', w )
	return m.group('pre', 'w', 'post')

def signature_transform(w):
	'''Signature transform of a single word.  Raises KeyError if it doesn't
	recognize the word.'''
	D = word(w)
	if D['signature'] is None:	# unanalyzed word
		return w
	f = D['suffix']
	sig = ".".join( D['signature'] )
	return '%s_%s' % (sig, f)

def transform_corpus(text):
	'''Signature-transform a whole corpus (given as a text string), preserving
	punctuation.'''
	# Remember transforms of words I've already seen:
	transforms = { }
	def transform_word(word_match):
		'''For use in re.sub() below.  This function transforms the given word
		(passed as a match object), remembering ones that it's already done.'''
		# Scrubbing:
		pre, w, post = scrub( word_match.group() )
		if w not in transforms:
			try:
				transforms[w] = signature_transform(w)
			except KeyError:	# Not a known word.  Possibly not a word at all.
				transforms[w] = w
		return pre + transforms[w] + post
	#
	return re.sub( '\S*', transform_word, text )
