%{
#include "/usr/include/qt4/QtCore/qchar.h"
%}

class QChar {
public:
	QChar();
	QChar(char ch);
	QChar(int code);
	bool hasMirrored () const;
	bool isDigit () const;
//	bool isHighSurrogate () const;
	bool isLetter () const;
	bool isLetterOrNumber () const;
//	bool isLowSurrogate () const;
	bool isLower () const;
	bool isMark () const;
	bool isNull () const;
	bool isNumber () const;
	bool isPrint () const;
	bool isPunct () const;
	bool isSpace () const;
	bool isSymbol () const;
//	bool isTitleCase () const;
	bool isUpper () const;

	char toAscii () const;
//	QChar toCaseFolded () const;
	char toLatin1 () const;
	QChar toLower () const;
//	QChar toTitleCase () const;
	QChar toUpper () const;
	ushort & unicode ();
	ushort unicode () const;
%extend {
	char	__str__() const {
		return $self->toAscii();
	}
}
};
