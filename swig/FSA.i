%include "qt_qlist.i"

%ignore FSAedge::FSAedge(FSA*, FSAstate*, FSAstate*, class CParse*);

%{
#include "FSA.h"
%}

%include "FSA.h"


%template(QList_FSAMorpheme)	QList<FSAMorpheme*>;
%template(QList_FSAedge)	QList<FSAedge*>;
%template(QList_FSAstate)	QList<FSAstate*>;

%template(QListIterator_FSAMorpheme)	QListIterator<FSAMorpheme*>;
%template(QListIterator_FSAedge)	QListIterator<FSAedge *>;
%template(QListIterator_FSAstate)	QListIterator<FSAstate *>;
