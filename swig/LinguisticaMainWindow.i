%rename(_LinguisticaMainWindow)	LinguisticaMainWindow;

%{
#include "linguisticamainwindow.h"
%}

// I can't just do the following because of Qt stuff.
// %include "linguisticamainwindow.h"

// Adapted from main() in main.cpp
%inline %{
	QApplication *_newApp() {
		int argc = 0;
		char *argv[0] = { };
		return new QApplication(argc, argv);
	}
%}


class LinguisticaMainWindow : public QMainWindow,
					public Ui::LinguisticaMainWindowBase
{
public:
  LinguisticaMainWindow( const int argc, char** argv, QApplication* app = 0,
                  QWidget* parent = 0, Qt::WindowFlags f = Qt::WType_TopLevel );
  ~LinguisticaMainWindow();

  void                       SetNumberOfTokens( int i ) { m_numberOfTokens = i; }
  int                        GetNumberOfTokens() { return m_numberOfTokens; }

  void                       SetUpNewLexicon	();

  void                       setProjectDirectory( QString dir ) { m_projectDirectory = dir; }
  bool					getProjectDirty	() { return m_projectDirty; }

  void					setStatusBar1	(QString);
  void					setStatusBar2	(QString);

  void					BeginCountDown	();
  void					EndCountDown	();
  void					CountDownOnStatusBar(int i, int total, int step = 100);

  QString				GetLogFileName	() { return m_logFileName; }
  void					AskUserForLogFileName();

  void					SetEastFont		( QFont font ) { m_eastFont = font; }
  CLPreferences*                        GetPreferences	() { return &m_preferences; }
  QSettings*                            GetSettings		() { return &m_Settings; }

  void					SetDocType		( eDocumentType dt ) { m_docType = dt; }
  eDocumentType                         GetDocType		() { return m_docType; }

  bool                                  autoLayering	() { return m_autoLayeringAction->isOn(); }
	
  bool					isCorrectAffix	( eAffixLocation );

	void executeCommandLineArgs(int words_count, bool auto_layer,
		bool prefix, bool suffix);
	/// true if running in batch mode
	bool commandLineMode() { return m_commandLineFlag; }

  QFont					GetEastFont		() { return m_eastFont; }

	// Logging
  void					CloseLogFile	();
  void					OpenLogFile		();
  bool					LogFileOn		() { return m_logging; }
  Q3TextStream*                          GetLogFileStream() { return m_logFileStream; }
  

	Q3Canvas*	m_SmallCanvas;
	GraphicView*	m_SmallGraphicDisplay;	///< deleted by m_SmallGraphicDisplayTab
	Q3VBox*		m_SmallGraphicDisplayVBox;
  	QLabel * pImgLabel;

	CLexicon*	GetLexicon(void) { return m_lexicon; }

	bool	getLogging(){ return m_logging; }
};
