%include "TCollection.i"
%include "CStem.i"

%ignore CWordCollection::ComputeZeta;

%{
#include "WordCollection.h"
%}
%include "WordCollection.h"
