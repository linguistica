// Implementation of the cMTModel1 class
// Copyright © 2009 The University of Chicago
#include "cMTModel1.h"

#include <QMessageBox>
#include "mTVolca.h"
#include "cMT.h"
#include "Typedefs.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

cMTModel1::cMTModel1(cMT* myMT, int Iterations)
	: m_myMT(myMT), m_Iterations(Iterations),
	m_T(),
	m_softCountOfT(),
	m_lamdaT() { }

cMTModel1::~cMTModel1() { }

void			cMTModel1::initT()
{
	mTVolca*								myVolca; 
	int										totalAssociatedLanguage2Words; 
	IntToIntToDouble::iterator				IntToIntToDoubleIt; 
	IntToDouble::iterator					IntToDoubleIt; 
	IntToDouble*							oneListForOneLanguage1Word; 
	double									uniformProb;
	int										key; 
	


	myVolca = m_myMT ->m_Volca; 

	m_T = myVolca ->m_fastWordsPairs;
	m_softCountOfT = myVolca ->m_fastWordsSoftCounts; 


	for ( IntToIntToDoubleIt = m_T.begin(); IntToIntToDoubleIt != m_T.end();IntToIntToDoubleIt++)
	{
		oneListForOneLanguage1Word = IntToIntToDoubleIt.data(); 
		
		totalAssociatedLanguage2Words = oneListForOneLanguage1Word ->size(); 

		uniformProb = 1.0 / totalAssociatedLanguage2Words;

		for ( IntToDoubleIt = oneListForOneLanguage1Word ->begin(); IntToDoubleIt != oneListForOneLanguage1Word ->end();  IntToDoubleIt++)
		{
			key = IntToDoubleIt.key();
			(*oneListForOneLanguage1Word)[key] = uniformProb;
		}
	}
	
	for ( IntToIntToDoubleIt = m_softCountOfT.begin(); IntToIntToDoubleIt != m_softCountOfT.end();IntToIntToDoubleIt++)
	{
		oneListForOneLanguage1Word = IntToIntToDoubleIt.data(); 
		
		for ( IntToDoubleIt = oneListForOneLanguage1Word ->begin(); IntToDoubleIt != oneListForOneLanguage1Word ->end();  IntToDoubleIt++)
		{
			IntToDoubleIt.data() = 0.0; 
		}
	}


	QMessageBox::information ( NULL, "Linguistica : MT Model1", "Finished InitT", "OK" );

} 

 
void			cMTModel1::EMLoops(int numberOfIterations)
{
	int							loopI; 

	for (loopI =0; loopI < numberOfIterations; loopI++)
	{
		// E step 
		EStep();

		//QMessageBox::information ( NULL, "Linguistica : MT Model1", "Finished E-Step", "OK" );

		// M step
		MStep();

		//QMessageBox::information ( NULL, "Linguistica : MT Model1", "Finished M-Step", "OK" );

		// Clear softcouts for T
		clearSoftCounts();
	}

	// Release softcountT memory
	//releaseSoftCounts(); 
} 

void			cMTModel1::clearSoftCounts()
{
	IntToIntToDouble::iterator				IntToIntToDoubleIt;
	IntToDouble*							oneList; 
	IntToDouble::iterator					IntToDoubleIt; 
	int										key; 

	for ( IntToIntToDoubleIt = m_softCountOfT.begin(); IntToIntToDoubleIt != m_softCountOfT.end(); IntToIntToDoubleIt++)
	{
		oneList = IntToIntToDoubleIt.data(); 

		for (IntToDoubleIt = oneList ->begin(); IntToDoubleIt != oneList ->end(); IntToDoubleIt++)
		{
			key = IntToDoubleIt.key(); 
			(*oneList)[key] = 0.0; 
		}
	}

}

void			cMTModel1::EStep()
{
	mTVolca*						myVolca; 
	int								i; 

	myVolca = m_myMT ->m_Volca; 

	m_lamdaT.clear(); 
	for ( i=0; i < myVolca ->m_countOfSentences; i++)
	{
		addSoftCountOfT(i); 		
	}
}


void			cMTModel1::MStep()
{
	IntToIntToDouble::iterator				IntToIntToDoubleIt;
	IntToDouble*							oneList; 
	IntToDouble::iterator					IntToDoubleIt; 
	int										language1Id;
	int										language2Id;
	double									oneTotalSoftCount; 
	double									oneLamda;


	for ( IntToIntToDoubleIt = m_softCountOfT.begin(); IntToIntToDoubleIt != m_softCountOfT.end(); IntToIntToDoubleIt++)
	{
		language1Id = IntToIntToDoubleIt.key();
		oneLamda = m_lamdaT[language1Id] ;
		oneList = IntToIntToDoubleIt.data(); 


		for (IntToDoubleIt = oneList ->begin(); IntToDoubleIt != oneList ->end(); IntToDoubleIt++)
		{
			language2Id = IntToDoubleIt.key();
			oneTotalSoftCount = IntToDoubleIt.data(); 
			(*(m_T[language1Id]))[language2Id] = oneTotalSoftCount / oneLamda ;
		}
	}


}

void			cMTModel1::addSoftCountOfT(int sentenceId)
{
	mTVolca*						myVolca; 
	double							oneT; 
	double							deNumerator; 
	int								l,m; 
	int								language1WordId;
	int								language2WordId;
	IntToInt*						oneLan1Sentence;
	IntToInt*						oneLan2Sentence;


	myVolca = m_myMT ->m_Volca; 

	oneLan1Sentence = myVolca ->m_language1Sentences[sentenceId];
	oneLan2Sentence = myVolca ->m_language2Sentences[sentenceId];
	

	for ( m=0; m< static_cast <int> (oneLan2Sentence ->size()); m++) //type cast to fix unsigned-signed warning
	{
		language2WordId = (*oneLan2Sentence)[m];

		deNumerator =0; 
		
		for ( l=0; l < static_cast<int> (oneLan1Sentence ->size()); l++) //type cast: unsigned-signed int comparison
		{
			language1WordId = (*oneLan1Sentence)[l];

			oneT = (*(m_T[language1WordId]))[language2WordId];
			deNumerator += oneT; 
		}

		for ( l=0; l < static_cast <int> (oneLan1Sentence ->size()); l++)
		{

			language1WordId = (*oneLan1Sentence)[l];

			oneT = (*(m_T[language1WordId]))[language2WordId] / deNumerator;
						
			(*(m_softCountOfT[language1WordId]))[language2WordId] += oneT;
			
			if ( m_lamdaT.contains(language1WordId))
			{
				m_lamdaT[language1WordId] += oneT;
			}
			else
			{
				m_lamdaT.insert(language1WordId, oneT); 
			}
		}

	}

}

void			cMTModel1::releaseSoftCounts()
{
	IntToIntToDouble::iterator				IntToIntToDoubleIt;
	IntToDouble*							oneList; 
	
	for ( IntToIntToDoubleIt = m_softCountOfT.begin(); IntToIntToDoubleIt != m_softCountOfT.end(); IntToIntToDoubleIt++)
	{
		oneList = IntToIntToDoubleIt.data(); 

		delete oneList; 
	}


} 
