// Driver state type for syllable discovery with Dynamic Computational Networks
// Copyright © 2009 The University of Chicago
#ifndef LEARNINGSYL_H
#define LEARNINGSYL_H

// See the corpussyl documentation in DCNcorpussyl.h for an overview.

#include <ostream>
#include "DCNgrammarsyl.h"
#include "DCNnetworksyl.h"
#include "DCNcorpussyl.h"

namespace linguistica { namespace ui { class status_user_agent; } }

/**
 *	The learning class uses a simplified version of Gary Larson's
 *  simulated annealing process to find an appropriate grammar
 *  given a corpus of various words with stress. The name is kind
 *  of weird -- it's easiest to think of an instance as a learning
 *  machine. It takes a corpus (an array of QStrings) and returns
 *  a possible grammar. It's also not very well implemented --
 *  nearly everything is public. 
 *
 *  The most interesting function is
 *  runHelper(), which is the essence of the learning algorithm.
 *  It is a first attempt at the learning algorithm, so at the 
 *  moment it's kind of crappy. Luckily, the GUI supports
 *  inputting different values for the learning algorithm.
 */
class learningsyl 
{
	bool successful;
	corpussyl corpus;
	unsigned int numberOfWords;

	grammarsyl grammar;
	networksyl net;
	grammarsyl possibleGrammar;
	networksyl possibleNet;
	grammarsyl* rGrammar;
public:
	float TfromUser;
	float increaseWhenWrong;
	float decreaseWhenRight;
	int numberOfTries;
	unsigned int cutoffFromUser;

	float startingAlpha;
	float startingBeta;
public:
	learningsyl();
	virtual ~learningsyl();
	
	void run(linguistica::ui::status_user_agent& status_display);
	bool isSuccessful() { return successful; }
	void setCorpus(corpussyl corpus);
	void print(class QLabel* label) { net.print(label); }
	grammarsyl* returnGrammar() { return rGrammar; }

private:
	void runHelper(std::ostream& logstream,
	               linguistica::ui::status_user_agent& status_display);
};

#endif // LEARNINGSYL_H
