// Prefix list for suffix/signature-based morphology discovery and display
// Copyright © 2009 The University of Chicago
#ifndef PREFIXCOLLECTION_H
#define PREFIXCOLLECTION_H

class CPrefixCollection;

// See the CMiniLexicon class in MiniLexicon.h for an overview of
// suffix/signature-based discovery of morphology.

#include "CollectionTemplate.h"
#include "StringSurrogate.h"
template<class K, class V> class QMap;
class QString;
namespace linguistica { namespace ui { class status_user_agent; } }

class CPrefixCollection: public TCollection<class CPrefix> {
	// XXX. SuffixCollection remembers sum(dl of pointers to members).
	// Who’s right?
public:
	CPrefixCollection();
	explicit CPrefixCollection(class CMiniLexicon* Lex);
	~CPrefixCollection();

	// output to GUI.
	// XXX. SuffixCollection has a knob for express_deletees
	void ListDisplay(class Q3ListView* widget,
		QMap<QString, QString>* filter,
		linguistica::ui::status_user_agent& status_display);

	// long-term storage.
	void OutputPrefixes(QString filename);
	void ReadPrefixFile(QString FileName);

	// XXX. SuffixCollection has SortByIndex(),
	// Get/CalculateTotalUseCount().

	// insert.
	CPrefix* operator<<(CStringSurrogate prefix_text);
	CPrefix* operator<<(class CParse* prefix_text);
	// XXX. SuffixCollection provides operator<<(CParse&)
	CPrefix* operator<<(QString prefix_text);
	void AddPointer(CPrefix* prefix);
	CPrefix* AddToCollection(CParse& prefix_text);
	CPrefix* AddToCollection(CStringSurrogate& prefix_text);

	// clear.
	void Empty();
	void RemoveAll();

	// remove.
	bool Remove(CPrefix* prefix);	///< doesn't delete prefix
	bool RemoveMember(CPrefix* prefix);	///< deletes prefix
	bool RemoveMember(CStringSurrogate& prefix_text);	///< deletes prefix
	bool RemoveMember(CStringSurrogate& prefix_text, bool delete_it);
	void DeleteMarkedMembers();

	// XXX. SuffixCollection has FindCombinations(),
	// FindFactorableSuffixes().

	// description length.
	double GetDL_PhonologicalContent();
	// XXX. SuffixCollection has CalculatePointersToMySuffixes()
};

#endif // PREFIXCOLLECTION_H
