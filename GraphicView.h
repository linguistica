// GraphicView widget and supporting classes
// Copyright © 2009 The University of Chicago
#ifndef GRAPHICVIEW_H
#define GRAPHICVIEW_H

class VideoFrame;
class Video;
class LabeledDataPoint;
class GraphicView;
class DiagramBox;
class DiagramLine;
class ProbBox;
class Anchor;

#include <QList>
#include <Q3CanvasView>
#include <Q3CanvasRectangle>
#include <Q3CanvasLine>

#include <QPainter>
#include <QString>
#include <QMap>
#include "StringSurrogate.h"
#include "Edge.h"

class QAction;
class CState; 
class CStem; 

typedef QMap<QString,int>						StringToInt;
typedef QMap<CState*,StringToInt*>				StateToStringToInt;
typedef QMap<CState*,int>						StateToInt;
typedef QMap<int,double>						IntToDouble;
typedef QMap<int,QString>						IntToString;
 
class VideoFrame : public QList< LabeledDataPoint*>
{
//	QPtrList< LabeledDataPoint>		m_Data;
	
	public:
	int								m_Dimensionality;
	CParse							m_DimensionLabels;
	VideoFrame	(int dimensionality);
	VideoFrame	( );
	VideoFrame	(VideoFrame& );

	void							operator<<	( LabeledDataPoint* );
	void							AddLabels	( CStringSurrogate  );
	void							Incorporate ( VideoFrame*       );
//	QPtrList<LabeledDataPoint>*		GetData		( ) {return &m_Data;}
};

class Video : public QList<VideoFrame*>
{	
	
	int						m_Dimensionality;
	int						m_NumberOfDataPoints;
	int						m_NumberOfFrames;
	CParse					m_DimensionLabels;
//	QPtrList<VideoFrame>	m_VideoFrames;

public: 
	Video					(int dimensionality ); 
	~Video					( );
	void					operator<<				( VideoFrame *);
	void					AddLabels				( CStringSurrogate );
	int						GetDimensionality		() { return m_Dimensionality;}
	int						GetNumberOfDataPoints	() { return m_NumberOfDataPoints; }
//	int						GetNumberOfFrames		() { return m_NumberOfFrames; }
//	QPtrList<VideoFrame>*	GetVideoFrames			() { return m_VideoFrames; }
};


class LabeledDataPoint
{
public:
	QString					m_Label;
	int						m_Dimensionality;
	double*					m_Values;
	int						m_CurrentLocation;
 
	LabeledDataPoint		();
	LabeledDataPoint		( QString label, int dimensionality);
	~LabeledDataPoint		();	
	bool					operator<< (double);


};




class GraphicView : public Q3CanvasView
{
	Q_OBJECT

	Q3CanvasItem* pendingItem;
	Q3CanvasItem* activeItem;

	QAction* PropertiesAct;
	QAction* PlotStatesAct;
	QAction* RefreshAct;
	QAction* DisplayMeAct;

	CState* m_MyState;
        Q3PtrList<CEdge>* m_MyRoots;
	bool m_IsStateView;
	CStem* m_MyStem;

	QMap<QString, QString>* m_filter;

	// HMM Display Related members
	int m_numberOfDimension;
	double** m_multiDimensionDataPoints;
public:
	GraphicView();
	GraphicView(Q3Canvas *canvas,
		QMap<QString, QString>* filter = 0, QWidget* parent = 0);
	virtual ~GraphicView();

	void SetFilter(QMap<QString, QString>* filter) { m_filter = filter; }
public slots:
	void Properties();
	void PlotStates();
	void PlotStems();
	void PlotNVectors(int, int, double**, QMap<int, QString>);
	void PlotNVectors2(VideoFrame* data);
	void PlotVideo(Video* pVideo);
	void Refresh();
	void DisplayMe();
protected:
	void contentsContextMenuEvent(QContextMenuEvent* event);
	void contentsMousePressEvent(QMouseEvent* event);
	void contentsMouseDoubleClickEvent(QMouseEvent *event);
protected:
	void computeProjectedLocForOneDataPoint(LabeledDataPoint* Data,
		double** anchors, double& xcord, double& ycoor);
	/// deprecated
	void computeProjectedLocForOneDataPoint(int num_states,
		double** anchor_coords, double* highdim_data,
		double& xcoord, double& ycoord);
private:
	void createActions();
	void setActiveItem(Q3CanvasItem *item);
	void showNewItem(Q3CanvasItem *item);
public:
	void SetMyState(CState* pState)
	{ m_MyState = pState; m_IsStateView = true; }
	void SetMyStem(CStem* pStem)
	{ m_MyStem = pStem; m_IsStateView = false;};
        void SetMyRoots(Q3PtrList<CEdge>* roots)
	{ m_MyRoots = roots; m_IsStateView = false;}
};

class DiagramBox : public Q3CanvasRectangle
{
public:
    enum { RTTI = 1001 };

    DiagramBox(Q3Canvas *canvas);
	DiagramBox(Q3Canvas *canvas, int, int, CState*);
    ~DiagramBox();

    void					setText(const QString &newText);
    QString					text() const { return str; }
    void					drawShape(QPainter &painter);
    int						rtti() const { return RTTI; }


private:
    QString str;
public:
	CState*	m_MyState; 

};

class DiagramLine : public Q3CanvasLine
{
public:
    enum { RTTI = 1002 };

    DiagramLine(Q3Canvas *canvas);
     ~DiagramLine();

    void					 drawShape(QPainter &painter);
    int						 rtti() const { return RTTI; }

public:
	int						m_FromWhichState;
	int						m_ToWhichState;
	StringToInt				m_Morphemes; 
 




};


class ProbBox : public Q3CanvasRectangle
{
public:
    enum { RTTI = 1003 };

    ProbBox(Q3Canvas *canvas);
	ProbBox(Q3Canvas *canvas, int, int, bool, int);
    ~ProbBox();

    void					setText(const QString &newText);
    QString					text() const { return str; }
    void					drawShape(QPainter &painter);
    int						rtti() const { return RTTI; }


private:
    QString str;

};

class Anchor : public Q3CanvasEllipse
{
public:
    enum { RTTI = 1010 };

    Anchor(Q3Canvas *canvas);
	Anchor(Q3Canvas *canvas, int, int);
    ~Anchor();

    void					setText(const QString &newText);
    QString					text() const { return m_str; }
    void					drawShape(QPainter &painter);
    int						rtti() const { return RTTI; }


private:
    QString				m_str;


};


#endif // GRAPHICVIEW_H
