// Implementation of CStem phonology methods
// Copyright © 2009 The University of Chicago
#include "Stem.h"
#include "Biphone.h"
#include "Phone.h"
#include "BiphoneCollection.h"
#include "PhoneCollection.h"
#include "WordCollection.h"
#include "log2.h"

void CStem::ComputeProbabilities(CWordCollection* Words)
{
	m_BigramLogProb = 0;
	m_UnigramLogProb = 0;

	m_Tier2_LocalMI_Score = 0;
	m_Tier2_DistantMI_Score = 0;

	m_LocalMI_TotalBoltzmannScore = 0;	
	m_DistantMI_TotalBoltzmannScore = 0;

	CPhone* prevPhone = 0;
	for (int i = 1; i <= m_Phonology_Tier1.Size(); ++i) {
		CPhone* pPhone = *Words->GetPhones() ^=
			m_Phonology_Tier1.GetPiece(i);
		Q_ASSERT(pPhone != 0);

		if (i == 1) {
			prevPhone = pPhone;
			continue;
		}
		m_UnigramLogProb += pPhone->m_LogFreq; 
		CBiphone* pBiphone = Words->GetPhones()->GetMyBiphones()
			->GetBiphone(prevPhone, pPhone);	
		if (pBiphone == 0)
			return;

		m_BigramLogProb += pPhone->m_LogFreq - pBiphone->m_MI;
		prevPhone = pPhone;
	}

	m_UnigramComplexity = m_UnigramLogProb / (m_Phonology_Tier1.Size()-1);
	m_BigramComplexity = m_BigramLogProb / (m_Phonology_Tier1.Size()-1);

	if (m_Phonology_Tier2.Size() == 0)
		return;

	for (int i = 1; i <= m_Phonology_Tier2.Size(); ++i) {		
		CPhone* pPhone = *Words->GetPhones() ^=
			m_Phonology_Tier2.GetPiece(i);		
		if (i == 1) {
			prevPhone = pPhone;
			continue;
		}
		CBiphone* pBiphone = Words->GetPhones_Tier2()->GetMyBiphones()
			->GetBiphone(prevPhone, pPhone);	
		if (pBiphone == 0)
			return;
		else
			m_Tier2_LocalMI_Score += pBiphone->m_MI;
		prevPhone = pPhone;
	}

	m_LocalMI_TotalBoltzmannScore = m_BigramLogProb - m_Tier2_LocalMI_Score;
	m_Tier2_DistantMI_Score = 0;

	for (int i = 1; i <= m_Phonology_Tier2.Size(); ++i) {		
		CPhone* pPhone = *Words->GetPhones() ^=
			m_Phonology_Tier2.GetPiece (i);		
		for (int j = i+1; j <= m_Phonology_Tier2.Size(); ++j) {
			CPhone* qPhone = *Words->GetPhones() ^=
				m_Phonology_Tier2.GetPiece (j);	
			CBiphone* pBiphone = Words->m_Phones_Tier2->GetMyBiphones()
				->GetBiphone(pPhone, qPhone);	
			if (pBiphone != 0)
				m_Tier2_DistantMI_Score += pBiphone->m_MI / (j-i);
		}
	}
	m_DistantMI_TotalBoltzmannScore = m_BigramLogProb -
	                                  m_Tier2_DistantMI_Score;
}

void CStem::ComputeBoltzmannProbabilities(double Z, double ZStar)
{
	// XXX. what if Z or Zstar == 0.0?

	if (Z != 0.0)
		m_LocalMI_Plog = m_LocalMI_TotalBoltzmannScore + log2(Z);

	if (ZStar != 0.0)
		m_DistantMI_Plog = m_BigramLogProb - m_Tier2_DistantMI_Score +
			log2(ZStar);		
}

void CStem::GetPhonogyTier1InfoForGraph(CWordCollection* Words)
{
	int					i;
	CPhone*				pPhone, *prevPhone;
	prevPhone = NULL;
	QString				biphone;
	CBiphone*			pBiphone;
	double				ugram; 
	double				mi;

	m_phonologies.clear(); 
	m_unigrams.clear(); 
	m_mis.clear();

	m_countofunigrams = 0; 
	m_countofmis = 0; 
	m_maxpositive =0; 
	m_maxnegative =0; 

	for (i= 1; i <= m_Phonology_Tier1.Size(); i++)
	{
		QString temp2 = m_Phonology_Tier1.GetPiece (i).Display();
		pPhone = *Words->GetPhones() ^= m_Phonology_Tier1.GetPiece (i);
		Q_ASSERT (pPhone);
		if (i == 1)
		{
			prevPhone = pPhone;
			continue;
		}
		ugram = pPhone->m_LogFreq; 
 
		pBiphone = Words->GetPhones()->GetMyBiphones()->GetBiphone (prevPhone, pPhone);	
		
		Q_ASSERT (pBiphone);

		mi = pBiphone->m_MI;

		
		m_phonologies.insert(m_countofunigrams, temp2);
		m_unigrams.insert(m_countofunigrams, ugram);
		m_mis.insert(m_countofmis, mi);

		m_countofunigrams++;
		m_countofmis++; 

		prevPhone = pPhone;

		if ( ugram > 0)
		{
			if ( ugram > m_maxpositive)
			{
				m_maxpositive = ugram; 
			}
		}
	
		if ( mi > 0)
		{
			if ( mi > m_maxpositive)
			{
				m_maxpositive = mi; 
			}
		}
		else
		{
			if ( mi < m_maxnegative)
			{
				m_maxnegative = mi; 
			}
		}


	}

	m_donephonology = true; 
}

QString	CStem::GetProbabilityInformation() 
{
	return QString("\nUnigram log probability %1"
		"\nUnigram complexity  %2"
		"\nBigram log probability  %3"
		"\nBigram complexity %4"
		"\nTier 2 MI score %5"
		"\nLocal tier 2 model score: %6"
		"\nLocal tier 2 model log probability: %7"
		"\nDistant tier 2 MI:  %8"
		"\nDistant tier 2 model score %9")
		.arg(m_UnigramLogProb)
		.arg(m_UnigramComplexity)
		.arg(m_BigramLogProb)
		.arg(m_BigramComplexity)
		.arg(m_Tier2_LocalMI_Score)
		.arg(m_LocalMI_TotalBoltzmannScore)
		.arg(m_LocalMI_Plog)
		.arg(m_Tier2_DistantMI_Score)
		.arg(m_DistantMI_TotalBoltzmannScore);
}

void CStem::SplitPhonologyToTiers(enum ePhonologySplitType Type,
	CParse& PhonesToMove)
{
	const QString DummySymbol = "*";

	m_Phonology_Tier2.ClearParse();
	for (int i = 1; i <= m_Phonology_Tier1.Size(); ++i)
		if (PhonesToMove.Contains(m_Phonology_Tier1.GetPiece(i))) {
			m_Phonology_Tier2.Append(m_Phonology_Tier1.GetPiece(i));

			if (Type == Split_LeaveSlot) {
				CStringSurrogate dummy = DummySymbol;
				m_Phonology_Tier1.Replace(i, dummy);
			}
		}
}

// this is here specifically to do probabilistic tests on projections to C and V. 
void CStem::CreateCVTemplate(CParse* Vowels)
{
	QString V ("V"), C("C"); QString boundary ("#");
	
	m_Phonology_Tier1_Skeleton.ClearParse();
	m_Phonology_Tier1_Skeleton.Append ( boundary );
 	
	CParse VowelsAndAsterisk ( *Vowels);
	VowelsAndAsterisk.Append ('*');
 

	for (int i = 2; i < m_Phonology_Tier1.Size(); i++)
	{
		QString b; b= m_Phonology_Tier1.GetPiece(i).Display();
		if (VowelsAndAsterisk.Contains ( m_Phonology_Tier1.GetPiece(i) ) )
		{
			m_Phonology_Tier1_Skeleton.Append ( V );
		}
		else
		{
			m_Phonology_Tier1_Skeleton.Append ( C );
		}
	}
	m_Phonology_Tier1_Skeleton.Append ( boundary );
}

void CStem::CreatePhonologyFromOrthography(eAddBoundarySymbols AddBoundaries)
{
	if (m_Phonology_Tier1.GetKeyLength() > 0 ) return;

	if (AddBoundaries == BOUNDARIES) 
		m_Phonology_Tier1.Append(QChar('#'));

	for (int i = 0; i < GetKeyLength(); ++i)
		m_Phonology_Tier1.Append(CStringSurrogate(m_Key, i, 1));

	if (AddBoundaries == BOUNDARIES)
		m_Phonology_Tier1.Append(QChar('#'));
}

void CStem::SetPhonology_Tier1(CParse* PhonoRep)
{
	m_Phonology_Tier1.Append(QChar('#'));
	m_Phonology_Tier1.Append(*PhonoRep);
	m_Phonology_Tier1.Append(QChar('#'));
}
