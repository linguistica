// Network set type for stress discovery with Dynamic Computational Networks
// Copyright © 2009 The University of Chicago
#ifndef SNETWORK_H
#define SNETWORK_H

// See the learning documentation in DCNlearning.h for an overview.

class	network;
class	QString;
class	Q3TextEdit;
class	grammar;

#include	<q3ptrvector.h>

/**
 *	A snetwork is a Set of NETWORKs.
 *	Give it the smallest and largest word, and a grammar,
 *	and the snetwork will make a vector of networks, one
 *	for each word length. This makes it easy to see what
 *	a grammar does for a DCN. The print() function is
 *  especially useful.
 */
class snetwork  
{

private:
	int					leastNumberOfSyl;
	int					greatestNumberOfSyl;
	bool				converged;
	grammar*			theGrammar;
	Q3PtrVector<network>	theNetworks;

public:
	snetwork(int leastNumberOfSyl, int greatestNumberOfSyl);
	virtual ~snetwork();
	void	setGrammar(grammar* theGrammar);
	void	print(Q3TextEdit *label);
	void	equilibrium();
	bool	isTotallyConverged() { return converged; }

};

#endif // SNETWORK_H
