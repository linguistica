// Implementation of graphic display
// Copyright © 2009 The University of Chicago
#include "GraphicView.h"

#include <cmath>
#include <QMessageBox>
#include <Q3PopupMenu>
#include <QContextMenuEvent>
#include <QMouseEvent>
#include <QAction>
#include "linepropertiesdialog.h"
#include "propertiesdialog.h"
#include "Stem.h"
#include "StateEmitHMM.h"
#include "log2.h"

namespace {
	const double PI = M_PI;
}
//----------------------------------------------------------------------------------//
//								Video class
//----------------------------------------------------------------------------------//

Video::Video(int dimensionality ) 
{
	m_Dimensionality		= dimensionality;
//	m_NumberOfDataPoints	= NumberOfDataPoints;
	m_NumberOfFrames		= 0;
}

Video::~Video()
{
        //setAutoDelete(TRUE);  **** == Fix this -- we don't have auto-delete for the (new) QList. Memory leak will result now.
}

void	Video::operator<<	( VideoFrame * pVideoframe)
{
	m_NumberOfFrames++;
	append (pVideoframe);

}

//----------------------------------------------------------------------------------//
//								Labeled Data Point class
//----------------------------------------------------------------------------------//
 
	LabeledDataPoint::LabeledDataPoint( QString label, int dimensionality)
	{
		m_Label					= label;
		m_CurrentLocation		= 0;
		m_Dimensionality		= dimensionality;
		m_Values				= new double [m_Dimensionality];
	}
//----------------------------------------------------------------------------------//
	
	LabeledDataPoint::LabeledDataPoint( )
	{
		m_CurrentLocation		= 0;
		m_Dimensionality		= 0;
		m_Values				= NULL;

	}
//----------------------------------------------------------------------------------//
	LabeledDataPoint::~LabeledDataPoint( )
	{
 		delete m_Values;

	}
//----------------------------------------------------------------------------------//

	bool			LabeledDataPoint::operator<< (double value)
	{
		if ( m_CurrentLocation < m_Dimensionality )
		{
			m_Values[m_CurrentLocation++] = value;
		}
		return TRUE;
	}


//----------------------------------------------------------------------------------//
//								Video Frame class
//----------------------------------------------------------------------------------//
 	VideoFrame::VideoFrame	(int dimensionality)
	{
		m_Dimensionality = dimensionality;		
		
	};
//----------------------------------------------------------------------------------//
	

	VideoFrame::VideoFrame (  )
	{
		m_Dimensionality = 0;
	}	
//----------------------------------------------------------------------------------//
	
	
void VideoFrame::Incorporate(VideoFrame* OtherOne)
{
		//This int-to-int maps will "translate" from each label index
		// to the CParse which is the union of the two.
		IntToInt				Translator1, Translator2;
		CParse					BothLabels (m_DimensionLabels);		
		LabeledDataPoint*		pDataPoint;
		int						i, n;
		double*					tempVector = NULL;

		BothLabels.Append(OtherOne->m_DimensionLabels);

		for ( i = 1; i <= m_Dimensionality; i++)
		{
			n = BothLabels.Find( m_DimensionLabels[i] );
			Translator1.insert (i,  n);
		}
		
		for ( i = 1; i <= OtherOne->m_Dimensionality; i++)
		{
			n = BothLabels.Find( OtherOne->m_DimensionLabels[i] );
			{
				Translator2.insert (i, n);
			}
		}

		int newDimensionality = BothLabels.Size();
		
                //for ( pDataPoint = first(); pDataPoint; pDataPoint = next() )
                for ( int z = 0; z < size(); z++)
                {       pDataPoint = at(z);
 			tempVector	= new double[newDimensionality];
			for (i = 1; i <= newDimensionality; i++)
			{
				tempVector[i] = 0;
			}
			for (i = 1; i <= m_Dimensionality; i++)
			{
				tempVector[Translator1[i]] = pDataPoint->m_Values [ i] ;
			}// this puts the right value in the new column for the converted data-point-values
			
			delete pDataPoint->m_Values; 
			pDataPoint->m_Values			= tempVector;
			pDataPoint->m_Dimensionality	= newDimensionality;
		}
 	
		//Second we convert the data points in the OtherOne:
                //for ( pDataPoint = OtherOne->first(); pDataPoint; pDataPoint= OtherOne->next())
                for (int z = 0; z < OtherOne->size(); z++)
                {   pDataPoint = OtherOne->at(z);
			tempVector	= new double[newDimensionality];
			 
			for (i = 1; i <= OtherOne->m_Dimensionality; i++)
			{
				tempVector[Translator2[i]] = pDataPoint->m_Values [ i] ;

			}// this puts the right value in the new column for the converted data-point-values
			
			delete pDataPoint->m_Values;
			pDataPoint->m_Values			= tempVector;
			pDataPoint->m_Dimensionality	= newDimensionality;
			
		}
 		m_DimensionLabels	= BothLabels;

	}

//----------------------------------------------------------------------------------//

	void VideoFrame::operator<< (LabeledDataPoint* datapoint)
	{
		if (m_Dimensionality != datapoint->m_Dimensionality) return;
		append ( datapoint);

	}
//----------------------------------------------------------------------------------//
	void VideoFrame::AddLabels( CSS string)
	{
		m_DimensionLabels.Append (string);

	}

//----------------------------------------------------------------------------------//
//						Graphic View class
//----------------------------------------------------------------------------------//

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////7

GraphicView::GraphicView()
{
	m_filter = NULL;
}
GraphicView::GraphicView(Q3Canvas *canvas, QMap<QString, QString>* filter, QWidget *parent)
    : Q3CanvasView(canvas, parent)
{
    pendingItem = 0;
    activeItem = 0;
	m_MyState = 0;
	m_MyStem = 0;
	m_IsStateView = false;
	m_MyRoots = 0;
	m_filter = filter;
    createActions();
	
}


GraphicView::~GraphicView()
{

}


////////////////////////////////
//// Create Action function

void GraphicView::createActions()
{
   
    PropertiesAct = new QAction(tr("&Properties..."), this);
    connect(PropertiesAct, SIGNAL(activated()),this, SLOT(Properties()));

	PlotStatesAct = new QAction(tr("&Graphic Display State"), this);
    connect(PlotStatesAct, SIGNAL(activated()), this, SLOT(PlotStates()));

	RefreshAct = new QAction(tr("&Refresh"), this);
    connect(RefreshAct, SIGNAL(activated()), this, SLOT(Refresh()));


	DisplayMeAct = new QAction(tr("&DisplayMe"), this);
    connect(DisplayMeAct, SIGNAL(activated()), this, SLOT(DisplayMe()));

	

}

 
//////////////////////////////////
///  Three Slot functions
// this is used for compounds, among other things.
void GraphicView::PlotStates()
{
	this->activeItem = 0;

	if (m_MyState != 0) {
		// Removed with Yu Hu’s FSA code.
		struct not_implemented { };

		throw not_implemented();
	}
	if (m_MyRoots != 0) {
		// Clear qcanvas
		foreach (Q3CanvasItem* i, canvas()->allItems())
			delete i;

		// Get current parameters of qcanvasview
		// and resize the qcanvas to fit this view
		int CanvasView_Height = height();
		int CanvasView_Width = width();
		canvas()->resize(CanvasView_Width - 5, CanvasView_Height - 5);

		// Display the forest
		int x = 10;
		foreach (CEdge* edge, *m_MyRoots) {
			QRect rectangle = edge->m_Daughters.first()
				->drawTree(canvas(), x, 0, m_filter);
			x = rectangle.right() + 20;
		}
	}
}
 
void GraphicView::Refresh()
{
	if ( m_IsStateView)
	{
		if ( m_MyState)
		{
			PlotStates(); 		
		}
	}
	else
	{
		if ( m_MyStem)
		{
			PlotStems(); 		
		}
	}
}

void GraphicView::DisplayMe()
{
	if (DiagramBox* Box = dynamic_cast<DiagramBox*>(activeItem)) {
		CState* pState = Box->m_MyState;
		SetMyState(pState);
		PlotStates();
	}
}

void GraphicView::Properties()
{
	if ( !m_IsStateView) return; 

    if ((activeItem) && (activeItem->rtti() == DiagramBox::RTTI )) 
	{
        PropertiesDialog dialog;
        dialog.exec(activeItem);
		return; 
	}

	if ((activeItem) && (activeItem->rtti() == DiagramLine::RTTI )) 
	{
        LinePropertiesDialog dialog;
        dialog.exec(activeItem);
		return; 
	}


}

void GraphicView::PlotStems()
{
	
	if ( !m_MyStem) return; 

	//int						DebugInt; 
	int							CanvasView_Height,Canvas_Height; 
	int							CanvasView_Width,Canvas_Width; 
	int							CanvasLeftUpperX;
	int							CanvasLeftUpperY; 
	QRect						RectOfCanvas; 
	Q3CanvasItem					*AddedText; 
	int							XLocOfOneBar, YLocOfOneBar; 
	int							WidthOfOneBar = 60;
	int							leftReserve =60;
	int							rightReserve =60;
	int							topReserve = 20;
	int							bottemReserve = 20;
	int							nameBarHeight = 40;
	int							YOfTheNameBars; 
	QString						DisplayText;
	int							numberOfUnigrams;
	int							numberOfMIs;
	int							totalBars; 
	double						maximumPostive;
	double						maximumNegative;
	double						totalValueRange;
	int							attemptedHeight;
	int							attemptedWidthOfCanvas; 
	int							i; 
	double						oneLogValue; 
	QString						oneNameValue; 


	activeItem = 0;

	// Get some parmaters from the CStem
	numberOfUnigrams = m_MyStem ->m_countofunigrams;
	numberOfMIs = m_MyStem ->m_countofmis;
	totalBars = numberOfUnigrams + numberOfMIs; 
	maximumPostive = m_MyStem ->m_maxpositive;
	maximumNegative = -(m_MyStem ->m_maxnegative);
	totalValueRange = maximumPostive + maximumNegative;


	attemptedWidthOfCanvas =leftReserve + WidthOfOneBar* (totalBars +1)+ rightReserve ; // this 1 is for the first "#"


	// Clear qcanvas
	Q3CanvasItemList list = canvas()->allItems();
    Q3CanvasItemList::Iterator it = list.begin();
    for (; it != list.end(); ++it) 
	{
        if ( *it ) delete *it;
    }

	pendingItem = NULL;
	activeItem = NULL; 


	// Get current parameters of qcanvasview and resize the qcanvas to fit this view
	CanvasView_Height = height();
	CanvasView_Width  = width(); 
	
	if ( CanvasView_Width >= attemptedWidthOfCanvas)
	{
		canvas()->resize(CanvasView_Width, CanvasView_Height-20); 
	}
	else
	{
		canvas()->resize(attemptedWidthOfCanvas, CanvasView_Height - 20); 
	}

	RectOfCanvas = canvas()->rect(); 
	RectOfCanvas.rect(&CanvasLeftUpperX, &CanvasLeftUpperY, &Canvas_Width, &Canvas_Height);   

	
	// compute the YOfTheNameBars
	YOfTheNameBars = Canvas_Height - bottemReserve - nameBarHeight;


	// compute height for display bars
	attemptedHeight = Canvas_Height - topReserve - bottemReserve - nameBarHeight;

	// compute the zeroLineHeigt
	int zeroLineHeight = static_cast<int>(
		double(attemptedHeight) * (maximumPostive/totalValueRange));

	if ( zeroLineHeight > attemptedHeight) 
	{
		zeroLineHeight = attemptedHeight;
	}

	// Draw the zero line
	Q3CanvasLine* AddedLine = new Q3CanvasLine(canvas());
	zeroLineHeight += topReserve;
	AddedLine->setPoints(0,zeroLineHeight, Canvas_Width, zeroLineHeight);
	showNewItem(AddedLine);

	//Add the first probbox refer to the first "#" a WidthOfOneBar x 20 positive bar
	XLocOfOneBar = leftReserve;
	YLocOfOneBar = zeroLineHeight - 20;
	int HeightOfOneBar = 20;

	ProbBox* AddedBar = new ProbBox(canvas(),
		WidthOfOneBar, HeightOfOneBar, true, 1);
	DisplayText = QString("   Unigram");
	AddedBar->setText(DisplayText);
	AddedBar->move(XLocOfOneBar, YLocOfOneBar);
	showNewItem(AddedBar);

	// Add text for this "#" bar
	AddedText = new Q3CanvasText(QString("      # "),canvas() );
	AddedText ->move(XLocOfOneBar, YOfTheNameBars); 
    showNewItem(AddedText);

	
	// Add ProbBox one by one. They might be Unigram prob, MI...
	for ( i =0; i< numberOfUnigrams; i++)
	{
		// One MI bar
		oneLogValue = m_MyStem ->m_mis[i];
		
		if ( oneLogValue > 0)
		{
			XLocOfOneBar = leftReserve + WidthOfOneBar + i*2*WidthOfOneBar;
			int HeightOfOneBar = static_cast<int>(
				double(attemptedHeight) *
				(oneLogValue/totalValueRange));

			YLocOfOneBar = zeroLineHeight - HeightOfOneBar;

			ProbBox* AddedBar = new ProbBox(canvas(), WidthOfOneBar, HeightOfOneBar, true, 2);
			DisplayText = QString("   MI: \n:   %1 ").arg(oneLogValue);
			AddedBar->setText(DisplayText);
			AddedBar->move(XLocOfOneBar, YLocOfOneBar);
			showNewItem(AddedBar);	

		}
		else if ( oneLogValue < 0)
		{
			oneLogValue = -oneLogValue; 

			XLocOfOneBar = leftReserve + WidthOfOneBar + i*2*WidthOfOneBar;
			
			int HeightOfOneBar = static_cast<int>(
				double(attemptedHeight) *
				(oneLogValue/totalValueRange));

			YLocOfOneBar = zeroLineHeight;

			ProbBox* AddedBar = new ProbBox(canvas(),
				WidthOfOneBar, HeightOfOneBar, false, 2);
			DisplayText = QString("   MI: \n:   -%1 ").arg(oneLogValue);
			AddedBar->setText(DisplayText);
			AddedBar->move(XLocOfOneBar, YLocOfOneBar);
			showNewItem(AddedBar);	
		}
		
		// One Unigram Bar
		oneLogValue = m_MyStem ->m_unigrams[i]; // oneLogValue can only be > 0

		XLocOfOneBar = leftReserve + WidthOfOneBar + i*2*WidthOfOneBar + WidthOfOneBar;
			
		int HeightOfOneBar = static_cast<int>(
			double(attemptedHeight) *
			(oneLogValue/totalValueRange));

		YLocOfOneBar = zeroLineHeight - HeightOfOneBar;

		ProbBox* AddedBar = new ProbBox(canvas(),
			WidthOfOneBar, HeightOfOneBar, true, 1);
		DisplayText = QString("   Unigram: \n:   %1 ").arg(oneLogValue);
		AddedBar->setText(DisplayText);
		AddedBar->move(XLocOfOneBar, YLocOfOneBar);
		showNewItem(AddedBar);

		// Put on the Name Bar
		oneNameValue = m_MyStem ->m_phonologies[i];
		AddedText = new Q3CanvasText(QString("      %1").arg(oneNameValue),canvas() );
		AddedText ->move(XLocOfOneBar, YOfTheNameBars); 
		showNewItem(AddedText);
	}
	
	
	
}
/////////////////////////////////////////////////////////////////////////
// will not be used:
/////////////////////////////////////////////////////////////////////////

void GraphicView::PlotNVectors(int numberOfDimensions, int numberOfDataPoints, double** ptrData, IntToString listOfSymbols)
{
//	int							leftReserve			= 60;		unused variables: leftReserve, rightReserve, topReserve, bottemReserve, 
//	int							rightReserve		= 60;
//	int							topReserve			= 20;
//	int							bottemReserve		= 20;
	int							CanvasView_Height,Canvas_Height; 
	int							CanvasView_Width,Canvas_Width; 
	int							CanvasLeftUpperX;
	int							CanvasLeftUpperY; 
	int							CanvasCenterX;
	int							CanvasCenterY;
	int							XLocOfOneAnchor;
	int							YLocOfOneAnchor;
	int							radiusOfTheCircle;
	int							radiusOfOneAnchor	= 20; 
	QRect						RectOfCanvas; 
	Anchor*						oneAnchor; 
	QString						displayText; 
	int							stateNumber; 
	double						anchorDegree;
	double**					anchorCoordinates; 
	int							i; 
	double						XLocOfOneDataPoint, 
								YLocOfOneDataPoint;
	Q3CanvasText*				oneDataPointText; 

	

	if (numberOfDimensions < 2)
	{
		QMessageBox::information ( NULL, "Warning", "We don't support a dimension less than 2!", "OK" );
		return; 
	}

	if (numberOfDataPoints < 1)
	{
		QMessageBox::information ( NULL, "Warning", "Number Of data points is less than 1!", "OK" );
		return; 
	}
	
	if (ptrData == NULL)
	{
		QMessageBox::information ( NULL, "Warning", "Pointer to Data is NULL!", "OK" );
		return;
	}


	// Get data parameters 
	m_numberOfDimension			= numberOfDimensions;
//	m_numberOfDataPoints		= numberOfDataPoints; 
	m_multiDimensionDataPoints	= ptrData; 
	
	// Clear qcanvas
	Q3CanvasItemList list		= canvas()->allItems();
	Q3CanvasItemList::Iterator it = list.begin();
	for (; it != list.end(); ++it) 
	{
		if ( *it ) delete *it;
	}

	pendingItem					= NULL;
	activeItem					= NULL; 

	// Get current parameters of qcanvasview and resize the qcanvas to fit this view
	CanvasView_Height			= height();
	CanvasView_Width			= width(); 
	
	canvas()					->resize(CanvasView_Width -15, CanvasView_Height -15); 	
	RectOfCanvas				= canvas()->rect(); 
	RectOfCanvas				.rect(&CanvasLeftUpperX, &CanvasLeftUpperY, &Canvas_Width, &Canvas_Height);   

	if ( Canvas_Width > Canvas_Height )
	{
		radiusOfTheCircle = Canvas_Height/2 - 5; 
	}
	else
	{
		radiusOfTheCircle = Canvas_Width/2 - 5;
	}

	//JG:
	//radiusOfTheCircle			= 100;


	CanvasCenterX				= int ( CanvasLeftUpperX + 0.5 * Canvas_Width );
	CanvasCenterY				= int ( CanvasLeftUpperY + 0.5 * Canvas_Height );

	//allocate memory for anchors
	anchorCoordinates			= new double*[numberOfDimensions];
	for ( i=0; i<numberOfDimensions; i++)
	{
		anchorCoordinates[i] = new double[2];
	}


	// Next, locate the anchors one by one. 
	// first anchor is as easy as ( CanvasCenterX +radiusOfTheCircle ,CanvasCenterY)
	oneAnchor					= new Anchor(canvas(), radiusOfOneAnchor, radiusOfOneAnchor); 
	stateNumber					= 0; 
	displayText					= QString("%1").arg(stateNumber);
	oneAnchor					->setText(displayText);
	XLocOfOneAnchor				= CanvasCenterX +radiusOfTheCircle;
	YLocOfOneAnchor				= CanvasCenterY; 
	oneAnchor					->move(XLocOfOneAnchor, YLocOfOneAnchor); 
    showNewItem(oneAnchor);
	anchorCoordinates[stateNumber][0] = XLocOfOneAnchor;
	anchorCoordinates[stateNumber][1] = YLocOfOneAnchor;
	stateNumber++; 
	

	// Now the rest of the anchors:
	while(stateNumber < numberOfDimensions)
	{
		oneAnchor				= new Anchor(canvas(), radiusOfOneAnchor, radiusOfOneAnchor); 
		anchorDegree			= stateNumber * (360.0 / numberOfDimensions);
		XLocOfOneAnchor			= int (CanvasCenterX + radiusOfTheCircle*cos(PI * anchorDegree /180.0 )); 
		YLocOfOneAnchor			= int (CanvasCenterY - radiusOfTheCircle*sin(PI * anchorDegree /180.0 )); 
		oneAnchor				->move(XLocOfOneAnchor, YLocOfOneAnchor); 
		displayText				= QString("%1").arg(stateNumber);
		oneAnchor				->setText(displayText);
		showNewItem(oneAnchor);
		anchorCoordinates[stateNumber][0] = XLocOfOneAnchor;
		anchorCoordinates[stateNumber][1] = YLocOfOneAnchor;
		stateNumber++; 
	}
	

	// Display the data points
	for( i=0; i<numberOfDataPoints; i++)
	{
		// deal with i datapoint
		computeProjectedLocForOneDataPoint(
				numberOfDimensions, 
				anchorCoordinates, 
				ptrData[i], 
				XLocOfOneDataPoint, 
				YLocOfOneDataPoint); 
		
		oneDataPointText	= new Q3CanvasText(canvas());
		
		//to do: get symbol list
		displayText			= listOfSymbols[i]; 
		oneDataPointText	->setText(displayText); 
		oneDataPointText	->move(XLocOfOneDataPoint, YLocOfOneDataPoint); 
		showNewItem(oneDataPointText); 
	}


	//release memory
	for(i=0; i<numberOfDimensions; i++)
	{
		delete [] anchorCoordinates[i];	
	}

	delete []anchorCoordinates;

	return; 


}
// ------------------------------------------------------------------------//

void GraphicView::PlotVideo (Video* pVideo)
{
//	int							leftReserve			= 60;
//	int							rightReserve		= 60; unused variables: leftReserve, rightReserve, topReserve, bottemReserve,
//	int							topReserve			= 20;
//	int							bottemReserve		= 20;
	int							CanvasView_Height,Canvas_Height; 
	int							CanvasView_Width,Canvas_Width; 
	int							CanvasLeftUpperX;
	int							CanvasLeftUpperY; 
	int							CanvasCenterX;
	int							CanvasCenterY;
	int							XLocOfOneAnchor;
	int							YLocOfOneAnchor;
	int							radiusOfTheCircle;
	int							radiusOfOneAnchor	= 20; 
	QRect						RectOfCanvas; 
	Anchor*						oneAnchor; 
	QString						displayText; 
	int							stateNumber; 
	double						anchorDegree;
	double**					anchorCoordinates; 
	int							i; 
	double						XLocOfOneDataPoint, 
								YLocOfOneDataPoint;
	Q3CanvasText*				oneDataPointText;  	
 
	if (pVideo ->GetDimensionality() < 2)
	{
		QMessageBox::information ( NULL, "Warning", "We don't support a dimension less than 2!", "OK" );
		return; 
	}

 	
 

	// Get data parameters 
	m_numberOfDimension			= pVideo->GetDimensionality();
//	m_numberOfDataPoints		= pVideo->GetNumberOfDataPoints(); 
	 	
	// Get current parameters of qcanvasview and resize the qcanvas to fit this view
	CanvasView_Height			= height();
	CanvasView_Width			= width(); 
	
	canvas()					->resize(CanvasView_Width -15, CanvasView_Height -15); 	
	RectOfCanvas				= canvas()->rect(); 
	RectOfCanvas				.rect(&CanvasLeftUpperX, &CanvasLeftUpperY, &Canvas_Width, &Canvas_Height);   

	if ( Canvas_Width > Canvas_Height )
	{
		radiusOfTheCircle = Canvas_Height/2 - 5; 
	}
	else
	{
		radiusOfTheCircle = Canvas_Width/2 - 5;
	}

	//JG:
	//radiusOfTheCircle			= 100;


	CanvasCenterX				= int ( CanvasLeftUpperX + 0.5 * Canvas_Width );
	CanvasCenterY				= int ( CanvasLeftUpperY + 0.5 * Canvas_Height );

	//allocate memory for anchors
	anchorCoordinates			= new double*[m_numberOfDimension];
	for ( i=0; i<m_numberOfDimension; i++)
	{
		anchorCoordinates[i] = new double[2];
	}


	// Next, locate the anchors one by one. 
	// first anchor is as easy as ( CanvasCenterX +radiusOfTheCircle ,CanvasCenterY)

	
	oneAnchor					= new Anchor(canvas(), radiusOfOneAnchor, radiusOfOneAnchor); 
	stateNumber					= 0; 
	displayText					= QString("%1").arg(stateNumber);
	oneAnchor					->setText(displayText);
	XLocOfOneAnchor				= CanvasCenterX +radiusOfTheCircle;
	YLocOfOneAnchor				= CanvasCenterY; 
	oneAnchor					->move(XLocOfOneAnchor, YLocOfOneAnchor); 
    showNewItem(oneAnchor);
	anchorCoordinates[stateNumber][0] = XLocOfOneAnchor;
	anchorCoordinates[stateNumber][1] = YLocOfOneAnchor;
	stateNumber++; 
	

	// Now the rest of the anchors:
	while(stateNumber < m_numberOfDimension)
	{
		oneAnchor				= new Anchor(canvas(), radiusOfOneAnchor, radiusOfOneAnchor); 
		anchorDegree			= stateNumber * (360.0 / m_numberOfDimension);
		XLocOfOneAnchor			= int (CanvasCenterX + radiusOfTheCircle*cos(PI * anchorDegree /180.0 )); 
		YLocOfOneAnchor			= int (CanvasCenterY - radiusOfTheCircle*sin(PI * anchorDegree /180.0 )); 
		oneAnchor				->move(XLocOfOneAnchor, YLocOfOneAnchor); 
		displayText				= QString("%1").arg(stateNumber);
		oneAnchor				->setText(displayText);
		showNewItem(oneAnchor);
		anchorCoordinates[stateNumber][0] = XLocOfOneAnchor;
		anchorCoordinates[stateNumber][1] = YLocOfOneAnchor;
		stateNumber++; 
	}
	


//	int								time = 0;
	VideoFrame*						pVideoFrame;
	LabeledDataPoint*				pDataPoint;
	// Display the data pointsp
        //for ( pVideoFrame = pVideo->first(); pVideoFrame; pVideoFrame= pVideo->next() )
        for (int z = 0; z < pVideo->size(); z++)
        {       pVideoFrame = pVideo->at(z);
		
			// ----------	Clear qcanvas -------------------------------//
			Q3CanvasItemList list		= canvas()->allItems();
			Q3CanvasItemList::Iterator it = list.begin();
			for (; it != list.end(); ++it) 
			{
				if ( *it ) delete *it;
			}

			pendingItem					= NULL;
			activeItem					= NULL; 
			// ----------	End of Clear qcanvas -------------------------------//

		
		
                //for (pDataPoint = pVideoFrame->first(); pDataPoint; pDataPoint = pVideoFrame->next() )
                for (int y = 0; y < pVideoFrame->size(); y++)
                {       pDataPoint = pVideoFrame->at(y);
			computeProjectedLocForOneDataPoint(
//					&iter.data(),
					pDataPoint,
					anchorCoordinates, 
					XLocOfOneDataPoint, 
					YLocOfOneDataPoint); 
			oneDataPointText	= new Q3CanvasText(canvas());
			displayText			=  pDataPoint->m_Label; 
			oneDataPointText	->setText(displayText); 
			oneDataPointText	->move(XLocOfOneDataPoint, 
									   YLocOfOneDataPoint); 
			showNewItem(oneDataPointText); 
		}
		// Sleep for a short time : Is there any Qt sleep function ?
		int			dummy, j;
		for(j=0; j<500000; j++)
		{
			dummy = int (base2log (100));
			dummy = int (base2log (100));
		}



	}
		
		
//	for (time = 0; time < pVideo->size(); time++)
//	{
//		pData = &(*pVideo)[time];		
//		VideoFrame::Iterator iter = pData->begin();
//		for (; iter != pData->end(); ++iter) 
//		{
//			/////////////// this is where the main work is done: /////////////////
//			computeProjectedLocForOneDataPoint(
//					&iter.data(),
//					pData,
//					anchorCoordinates, 
//					XLocOfOneDataPoint, 
//					YLocOfOneDataPoint); 
//			oneDataPointText	= new QCanvasText(canvas());
//			displayText			=  iter.data().m_Label; 
//			oneDataPointText	->setText(displayText); 
//			oneDataPointText	->move(XLocOfOneDataPoint, 
//									   YLocOfOneDataPoint); 
//			showNewItem(oneDataPointText); 
//			///////////////////////////////////////////////////////////////////////
//
//		} 
//	}

	//release memory
	for(i=0; i<m_numberOfDimension; i++)
	{
		delete [] anchorCoordinates[i];	
	}

	delete []anchorCoordinates;

	return; 

	











}

// ------------------------------------------------------------------------//
// John rewrote: 7 2006 
void GraphicView::PlotNVectors2(VideoFrame* Data )
{
//	int							leftReserve			= 60;		unused variables: leftReserve, rightReserve, topReserve, bottemReserve,
//	int							rightReserve		= 60;
//	int							topReserve			= 20;
//	int							bottemReserve		= 20;
	int							CanvasView_Height,Canvas_Height; 
	int							CanvasView_Width,Canvas_Width; 
	int							CanvasLeftUpperX;
	int							CanvasLeftUpperY; 
	int							CanvasCenterX;
	int							CanvasCenterY;
	int							XLocOfOneAnchor;
	int							YLocOfOneAnchor;
	int							radiusOfTheCircle;
	int							radiusOfOneAnchor	= 20; 
	QRect						RectOfCanvas; 
	Anchor*						oneAnchor; 
	QString						displayText; 
	int							stateNumber; 
	double						anchorDegree;
	double**					anchorCoordinates; 
	int							i; 
	double						XLocOfOneDataPoint, 
								YLocOfOneDataPoint;
	Q3CanvasText*				oneDataPointText;  	
 
	if (Data->m_Dimensionality < 2)
	{
		QMessageBox::information ( NULL, "Warning", "We don't support a dimension less than 2!", "OK" );
		return; 
	}

	// TODO: put this back in (reformulated, though)
//	if (Data->size() < 1)
//	{
//		QMessageBox::information ( NULL, "Warning", "Number Of data points is less than 1!", "OK" );
//		return; 
//	}
	
 

	// Get data parameters 
	m_numberOfDimension			= Data->m_Dimensionality;
//	m_numberOfDataPoints		= Data->size(); 
	 	
	// Clear qcanvas
	Q3CanvasItemList list		= canvas()->allItems();
	Q3CanvasItemList::Iterator it = list.begin();
	for (; it != list.end(); ++it) 
	{
		if ( *it ) delete *it;
	}

	pendingItem					= NULL;
	activeItem					= NULL; 

	// Get current parameters of qcanvasview and resize the qcanvas to fit this view
	CanvasView_Height			= height();
	CanvasView_Width			= width(); 
	
	canvas()					->resize(CanvasView_Width -15, CanvasView_Height -15); 	
	RectOfCanvas				= canvas()->rect(); 
	RectOfCanvas				.rect(&CanvasLeftUpperX, &CanvasLeftUpperY, &Canvas_Width, &Canvas_Height);   

	if ( Canvas_Width > Canvas_Height )
	{
		radiusOfTheCircle = Canvas_Height/2 - 5; 
	}
	else
	{
		radiusOfTheCircle = Canvas_Width/2 - 5;
	}

	//JG:
	//radiusOfTheCircle			= 100;


	CanvasCenterX				= int (CanvasLeftUpperX + 0.5 * Canvas_Width);
	CanvasCenterY				= int (CanvasLeftUpperY + 0.5 * Canvas_Height);

	//allocate memory for anchors
	anchorCoordinates			= new double*[m_numberOfDimension];
	for ( i=0; i<m_numberOfDimension; i++)
	{
		anchorCoordinates[i] = new double[2];
	}


	// Next, locate the anchors one by one. 
	// first anchor is as easy as ( CanvasCenterX +radiusOfTheCircle ,CanvasCenterY)

	
	oneAnchor					= new Anchor(canvas(), radiusOfOneAnchor, radiusOfOneAnchor); 
	stateNumber					= 0; 
	displayText					= QString("%1").arg(stateNumber);
	oneAnchor					->setText(displayText);
	XLocOfOneAnchor				= CanvasCenterX +radiusOfTheCircle;
	YLocOfOneAnchor				= CanvasCenterY; 
	oneAnchor					->move(XLocOfOneAnchor, YLocOfOneAnchor); 
    showNewItem(oneAnchor);
	anchorCoordinates[stateNumber][0] = XLocOfOneAnchor;
	anchorCoordinates[stateNumber][1] = YLocOfOneAnchor;
	stateNumber++; 
	

	// Now the rest of the anchors:
	while(stateNumber < m_numberOfDimension)
	{
		oneAnchor				= new Anchor(canvas(), radiusOfOneAnchor, radiusOfOneAnchor); 
		anchorDegree			= stateNumber * (360.0 / m_numberOfDimension);
		XLocOfOneAnchor			= int(CanvasCenterX + radiusOfTheCircle*cos(PI * anchorDegree /180.0 )); 
		YLocOfOneAnchor			= int(CanvasCenterY - radiusOfTheCircle*sin(PI * anchorDegree /180.0 )); 
		oneAnchor				->move(XLocOfOneAnchor, YLocOfOneAnchor); 
		displayText				= QString("%1").arg(stateNumber);
		oneAnchor				->setText(displayText);
		showNewItem(oneAnchor);
		anchorCoordinates[stateNumber][0] = XLocOfOneAnchor;
		anchorCoordinates[stateNumber][1] = YLocOfOneAnchor;
		stateNumber++; 
	}
	

  
 	LabeledDataPoint*				pDataPoint;
	// Display the data pointsp
        //for (pDataPoint = Data->first(); pDataPoint; pDataPoint = Data->next() )
        for (int z = 0; z < Data->size(); z++)
        {       pDataPoint = Data->at(z);
		computeProjectedLocForOneDataPoint(
				pDataPoint,
				anchorCoordinates, 
				XLocOfOneDataPoint, 
				YLocOfOneDataPoint); 
		oneDataPointText	= new Q3CanvasText(canvas());
		displayText			=  pDataPoint->m_Label; 
		oneDataPointText	->setText(displayText); 
		oneDataPointText	->move(XLocOfOneDataPoint, 
									   YLocOfOneDataPoint); 
		showNewItem(oneDataPointText); 
	}
	


	//release memory
	for(i=0; i<m_numberOfDimension; i++)
	{
		delete [] anchorCoordinates[i];	
	}

	delete []anchorCoordinates;

	return; 


}
//rewritten JG July 2006
void GraphicView::computeProjectedLocForOneDataPoint( LabeledDataPoint* Data,
													double** anchorCoordinates, 
													double& xCoordinate, 
													double& yCoordinate)
{
	int				i; 
	double*			weights;
	double			sumHighDimensionData = 0.0; 
	
	for ( i=0; i< Data->m_Dimensionality; i++)
	{
		sumHighDimensionData += Data->m_Values[i];		
	}

	weights = new double[Data->m_Dimensionality];	

	for ( i=0; i< Data->m_Dimensionality; i++)
	{
		weights[i] = Data->m_Values[i] / sumHighDimensionData; 
	}

	xCoordinate = 0.0;
	yCoordinate = 0.0;
	
	// Compute xCoordinate and  yCoordinate
	for ( i=0; i< Data->m_Dimensionality; i++)
	{
		xCoordinate += weights[i] * anchorCoordinates[i][0]; // anchorCoordinates[i][0] keeps the xCoordinate of anchor i
		yCoordinate += weights[i] * anchorCoordinates[i][1]; // anchorCoordinates[i][1] keeps the yCoordinate of anchor i
	}

	delete []weights; 
}

//: will not be used:
void GraphicView::computeProjectedLocForOneDataPoint(int numberOfStates, 
													double** anchorCoordinates, 
													double* highDimensionData, 
													double& xCoordinate, 
													double& yCoordinate)
{
	int				i; 
	double*			weights;
	double			sumHighDimensionData = 0.0; 
	
	for ( i=0; i< numberOfStates; i++)
	{
		sumHighDimensionData += highDimensionData[i];		
	}

	weights = new double[numberOfStates];	

	for ( i=0; i< numberOfStates; i++)
	{
		weights[i] = highDimensionData[i] / sumHighDimensionData; 
	}

	xCoordinate = 0.0;
	yCoordinate = 0.0;
	
	// Compute xCoordinate and  yCoordinate
	for ( i=0; i< numberOfStates; i++)
	{
		xCoordinate += weights[i] * anchorCoordinates[i][0]; // anchorCoordinates[i][0] keeps the xCoordinate of anchor i
		yCoordinate += weights[i] * anchorCoordinates[i][1]; // anchorCoordinates[i][1] keeps the yCoordinate of anchor i
	}

	delete []weights; 
}

/////////////////////////////
//// Mouse event functions


void GraphicView::contentsContextMenuEvent(QContextMenuEvent *event)
{
	if ( !m_IsStateView) return; 

    Q3PopupMenu contextMenu(this);
    if (activeItem ) 
	{
		
		if ( activeItem ->rtti() == DiagramBox::RTTI)
		{
			contextMenu.insertSeparator();
			PropertiesAct->addTo(&contextMenu);
			contextMenu.insertSeparator();
			DisplayMeAct->addTo(&contextMenu);
		}
		else
		{
			contextMenu.insertSeparator();
			PropertiesAct->addTo(&contextMenu);	
		}

    }
	else 
	{
		RefreshAct->addTo(&contextMenu);
        contextMenu.insertSeparator();
		PlotStatesAct->addTo(&contextMenu); 
    }
    contextMenu.exec(event->globalPos());
}



void GraphicView::contentsMousePressEvent(QMouseEvent *event)
{
    if ( !m_IsStateView) return; 
     
	 if ( event->button() == Qt::LeftButton )
	 {
		Q3CanvasItemList items = canvas()->collisions(event->pos());

		if (items.empty())
		{  
			setActiveItem(0);
		}
		else
		{
			setActiveItem(*items.begin());
		}
	 }
}



void GraphicView::contentsMouseDoubleClickEvent(QMouseEvent *event)
{
	if ( !m_IsStateView) return; 

    if (event->button() == Qt::LeftButton && activeItem) 
	{
		Properties(); 
    }
    
}


///////////////////////////////////////
//// Some other necessary functions

void GraphicView::showNewItem(Q3CanvasItem *item)
{
    setActiveItem(item);
    item->show();
    canvas()->update();
}

void GraphicView::setActiveItem(Q3CanvasItem *item)
{
    if (item != activeItem) 
	{
        if (activeItem) activeItem->setActive(false);
        
		activeItem = item;
        
		if (activeItem) activeItem->setActive(true);

        canvas()->update();
    }
}



//////////////////////////////////////////////
/////////////////////////////////////////////
///  Next are implementation of DiagramBox


DiagramBox::DiagramBox(Q3Canvas *canvas)
    : Q3CanvasRectangle(canvas)
{
	setSize(100, 60);
	setPen(QPen(Qt::blue));
	setBrush(Qt::white);
	str = "Text";
}

DiagramBox::DiagramBox(Q3Canvas *canvas, int Width, int Height, CState* MyState)
	: Q3CanvasRectangle(canvas)
{
	setSize(Width, Height);
	setPen(QPen(Qt::blue));
	setBrush(Qt::white);
	str = "Text";
	m_MyState = MyState;  
}


DiagramBox::~DiagramBox()
{
    hide();
}

void DiagramBox::setText(const QString &newText)
{
    str = newText;
    update();
}


void DiagramBox::drawShape(QPainter &painter)
{
	Q3CanvasRectangle::drawShape(painter);
    painter.drawText(rect(), Qt::AlignLeft, text());
   
}


//////////////////////////////////////////////
/////////////////////////////////////////////
///  Next are implementation of ProbBox


ProbBox::ProbBox(Q3Canvas *canvas)
    : Q3CanvasRectangle(canvas)
{
    setSize(100, 60);
    setPen(QPen(Qt::blue));
    setBrush(Qt::white);
    str = "Text";
}

ProbBox::ProbBox(Q3Canvas *canvas, int Width, int Height, bool positive, int type)
	: Q3CanvasRectangle(canvas)
{
	// type =1 --> Unigram;  type = 2 --> MI

	setSize(Width, Height);
	if ( positive)
	{
		if ( type == 1)
		{
			setPen(QPen(Qt::blue));
			setBrush(Qt::green);
		}
		else if ( type == 2)
		{
			setPen(QPen(Qt::red));
			setBrush(Qt::blue);
		}
		else
		{
			setPen(QPen(Qt::white));
			setBrush(Qt::white);
		}
	}
	else
	{
		setPen(QPen(Qt::black));
		setBrush(Qt::red);
	}
    str = "Text";

}


ProbBox::~ProbBox()
{
    hide();
}

void ProbBox::setText(const QString &newText)
{
    str = newText;
    update();
}


void ProbBox::drawShape(QPainter &painter)
{
	Q3CanvasRectangle::drawShape(painter);
    painter.drawText(rect(), Qt::AlignLeft, text());
   
}




/////////////////////////////////////////////
///// Implementation of DiagramLine

DiagramLine::DiagramLine(Q3Canvas *canvas)
    : Q3CanvasLine(canvas)
{
    setPoints(0, 0, 0, 99);
}
/*
DiagramLine::DiagramLine(QCanvas *canvas, int SX, int SY, int EX, int EY, int FromState, int ToState, QMap<QString, int>* Morphemes, Morphology* MyMorphology)
	: QCanvasLine(canvas)
{
	setPen(QPen(Qt::red));
	setPoints(SX, SY, EX, EY);
	m_FromWhichState = FromState; 
	m_ToWhichState = ToState;
	m_Morphemes = *Morphemes; 
	m_MyMorphology = MyMorphology; 
}
*/
DiagramLine::~DiagramLine()
{
    hide();
}

void DiagramLine::drawShape(QPainter &painter)
{
    Q3CanvasLine::drawShape(painter);
}

//////////////////////////////////////////////
/////////////////////////////////////////////
///  Next are implementation of DiagramBox


Anchor::Anchor(Q3Canvas *canvas)
    : Q3CanvasEllipse(canvas)
{
    setSize(60, 60);
    setPen(QPen(Qt::blue));
    setBrush(Qt::white);
    m_str = "Text";
}

Anchor::Anchor(Q3Canvas *canvas, int Width, int Height)
	: Q3CanvasEllipse(canvas)
{           
	setSize(Width, Height);
    setPen(QPen(Qt::white));
    setBrush(Qt::blue);
    m_str = "Text";

}


Anchor::~Anchor()
{
    hide();
}

void Anchor::setText(const QString &newText)
{
    m_str = newText;
    update();
}


void Anchor::drawShape(QPainter &painter)
{	
	QPen		pen(Qt::white, 2);

	Q3CanvasEllipse::drawShape(painter);
    
	painter.setPen(pen); 
	painter.drawText(boundingRect(), Qt::AlignCenter, text());  
}
