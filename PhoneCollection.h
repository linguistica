// Description length bookkeeping, search, and display of encountered phonemes
// Copyright © 2009 The University of Chicago
#ifndef PHONECOLLECTION_H
#define PHONECOLLECTION_H
 
class CPhoneCollection;

#include "CollectionTemplate.h"
#include "BiphoneCollection.h"
#include "generaldefinitions.h"
namespace linguistica { namespace ui { class status_user_agent; } }
class CPhone;

/// A collection of phonemes,
/// plus a collection of biphones
/// to keep track of conditional probabilities.
class CPhoneCollection : public TCollection<CPhone> {
	class CWordCollection* m_MyWords;
	CBiphoneCollection m_MyBiphones;

	double* m_expMI;
	double* m_expMIFromBoundary;
	double* m_expMIToBoundary;
public:
	explicit CPhoneCollection(class CWordCollection* words);

	class CBiphoneCollection* GetMyBiphones() { return &m_MyBiphones; }
	void CountPhonesAndBiphonesInWord(class CStem* stem, enum eTier tier);
	void Normalize();
	void ListDisplay(class Q3ListView* widget,
		linguistica::ui::status_user_agent& status);
	double GetSumOfMyMIs();
	void PopulateMonteCarlo(class MonteCarlo* pMyMonteCarlo);
	/// XXX. not fully implemented
	void ComputeStringAgreementAndDisagreement(
		CLParse* string1, CLParse* string2,
		double& agreement_unigram, double& agreement_bigram,
		double& disagreement_unigram, double& disagreement_bigram);
};

#endif // PHONECOLLECTION_H
