// Network type for syllable discovery with Dynamic Computational Networks
// Copyright © 2009 The University of Chicago
#ifndef NETWORKSYL_H
#define NETWORKSYL_H

// See the corpussyl documentation in DCNcorpussyl.h for an overview.
// See also the very similar grammar type in DCNnetwork.h

#include "DCNgrammarsyl.h"
#include <qstring.h>
#include <qlabel.h>

/**
 *	A network is a DCN that takes a grammar and puts it
 *	in equilibrium. The network is represented as an array
 *  of nodes, each node being type float. The number of nodes
 *  correspondes to the number of syllables in the word (syl).
 *  A network can output the stress of the word with getStress().
 */
class networksyl 
{

private:
	bool		converged;

	float*		node;
	float*		sonority;

	QString		word;
	grammarsyl*	theGrammar;


public:
	networksyl();
	// COPY CONSTRUCTOR?!?
	virtual ~networksyl();

	void		setGrammar(grammarsyl* theGrammar);
	void		setWord(QString word);

	void		equilibrium();

	bool		isConverged() const { return converged; }

	int			syllable() const { return word.length(); }

	QString		getMaxima() const;
	QString		getSyllabifiedWord() const;
	grammarsyl*	getGrammar() { return theGrammar; }
	void		print(QLabel* label);
	QString		sylPrintout();

};

#endif // NETWORKSYL_H
