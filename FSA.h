// Finite-state automaton representation of morphology
// Copyright © 2009 The University of Chicago
#ifndef FSA_H
#define FSA_H

#include <Q3ListView>
#include <Q3TextEdit>
#include <QTableView>
#include <QStandardItemModel>
#include "MiniLexicon.h"
#include <QString>
#include <set>
#include <list>

class FSA;
class FSAedge;
class FSAstate;
class FSAMorpheme;
class SigAlignment;


typedef std::list<FSAMorpheme*> FSAMorphemeList;
typedef std::list<FSAstate*> FSAStateList;
typedef std::list<FSAedge*> FSAedgeList;
typedef std::list<FSAedgeList*> FSApathList;

class FSAMorpheme {
  QString str;
  int m_corpusCount;

public:
  FSAMorpheme(QString qs,int cc):str(qs),m_corpusCount(cc){};  

  double GetDL(int characterCount) const;
  const QString & toStr() const {return str;};

  bool operator==(const FSAMorpheme & other) const;

  friend class FSA;  
  friend class FSAedge;
  friend class FSAstate;
};

extern QString join(const FSAMorphemeList& v, const QString& delim);

class FSA {
	FSAStateList* m_States;
	FSAedgeList* m_Edges;
	FSAStateList* m_StartStates;
	std::list<FSAedgeList*> m_FSAPathList;
	bool m_searched;
	int m_MaxPathLength;
	CMiniLexicon* m_lexicon;
	int m_charCount;
	// the same morpheme may occur at multiple edges
	QMap<QString, FSAMorpheme*> m_morphemes;
	//QMultiMap<QString, FSAedge*> m_morphemeEdges;
	int m_nextStartStateId;
	int m_nextStateId;
public:
	// construction/destruction.
	FSA();
	explicit FSA(CMiniLexicon* pMiniLex);

	~FSA();

private:
	// disable copy.
	FSA(const FSA& x);
	FSA& operator=(const FSA& x);
	void AddEdge(FSAedge* edge);
public:

	// insert.

	void AddSignature(CSignature* pSig);
	//void AddPrefixSignature(CSignature* pSig);
	void AddState(FSAstate* state);

	// remove.

	void RemoveEdge(FSAedge* edge);

	/// description length
	double ComputeDL();

	void AddPrefixes(CMiniLexicon* pMiniLex);
	// output to GUI.

	void FSAListDisplay(Q3ListView* widget,
		QMap<QString, QString>* filter,
		bool unused);

	// display helpers.

	/// list paths to leaves in m_FSAPathList
	void DoSearch();
	void ResetDisplay();

	/// state list
	FSAStateList* GetStates() { return m_States;}

	// linguistic analysis.

	/// sample function that does some manipulation of FSA
	void FSATestFunc();

	static int GetRobustness(FSAedgeList * pPath);

   void OutputFSAXfst ( const QString& filename );

	//returns pointer to new edge
	FSAedge* DoParallelSplit (FSAedge* pEdge, FSAMorphemeList& morphsToMove);
	//returns pointer to "first" new edge, ie, edge from start state
	FSAedge* DoSeriesSplit(FSAedge* pEdge, unsigned int len, bool suffix=true);

   //first,last are iterators to list of lists,
   // union of lists must be equal to set of morphemes on pEdge
	void DoMultParallelSplit( FSAedge* pEdge, 
		std::list<FSAMorphemeList>::iterator fisrt,
		std::list<FSAMorphemeList>::iterator last );

	friend class FSAedge;
	friend class FSAstate;
};

class FSAedge {
	FSAMorphemeList m_Morphemes;
	FSAstate* m_FromState;
	FSAstate* m_ToState;
	FSA* m_FSA;
public:
	// construction/destruction.

	explicit FSAedge(FSA* pFSA, FSAstate* start = 0, FSAstate* end = 0);
//	FSAedge(FSA* pFSA, FSAstate* start, FSAstate* end, class CParse* pLabels);
	// suppress implied default constructor
private:
	FSAedge();
public:
	// destructor implicitly defined.

	// disable copy.
private:
	FSAedge(const FSAedge& x);
	FSAedge& operator=(const FSAedge& x);

public:
	void AddMorpheme(const QString& morpheme_text, int count);
	void RemoveMorpheme(FSAMorphemeList::iterator iter);
	FSAMorphemeList* GetMorphemes() { return &m_Morphemes; }

	// source and target states.

	FSAstate* GetFromState() const { return m_FromState; }
	FSAstate* GetToState() const { return m_ToState; }
};

class FSAstate
{
	FSAedgeList m_EdgesOut;
	FSAedgeList m_EdgesIn;
	int m_MaxDepth;
	QString m_stateLabel;

	// XXX. only used for breadth-first search
	std::list<FSAedgeList*> m_PathList; //list of paths to this node
	unsigned int m_DiscoverCount;
	int m_stateId;

public:
	std::list<FSAedgeList*>* GetPathList(){ return &m_PathList; }
	void addPath(FSAedgeList* pPath) { m_PathList.push_back(pPath);}
	void setMaxDepth(int d){this->m_MaxDepth=d;}
	int  getMaxDepth(){return this->m_MaxDepth;}

	FSAstate(FSA* pFSA);

	FSAedgeList* GetEdgesOut() { return &m_EdgesOut; }
	void AddEdgeOut(FSAedge* pEdge);

	FSAedgeList* GetEdgesIn() { return &m_EdgesIn; }
	void AddEdgeIn(FSAedge* pEdge);

	//xfst output
	void OutputXfst(QTextStream& outf);
	void SearchEdgeXfst (QTextStream& outf, std::set<FSAstate*>& discovered);
	QString getStateName(){ return QString("S%1").arg(this->m_stateId) ;}

	friend class FSA;
	friend class FSAListViewItem;
};

class SigAlignment
{
  class CSignature*	m_Sig1;  // the standard of comparison
  class CSignature*	m_Sig2;  // the signature being reanalyzed

  int				m_Length; // number of affixes in longer signature
  
};

/// Qt3-style row object for a table displaying the FSA
/// Each row corresponds to a complete edge path
class FSAListViewItem : public Q3ListViewItem {
	QPixmap* m_pImage;
	/// points to path to some final state
	FSAedgeList* m_pPath;
public:
	// construction/destruction.

	FSAListViewItem(Q3ListView* pView, FSAedgeList* path)
		: Q3ListViewItem(pView), m_pImage(), m_pPath(path) { }
	FSAListViewItem(FSAListViewItem* pParent, FSAedgeList* path)
		: Q3ListViewItem(pParent), m_pImage(), m_pPath(path) { }
	// disable default-construction.
private:
	FSAListViewItem();
public:
	~FSAListViewItem();

	// copy.

	FSAListViewItem(const FSAListViewItem& x)
		: Q3ListViewItem(x),
		// drop cached pixmap to avoid deleting it twice
		m_pImage(),
		m_pPath(x.m_pPath) { }
	FSAListViewItem& operator=(const FSAListViewItem& x)
	{
		Q3ListViewItem::operator=(x);
		m_pImage = 0;
		m_pPath = x.m_pPath;
		return *this;
	}

	// output to GUI.

	/// graphical display
	/// repeated requests are fast since created image is cached
	QPixmap* GetQPixmap();
	/// write information about path to “command line” pane
	void DisplayEdgePath(Q3TextEdit* m_commandLine);

	/// start state
	FSAstate* GetLVStartState()
		{ return m_pPath->front()->GetFromState(); }
private:
	/// helper for graphical display
	void build_pixmap();

  friend class FSA;
};

#ifndef USE_GRAPHVIZ
inline void FSAListViewItem::build_pixmap()
{
	// not using graphviz, empty image
	m_pImage = new QPixmap;
}
#endif

#endif // FSA_H
