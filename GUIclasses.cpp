// Implementation of word list and stem list displays
// Copyright © 2009 The University of Chicago
#include "GUIclasses.h"
#include "Stem.h"

#include <Q3ListView>
#include "ui/Status.h"
#include "MiniLexicon.h"
#include "Lexicon.h"
#include "WordCollection.h"
#include "StemCollection.h"

// Word collections.

GUIWordCollection::GUIWordCollection(CMiniLexicon* lex, CWordCollection* words)
	: m_Words(words), m_MiniLexicon(lex),
	m_DisplayMode(CWordListViewItem::MiniLexicon_MorphologyStuffFirst) { }

void GUIWordCollection::ListDisplay(Q3ListView* pView,
	QMap<QString, QString>* filter, bool bAnalyzedWordsOnly)
{
	int MaxNumberOfStems = 0;
	int n;

	pView->setRootIsDecorated(false);
	pView->setSorting(1);
	for (int i = 0; i < m_Words->GetCount(); ++i) {
		n = m_Words->GetAt(i)->GetNumberOfStems();
		if (n > MaxNumberOfStems)
			MaxNumberOfStems = n;
	}

	// Remove all previous columns
	while (pView->columns() != 0)
		pView->removeColumn( 0 );

	// Add Column headers
	switch (m_DisplayMode) {
	case CWordListViewItem::MiniLexicon_MorphologyStuffFirst:
		pView->addColumn("Word");
		pView->addColumn("Mini");
		pView->hideColumn(1);  // Not needed in this view
		pView->addColumn("Descr. Length");
		pView->addColumn("Corpus Count");
		pView->addColumn("Type");
		pView->addColumn("Signature");
		pView->addColumn("Source");
		pView->addColumn("Prefix");
		pView->addColumn("Stem");
		pView->addColumn("Suffix");
		pView->addColumn("Log prob");
		pView->addColumn("Av log prob");
		pView->addColumn("Log prob (cond.)");
		pView->addColumn("Av log prob (cond.)");
		pView->addColumn("Tier 1");
		pView->addColumn("Tier 1 template");
		pView->addColumn("Tier 2");
		break;
	case CWordListViewItem::MiniLexicon_PhonologyStuffFirst:
	default:
		pView->addColumn("Word");
		pView->addColumn("Log prob (unigram)");
		pView->addColumn("Av log prob (uni)");
		pView->addColumn("Log prob (bigram)");
		pView->addColumn("Av log prob (bi)");
		pView->addColumn("HMM log prob");
		pView->addColumn("Tier 1");
		pView->addColumn("Tier 1 template");
		pView->addColumn("Tier 2");
		pView->addColumn("Mini" );
		pView->hideColumn(8);  // Not needed in this view
		pView->addColumn("Descr. Length");
		pView->addColumn("Corpus Count");
		pView->addColumn("Type");
		pView->addColumn("Signature");
		pView->addColumn("Source");
		pView->addColumn("Prefix");
		pView->addColumn("Stem");
		pView->addColumn("Suffix");
		break;
	}

	if (m_Words->GetSortValidFlag() == false)
		m_Words->Sort(KEY);

	CLexicon& lex = *m_MiniLexicon->GetLexicon();
	linguistica::ui::status_user_agent& status = lex.status_display();

	status.major_operation = "Creating word list for display";
	status.progress.clear();
	status.progress.set_denominator(m_Words->GetCount());
	for (int wordno = 0; wordno < m_Words->GetCount(); ++wordno) {
		CStem* pWord = m_Words->GetAtSort(wordno);
		if (pWord != 0 && !bAnalyzedWordsOnly)
			pWord->WordListDisplay(pView, filter, m_DisplayMode,
				m_MiniLexicon->GetLexicon()->GetNumberOfCharacterTypes());
		else if (pWord != 0 && bAnalyzedWordsOnly && pWord->Size() > 1)
			pWord->WordListDisplay(pView, filter, m_DisplayMode,
				m_MiniLexicon->GetLexicon()->GetNumberOfCharacterTypes());
		status.progress = wordno;
	}
	status.progress.clear();
	status.major_operation.clear();
}

//------------------------------------------------------------------//
//
//     Word items
//
//------------------------------------------------------------------//


//--------------------------------------------------
// Public accessor methods
//--------------------------------------------------

// TODO: integrate this with the Qt interface list items
// Display this word in a list of words in the triscreen
// interface
//
// Parameters:
//    List - the listitem to be displayed
//    position - the position in the list
//    SuffixColumn - TODO

void CStem::WordListDisplay(Q3ListView* List,
	QMap<QString, QString>* filter,
	enum CWordListViewItem::display_mode DisplayMode,
	int char_count)
{
        static_cast<void>(new CWordListViewItem(
                List, Display(QChar(), filter),
                this->GetMyMini()->GetIndex(), this,
                filter, DisplayMode, char_count));
}

void CStemCollection::ListDisplay(Q3ListView* pView,
		QMap<QString, QString>* filter)
{
	pView->setRootIsDecorated(false);

	// Remove all previous columns
	while (pView->columns() != 0)
		pView->removeColumn(0);
	pView->setSorting(1);
	// Add Column headers
	pView->addColumn("Stem", 100);
	pView->addColumn("Phonological content", 140);
	pView->addColumn("Length ptr to me", 120);
	pView->addColumn("Corpus count"	, 100);
	pView->addColumn("Suffix sig");
	pView->addColumn("Prefix sig");
	pView->addColumn("Origin");
	pView->addColumn("Type");
	pView->setColumnAlignment(0,Qt::AlignCenter);
	pView->setColumnAlignment(1,Qt::AlignRight);
	pView->setColumnAlignment(2,Qt::AlignRight);
	pView->setColumnAlignment(3,Qt::AlignCenter);
	pView->setColumnAlignment(4,Qt::AlignCenter);
	pView->setColumnAlignment(5,Qt::AlignCenter);
	pView->setColumnAlignment(6,Qt::AlignCenter);

	if (m_SortValidFlag == false)
		Sort(KEY);

	CLexicon& lex = *m_pLexicon;
	linguistica::ui::status_user_agent& status = lex.status_display();

	// Display all items.

	status.major_operation = "Creating stem list for display";
	status.progress.clear();
	status.progress.set_denominator(GetCount());
	for (int i = 0; i < GetCount(); ++i) {
		CStem* pStem = GetAtSort(i);
		pStem->StemListDisplay(pView, filter,
		                       lex.GetNumberOfCharacterTypes());
		status.progress = i;
	}
	status.progress.clear();
	status.major_operation.clear();
}

// Display this stem in a list on Triscreen
//
// Parameters:
//    List - the list it will be added to
//    position - the position in the list

void CStem::StemListDisplay(Q3ListView* List,
	QMap<QString, QString>* filter, int char_count)
{
        static_cast<void>(new CStemListViewItem(
                List, Display(QChar(), filter),
                this->GetMyMini()->GetIndex(),
                this, filter, char_count));
}
