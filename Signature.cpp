// Implementation of CSignature, CSignatureListViewItem methods
// Copyright © 2009 The University of Chicago
#include "Signature.h"
#include  <QMessageBox>
#include <QTextStream>
#include <QList>
#include "linguisticamainwindow.h"
#include <QPair>
#include "MiniLexicon.h"
#include "LPreferences.h"
#include "CorpusWord.h"
#include "Suffix.h"
#include "Prefix.h"
#include "Stem.h"
#include "SignatureCollection.h"
#include "SuffixCollection.h"
#include "PrefixCollection.h"
#include "WordCollection.h"
#include "StemCollection.h"
#include "SparseIntVector.h"
#include "CompareFunc.h"
#include "HTML.h"
#include "log2.h"
#include "Typedefs.h"
#include "implicit_cast.h"

bool stemlessthan(const QPair<CStem*, int> pair1, const QPair<CStem*, int> pair2 );

bool stemlessthan(const QPair<CStem*, int> pair1, const QPair<CStem*, int> pair2 )
{
    return pair2.second < pair1.second;
}


//===================================================================================================//
//
//          Signature listview item
//
//===================================================================================================//
CSignatureListViewItem::CSignatureListViewItem(Q3ListView *parent,
	QString signature, int mini, CSignature* pSig,
	QMap<QString, QString>* filter)
: Q3ListViewItem( parent, signature )
{
  m_signature    = pSig;
  m_filter    = filter;
  m_label      = signature;
  m_parentlist  = parent;
  m_mini = mini;
}


CSignatureListViewItem::CSignatureListViewItem(Q3ListViewItem *parent,
	QString signature, int mini, CSignature* pSig,
	QMap<QString, QString>* filter)
: Q3ListViewItem( parent, signature )
{
  m_signature    = pSig;
  m_filter    = filter;
  m_label      = signature;
  m_parentlist  = parent->listView();
  m_mini = mini;
}
 
int  CSignatureListViewItem::compare(Q3ListViewItem *item, int col, bool asc) const
{

     if (col== 2)
     {
	    return MakeComparable (	m_signature->ComputeDLofModel() , ((CSignatureListViewItem*) item)->GetSignature()->ComputeDLofModel() );
     }
     if (col== 3)
     {
		return MakeComparable (	m_signature->GetCorpusCount() , ((CSignatureListViewItem*) item)->GetSignature()->GetCorpusCount() );
     }
    if (col== 4)
     {
		return MakeComparable (	m_signature->GetNumberOfStems() , ((CSignatureListViewItem*) item)->GetSignature()->GetNumberOfStems() );
     }
     if (col== 6)
     {
		return MakeComparable (	((CSignatureListViewItem*) item)->GetSignature()->GetRobustness(), m_signature->GetRobustness()   );
     }
     else
     {
        return Q3ListViewItem::compare(item, col, asc);
     }
}
 

QString CSignatureListViewItem::text( int column ) const
{


  CSignatureListViewItem* child = NULL;

  int count;
  QString dummy;


  switch( column )
  {
  case 0:
    if( m_signature && m_parentlist->sortColumn() == 0 && m_signature->GetMentor() )
    {
      return "  : " + m_label;
    }
    else return m_label;
  case 1:
    if( m_signature && m_signature->GetNumberOfStems() > 0  )
    {
      if (m_signature->GetNumberOfStems() > 0 ) return m_signature->GetStem(0)->Display( QChar(0), m_filter );
    }
    else return "";
  case 2:
        if( m_signature ) return dummy.setNum( m_signature->ComputeDLofModel() );
	else return "";
   case 3:
    if( m_signature ) return dummy.setNum ( m_signature->GetCorpusCount() );
    else
    {
      count = 0;
      child = (CSignatureListViewItem*) firstChild();
      while( child )
      {
        if( child->GetSignature() )
        {
          count += child->GetSignature()->GetCorpusCount();
        }
        child = (CSignatureListViewItem*) child->nextSibling();
      }
      return dummy.setNum( count );
    }
  case 4:
    if( m_signature && m_signature->GetNumberOfStems() > 0 ) return dummy.setNum(  m_signature->GetNumberOfStems() );
    else
    {
      count = 0;
      child = (CSignatureListViewItem*) firstChild();
      while( child )
      {
        if( child->GetSignature() &&
          child->GetSignature()->GetNumberOfStems() > 0 )
        {
          count += child->GetSignature()->GetNumberOfStems();
        }
        child = (CSignatureListViewItem*) child->nextSibling();
      }
      return dummy.setNum( count );
    }
  case 5:
    if( m_signature ) return m_signature->GetRemark();
    else return "";
 
  case 6:
    if( m_signature ) return dummy.setNum( (int) m_signature->GetRobustness() );
 
    else
    {
      count = 0;
      child = (CSignatureListViewItem*) firstChild();
      while( child )
      {
        if( child->GetSignature() &&
          child->GetSignature()->GetNumberOfStems() > 0  )
        {
          count += child->GetSignature()->GetNumberOfStems();
        }
        child = (CSignatureListViewItem*) child->nextSibling();
      }
      return dummy.setNum(  count );
    }
  case 7:
    return "";
  default:
    return Q3ListViewItem::text( column );
  }
}

//===================================================================================================//
//
//          GUI stuff
//
//===================================================================================================//
void CSignature::BorrowedSigsDisplay(Q3ListView* List,
                QMap<QString, QString>* filter)
{
        QString source = "Unknown", dummy;
        for (int minino = 0; minino < m_pMyMini->GetMiniSize(); ++minino) {
                CMiniLexicon* mini = m_pMyMini->GetMiniLexicon(minino);
                if (mini == 0)
                        continue;

                CSignatureCollection& sigs = *mini->GetSignatures();
                if (sigs ^= this) {
                        // found!
                        source = dummy.setNum(minino + 1);
                        break;
                }
        }

        static_cast<void>(new Q3ListViewItem(
                List, Display('.', filter), source));
}

//===================================================================================================//
//
//          Constructor/destructor
//
//===================================================================================================//

CSignature::CSignature( CMiniLexicon* Lexicon ) : CLParse( Lexicon )
{
    m_pMyMini		= Lexicon;

    m_StemPtrList		= new QList<CStem*>();
    m_WordPtrList		= new QList<CStem*>();
    m_MentorList		= new QList<CSignature*>();
    m_SuffixPtrList		= new QList<CSuffix*>();
    m_PrefixPtrList		= new QList<CPrefix*>();
    m_SortStyle             = eAlphabetized;
       // Description Length
    m_DLofMyCorpus         = 0;
    m_DLofMyStemPointers   = 0;
    m_DLofMyAffixPointers  = 0;
    m_LengthOfPointerToMe  = 0;
    m_MyGeneralizer         = NULL;
    m_Remark		= "";
    m_Robustness		= 0;
    m_Mentor		= NULL;
  if( Lexicon ) m_AffixLocation = Lexicon->GetAffixLocation();
}


CSignature::CSignature( eAffixLocation AffixLocation, CMiniLexicon* Lexicon ) : CLParse( Lexicon )
{
    m_pMyMini		= Lexicon;
    m_StemPtrList		= new QList<CStem*>();
    m_WordPtrList		= new QList<CStem*>();
    m_MentorList		= new QList<CSignature*>();
    m_SuffixPtrList		= new QList<CSuffix*>();
    m_PrefixPtrList		= new QList<CPrefix*>();
    m_SortStyle                 = eAlphabetized;
    m_MyGeneralizer         = NULL;
    m_AffixLocation	        = AffixLocation;

    m_Remark		= "";
       // Description Length
    m_DLofMyCorpus         = 0;
    m_DLofMyStemPointers   = 0;
    m_DLofMyAffixPointers  = 0;
    m_LengthOfPointerToMe  = 0;
  if( Lexicon ) m_AffixLocation = Lexicon->GetAffixLocation();
}


CSignature::CSignature (const CParse& ParseSig, CMiniLexicon* Lexicon) : CLParse ( ParseSig, Lexicon )
{
    m_pMyMini                   = Lexicon;
    m_AffixLocation	        = Lexicon->GetAffixLocation(); 
    m_StemPtrList		= new QList<CStem*>();
    m_WordPtrList		= new QList<CStem*>();
    m_MentorList		= new QList<CSignature*>();
    m_SuffixPtrList		= new QList<CSuffix*>();
    m_PrefixPtrList		= new QList<CPrefix*>();
    m_SortStyle                 = eAlphabetized;
    m_Remark                    = "";
    m_MyGeneralizer             = NULL;
   // Description Length
    m_DLofMyCorpus              = 0;
    m_DLofMyStemPointers        = 0;
    m_DLofMyAffixPointers       = 0;
    m_LengthOfPointerToMe       = 0;
    if( Lexicon ) m_AffixLocation = Lexicon->GetAffixLocation();
}


CSignature::CSignature (const CParse* pParseSig, CMiniLexicon* Lexicon) : CLParse ( *pParseSig, Lexicon )
{
    m_pMyMini                   = Lexicon;

    m_StemPtrList		= new QList<CStem*>();
    m_WordPtrList		= new QList<CStem*>();
    m_MentorList		= new QList<CSignature*>();
    m_SuffixPtrList		= new QList<CSuffix*>();
    m_PrefixPtrList		= new QList<CPrefix*>();
    m_SortStyle                 = eAlphabetized;
    m_MyGeneralizer             = NULL;
    m_AffixLocation	        = Lexicon->GetAffixLocation();
    m_Remark                    = "";
   // Description Length
    m_DLofMyCorpus              = 0;
    m_DLofMyStemPointers        = 0;
    m_DLofMyAffixPointers       = 0;
    m_LengthOfPointerToMe       = 0;
if( Lexicon ) m_AffixLocation = Lexicon->GetAffixLocation();
}


CSignature::CSignature(const CSignature& Sig) : CLParse (Sig, Sig.GetLexicon())
{
    int     affixno,
            stemno;
            m_AffixLocation         = Sig.GetAffixLocation();
            m_Remark                = Sig.GetRemark();
            m_pMyMini               = Sig.GetLexicon();
            m_MyGeneralizer         = Sig.GetGeneralizer();

   int      NumberOfStems           = Sig.GetNumberOfStems();
   int      NumberOfAffixes         = Sig.Size();
   int      NumberOfWords           = NumberOfStems*NumberOfAffixes;
            QVector<double>         m_WordCounts (NumberOfAffixes * NumberOfStems );
            QVector<double>         m_StemCounts ( NumberOfStems );
            QVector<double>         m_AffixCounts( NumberOfAffixes );
            QVector<double>         m_WordFrequencies (NumberOfWords);
            QVector<double>         m_StemFrequencies (NumberOfStems);
            QVector<double>         m_AffixFrequencies (NumberOfAffixes);
            m_TotalCount            = Sig.GetTotalCount();

            m_StemPtrList                    = new QList<CStem*>();
            for ( stemno = 0; stemno < NumberOfStems; stemno++)
            {
                AppendStemPtr( Sig.GetStem(stemno));
                m_StemCounts[stemno]        = Sig.GetStemCount(stemno);
                m_StemFrequencies[stemno]   = Sig.GetStemFrequency(stemno);
            }
            if (m_AffixLocation == WORD_FINAL || m_AffixLocation == STEM_FINAL)            {
                m_SuffixPtrList          = new QList<CSuffix*>();
                for ( affixno = 0; affixno < NumberOfAffixes; affixno++)
                {
                    AppendSuffixPtr ( Sig.GetSuffix(affixno) );
                    m_AffixCounts[affixno]      = Sig.GetAffixCount(affixno);
                    m_AffixFrequencies[affixno] = Sig.GetAffixFrequency(affixno);
                }
            }
            if (m_AffixLocation == WORD_INITIAL || m_AffixLocation == STEM_INITIAL)            {
                m_PrefixPtrList          = new QList<CPrefix*>();
                for ( affixno = 0; affixno < NumberOfAffixes; affixno++)
                {
                    AppendPrefixPtr ( Sig.GetPrefix(affixno) );
                    m_AffixCounts[affixno]      = Sig.GetAffixCount(affixno);
                    m_AffixFrequencies[affixno] = Sig.GetAffixFrequency(affixno);
                }
            }


            m_WordPtrList = new QList<CStem*>();
            for (stemno = 0; stemno < NumberOfStems ; stemno++) {
                for (affixno = 0; affixno < NumberOfAffixes; affixno++)  {
                    SetWordCount(stemno, affixno, 0);
                    AppendWordPointer (Sig.GetWord(stemno, affixno));
                 }
             }

            m_Robustness                    = Sig.GetRobustness();
            m_Mentor                        = NULL;
            m_SortStyle                     = eAlphabetized;
            m_MentorList                    = new QList<CSignature*>();





}


CSignature::CSignature(const CStringSurrogate& ssSig, CMiniLexicon* Lexicon) : CLParse(ssSig, Lexicon)
{
    Collapse( ssSig, '.');
    m_pMyMini                   = Lexicon;

    m_StemPtrList		= new QList<CStem*>();
    m_WordPtrList		= new QList<CStem*>();
    m_MentorList		= new QList<CSignature*>();
    m_SuffixPtrList		= new QList<CSuffix*>();
    m_PrefixPtrList		= new QList<CPrefix*>();
    m_SortStyle                 = eAlphabetized;
    m_MyGeneralizer             = NULL;
   // Description Length
    m_DLofMyCorpus              = 0;
    m_DLofMyStemPointers        = 0;
    m_DLofMyAffixPointers       = 0;
    m_LengthOfPointerToMe       = 0;
    m_Remark                    = "";
    if( Lexicon ) m_AffixLocation = Lexicon->GetAffixLocation();
    m_Robustness                = 0;
    m_Mentor                    = NULL;

    m_SortStyle                 = eAlphabetized;
}


CSignature::~CSignature()
{

  if( m_StemPtrList   ) delete m_StemPtrList;
  if( m_WordPtrList   ) delete m_WordPtrList;
  if( m_MentorList    ) delete m_MentorList;
  if( m_SuffixPtrList ) delete m_SuffixPtrList;
  if( m_PrefixPtrList ) delete m_PrefixPtrList ;
}
//===================================================================================================//
//
//          Display
//
//===================================================================================================//
QString CSignature::Display(QChar sep, QMap<QString, QString>* filter) const
{
	QString sd = sep;
	if (sd == ".") {
		sd = m_pMyMini->GetDocument()->GetPreferences()
			->GetPreference("Sig_Delimiter");
		if (sd.size() != 1)
			sd = ".";
	}
	return CParse::Display(sd.at(0), filter);
}

QString CSignature::Display(QMap<QString, QString>* filter) const
{ return CParse::Display(filter); }

QString CSignature::Display() const
{ return CParse::Display('.'); }

//===================================================================================================//
//
//
//
//===================================================================================================//

void CSignature::ConsumeParse( CParse* pParse )
{
	ClearParse();
	SetKey( pParse );
	CopyParseStructure( *pParse );	
}


void CSignature::Suicide()
{
  //TODO: fill this in;
}
void CSignature::SetMyGeneralizer (CSignature* pSig)
{
	m_MyGeneralizer = pSig;
}
//===================================================================================================//
//
//          Operators
//
//===================================================================================================//
void CSignature::operator=(const CSignature* pSig)
{
  m_pMyMini = pSig->GetMyMini();
  CLParse::operator=(*pSig);
  m_AffixLocation = pSig->GetAffixLocation();

  int NumberOfStems = pSig->GetNumberOfStems();
  int NumberOfAffixes = pSig->GetNumberOfAffixes();
  int NumberOfWords = NumberOfStems*NumberOfAffixes;
  m_StemCounts.resize(NumberOfStems);
  m_WordCounts.resize(NumberOfWords);
  m_AffixCounts.resize(NumberOfAffixes);

  m_StemCounts.resize(NumberOfStems);
  for (int stemno = 0; stemno < pSig->GetNumberOfStems(); stemno++)  {
      m_StemPtrList->append ( pSig->GetStem(stemno)  );
      m_StemCounts[stemno]=pSig->GetStemCount(stemno);
      for (int affixno = 0; affixno < pSig->GetNumberOfAffixes(); affixno++)
      {
          m_WordPtrList->append ( pSig->GetWord(stemno, affixno));
          SetWordCount(stemno, affixno, pSig->GetWordCount(stemno, affixno));
      }
  }

  if (m_AffixLocation == WORD_FINAL || m_AffixLocation == STEM_FINAL ) {
    for (int suffixno = 0; suffixno < pSig->GetNumberOfAffixes(); suffixno++)
    {
        m_SuffixPtrList->append ( pSig->GetSuffix(suffixno)  );
        m_AffixCounts[suffixno] = pSig->GetAffixCount(suffixno);
    }
  } else  {
    for (int prefixno = 0; prefixno < GetNumberOfAffixes(); prefixno++)  {
         m_PrefixPtrList->append(pSig->GetPrefix(prefixno) );
         m_AffixCounts[prefixno] = pSig->GetAffixCount(prefixno);
    }
}



  m_Robustness = pSig->GetRobustness();
  m_Mentor      = NULL;
  m_Remark = pSig->GetRemark();
}


QTextStream& operator<< (QTextStream& stream, CSignature* pSig)
{
  CStem*      pStem;

  stream << endl << pSig->Display();
  stream.width(6);
  stream << pSig -> GetNumberOfStems() << "  " <<  pSig->GetCorpusCount();

  for (int stemno = 0; stemno < pSig->GetNumberOfStems(); stemno++)
  {
    pStem = pSig->GetStem(stemno);
    if ( pStem->GetKey() != CStringSurrogate() )
    {
      stream << endl;
      stream.width(20);
      stream << pStem->GetKey().Display();
    } else
    {
      stream << endl;
      stream.width(20);
      stream << "???";
    }
  }

  return stream;

}
//   <<-------------------------------------------------------------------------------------------------------->>
void  CSignature::operator<< (CStem* pStem) //add to tail of list.
{

  CStem* pWord;

  if ( m_StemPtrList->indexOf ( pStem ) < 0 )
  {
    Q_ASSERT (pStem->GetKeyLength() > 0);
    m_StemPtrList->append(pStem);
  }

  Q_ASSERT ( m_PieceCount  <= m_LengthOfPieceVector  )  ;

  for (int wordno = 0; wordno < pStem->GetWordPtrList()->size(); wordno++)
  {
    pWord = pStem->GetWord(wordno);
    Q_ASSERT (pWord->GetKeyLength() > 0);
    m_WordPtrList->append (pWord);
  }
  pStem->SetSuffixSignature (this);

  m_Robustness = 0;
  m_Robustness = GetRobustness();
}

//===================================================================================================//
//
//          Accessors and setters
//
//===================================================================================================//
CSignature*    CSignature::GetMentor  ( )            { return m_Mentor; }
//   <<-------------------------------------------------------------------------------------------------------->>
void      CSignature::SetMentor  ( CSignature* pSig )
{
  m_Mentor = pSig;
  if( pSig && pSig->GetMentorList() && pSig->GetMentorList()->indexOf (this) < 0)  { 
    pSig->GetMentorList()->append( this );
  }
}


int                     CSignature::GetNumberOfAffixes()    const
{

    if ( m_AffixLocation == STEM_FINAL || m_AffixLocation == WORD_FINAL)
    {
        return m_SuffixPtrList->count();
    }
    if ( m_AffixLocation == STEM_INITIAL || m_AffixLocation == WORD_INITIAL)
    {
         return m_PrefixPtrList->count();
    }
    return 0;
}


void                    CSignature::AppendSuffixPtr (CSuffix* pSuffix) { m_SuffixPtrList->append(pSuffix);}
QList<CSignature*>*     CSignature::GetMentorList( )              { return m_MentorList; }
int                     CSignature::GetNumberOfStems()      const { return m_StemPtrList->count(); }
//int                   CSignature::GetNumberOfSuffixes ()  const { return m_SuffixPtrList->count();      }
void                    CSignature::SetRemark ( QString remark)   { m_Remark = remark; }
CPrefix*                CSignature::GetPrefix(int prefixno) const { return m_PrefixPtrList->at(prefixno); }
QList<CPrefix*>*        CSignature::GetPrefixPtrList()      const { return m_PrefixPtrList; }
QString                 CSignature::GetRemark()             const { return m_Remark; }
QList<CStem*>*          CSignature::GetStemPtrList()        const { return m_StemPtrList;}
CStem*                  CSignature::GetStem(int stemno)     const { return m_StemPtrList->at(stemno);     }
CSuffix*                CSignature::GetSuffix(int suffixno) const { return m_SuffixPtrList->at(suffixno); }
QList<CSuffix*>*        CSignature::GetSuffixPtrList()      const { return m_SuffixPtrList; }
int                     CSignature::GetTotalCount()         const { return m_TotalCount; }
double                  CSignature::GetCorpusCount()        const { return corpus_count::GetCorpusCount();}
float                   CSignature::GetSortingQuantity()    const { return (float) GetRobustness();} 

bool                    CSignature::StemListContains(CStem* pstem)    { return m_StemPtrList->contains(pstem); }
void                    CSignature::AppendStemPtr(CStem* pStem) const { m_StemPtrList->append(pStem);}


eAffixLocation CSignature::GetAffixLocation() const { return m_AffixLocation; }
//   <<-------------------------------------------------------------------------------------------------------->>
CStem*                  CSignature::GetWord(int stemno, int affixno)     const
{
    if (stemno < 0 || affixno <  0  || stemno >= GetNumberOfStems() || affixno >= GetNumberOfAffixes())
        return NULL;
    if (stemno * GetNumberOfAffixes() + affixno >= m_WordPtrList->size() )
        return NULL;
    return m_WordPtrList->at(stemno* GetNumberOfAffixes() + affixno);
}
CParse  CSignature::GetStems()
{
  CParse  List;
  List.Alphabetize();
  if ( m_StemPtrList->count() == 0 ) { return List; } // **********  This is clearly a mistake. Fix it.

  for (int stemno = 0; stemno < m_StemPtrList->size(); stemno++)
  {
    List.Append( GetStem(stemno)->GetKey() );
  }
  return List;
}
//   <<-------------------------------------------------------------------------------------------------------->>








//   <<-------------------------------------------------------------------------------------------------------->>
double CSignature::GetStemFrequency(int stemno ) const {
    if (stemno < 0 || stemno > GetNumberOfStems()  ) return 0;
    return m_StemFrequencies[stemno];
}

//   <<-------------------------------------------------------------------------------------------------------->>
double CSignature::GetAffixFrequency(int affixno ) const {
    if (affixno < 0 || affixno > GetNumberOfAffixes()  ) {return 0; }
    return m_AffixFrequencies[affixno];
}
//   <<-------------------------------------------------------------------------------------------------------->>
double CSignature::GetStemCount(int stemno) const {
    if (stemno < 0 || stemno > GetNumberOfStems()  ){ return 0; }
    return m_StemCounts[stemno];
}
//   <<-------------------------------------------------------------------------------------------------------->>

double CSignature::GetAffixCount(int affixno) const
{   if (affixno < 0 || affixno > GetNumberOfAffixes()   ) return 0;
    return m_AffixCounts[affixno];
}
//   <<-------------------------------------------------------------------------------------------------------->>
double  CSignature::GetWordCount(int wordno)const {
    if (wordno < 0  || wordno > GetNumberOfWords() ) { return 0;}
    return m_WordCounts[wordno]; }
//   <<-------------------------------------------------------------------------------------------------------->>

//===================================================================================================//
//
//          Calculate frequencies and counts
//
//===================================================================================================//
void CSignature::CalculateFrequencies(CMiniLexicon* Lexicon)
{
  CStringSurrogate  Suffix;
  CSuffix*          pSuffix;
  CStem*            pStem;
  CCorpusWord*      pCorpusWord;
  Q_ASSERT( GetCorpusCount() > 0);
  int               TotalCorpusCount = 0;
  int*              SuffixCount = new int [ Size()+ 1 ];
  for (int suffixno = 1; suffixno <= Size(); ++suffixno)
  { SuffixCount[suffixno] = 0; }

  for (int suffixno  = 1; suffixno <= Size(); suffixno++)
  {
        Suffix      = GetPiece(suffixno);
        pSuffix      = new CSuffix(Suffix);

        for (int stemno= 0; stemno < GetNumberOfStems();  stemno++)
        {
            pStem = GetStem(stemno);
            pCorpusWord        = Lexicon->FindAWord (pStem, pSuffix);
            if( pCorpusWord )                // might not exist if we have collapsed signatures.
            {
                TotalCorpusCount  += pCorpusWord->GetCorpusCount();
                SuffixCount[suffixno]    += pCorpusWord->GetCorpusCount();
            }
        }
  }

  delete [] SuffixCount;
}

//   <<-------------------------------------------------------------------------------------------------------->>
void CSignature::ListDisplay(Q3ListView* List,
	QMap<QString, QString>* filter, bool ExpressDeletees)
{
	CSignature sig(m_pMyMini);
	Express(sig, ExpressDeletees);
	QString text = sig.Display('.', filter);

	static_cast<void>(new CSignatureListViewItem(
		List, text, m_pMyMini->GetIndex(), this, filter));
}

//   <<-------------------------------------------------------------------------------------------------------->>
void    CSignature::FindCorpusCount( )
{
    SetCorpusCount ( 0 );
    for (int stemno =0; stemno < GetNumberOfStems(); stemno++)   {
        for (int affixno = 0; affixno < GetNumberOfAffixes(); affixno ++)
            IncrementCorpusCount ( GetWord(stemno, affixno)->GetCorpusCount() );
    }
}
//   <<-------------------------------------------------------------------------------------------------------->>
void  CSignature::AttachToSuffixSig(CStem* pStem, bool bLookAtPreviousSig) //add to tail of list.
{
        int             stemno;
        int             numberofaffixes = GetNumberOfAffixes();
        CStem*          pWord;
        CSignature*     pOldSig = pStem->GetSuffixSignature();
        QString         stem = pStem->Display();

	/*  First, remove pStem from any other SuffixSignature it might be linked to.*/
        if ( pOldSig && pOldSig != this ) 	{
		pOldSig->DetachStem( pStem, eDo_Not_Call_Words );
                pOldSig->RecalculateStemAndWordPointers();
	}
    
         stemno = m_StemPtrList->indexOf ( pStem );
        if( stemno <  0 ) 	{
		m_StemPtrList->append( pStem );
                stemno = GetNumberOfStems()-1;
	}

        switch( m_AffixLocation){
            case (WORD_FINAL):
            case (STEM_FINAL):
            for (int affixno = 0; affixno < numberofaffixes; affixno++)
            {
                pWord = GetLexicon()->GetWordFromStemSuffix(pStem, GetSuffix(affixno));
                if (pWord)
                {
                    AppendWordPointer( pWord);
                    pWord->SetSuffixSignature (this);
                }
                else
                {
                    AppendWordPointer(NULL);
                }
            }
            break;
            case (WORD_INITIAL):
            case (STEM_INITIAL):
            for (int prefixno = 0; prefixno < numberofaffixes; prefixno++)
            {
                pWord = GetLexicon()->GetWordFromStemPrefix(pStem, GetPrefix(prefixno));
                if (pWord)
                {
                    AppendWordPointer( pWord);
                    pWord->SetPrefixSignature (this);
                }
                else
                {
                    AppendWordPointer(NULL);
                }
            }
            break;
        }

	pStem->SetSuffixSignature( this );
	IncrementCorpusCount( pStem->GetCorpusCount()-1 );// first time CC is incremented
		
	m_Robustness = 0;
	m_Robustness = GetRobustness();
}
//   <<-------------------------------------------------------------------------------------------------------->>
void  CSignature::AttachToPrefixSig( CStem* pStem, bool bLookAtPreviousSig ) //add to tail of list.
{
	CStem*        pWord;
	CSignature*   pOldSig = pStem->GetPrefixSignature();
	
	/*  First, remove pStem from any other PrefixSignature it might be linked to.*/
	if ( pOldSig && pOldSig != this )	{
		pOldSig->DetachStem( pStem, eDo_Not_Call_Words );
                RecalculateStemAndWordPointers();
	}

        if( m_StemPtrList->indexOf ( pStem ) < 0 )	{
                AppendStemPtr(  pStem );
	}

	// move the Words from the old signature to this, the new one.

        for (int wordno = 0; wordno < pStem->GetNumberOfWords(); wordno++)	{
            pWord = pStem->GetWord(wordno);
            m_WordPtrList->append (pWord);
            pWord->SetPrefixSignature (this);
	

	}

	pStem->SetPrefixSignature( this );
        IncrementCorpusCount( pStem->GetCorpusCount()-1 );
        m_Robustness = GetRobustness();
}

//   <<-------------------------------------------------------------------------------------------------------->>
double CSignature::GetRobustness() const
{
  int   SuffixLetters = 0,
        StemLetters = 0;

  if (m_Robustness == 0)
  {
    SuffixLetters = GetKeyLength();
    QString Null = "NULL";
    if ( Contains( CStringSurrogate(Null.unicode(),0,Null.length()) ) ) { SuffixLetters -= 4; }

    CStem* pStem;
    for (int stemno = 0; stemno < GetNumberOfStems(); stemno++)    {
      pStem = GetStem(stemno);
      StemLetters += pStem->GetKeyLength();
    }

    m_Robustness = ( Size() - 1 ) * StemLetters  + (GetNumberOfStems() - 1) * SuffixLetters;

    }
  return m_Robustness;
}
//   <<-------------------------------------------------------------------------------------------------------->>
void CSignature::SetRobustness ( double R ) { m_Robustness = R; }
//   <<-------------------------------------------------------------------------------------------------------->>

// the counts of each individual word analyzed by this signature.
//double*  CSignature::GetWordCounts() const {  return m_WordCounts;
//}
//   <<-------------------------------------------------------------------------------------------------------->>
double CSignature::GetWordCount(int stemno, int affixno) const
{
    if ( stemno < 0 || affixno < 0 || stemno >= GetNumberOfStems() || affixno >= GetNumberOfAffixes() ) return 0;
    return m_WordCounts[stemno * GetNumberOfStems() + affixno];
}
//   <<-------------------------------------------------------------------------------------------------------->>
void CSignature::SetWordCount (int stemno, int affixno, double value)
{
    if ( stemno < 0 || affixno < 0 || stemno >= GetNumberOfStems() || affixno >= GetNumberOfAffixes() )
        return;
   m_WordCounts[stemno * GetNumberOfAffixes() + affixno] = value;
    return;

}

//   <<-------------------------------------------------------------------------------------------------------->>

void CSignature::CalculateWordCounts()
{   QString string;

    int numberofstems = GetNumberOfStems();
    int numberofaffixes = GetNumberOfAffixes();
    int count = 0;

    CStem* pWord;

     m_WordCounts.clear();
     m_WordCounts.resize(numberofstems*numberofaffixes);
     m_StemCounts.clear();
     m_StemCounts.resize(numberofstems);
     m_AffixCounts.clear();
     m_AffixCounts.resize(numberofaffixes);
     m_TotalCount = 0;
     for (int affixno = 0; affixno < numberofaffixes; affixno++)    { m_AffixCounts[affixno] = 0; }
     for (int stemno = 0; stemno < numberofstems; stemno++)         { m_StemCounts[stemno]   = 0; }



     for (int  stemno = 0; stemno < numberofstems; stemno++)
     {
            for ( int affixno = 0; affixno < numberofaffixes; affixno++)
            {       
                pWord = GetWord(stemno, affixno);             
                count = pWord->GetCorpusCount();
//                SetWordCount (stemno, affixno, count);
//                m_StemCounts[stemno] = m_StemCounts[stemno] + count;
  //              m_AffixCounts[affixno] = m_AffixCounts[affixno] + count;
    //            m_TotalCount += count;
            }
     }
/*
     if (m_TotalCount <= 0) return;

     m_WordFrequencies.resize(numberofstems*numberofaffixes);
     m_StemFrequencies.resize(numberofstems);
     m_AffixFrequencies.resize(numberofaffixes);

     for ( int stemno = 0; stemno < numberofstems; stemno++)
     {
            m_StemFrequencies[stemno] = m_StemCounts[stemno]/m_TotalCount;
            for ( affixno = 0; affixno < numberofaffixes; affixno++)
            {
                wordno = stemno * numberofaffixes + affixno;
                m_WordFrequencies[wordno] = GetWordCount(stemno, affixno) /  m_TotalCount;
            }
      }

     for (int affixno = 0; affixno < numberofaffixes; affixno++){
           m_AffixFrequencies[affixno] = m_AffixCounts[affixno] / m_TotalCount;
     }
     */
}


//=================================================================================================/
//
// TODO: make sure COST function is consistent with older versions and working right
                    double CSignature::FindCost(CMiniLexicon* Lexicon)
//
//=================================================================================================/


/*
Cost of a sig =

  Sum over all of its stems :

      log ( CorpusSize / Stem-count )      ( cost    )
      length ( stem ) * cost of a letter    ( savings )

  Sum over all of its suffixes:

      log ( CorpusSize / suffix-count )      ( cost    )
      length ( suffix ) * cost of a letter    ( savings )
*/
{
  CStem*      pStem;
  double      Cost          = 0,
              AffixCost     = 0,
              AffixSavings  = 0,
              SignatureCost = 0,
              StemCost      = 0,
              StemSavings   = 0,
              CostOfALetter = base2log (26),
              ThisAffixCost = 0,
              NumberOfWords = Lexicon->GetWords()->GetCount();
  CAffix*     pAffix;


  for (int  affixno = 1; affixno <= Size(); affixno++)
  {
    if( m_AffixLocation == WORD_FINAL || m_AffixLocation == STEM_FINAL ) 
		{
                        pAffix = *Lexicon->GetSuffixes() ^= GetPiece(affixno);
		}
		else
		{
                  pAffix = *Lexicon->GetPrefixes() ^= GetPiece(affixno);
		}

    if ( pAffix ) // it already exists
    {
      ThisAffixCost  =  base2log ( NumberOfWords / pAffix->GetUseCount() );
    }
    else
    {
      ThisAffixCost = base2log ( NumberOfWords/GetNumberOfStems() );
      ThisAffixCost += GetPiece(affixno).GetLength() * CostOfALetter;
    }
    AffixCost += ThisAffixCost;

    AffixSavings +=  GetPiece(affixno).GetLength() * CostOfALetter;

    SignatureCost += ThisAffixCost;
  }


  for (int stemno = 0; stemno < m_StemPtrList->size(); stemno++)
    {
       pStem = m_StemPtrList->at(stemno);
        StemCost += base2log ( NumberOfWords / Size() ); // Size is the number of words that use stem, of course.
        StemCost += pStem->GetKeyLength() * CostOfALetter;
        StemSavings += pStem->GetKeyLength() * CostOfALetter * Size(); // save for each time stem appears, with each suffix
        SignatureCost += StemCost;
  }

  Cost = AffixCost + StemCost - AffixSavings - StemSavings + SignatureCost;

  return Cost;
}


//   <<-------------------------------------------------------------------------------------------------------->>

void  CSignature::OutputSignature( QTextStream& outf )
{


                            QString				string;
                            CStem*				pStem;


                            outf << " ------------------------------------------------------------------------------------------ " << endl;
                            outf << Display( '.', m_pMyMini->GetOutFilter() );
                            outf << endl << " ------------------------------------------------------------------------------------------ " << endl;

                            outf << endl;
                            outf << "  ";



                            outf << "Number of stems: ";
                            outf << QString("%1").arg( (int) GetNumberOfStems() );

                            outf << "  Corpus count: ";
                            outf << QString("%1").arg( GetCorpusCount() );
                            outf << " ";                            

                            outf << "  ";
                            outf << GetRemark().replace( QChar(' '), "_" );
                            outf << " ";                            

                            outf << "Number of affixes: ";
                            outf << GetNumberOfAffixes();
                            outf << " Word Pointer List length: ";
                            outf << m_WordPtrList->count();
                            outf << endl;


                            QStringList stems;

                           CalculateWordCounts();
                            int     maxlength = 0;
                            CStem*  pWord;

                            outf.setFieldAlignment( QTextStream::AlignLeft );
                            QList< QPair<CStem*, int> > pstems;
                            for (int stemno =0; stemno< GetNumberOfStems(); stemno++ )
                            {
                                pStem = GetStem(stemno);
                                pstems.append( qMakePair(pStem, pStem->GetCorpusCount()  ) );
                                if (pStem->GetKeyLength() > maxlength) { maxlength = pStem->GetKeyLength();}
                            }
                            qSort(pstems.begin(), pstems.end(), stemlessthan);

                            outf << "Sorted by stem frequency: " << endl << endl;
                            outf << "# Rank      | Stem                 | Words ....      " << endl;
                            outf << "# ------------------------------------------------------------------------------------------ " << endl;


                            for (int stemno = 0; stemno < GetNumberOfStems(); stemno++)
                            {
                                outf.setf(2);
                                outf.width(5);
                                pStem = pstems[stemno].first;                             
                                outf. width(6);
                                outf << stemno;
                                outf. width( maxlength + 5);
                                outf <<  pStem->Display();
                                outf.width (9);
                                outf << pstems[stemno].second;
                                outf << endl;
                            }
                            outf << endl << "# ------------------------------------------------------------------------------------------ " << endl;
                            outf << endl << endl <<"Display all words with counts: " << endl;
                            outf << "# ------------------------------------------------------------------------------------------ " << endl;

                            for (int stemno = 0; stemno < GetNumberOfStems(); stemno++)
                            {
                                for ( int affixno = 0; affixno < GetNumberOfAffixes(); affixno++)
                                {
                                    pWord = GetWord(stemno, affixno );
                                    if (pWord)
                                    {
                                        outf.setFieldWidth (maxlength + 5); outf << pWord->Display();
                                        outf.setFieldWidth (5) ;            outf << string.setNum( pWord->GetCorpusCount() );
                                    }
                                }
                                outf << endl;
                            }
                            outf << endl << endl;
}




/*    This purpose of this function is to take a signature of the form A.SUFFIX
    and make it NULL.SUFFIX (the pAlternateSig), and move that letter A back onto its stems.
*/

//   <<-------------------------------------------------------------------------------------------------------->>
void CSignature::RemoveLetter (CStringSurrogate& ssLetter, CMiniLexicon* Lexicon, CSignature* pAlternateSig)
{

  CStem* qStem;
  CSuffix*      pSuffix,
                *pNewSuffix = NULL;
  QString        Stem,
                Suffix,
                Null = "NULL";
  QString        OldKey = Display();
  CStringSurrogate  ssSuffix,
                ssStem;
  CStem*        pWord;
  CSignature    NewSig ( WORD_FINAL, Lexicon );
  int           LetterLength = ssLetter.GetLength();

  CSignature   *qSig = NULL,
                *pOlderSig = NULL;
  CParse        PSuffix,
                PWord,
                PNewStem;

  QMap<QString,CSuffix*>  SuffixPtrTranslation;

  /*    Create the NewSig    */
  for (int affixno = 1; affixno <= Size(); affixno++)
  {
    ssSuffix = GetPiece(affixno);
    if(!NewSig.GetSortStyle()== eAlphabetized) NewSig.Alphabetize();
    if ( ssSuffix == ssLetter )
    {
      if(!NewSig.GetSortStyle()==eAlphabetized) NewSig.Alphabetize();
      NewSig.Append ( CStringSurrogate(Null.unicode(),0,Null.length()) );
    }
    else
    {
      QString lt_brak = "<", rt_brak = ">";

      PSuffix = CStringSurrogate(lt_brak.unicode(),0,1);
      PSuffix += ssLetter;
      PSuffix += CStringSurrogate(rt_brak.unicode(),0,1);
      PSuffix += ssSuffix;

      pSuffix = *Lexicon->GetSuffixes() << PSuffix;

      Suffix  = "<" + ssLetter.Display() + ">" + ssSuffix.Display();
      SuffixPtrTranslation[ ssSuffix.Display() ] = pSuffix; // based on old suffix
//      SuffixStringTranslation[ ssSuffix.Display() ] = Suffix;

      NewSig.Append ( PSuffix.GetKey() );
    }
  }

  /*    Change the KEY of this signature    */

  SetKey ( NewSig );
  QString remark = GetRemark() + " +allomorphy";
  SetRemark ( remark );

  //-----------------------------------------------------------//
  // Change the signature, the stems, the words -- and the suffixes.
    //-----------------------------------//
  /*  Deal with the stems         */
  //-----------------------------------//


  for (int stemno = 0; stemno < m_StemPtrList->size(); stemno++)
  {
    CStem* pStem = m_StemPtrList->at(stemno);
    ssStem   = pStem->GetKey();
    PNewStem = ssStem + ssLetter;
    qStem    = *Lexicon->GetStems() ^= PNewStem;

    if (qStem)    // -- if the larger one already existed
    {
      pOlderSig = *Lexicon->GetSignatures() ^= qStem->GetSuffixList();

      // this removes both stem and word from signature:
      pOlderSig ->  DetachStem ( qStem, eCall_Words ); // we might want to eliminate this sig if it has no more stems

      qStem    ->  GetSuffixList()->MergeAndAlphabetizeParse( CParse(NewSig) );

      qSig    =    *Lexicon->GetSignatures() << qStem->GetSuffixList();

      // attaches both stems and words to qSig
      qSig    ->  AttachToSuffixSig(qStem, false);

    }
    else // make the old stem into this new one
    {
      pStem   -> RepairSuffixList ( Lexicon );
      Lexicon   ->  GetStems()-> SetKey( pStem, PNewStem );
      pStem   ->  SetKey( PNewStem );

    }
  }
  Q_ASSERT(m_StemPtrList->size() > 0);
  CStem* pStem = m_StemPtrList->at(m_StemPtrList->size() - 1);

  //---------------------------------------------//
  /*  Deal with the WORDs of this signature     */
  //---------------------------------------------//

  for (int wordno = 0; wordno < m_WordPtrList->size(); wordno++)
  {
    pWord = m_WordPtrList->at(wordno);
    pNewSuffix = SuffixPtrTranslation[ pWord->GetSuffix().Display() ];
    pWord -> ShiftStemSuffixBoundary ( LetterLength );

    pWord -> SetSuffixPtr ( pNewSuffix );
    pWord -> AttachWordAndSuffixalStem   ( pStem );
    pWord -> SetSuffixSignature ( this );
    }


  //------------------------------------------------------------//
  //    Alternate Sig
  //------------------------------------------------------------//
  /*  Shift stems from AlternateSig to the NewSig, but NOT
    if the stem ends with Letter; if it does, we'll
    keep the old signature with that stem.


      This will replace some or all of pAlternateSig --
    "some" when there are any stems that don't allow removal of the Letter.
    For example, NULL.ing will not disappear when <e>ing.NULL is created,
    because the stem "be"  still requires NULL.ing --
   */

  // Deal with stems in AlternateSig....

  for (int stemno = 0; stemno < pAlternateSig->GetNumberOfStems(); stemno++)
  {
    pStem = pAlternateSig->GetStem(stemno);
    ssStem    = pStem->GetKey();
    if ( ssStem.Right(LetterLength) == ssLetter )
    { continue; }

    pAlternateSig->DetachStem( pStem, eCall_Words );
    AttachToSuffixSig( pStem, false );
  }
  // Deal with Words in Alternate signature

  for (int stemno = 0; stemno < pAlternateSig->GetNumberOfStems(); stemno++)
  {
    pWord = pAlternateSig->GetStem(stemno);
    pNewSuffix = SuffixPtrTranslation[ pWord->GetSuffix().Display() ];

    pWord -> SetSuffixPtr ( pNewSuffix );
    pWord -> AttachWordAndSuffixalStem   ( pStem );
    pWord ->SetSuffixSignature ( this );
  }

  //------------------------------------------------------------//

  /*    Get rid of the Alternate Sig ("NULL.ing" )  */

  if  ( pAlternateSig->GetNumberOfStems() == 0 )
  {
    Lexicon->GetSignatures()->Remove( pAlternateSig );
  }


}
//   <<------------------------------------------------------------------------>>
bool CSignature::EachSuffixCanHaveThisLetterPrefixedToIt ( const QString& Letter)
{
  QString    Suffix;
  for (int affixno = 1; affixno <= Size(); ++affixno) {
    Suffix = GetPiece(affixno).Display();
    if (Suffix == "NULL" ) { Suffix = ""; }
    Suffix = Letter + Suffix;
    if(0)// TODO:    if ( ! (*Lexicon->GetSuffixes() ^= Suffix )   )
    {
      return FALSE;
    }
  }

  return TRUE;
}
//   <<------------------------------------------------------------------------>>
void CSignature::ShiftStemSuffixCutToTheLeft(int Distance,
	const QString& Piece)
{
	struct not_implemented { };
	throw not_implemented();

	// XXX. suppresses “unused parameter” warnings
	static_cast<void>(Distance);
	static_cast<void>(Piece);

	foreach (CStem* word, *m_WordPtrList) {
		word->ShiftStemSuffixBoundary(-1);
		Q_ASSERT(word->GetStemLoc() != 0);
	}

	foreach (CStem* stem, *m_StemPtrList) {
		CStringSurrogate stem_text = stem->GetKey();
		stem->ClearParse();
		stem->SetKey(stem_text.Left(stem_text.GetLength() - 1));

		// XXX. Check to see if the new stem already exists.
//		Lexicon->GetStems()->GetHash()->RemoveKey ( Stem );
//		Lexicon->GetStems()->GetHash()->SetAt( NewStem, pStem );
//		Lexicon->GetStems()->SetKey( pStem, NewStem  );
	}

	// XXX. fix the signature
//	AddLetter ( 1, Piece );

//	Lexicon->AddToScreen ( Display() );
}

// Variant in which the shifted string varies from stem to stem.
void CSignature::ShiftStemSuffixCutToTheLeft(int Distance)
{
	// XXX. suppresses “unused parameter” warning
	static_cast<void>(Distance);
	struct not_implemented { };
	throw not_implemented();

	// first, fix the words;
	foreach (CStem* word, *m_WordPtrList) {
		word->ShiftStemSuffixBoundary(-1);
		Q_ASSERT(word->GetStemLoc() != 0);
	}

	// XXX. fix the signature
//	AddLetter ( 1, Piece );

//	Lexicon->AddToScreen ( Display() );
}

void CSignature::AddLetter(const QString& Letter )
{
  PrefixToAllPieces ( CStringSurrogate(Letter.unicode(),0,Letter.length() ) );

}

/// Looks at the final ngrams of the stems, and calculates its entropy
double CSignature::ComputeFinalNgramEntropyOfStems(int n)
{
	TCollection<CLParse> Ngrams;
	foreach (CStem* pStem, *m_StemPtrList) {
		if (pStem->GetKeyLength() <= n)
			// too short
			return -1;

		CStringSurrogate ssPiece = pStem->GetKey();
		ssPiece = is_initial(GetAffixLocation()) ?
			ssPiece.Left(n) : ssPiece.Right(n);
		Ngrams << ssPiece;
	}

	double Entropy = 0.0;
        const double StemCount = GetNumberOfStems();
	const int ngram_count = Ngrams.GetCount();
	for (int i = 0; i < ngram_count; ++i) {
		const double fraction = StemCount / Ngrams[i]->GetCorpusCount();
		Entropy += log2(fraction) / fraction;
	}

	return Entropy;
}
//===================================================================================================//
//
//          CHECK OUT: major function
//
//===================================================================================================//
/// Test to see whether the break with its stems is a good one.
int CSignature::CheckOut(CMiniLexicon* Lexicon)
{
	using linguistica::implicit_cast;
	// Throughout, “DL” stands for “description length”.
        Lexicon->LogFileSmallTitle("Empirical signature: "+ Display() );
	if (Lexicon->LogFileOn()) {
		// dump stem list to log file.
                Lexicon->LogFileStartTable();
                Lexicon->LogFileStartRow();
		const int num_columns = 5;

		// For each stem:
		CParse Stems = GetStems();
                for (int stemno = 1; stemno <= GetNumberOfStems(); ++stemno) {
                    if (stemno % num_columns == 0) {
                                Lexicon->LogFileEndRow(); Lexicon->LogFileStartRow();
                            }
                    Lexicon->LogFileSimpleString( Stems[stemno].Display()); //JG June 2010
		}
                Lexicon->LogFileEndRow(); Lexicon->LogFileEndTable();
        } // end of logfile on
        Lexicon->LogFileHeader("Number of letters","Entropy", "Resolution?" );
	bool LowEntropyFlag = false;
	int LargestSizeChunkToPullOffStem = 0;
	// Use entropy to see how many letters to consider shifting
	// XXX. Make this user-changeable.
	const double EntropyThreshold = 1.5;
	const int LengthToConsiderShifting = 4;
	for (int n = 1; n <= LengthToConsiderShifting; ++n) {
		const double Entropy = ComputeFinalNgramEntropyOfStems(n);

		if (Entropy < 0) {
			// Negative entropy:
			// stem too short to consider shortening.
                        Lexicon->LogFile("", "", "No reanalysis");
			continue;
		}

		if (Entropy >= EntropyThreshold) {
                    Lexicon->LogFileSimpleString ("");
                    Lexicon->LogFileSimpleDouble(Entropy);
                    Lexicon->LogFileSimpleString("Entropy too large.");
                    break;
		}

		// set of n-suffixes of stems has low entropy:
		// maybe stems have a common suffix that should be
		// incorporated into the signature.
		LowEntropyFlag = true;
		LargestSizeChunkToPullOffStem = n;
                Lexicon->LogFile(n, Entropy, "Entropy sufficiently small.");
        } //end of loop on n
        Lexicon->LogFileEndTable();
	if (!LowEntropyFlag)
		// Not enough stems share common endings to restructure,
		// so leave this signature alone.
		return 0;

	const bool analyzingSuffixes = !is_initial(GetAffixLocation());

        const double TotalNumberOfAnalyzedWords =
		Lexicon->GetSignatures()->GetTotalNumberOfWords();
	const double LogTotalNumberOfAnalyzedWords =
		base2log(TotalNumberOfAnalyzedWords);
	const double LengthOfPointerToThisSig =
		LogTotalNumberOfAnalyzedWords -
		base2log(Size() * GetNumberOfStems());

	// Description length of the original analysis
	double CurrentDL;
	{
		// DL of this signature:
		//
		// a. Length of pointers to its suffixes;								var: LengthOfPointersToAllAffixesOfSig
		// b. Prorated responsibility for phonological content of suffixes
		//	var: TotalResponsibilityForAffixListings
		// c. List of pointers from each stem to this signature
		//	var: StemPointersToThisSig;
		// d. List of pointers from each word to its suffix

		// Compute DL of 'original' analysis.
                Lexicon->LogFileSmallTitle ("Description length of current signature");
                Lexicon->LogFileHeader("Affix", "Use count", "Pointer to this affix");	 ;

		double LengthOfPointersToAllAffixesOfSig = 0.0;
		double TotalResponsibilityForAffixListings = 0.0;
		// for each suffix (resp. prefix) in this signature:
                for (int affixno = 1; affixno <= Size(); ++affixno) {
                        QString Affix = GetPiece(affixno).Display();
			CAffix* pAffix = analyzingSuffixes
				? implicit_cast<CAffix*>(
					*Lexicon->GetSuffixes() ^= Affix)
				: implicit_cast<CAffix*>(
					*Lexicon->GetPrefixes() ^= Affix);
		
			// Length of pointers to affixes
			//  part a
			const double LengthOfPointerToThisAffix =
				LogTotalNumberOfAnalyzedWords -
				base2log(pAffix->GetUseCount());
			LengthOfPointersToAllAffixesOfSig +=
				LengthOfPointerToThisAffix;
			
                        Lexicon->LogFile(Affix, pAffix->GetUseCount(), LengthOfPointerToThisAffix);

			// use count of affix; length of pointer to this affix.
			// Assign partial responsibility for this signature's
			// suffixes' entries.
			//  part b.
			const double LocalProportion =
				double(GetNumberOfStems()) / pAffix->GetUseCount();
			const double ResponsibilityForThisAffixListing =
				LocalProportion * Affix.length() * base2log(26);
			TotalResponsibilityForAffixListings +=
				ResponsibilityForThisAffixListing; // in *bits*
                }// end of affixno loop

                Lexicon->LogFileEndTable();
                Lexicon->LogFileStartTable();
                Lexicon->LogFile("Part 1: Length of pointer to affixes", LengthOfPointersToAllAffixesOfSig);
                Lexicon->LogFile("Part 2: Prorated responsibility for phonology of affixes:", TotalResponsibilityForAffixListings);

		// part c.
		const double StemPointersToThisSig =
			GetNumberOfStems() * LengthOfPointerToThisSig;

		// In sum:
		const double total_dl =
			LengthOfPointersToAllAffixesOfSig +
			TotalResponsibilityForAffixListings +
			StemPointersToThisSig;
                Lexicon->LogFile("Part 3: Stem poionters to this sig:", StemPointersToThisSig);
                Lexicon->LogFile("Length of 1 pointer to this sig: ", LengthOfPointerToThisSig);
                Lexicon->LogFile("Total", total_dl);
                Lexicon->LogFileEndTable();
		CurrentDL = total_dl;
	}
	double WinningDL = CurrentDL;
	int WinningLengthOfStemToShift = 0;

	// We might shift only those stems for which the EndPiece
	// occurs in more than 45% of the stems of this sig (that
	// leaves open the case of two closely related letters
	// comprising almost all of the cases).
	// But for now, we're not doing that.

	// The outer loop here is for the case where the entropy test
	// tells us that 2 or more letters can be shifted
	// (e.g., sig on.ve can be shifted either to ion.ive or
	// tion.tive), and we want to evaluate both.

	// Major loop through alternatives to the current signature
	CParse WinningSig;
	// loop through different lengths to shift:
	for (int NumberOfLettersShifted = LargestSizeChunkToPullOffStem;
			NumberOfLettersShifted > 0;
			--NumberOfLettersShifted) {

		TCollection<CLParse> EndPieces;
		foreach (CStem* pStem, *m_StemPtrList) {
			if (pStem->GetKeyLength() <= NumberOfLettersShifted)
				continue;

			CStringSurrogate stem_text = pStem->GetKey();
			CStringSurrogate ssPiece = analyzingSuffixes
				? stem_text.Right(NumberOfLettersShifted)
				: stem_text.Left(NumberOfLettersShifted);
			EndPieces << ssPiece;
		}

		// XXX. The function is supple enough to move material
		// from the stem to the affix in some cases but not in others.

		double AllNewSigsAnalysisDL = 0.0;
		double TotalDecreaseInDLDueToShorterStems = 0.0;
		// each of these is a distinct piece being, perhaps,
		// transferred from stem(s) to affixes
		// for each string of this length that would have to be shifted:
		CParse Sig;
                for (int pieceno = 0; pieceno < EndPieces.GetCount(); ++pieceno) {
                        CLParse* pPiece = EndPieces.GetAt(pieceno);

			// make a copy to play with.
			Sig = *this;

			if (analyzingSuffixes)
				Sig.PrefixToAllPieces2(pPiece->GetKey());
			else
				Sig.SuffixToAllPieces2(pPiece->GetKey());

			// DL of this signature:
			//
			// a. Length of pointers to its suffixes;
			//	var: LengthOfPointersToAllAffixesOfSig
			// b. Prorated responsibility for phonological
			//    content of suffixes
			//	var: TotalResponsibilityForAffixListings
			// c. List of pointers from each stem to this
			//    signature
			//	var: PointersToThisSig;
			// d. Savings because stems already existed
			//	var: SavingsBecauseStemAlreadyExisted
			// e. Savings because stems are shorter
			//	var: TotalDecreaseInDLDueToShorterStems :
			//	once for each *length* being shifted from
			//	stem to suffix
			// f. List of pointers from each word to its
			//    suffix
			//	XXX. not implemented.

			double LengthOfPointersToAllAffixesOfSig = 0.0;
			double TotalResponsibilityForAffixListings = 0.0;
			if (*Lexicon->GetSignatures() ^= Sig) {
				// new signature already exists
                                Lexicon->LogFileSmallTitle("Alternative analysis already existed", Sig.Display('-'));
				// XXX. address this case!
			} else {
				// new signature
                            Lexicon->LogFileSmallTitle("Conjectured signature: " + Sig.Display('-'));
                            Lexicon->LogFileStartTable();
				//  iterate through suffixes of the signature
                                Lexicon->LogFileHeader("Suffix", "Previous count", "New count", "Pointer length to this affix", "Responsibility for this affix (phonology) in bits:", "New DL for this affix");
				double ThisNewSigDL = 0.0;
				// for each suffix (resp prefix) in the new sig:
                                for (int affixno = 1; affixno <= Size(); ++affixno) {
					CStringSurrogate ssAffix =
                                                Sig.GetPiece(affixno);

					CAffix* pAffix = analyzingSuffixes
						? implicit_cast<CAffix*>(
							*Lexicon->GetSuffixes() ^= ssAffix)
						: implicit_cast<CAffix*>(
							*Lexicon->GetPrefixes() ^= ssAffix);
					double sum;
					if (pAffix != 0) {
						const double ResponsibilityForThisAffixListing =
							double(ssAffix.GetLength()) * base2log(26) *
							GetNumberOfStems() /
							(double(GetNumberOfStems()) +
								pAffix->GetUseCount());
						const double LengthOfPointerToThisAffix =
							LogTotalNumberOfAnalyzedWords -
							base2log(pAffix->GetUseCount() +
								GetNumberOfStems());

						TotalResponsibilityForAffixListings +=
							ResponsibilityForThisAffixListing;
						LengthOfPointersToAllAffixesOfSig +=
							LengthOfPointerToThisAffix;

						sum = ResponsibilityForThisAffixListing +
							LengthOfPointerToThisAffix;
                                                Lexicon->LogFile (ssAffix.Display(), pAffix->GetUseCount(), GetNumberOfStems() + pAffix->GetUseCount(), LengthOfPointerToThisAffix, ResponsibilityForThisAffixListing, sum);

					} else {
						// new affix
						const double ResponsibilityForThisAffixListing =
							double(ssAffix.GetLength()) * base2log(26);
						const double LengthOfPointerToThisAffix =
							LogTotalNumberOfAnalyzedWords -
							base2log(GetNumberOfStems());

						LengthOfPointersToAllAffixesOfSig +=
							LengthOfPointerToThisAffix;
						TotalResponsibilityForAffixListings +=
							ResponsibilityForThisAffixListing;
						sum = ResponsibilityForThisAffixListing +
							LengthOfPointerToThisAffix;
                                                Lexicon->LogFile(ssAffix.Display(), 0, GetNumberOfStems(), LengthOfPointerToThisAffix, ResponsibilityForThisAffixListing, sum);					 
                                        } //end of else
					ThisNewSigDL += sum;
				}
                                Lexicon->LogFile("Total", 0, 0, LengthOfPointersToAllAffixesOfSig, TotalResponsibilityForAffixListings, ThisNewSigDL);
                                Lexicon->LogFileEndTable();
			}

			// Length of the pointers to the sig from its stems:
			double SavingsBecauseStemAlreadyExisted = 0.0;
			double StemPointersToThisSig;
			IterateThroughStems(NumberOfLettersShifted,
				Lexicon,
				pPiece,
				TotalDecreaseInDLDueToShorterStems,
				LogTotalNumberOfAnalyzedWords,
				StemPointersToThisSig,
				SavingsBecauseStemAlreadyExisted,
				analyzingSuffixes);
			const double ThisNewSigDL =
				LengthOfPointersToAllAffixesOfSig +
				TotalResponsibilityForAffixListings +
				StemPointersToThisSig +
				-SavingsBecauseStemAlreadyExisted +
				-TotalDecreaseInDLDueToShorterStems;
			AllNewSigsAnalysisDL += ThisNewSigDL;
                        Lexicon->LogFile("Part 1: Length of pointer to affixes: ", LengthOfPointersToAllAffixesOfSig);
                        Lexicon->LogFile("Part 2: Prorated responsibility for phonology of affixes: ", TotalResponsibilityForAffixListings);
                        Lexicon->LogFile("Part 3: Stem pointers to this sig:", StemPointersToThisSig);
                        Lexicon->LogFile("Length of 1 poitner to this sig: ", LengthOfPointerToThisSig);
                        Lexicon->LogFile("Part 4: Total savings from stems that had already existed", SavingsBecauseStemAlreadyExisted);
                        Lexicon->LogFile("Part 5: Total decrease in DL due to shorter stems: ", TotalDecreaseInDLDueToShorterStems);
                        Lexicon->LogFile("Total DL: ", ThisNewSigDL);
		}
		if (Lexicon->LogFileOn()) *Lexicon->GetLogFile() <<
			"<br /><br />" <<
			QString("If we add %1 letters, total TD is %2")
			.arg(NumberOfLettersShifted).arg(AllNewSigsAnalysisDL) <<
			endl << "******" << endl <<
			"<br />";

		if (AllNewSigsAnalysisDL < WinningDL) {
			WinningDL = AllNewSigsAnalysisDL;
			WinningLengthOfStemToShift = NumberOfLettersShifted;
			WinningSig = Sig;
		}
	}

	if (WinningDL != CurrentDL) {
		if (Lexicon->LogFileOn()) *Lexicon->GetLogFile() <<
			SmallTitle(QString(
			"Change signature from \"%1\" to \"%2\"")
			.arg(Display(), WinningSig.Display('.'))) <<
			"<hr />";
		Lexicon->AddToScreen(
			QString("%1 >> %2")
			.arg(Display('.'), WinningSig.Display('.')));
		return WinningLengthOfStemToShift;
	} else {
		if (Lexicon->LogFileOn()) *Lexicon->GetLogFile() <<
			SmallTitle(QString(
			"%1: Conclusion: Keep original signature.")
			.arg(Display())) <<
			"<hr />";
		return 0;
	}
}
//   <<-------------------------------------------------------------------------------------------------------->>
void CSignature::IterateThroughStems(   int           NumberOfLettersShifted, 
									    CMiniLexicon* Lexicon, 
										CLParse*      pPiece,
										double&       TotalDecreaseInDLDueToShorterStems,
										double        LogTotalNumberOfAnalyzedWords,
										double&       StemPointersToThisSig,
										double& 	  SavingsBecauseStemAlreadyExisted,
										bool          analyzingSuffixes)
{



        CStem*				pStem;
        int				HowManyStemsForThisSig				 = 0; //check that
        int				NumberOfShortenedStemsThatPreExisted = 0;
        double				ThisSavingBecauseStemAlreadyExisted  = 0;
        double				DecreaseInDLDueToShorterStems		 = 0;
        double				LengthOfPointerToThisSig 			 = 0;
        CSS				ssNewStem;

                                        TotalDecreaseInDLDueToShorterStems	 = 0;
                                        SavingsBecauseStemAlreadyExisted 	 = 0;

        Lexicon->LogFile (pPiece->Display() );
        Lexicon->LogFileHeader( "Current stem", "Proposed stem", "Savings from preexisting stem");


        for (int stemno = 0; stemno < m_StemPtrList->size(); stemno++)
        {
            pStem = m_StemPtrList->at(stemno);
            ThisSavingBecauseStemAlreadyExisted =0;
            int StemLength = pStem->GetKeyLength();
            ssNewStem = pStem->GetKey().Left(
            StemLength - NumberOfLettersShifted);
            Lexicon->LogFileStartRow();
            if ( analyzingSuffixes ) // Suffixes
            {
                if ( pStem->GetKey().Right(NumberOfLettersShifted).Display() == pPiece->Display() )
                    {
				HowManyStemsForThisSig++;
                                Lexicon->LogFile1SimpleString(pStem->Display());
                                Lexicon->LogFile1SimpleString(ssNewStem.Display());

                    }
			else
			{
                            Lexicon->LogFile1SimpleString(pStem->Display());
                            Lexicon->LogFile1SimpleString(ssNewStem.Display());
                            continue;
			}
			ssNewStem = pStem->GetKey().Left( pStem->GetKeyLength() - NumberOfLettersShifted );
			}
		else // Prefixes
		{
			if ( pStem->GetKey().Left(NumberOfLettersShifted).Display() == pPiece->Display() )
			{
				HowManyStemsForThisSig++;
			}
			else
			{
				continue;
			}
			ssNewStem = pStem->GetKey().Right( pStem->GetKeyLength() - NumberOfLettersShifted );
		}

 				
					
		if ( Lexicon->GetStems()->Contains( ssNewStem )  ||  // ** Was: "GetStems_Suffixed
			 Lexicon->GetWords()->Contains( ssNewStem )      )
		{
			NumberOfShortenedStemsThatPreExisted ++;
			ThisSavingBecauseStemAlreadyExisted = ssNewStem.GetLength() * base2log (26);
			SavingsBecauseStemAlreadyExisted += ThisSavingBecauseStemAlreadyExisted;	
					
				// ** Add the cost of having a pointer to the stem ******

		}
						
		if ( Lexicon->LogFileOn()  && 
			   ( pStem->GetKey().Right(NumberOfLettersShifted).Display() == pPiece->Display() ) )
		{	
			 
 						if ( ThisSavingBecauseStemAlreadyExisted > 0)
						{
                                                        Lexicon->LogFileSimpleString("ThisSavingBecauseStemAlreadyExisted");
						} else
						{
                                                        Lexicon->LogFileSimpleString("none (did not exist)");
						}
                }
                Lexicon->LogFileEndRow();
        }

			DecreaseInDLDueToShorterStems = ( HowManyStemsForThisSig - NumberOfShortenedStemsThatPreExisted ) * 
											NumberOfLettersShifted * base2log (26);
			TotalDecreaseInDLDueToShorterStems += DecreaseInDLDueToShorterStems ;

		
			LengthOfPointerToThisSig = LogTotalNumberOfAnalyzedWords - base2log ( Size() * HowManyStemsForThisSig ) ;			
			StemPointersToThisSig	  =  HowManyStemsForThisSig * ( LengthOfPointerToThisSig ) ;
                        if ( Lexicon-> LogFileOn() )
			{
				 *Lexicon->GetLogFile() << // FILL THIS IN -- 
 					
					 StartTable							<<
						 StartTableRow							<<
							MakeTableHeader("Current stem")		<<
							MakeTableHeader("Proposed stem")		<<
							MakeTableHeader("Savings from preexisting stem")			<<
						EndTableRow;
			}


}

bool CSignature::IsValid()
//  tests that pieces of the signature are all non-null
{  for (int affixno = 1; affixno <= m_PieceCount; affixno++) {
    if ( GetPiece(affixno).GetLength() < 1 ) {
      return FALSE;
    }
  }
  return TRUE;
}
//   <<-------------------------------------------------------------------------------------------------------->>
void CSignature::DetachStem(CStem* pStem, detachment_parameter Parameter)
{
	if( !m_StemPtrList->isEmpty() &&
                m_StemPtrList->indexOf( pStem ) >= 0 &&
		m_StemPtrList->remove( pStem ) )
	{
		IncrementCorpusCount( -1 * pStem->GetCorpusCount() );

		if (Parameter != eDo_Not_Call_Words) {
			CStem *pWord;
			for (int wordno = 0; wordno < pStem->GetNumberOfWords(); wordno++) {
				pWord = pStem->GetWordPtrList()->at(wordno);
				const int index = m_WordPtrList->indexOf(pWord);
				if (index >= 0)
					m_WordPtrList->removeAt(index);
			}
		}
	}
}
//   <<-------------------------------------------------------------------------------------------------------->>
void CSignature::DetachWord(CStem* pWord, enum detachment_parameter param)
{
	struct not_implemented { };
	throw not_implemented();

	// Suppress warnings.
	static_cast<void>(pWord);
	static_cast<void>(param);
}
//   <<-------------------------------------------------------------------------------------------------------->>
void CSignature::TakeAllStems(CSignature* source)
{
        //QList<CStem*>& source_stems = *source->GetStemPtrList();
        CStem* pStem;
        for (int stemno = 0; stemno < source->GetNumberOfStems(); stemno++)
        {
            pStem=source->GetStem(stemno);;
            pStem->SetSuffixList(this);
            AppendStemPtr(pStem);
            IncrementCorpusCount(pStem->GetCorpusCount());
        }
	// Remove items from source.
        //Q_ASSERT(!source_stems.autoDelete());
        //source_stems.clear();
        source->ClearStemPtrList();

	// XXX. Decrement source corpus count in turn?
	// Hard to tell, since there are no call sites.
}
//   <<-------------------------------------------------------------------------------------------------------->>
void CSignature::AddWord (CStem* pWord)
{
  m_WordPtrList->append (pWord);
  IncrementCorpusCount (pWord->GetCorpusCount() );
}

void CSignature::ClearStemPtrList()                 { m_StemPtrList->clear();           }
void CSignature::AppendWordPointer(CStem* pWord )   { m_WordPtrList->append(pWord);     }
void CSignature::AppendPrefixPtr(CPrefix* pPrefix)  { m_PrefixPtrList->append (pPrefix);}
int CSignature::GetNumberOfWords() const
{
    return m_WordPtrList->count();
}

//   <<-------------------------------------------------------------------------------------------------------->>
CParse CSignature::CreateADeletingSignature( CParse& Deletee, CMiniLexicon* Lexicon )
{
  CStringSurrogate  ssSuffix;


  CParse        PSuffix,
            NewSig,
            Suffix;
  CSuffix*      pSuffix;
  QString        Null = "NULL", lt_brak = "<", rt_brak = "<";


  Q_ASSERT (Deletee.Size() == 1);

  for (int affixno = 1; affixno <= Size(); affixno++)
  {
    ssSuffix = GetPiece(affixno);
    if(NewSig.GetSortStyle() != eAlphabetized ) NewSig.Alphabetize();
    if ( ssSuffix == Deletee )
    {
      NewSig.Append ( CStringSurrogate(Null.unicode(),0,Null.length() ) );
    }
    else
    {
      PSuffix = CStringSurrogate(lt_brak.unicode(),0,1);
      PSuffix += Deletee;
      PSuffix += CStringSurrogate(rt_brak.unicode(),0,1);
      PSuffix.ClearParseStructure();
      PSuffix += ssSuffix;
      NewSig.Append ( PSuffix.GetKey() );

      pSuffix = *Lexicon->GetSuffixes() << PSuffix;

      QString line = "<" + Deletee.Display() + ">" + ssSuffix.Display();
      Suffix  = CStringSurrogate( line.unicode(),0,line.length());

      NewSig.Append (Suffix.GetKey());
//      Lexicon->SetSuffixTranslation(this, ssSuffix, Suffix);

    }
  }
  return NewSig;
}

//   <<-------------------------------------------------------------------------------------------------------->>
bool CSignature::RemoveStem(CStem * pStem )
{
  return m_StemPtrList->remove( pStem );
}
//   <<-------------------------------------------------------------------------------------------------------->>


bool CSignature::RemoveWord(CStem* pWord)
{
  return m_WordPtrList->remove( pWord );
}
//   <<-------------------------------------------------------------------------------------------------------->>
// copy out affixes, with null affix replaced with "NULL",
// possibly with deletees marked with angle brackets
CSignature& CSignature::Express(CSignature& Output, bool bDisplayDeletees)
{
	CSuffixCollection* Suffixes = 0;
	CPrefixCollection* Prefixes = 0;
	if (!is_initial(GetAffixLocation()))
		Suffixes = GetSignatureCollection()->GetMySuffixes();
	else
		Prefixes = GetSignatureCollection()->GetMyPrefixes();

	Output.ClearParse();

        for (int affixno = 1; affixno <= Size(); ++affixno) {
                CStringSurrogate affix_text = GetPiece(affixno);

		if (affix_text.IsNULL()) {
			Output.Append(TheStringNULL);
			continue;
		}
		if (!is_initial(m_AffixLocation)) {
			CSuffix* suffix = *Suffixes ^= affix_text;
			Q_ASSERT(suffix != 0);

			CParse Temp;
			Output.Append(
				suffix->Express(Temp, bDisplayDeletees));
		} else {
			CPrefix* prefix = *Prefixes ^= affix_text;
			Q_ASSERT(prefix != 0);

			CParse Temp;
			Output.Append(
				prefix->Express(Temp, bDisplayDeletees));
		}
	}
	return Output;
}
//   <<-------------------------------------------------------------------------------------------------------->>
/// concatenate affixes, separated by -.
QString CSignature::Express(bool bDisplayDeletees)
{
	CSuffixCollection* Suffixes = 0;
	CPrefixCollection* Prefixes = 0;
	if (!is_initial(GetAffixLocation()))
		Suffixes = GetSignatureCollection()->GetMySuffixes();
	else
		Prefixes = GetSignatureCollection()->GetMyPrefixes();

	QString Outstring;
        for (int affixno = 1; affixno <= Size(); ++affixno) {
                CStringSurrogate affix_text = GetPiece(affixno);

		if (affix_text.IsNULL()) {
			if (!Outstring.isEmpty())
				Outstring.append('-');
			Outstring.append(TheStringNULL);
			continue;
		}

		if (is_initial(m_AffixLocation)) {
			CPrefix* prefix = *Prefixes ^= affix_text;
			Q_ASSERT(prefix != 0);
			if (!Outstring.isEmpty())
				Outstring.append('-');

			CParse Temp;
			Outstring.append(prefix->Express(Temp,
				bDisplayDeletees).Display());
		} else {
			CSuffix* suffix = *Suffixes ^= affix_text;
			Q_ASSERT(suffix != 0);
			if (!Outstring.isEmpty())
				Outstring.append('-');

			CParse Temp;
			Outstring.append(suffix->Express(Temp,
				bDisplayDeletees).Display());
		}
	}

	return Outstring;
}
//   <<-------------------------------------------------------------------------------------------------------->>

// this should probably be replaced by ComputeDLofModel
/*
double CSignature::ComputeDL( int char_count )
{
	CStem* pStem;
	CAffix* pAffix;

	CStringSurrogate Affix;

	bool CORPUS_BASED_AFFIX_COUNT = m_pMyMini->GetIntParameter( "SignatureDL\\CorpusBasedAffixCount", 0 );
	bool CORPUS_BASED_STEM_COUNT = m_pMyMini->GetIntParameter( "SignatureDL\\CorpusBasedStemCount", 1 );

	double stems_dl = 0.0,
		   affixes_dl = 0.0;

	uint stem_total = 0,
		 affix_total = 0;

	if( CORPUS_BASED_STEM_COUNT )
	{
		for( pStem = m_StemPtrList->first(); pStem; pStem = m_StemPtrList->next() )
		{
			stems_dl += ( (double) -1 ) * base2log( (double) pStem->GetCorpusCount() / (double) m_pMyMini->GetCorpusCount() );
		}
	}
	else
	{
		for( pStem = m_StemPtrList->first(); pStem; pStem = m_StemPtrList->next() )
		{
			stems_dl = ( (double) -1 ) * base2log( (double) pStem->GetWordPtrList()->count() / (double) m_pMyMini->GetWords()->GetCount() );
		} 
	}

	bool analyzedSuffixes = TRUE;
	if( GetAffixLocation() == STEM_INITIAL || GetAffixLocation() == WORD_INITIAL ) analyzedSuffixes = FALSE;

	int i;
	if( !CORPUS_BASED_AFFIX_COUNT )
	{
		for( i = 1; i <= m_PieceCount; i++ )
		{
			Affix = GetPiece(i);

			if( analyzedSuffixes )
			{
				pAffix = *m_pMyMini->GetSuffixes() ^= Affix;
			}
			else
			{
				pAffix = *m_pMyMini->GetPrefixes() ^= Affix;
			}

			if( pAffix ) affix_total += pAffix->GetCorpusCount();
		}
	}

	for( i = 1; i <= m_PieceCount; i++ )
	{
		Affix = GetPiece(i);

		if( analyzedSuffixes )
		{
			pAffix = *m_pMyMini->GetSuffixes() ^= Affix;
		}
		else
		{
			pAffix = *m_pMyMini->GetPrefixes() ^= Affix;
		}

		if( pAffix ) 
		{
			if( CORPUS_BASED_AFFIX_COUNT ) affixes_dl += ( (double) -1 ) * base2log( (double) pAffix->GetCorpusCount() / (double) m_pMyMini->GetCorpusCount() );
			else affixes_dl += ( (double) -1 ) * base2log( (double) pAffix->GetCorpusCount() / (double) affix_total );
		}
	}

	return stems_dl + affixes_dl;
}
*/
//   <<-------------------------------------------------------------------------------------------------------->>
//====================================================================//
//				Description Length								      //
//====================================================================//
  double  CSignature::GetDLofMyAffixPointers(	) 
{
	if (m_DLofMyAffixPointers == 0)
        {
           bool analyzedSuffixes = TRUE;
           CSuffix * pSuffix;
           CPrefix* pPrefix;
	   if( GetAffixLocation() == STEM_INITIAL || GetAffixLocation() == WORD_INITIAL ) analyzedSuffixes = FALSE;
	   if (analyzedSuffixes)
           {
               for (int suffixno = 0; suffixno < GetSuffixPtrList()->size(); suffixno++)
               {     pSuffix= GetSuffixPtrList()->at(suffixno);
			m_DLofMyAffixPointers += pSuffix->GetLengthOfPointerToMe ();
                }
	   }
           else
           {
                for (int prefixno  = 0; prefixno < GetPrefixPtrList()->size(); prefixno++)
                {
                    pPrefix= GetPrefixPtrList()->at(prefixno);
                    m_DLofMyAffixPointers += pPrefix->GetLengthOfPointerToMe ();
                }
           }
         }
     return m_DLofMyAffixPointers;
}
  //   <<-------------------------------------------------------------------------------------------------------->>
double  CSignature::GetDLofMyStemPointers()
{
       if (m_DLofMyStemPointers == 0)
        {
           CStem * pStem;
           for (int stemno  = 0; stemno < GetNumberOfStems(); stemno++)
           {
                pStem = GetStem(stemno);
                m_DLofMyStemPointers += pStem->GetLengthOfPointerToMe ();
	   }
         }
     return m_DLofMyStemPointers;
}  
//   <<-------------------------------------------------------------------------------------------------------->>
double CSignature::ComputeDLofModel(int /* char_count, not used */)
{
	// XXX. take SignatureDL\CorpusBased{Stem,Affix}Count parameters
	// into account

	m_DLofMyStemPointers = GetDLofMyStemPointers();
	m_DLofMyAffixPointers = GetDLofMyAffixPointers();
	return m_DLofMyStemPointers + m_DLofMyAffixPointers;
}
//   <<-------------------------------------------------------------------------------------------------------->>
double CSignature::ComputeDLofMyCorpus()
{
	using linguistica::implicit_cast;

	if (m_pMyMini == 0)
		return 0.0;

	m_DLofMyCorpus = 0.0;
	foreach (CStem* pWord, *m_WordPtrList) {
		CStringSurrogate stem_text = pWord->GetStem();
		CStem* stem = *m_pMyMini->GetStems() ^= stem_text;

/***** DEBUG******/
if(stem==NULL)
{
  std::cout << "NULL stem -- in CSignature::ComputeDLofMyCorpus() "<< std::endl;
  std::cout << " word: "<<pWord->Display().toStdString()<< std::endl;
  std::cout << " stem: "<< stem_text.Display().toStdString()<<std::endl;
  CStringSurrogate afx_str 
    = (is_initial(m_AffixLocation) ? pWord->GetPrefix() : pWord->GetSuffix());
  std::cout << " affix:"<< afx_str.Display().toStdString() << std::endl;
  std::cout << std::endl;
  Q_ASSERT(stem);
}
/* end DEBUG-s.w.*/

		CStringSurrogate affix_text = is_initial(m_AffixLocation)
			? pWord->GetPrefix()
			: pWord->GetSuffix();
		if (affix_text.GetLength() == 0)
			affix_text = TheStringNULL;

		CAffix* affix = is_initial(m_AffixLocation)
			? implicit_cast<CAffix*>(
				*m_pMyMini->GetPrefixes() ^= affix_text)
			: implicit_cast<CAffix*>(
				*m_pMyMini->GetSuffixes() ^= affix_text);

		CStem* word = *m_pMyMini->GetWords() ^= pWord;
		const double ThisWordDL =
			stem->GetLengthOfPointerToMe() +
			affix->GetLengthOfPointerToMe();
		m_DLofMyCorpus += word->GetCorpusCount() * ThisWordDL;
	}
	return m_DLofMyCorpus;
}
//   <<-------------------------------------------------------------------------------------------------------->>
/*
namespace {
/// Get the corpus counts of each suffix with this stem
int* GetSuffixCounts(CStem* stem, int* output)
{
	if (output) delete output; // error if this occurs.
	output = new int[ stem->GetNumberOfSuffixes() ];

	for (int i = 1; i <= stem->GetSuffixList()->Size(); ++i) {
		QString Suffix = stem->GetSuffixList()->GetPiece(i).Display();
		if (Suffix == "NULL")
			Suffix = "";
		QString Word = stem->Display() + Suffix;
		CStem* pWord = *stem->GetMyMini()->GetWords() ^=
			CStringSurrogate(Word);

		output[i-1] = pWord->GetCorpusCount();
	}
	return output;
}
}

//the output is a vector of integers, whose length is 
// the number of stems times the number of suffixes. Pass it
// an int pointer that points to NULL; it will delete the memory
// that this function creates.
int* CSignature::GetIndividualCountsForEachStem (int* output )
{
        int     affixno, stemno;
        int*	temp = NULL;
	CStem*	pStem;

       if (output) delete output; //if this occurs, it's an error.
	output = new int [GetNumberOfStems() * GetNumberOfAffixes() ];

       CMiniLexicon* pMiniLexicon = GetLexicon();
        NOT FINISHED YET _--- use GETaWord -- JG
       for (stemno = 0; stemno < m_StemPtrList->size(); stemno++)
	{
            pSt em = m_StemPtrList->at(stemno);
            temp = GetSuffixCounts(pStem, temp);
            for (affixno = 0; affixno < GetNumberOfAffixes(); affixno++)
            {
                output[stemno * GetNumberOfAffixes() + affixno] = temp[affixno];
            }
            delete temp;
            temp = NULL;
	}
	return output;
}
*/

//===================================================================================================//
//
//          Description length
//
//===================================================================================================//
double CSignature::GetSumOfDLofInternalPointers()
{

	double		StemTotal = 0, SuffixTotal = 0;
	CStem*		pStem;
	CSuffix*	pSuffix;
        CSS		ssSuffix;
	CSuffixCollection& Suffixes = *m_pMyMini->GetSuffixes();
       for (int stemno = 0; stemno < m_StemPtrList->size(); stemno++)
	{
            pStem = m_StemPtrList->at(stemno);
            StemTotal += pStem->GetLengthOfPointerToMe_2 ();
	}
	
        for (int affixno = 1; affixno <= GetNumberOfAffixes(); affixno++)
	{
                ssSuffix = GetPiece(affixno);
		pSuffix = Suffixes ^= ssSuffix;
		SuffixTotal += pSuffix->GetLengthOfPointerToMe();
	}
	return StemTotal + SuffixTotal;
}
//   <<-------------------------------------------------------------------------------------------------------->>

void CSignature::SetLengthOfPointerToMe(double L)  
{ 
	m_LengthOfPointerToMe = L;
	return;
}

//   <<-------------------------------------------------------------------------------------------------------->>

void CSignature::AppendSatelliteAffix(CParse& suffix)
{

    m_SatelliteAffixes.Append(suffix);
}

//===================================================================================================//
//
//      Allomorphy
//
//===================================================================================================//
bool CSignature::Generalizes(CSignature* pSig)
{
	struct not_implemented { };
	throw not_implemented();

	// 1. Check they have the same length; find which one is longer.
	// 2. Go from longest to shortest pieces of the longer signature:
	//    look for unambiguous correspondents in the other signature, and
	//    put those pairs of corresponding affixes in some structure.
	// 3. After unambiguous cases, deal with ambiguous cases, if any exist.
	// 4. Find alignment
	//
	// ed |NULL | NULL | ed  |
	// ing|NULL | NULL | ing |
	// es |e    | NULL |  s  |
	// e  |e    | NULL | NULL|
	//
	//
	// ed |e    | <e>  | ed  |
	// ing|e    | <e>  | ing |
	// es |e    | NULL |  s  |
	// e  |e    | NULL | NULL|
	//
	//
	// ien     |ien  | NULL | NULL  |
	// ienne   |ienn | NULL | e     |
	// iens    |ien  | NULL |  s    |
	// iennes  |ienn | NULL | es    |
	//
	// ien     |ien  | NULL | NULL  |
	// ienne   |ien  | n    | e     |
	// iens    |ien  | NULL |  s    |
	// iennes  |ien  | n    | es    |

	CSignature* LongerSig, *ShorterSig;

	struct Row {
	    QString LongAffix;
	    QString Extension;
	    QString Operation;
	    QString ShortAffix;
	};

	if (Size() != pSig->Size())
		return false;

	const int dif = GetKeyLength() - pSig->GetKeyLength();
	if (dif > 0) {
		LongerSig = this; ShorterSig = pSig;
	} else if (dif == 0) {
		return false;
	} else {
		LongerSig = pSig; ShorterSig = this;
	}

	const int MAXAFFIXSIZE = 10;

	QStringList ShorterSigPieces;
	{

		// Copy the affixes of ShorterSig,
		// from shortest to longest
		// onto the list ShorterSigPieces.
		if (ShorterSig->ContainsNULL())
			ShorterSigPieces.append(TheStringNULL);
		for (int m = 1; m < MAXAFFIXSIZE &&
				ShorterSigPieces.count() < ShorterSig->Size();
				++m) {
			// XXX. this test makes no sense
			if (ShorterSig->ThisPieceLength(m) == m)
				ShorterSigPieces.prepend(
					ShorterSig->GetPiece(m).Display());
		}
		Q_ASSERT(ShorterSigPieces.count() == ShorterSig->Size());
	}

	QStringList LongerSigPieces;
	{
		// Copy the affixes of LongerSig,
		// from shortest to longest
		// onto the list LongerSigPieces.
		if (LongerSig->ContainsNULL())
			LongerSigPieces.append(TheStringNULL);
		for (int m = 1; m < MAXAFFIXSIZE &&
				LongerSigPieces.count() < LongerSig->Size();
				++m)
			if (LongerSig->ThisPieceLength(m) == m)
				LongerSigPieces.prepend(
					LongerSig->GetPiece(m).Display());
		Q_ASSERT(LongerSigPieces.count() == LongerSig->Size());
	}

        CStringSurrogate ssIng, ssTing;
	foreach (QString shortersig_piece, ShorterSigPieces) {
		// example: "ing"
		CStringSurrogate short_affix(shortersig_piece);
		bool match = false;
		foreach (QString longersig_piece, LongerSigPieces) {
			// example "ting"
			CStringSurrogate long_affix(longersig_piece);
			if (long_affix.IsNULL())
				continue;
			if (short_affix != long_affix.Right(
					short_affix.GetLength()))
				continue;
			bool unambiguous_match = !match;
			if (!match)
				match = true;

			if (!unambiguous_match)
				continue;

			Row ThisRow;
			ThisRow.LongAffix =
				long_affix.Display();
			ThisRow.ShortAffix =
				short_affix.Display();
			ThisRow.Extension = long_affix.Left(
				long_affix.GetLength() -
				short_affix.GetLength())
				.Display();
			// XXX. use ThisRow...
			static_cast<void>(ThisRow);
		}
	}
	return false;
}
//   <<-------------------------------------------------------------------------------------------------------->>
//   <<-------------------------------------------------------------------------------------------------------->>
void  CSignature::CutMyWordsAsIDeclare()
{   CStem* stem;
    
    if ( is_initial (GetAffixLocation())  )
    {
        for (int stemno = 0; stemno < GetNumberOfStems(); stemno++) {
                stem = GetStem(stemno);

                // For each prefix in signature:
                for (int prefixno = 1; prefixno <= Size(); ++prefixno) {
                        CStringSurrogate prefix = GetPiece(prefixno);

                        prefix.SetBackwards(false);
                        if (prefix.IsNULL())
                                // NULL + stem prefix needs no cut
                                continue;

                        // get correspond word
                        CParse word_text = prefix + stem->GetKey();
                        CStem* word = *GetLexicon()->GetWords() ^= word_text;
                        Q_ASSERT(word != 0);

                        if (word->Size() > 1 )
                                // already analyzed
                                continue;
                        GetLexicon()->LogFile ("", "", word->GetKey().Display());

                        // analyze word
                        const int cut_point =  word->GetKeyLength() - stem->GetKeyLength();
                        word->CutRightBeforeHere(cut_point);
                        word->SetStemLoc(2);
                        word->SetPrefixLoc(1);
                        //m_pLexicon->UpdateWord(word);
                }
        }
    }
    else
    {
        for (int stemno = 0; stemno < GetNumberOfStems(); stemno++) {
                stem = GetStem(stemno);

                // For each affix in signature:
                for (int suffixno = 1; suffixno <= Size(); ++suffixno) {
                        CStringSurrogate suffix = GetPiece(suffixno);

                        suffix.SetBackwards(false);
                        if (suffix.IsNULL())
                                // stem + NULL suffix needs no cut
                                continue;

                        // get correspond word
                        CParse word_text = stem->GetKey() + suffix;
                        CStem* word = *GetLexicon()->GetWords() ^= word_text;
                        Q_ASSERT(word != 0);

                        if (word->Size() > 1 )
                                // already analyzed
                                continue;
                        GetLexicon()->LogFile ("", "", word->GetKey().Display());

                        // analyze word
                        const int cut_point = word->GetKeyLength() - stem->GetKeyLength();
                        word->CutRightBeforeHere(cut_point);
                        word->SetStemLoc(1);
                        //m_pLexicon->UpdateWord(word);
                }
        }
    }
}
//
void  CSignature::OutputSignatureXfst( QTextStream& outf, int count)
{
   QString           strOutput;
   CParse            StemList;
   QString           string;

   outf << endl;

   outf << "# " << count << ": " << Display('.', m_pMyMini->GetOutFilter()) << endl;
   if (this->GetMentorList()->count() > 0)
     outf << "# MentorList() size: " << this->GetMentorList()->count() << endl;
   else
     outf << "# No MentorList() items" << endl;

   outf << "# robustness: " << m_Robustness << endl;

   if( GetMentor()!=NULL )
   {
     outf << "# Has mentor: skipping" << endl;
     return;
   }

   outf << "define STEM" << count << " "; // << " \\" << endl;

  //added
  QStringList stems;
  for (int i = 0; i < this->GetNumberOfStems(); i++)
  {
    stems.append( this->GetStem(i)->Display() );
  }

  // add stems from child sigs
  /*
  for (int z = 0; z < this->GetMentorList()->size(); z++)
  {
     CSignature * qSig = this->GetMentorList()->at(z);

     QStringList qSufList;
     for (int i = 0; i < qSig->GetNumberOfAffixes(); i++)
       qSufList.append(qSig->GetSuffix(i)->Display());
  
     //generate new words here:
     for (int i = 0; i < this->GetNumberOfAffixes(); i++)
     {
        outf << endl;
        CSuffix* pSuf = this->GetSuffix(i);
        QString sufStr = pSuf->Display( 0 );//, m_pMyMini->GetOutFilter() );
        if ( !qSufList.contains(sufStr) )
        { 
          outf<< "#### Suffix to be expanded: "<< sufStr << endl;
          for (int j = 0; j < qSig->GetNumberOfStems(); j++)
          {
            QString stemStr = qSig->GetStem(j)->Display();
            if (sufStr.compare("NULL") == 0)
              outf << "### "<< stemStr << endl;
            else
              outf << "### "<< stemStr << " " << sufStr << endl;
          } 
        }
     }
  }*/

  // add stems from child sigs
  for (int z = 0; z < this->GetMentorList()->size(); z++)
  {
     CSignature * qSig = this->GetMentorList()->at(z);
     for (int i = 0; i < qSig->GetNumberOfStems(); i++)
     {
       stems.append( qSig->GetStem(i)->Display( 0, m_pMyMini->GetOutFilter() ) );
     }
  }

   stems.sort();
   int m = 1;

   QStringList::Iterator strIt = stems.begin();
   outf << "[ {" << *strIt << "} ";
   ++strIt;


   for(  ; strIt != stems.end(); ++strIt )
   {
      if( m % 5 == 0 )
      {
         outf << endl;
         outf << "    ";
      }
      outf << "| {" << *strIt << "} ";
      m++;
   }

   outf << "]; "<<endl;
   outf << "define SUF" << count << " [ ";
   QStringList suffixes;
   bool first = 1;

   for (int i = 0; i < this->GetNumberOfAffixes(); i++)
   {
      CSuffix* pSuffix = this->GetSuffix(i);
      if(first)
         first=0;
      else
         outf << "|";
      QString str = pSuffix->Display( 0 );
      if (str.compare("NULL") == 0)
         outf << " 0 ";
      else
         outf << " {" << str << "} ";
   }

   outf << "];" << endl;

   outf << "define SIG" << count << " STEM" << count << " SUF"<< count << ";" << endl ;

   outf << "push SIG"<< count << endl;

   /* TEMP SOLN: now write cross product in comments */
   for ( QStringList::Iterator strIt = stems.begin() ; strIt != stems.end(); ++strIt )
   {
     //QList<CSuffix*>::iterator suffix_it = m_SuffixPtrList->begin();
     //CSuffix* pSuffix;
     //while ( (pSuffix = *suffix_it) != 0 )
     
     for (int i = 0; i < this->GetNumberOfAffixes(); i++)
     {
        CSuffix* pSuffix = this->GetSuffix(i);
        QString str = pSuffix->Display( 0 );//, m_pMyMini->GetOutFilter() );
        if (str.compare("NULL") == 0)
           outf << "## "<< *strIt << endl;
        else
           outf << "## "<< *strIt << str << endl;
     }
   }
  
}//


//--------------------------------------------------------------------------//
void CSignature::RecalculateStemAndWordPointers()
//--------------------------------------------------------------------------//
{

    for (int stemno = 0; stemno < GetNumberOfStems(); stemno++)
    {
        QString stem = GetStem(stemno)->Display();
        switch (m_AffixLocation)
        {
        case WORD_FINAL:
        case STEM_FINAL:
            for (int suffixno = 0; suffixno < GetNumberOfAffixes(); suffixno++)
            {           
                QString suffix = GetSuffix(suffixno)->Display();
                if (suffix == "NULL") suffix = "";
                QString word = stem + suffix;
                CStem* pWord = *GetLexicon()->GetWords() ^= word;
                AppendWordPointer( pWord);
            }
            break;
        case WORD_INITIAL:
        case STEM_INITIAL:
            for (int prefixno = 0; prefixno < GetNumberOfAffixes(); prefixno++)
            {
                QString prefix = GetPrefix(prefixno)->Display();
                if (prefix == "NULL") prefix = "";
                QString word = prefix + stem;
                CStem* pWord = *GetLexicon()->GetWords() ^= word;
                AppendWordPointer(pWord);
            }
        }
    } // end of stemno loop
}
//--------------------------------------------------------------------------//
