// Reconsidering discovered suffix-based morphology
// Copyright © 2009 The University of Chicago
#include "MiniLexicon.h"

#include <memory>
#include "ui/Status.h"
#include "DLHistory.h"
#include "Lexicon.h"
#include "Signature.h"
#include "Suffix.h"
#include "Prefix.h"
#include "Affix.h"
#include "Stem.h"
#include "SignatureCollection.h"
#include "SuffixCollection.h"
#include "PrefixCollection.h"
#include "StemCollection.h"
#include "WordCollection.h"
#include "HTML.h"
 
void CMiniLexicon::CheckSignatures() // Suffixes/Check signatures
{
	int                   NumberOfLettersToShift = 0;
	int                   InternalCount = 0;
	int                   LoopCount = 0;
	int                   LoopLimit = m_pLexicon->GetIntParameter( "CheckSignatures\\LoopLimit", 1 ); // 3 );
        //int                   i;
        CSignature*           pSig;
        CStem*                pStem;
	QString               Null = "NULL", msg;
	CStringSurrogate      ssStem;
	CParse                PWord;
	const int             StemCountThreshold = m_pLexicon->GetIntParameter( "CheckSignatures\\StemCountThreshold", 2 );
	CStringSurrogate      ssAffix;
	bool				  analyzingSuffixes = TRUE;
	if( m_AffixLocation == STEM_INITIAL || m_AffixLocation == WORD_INITIAL ) analyzingSuffixes = FALSE;

	std::auto_ptr<CSignatureCollection> Actions(analyzingSuffixes ?
		new CSignatureCollection(this, m_pSuffixes, m_AffixLocation ) :
		new CSignatureCollection(this, m_pPrefixes, m_AffixLocation ));

	linguistica::ui::status_user_agent& status = m_pLexicon->status_display();
	status.major_operation = QString(
		"Mini-Lexicon %1: Check signatures: stem/suffix edge.")
		.arg(m_Index+1);
	status.progress.clear();
	
        QHash<CSignature*, int>         HowManyLettersToShift;
        QList<CSignature*>              SignaturesToModify;

        if (analyzingSuffixes)  { LogFileLargeTitle("Phase: Check signatures (stem/suffix edge"); }
        else                    { LogFileLargeTitle("Phase: Check signatures (prefix/stem edge"); }

    //======================================================================//
    //                  Principal loop, through Signatures
	//======================================================================//

	while ( LoopCount < LoopLimit )
	{
		LoopCount++;	
                SignaturesToModify.clear();
		InternalCount = 0;
		m_pSignatures->Sort(SIGS);

	
	    //----------------------------------------------------------------//
        //  Call to "CheckOut" to check each signature.
        //----------------------------------------------------------------//

	status.progress.set_denominator(m_pSignatures->GetCount());
        for ( int signo = 0; signo < (int)m_pSignatures->GetCount(); signo++)
		{
                        msg = QString("%1").arg(LoopCount) + ": " + QString("%1").arg( m_pSignatures->GetCount() - signo );
			status.details = msg;
			status.progress = signo;

                        pSig = m_pSignatures->GetAtSort(signo);
			pSig->SetAffixLocation( m_AffixLocation );
	
			if ( pSig->GetNumberOfStems() < StemCountThreshold ) { continue; }
	
  			//==========================================================
			NumberOfLettersToShift = pSig->CheckOut(this);
			//==========================================================
	
			if ( NumberOfLettersToShift > 0)
			{
				InternalCount ++;
                                SignaturesToModify.append(pSig);
                                HowManyLettersToShift.insert (pSig,NumberOfLettersToShift);
			}
                } // end of signo loop
	
		if (InternalCount == 0) {
			// There are no signatures being modified.
			// Leave function.
			status.details.clear();
			// XXX. not really an operation.
			status.major_operation = (analyzingSuffixes ?
				QString("Mini-Lexicon %1: End of Check signatures: stem/suffix edge.") :
				QString("Mini-Lexicon %1: End of Check signatures: prefix/stem edge."))
				.arg(m_Index+1);
                        LogFile("No signatures to modify now");
                        return;
		}


        //----------------------------------------------------------------//
        //  Section *A*
		//  Now we make the changes in the words which we have identified above.
		//  Bear in mind that the (positive or negative) integer in Sig.CorpusCount is the number of
		//  letters to the right or left that the stem/suffix cut should be shifted.
        //
        //----------------------------------------------------------------//
	
                LogFileSmallTitle("Remaking signatures");
                 QString newstem;
                 for (int signo = 0; signo < (int)SignaturesToModify.size(); signo++)
                 {
                         pSig = SignaturesToModify.at(signo);
                         int NumberOfLettersShifted = HowManyLettersToShift.value(pSig);
                         LogFileSmallTitle(pSig->Display());
                         LogFileStartTable();
                         LogFileHeader("New signature", "Old stem", "New stem");
                         for (int stemno = 0; stemno < pSig->GetNumberOfStems(); stemno++)
                         {
                             pStem = pSig->GetStem(stemno);
                             ssStem = pStem->GetKey();

                             for (int affixno = 1; affixno <= pSig->Size(); affixno++)
                             {
                                         ssAffix = pSig->GetPiece(affixno);
                                         if ( ssAffix == CStringSurrogate(Null) )
                                         {
                                                 ssAffix.MakeNull();
                                         }

                                         if( analyzingSuffixes ) PWord = ssStem + ssAffix;
                                         else PWord = ssAffix + ssStem;

                                         CStem* pWord = *m_pWords ^= PWord;

                                         if( analyzingSuffixes )
                                         {
                                                 if( ssAffix.GetLength() == 0 && pWord && pWord->GetSuffixLoc() > 0 ) continue; // the stem has an internal analysis already 3/2003
                                         }
                                         else
                                         {
                                                 if( ssAffix.GetLength() == 0 && pWord && pWord->GetStemLoc() > 0 ) continue;
                                         }

                                         if (pWord->GetWordType() == CStem::BIWORD_COMPOUND ||
                                             pWord->GetWordType() == CStem::MULTIPLE_COMPOUND ||
                                             pWord->GetWordType() == CStem::POSSIBLE_COMPOUND)
                                                 continue;

                                         Q_ASSERT ( pWord->IsValid() );

                                         if ( (int)ssStem.GetLength() <=  NumberOfLettersShifted ) { continue; } ;
                                         // TODO: do the same thing below for prefixes if necessary

                                         if( analyzingSuffixes ) pWord->ShiftStemSuffixBoundary ( -1 * NumberOfLettersShifted );
                                         //else pWord->ShiftPrefixStemBoundary( pSig->GetCorpusCount() );

                                         Q_ASSERT ( pWord->IsValid() );
                                         newstem = pWord->GetStem().Display();
                             }// end of affixno loop
                              LogFile("", ssStem.Display(), newstem);
                         }

                          LogFileEndTable();
                 }   //end of signo loop


        //----------------------------------------------------------------//
        //               End of Section *A*
        //
        //----------------------------------------------------------------//
        LogFileEndTable();
    } // end of LoopCount loop;
 		
        //-------------------------------------------------------------//
  //////////////////////////////////////////////////////////////////////////////
  //  Redo Signatures
  	QString Remark ("Checking signatures");
  	CStringSurrogate ssRemark ( Remark);   
    RebuildAffixesStemsAndSignaturesFromWordSplits( ssRemark );


	// XXX. not an operation
	status.major_operation = (analyzingSuffixes ?
		QString("Mini-Lexicon %1: "
		        "End of Check signatures: stem/suffix edge.") :
		QString("Mini-Lexicon %1: "
			"End of Check signatures: prefix/stem edge." ))
		.arg(m_Index + 1);
	status.progress.clear();
	status.details.clear();

	QString mini_name( "Mini-Lexicon %1" );
	mini_name = mini_name.arg( GetIndex() + 1 );
	QString remark = "Check stem/suffix cut";
	GetDLHistory()->append( mini_name, remark, this );
}
