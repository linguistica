// Implementation of finite-state automaton class
// Copyright © 2009 The University of Chicago
#include "FSA.h"

#include <algorithm>
#include <functional>
#include <numeric>
#include <QTableWidgetItem>
#include <QPixmap>
#include "MiniLexicon.h"
#include "Signature.h"
#include "Parse.h"
#include "SignatureCollection.h"
#include "StemCollection.h"
#include "WordCollection.h"
#include "SuffixCollection.h"
#include "Suffix.h"
#include "Stem.h"
#include "log2.h"

#include "PrefixCollection.h"
#include "Prefix.h"

#ifdef USE_GRAPHVIZ
#include <graphviz/gvc.h>
#endif 

#define TEST_FUNC_MAX 32 

#define FSA_DEBUG 1

double FSAMorpheme::GetDL(int characterCount) const 
{
	if (this->str == "NULL")
		return 0.0;
	return double(this->str.size()) * base2log(characterCount);
}

bool FSAMorpheme::operator==(const FSAMorpheme & other) const
{
   return this->toStr() == other.toStr();
}

namespace linguistica {
namespace join_helper {
	class cat_with_delim : public std::binary_function<
				QString, const FSAMorpheme*, QString> {
		const QString& delim;
	public:
		cat_with_delim(const QString& d) : delim(d) { }
		QString operator()(const QString& lhs,
				const FSAMorpheme* rhs) const
		{
			QString result = lhs;
			result.append(delim);
			result.append(rhs->toStr());
			return result;
		}
	};
}
}

QString join(const FSAMorphemeList& v, const QString& delim)
{
	using linguistica::join_helper::cat_with_delim;
	using std::accumulate;

	FSAMorphemeList::const_iterator iter = v.begin();
	if (iter == v.end())
		// empty list
		return QString();

	const QString first = (*iter)->toStr();
	return accumulate(++iter, v.end(), first, cat_with_delim(delim));
}
 
FSAstate::FSAstate(FSA* pFSA)
	: m_EdgesOut(), m_EdgesIn(),
	m_MaxDepth(-1), //m_stateLabel(),
	m_PathList(), m_DiscoverCount(0),
   m_stateId(pFSA->m_nextStateId++)
{ pFSA->AddState(this); }

/*int FSAstate::getWordCount() const
{
  int sumIn=0,sumOut=0;

  for(int j=0; j<this->GetEdgesIn()->size();j++) {
    FSAedge* pEdge = this->GetEdgesIn()->at(j);
    for(int k=0; k<pEdge->GetMorphemes()->size(); k++)
    {
      sumIn += pEdge->GetMorphemes()->at(k)->m_corpusCount;
    }
  }

  for(int j=0; j<this->GetEdgesOut()->size();j++) {
    FSAedge* pEdge = this->GetEdgesOut()->at(j);
    for(int k=0; k<pEdge->GetMorphemes()->size(); k++)
      sumOut += pEdge->GetMorphemes()->at(k)->m_corpusCount;
  }

  Q_ASSERT(sumIn==0 || sumOut==0 || sumOut==sumIn);
  Q_ASSERT(sumOut+sumIn > 0);

  if(sumIn)
    return sumIn;
  else
    return sumOut;
}*/


void FSAstate::AddEdgeOut(FSAedge* pEdge)
{
  this->m_EdgesOut.push_back(pEdge);
} 

void FSAstate::AddEdgeIn(FSAedge* pEdge)
{
  this->m_EdgesIn.push_back(pEdge);
} 
//----------------------------------------------------------------// 
FSAedge::FSAedge(FSA* pFSA, FSAstate* pStartState, FSAstate* pEndState)
  : m_Morphemes(), m_FromState(pStartState), m_ToState(pEndState), m_FSA(pFSA)
{
  pFSA->AddEdge(this); 
}
/*
FSAedge::FSAedge(FSA* pFSA, FSAstate* pStartState, FSAstate* pEndState, CParse* pLabels)
  : m_Morphemes(), // initialized below
  m_FromState(pStartState),
  m_ToState(pEndState)
{
  pFSA->AddEdge(this);
  pLabels->CreateQStringList(m_Morphemes);
}*/

//void FSAedge::AddMorpheme(Morpheme* pMorph)
void FSAedge::AddMorpheme(const QString& str, int addedCount)
{
  Q_ASSERT(addedCount);
  //if Morpheme not included in FSA, update
  if(this->m_FSA->m_morphemes.contains(str))
  {
    FSAMorpheme * pMorph = this->m_FSA->m_morphemes[str];
    pMorph->m_corpusCount+=addedCount;
  } else {
    Q_ASSERT(1); //should not be called now, we are not adding stems or suffixes
    FSAMorpheme * pMorph = new FSAMorpheme(str,addedCount);
    this->m_FSA->m_morphemes.insert(str, pMorph);
  }

  //Q_ASSERT(!this->m_FSA->m_morphemeEdges.values(str).contains(this));
  //this->m_FSA->m_morphemeEdges.insert(str,this);
  this->m_Morphemes.push_back( new FSAMorpheme(str,addedCount) );
}

void FSAedge::RemoveMorpheme(FSAMorphemeList::iterator iter)
{
  FSAMorpheme* pMorph = *iter;
  QString str = pMorph->toStr(); 
  int count = pMorph->m_corpusCount;
  //this->GetMorphemes()->remove(pMorph);
  this->GetMorphemes()->erase(iter);

  // Do cleanup at FSA level
  FSAMorpheme * FsaMorph = this->m_FSA->m_morphemes[str];
  Q_ASSERT(FsaMorph);
  Q_ASSERT(FsaMorph->m_corpusCount >= count);
//std::cout << "Removing: " << pMorph->toStr().toStdString() << " FSA count: " << FsaMorph->m_corpusCount << " This edge count:" << count << std::endl; 
  if(count == FsaMorph->m_corpusCount ) //remove from FSA
    this->m_FSA->m_morphemes.remove(str);
  else
    this->m_FSA->m_morphemes[str]->m_corpusCount-=count;

  delete pMorph;
}

FSAedge* FSA::DoParallelSplit(FSAedge* pEdge, FSAMorphemeList& morphsToMove)
{
  Q_ASSERT(!morphsToMove.empty());
  //if(morphsToMove.empty()) return NULL;

  if(morphsToMove.size() == pEdge->GetMorphemes()->size())
  {
#ifdef FSA_DEBUG
    FSAMorphemeList copy1 = *pEdge->GetMorphemes();
    FSAMorphemeList copy2 = morphsToMove;
    copy1.sort();
    copy2.sort();
    Q_ASSERT(copy1==copy2);
#endif
    return NULL;
  }
#ifdef FSA_DEBUG
  FSAMorphemeList* pEdgeMorphemes = pEdge->GetMorphemes();
  foreach (FSAMorpheme* pMorph, morphsToMove) {
    FSAMorphemeList::iterator m_it = 
      std::find(pEdgeMorphemes->begin(),pEdgeMorphemes->end(),pMorph);
    Q_ASSERT(m_it!=pEdgeMorphemes->end());
  }
#endif
  //just move dynamically allocated FSAMorph object from one edge to another.
  // No other bookkeeping needed, but watch for this if defn any of FSA classes 
  // change. Note that morpheme counts are tracked at FSA level, but split does
  //  not change these counts.
  FSAedge* pNewEdge = new FSAedge(this, pEdge->GetFromState(), pEdge->GetToState());
  foreach (FSAMorpheme *pMorph,morphsToMove) {
    pEdge->GetMorphemes()->remove(pMorph);
    pNewEdge->GetMorphemes()->push_back(pMorph);
  }
  return pNewEdge;
}

void FSA::DoMultParallelSplit( FSAedge* pEdge,
      std::list<FSAMorphemeList>::iterator first,
      std::list<FSAMorphemeList>::iterator last )
{
  std::list<FSAMorphemeList>::iterator list_it = first;
#ifdef FSA_DEBUG
  FSAMorphemeList all;
  while(list_it!=last)
  {
    foreach(FSAMorpheme* pMorph,*list_it++)
      all.push_back(pMorph);
  }
  Q_ASSERT(all.size()==pEdge->GetMorphemes()->size());
  all.sort(); 
  FSAMorphemeList copy = *pEdge->GetMorphemes(); 
  copy.sort();
  Q_ASSERT(all==copy);
  list_it=first; //reset iterator
#endif
  //remove 1 non-empty list, some morphs will stay on pEdge
  while((*list_it++).size()==0)
    ;
  while(list_it!=last)
  {
    //this->DoParallelSplit(pEdge,*list_it++);
    FSAMorphemeList& morphList = *list_it++;

    if(morphList.size()==0) continue; //ignore empty lists

    FSAedge* pNewEdge = new FSAedge(this, pEdge->GetFromState(), pEdge->GetToState());
    foreach (FSAMorpheme *pMorph,morphList) {
      pEdge->GetMorphemes()->remove(pMorph);
      pNewEdge->GetMorphemes()->push_back(pMorph);
    }
  }
}


//breaks an edge in
FSAedge* FSA::DoSeriesSplit(FSAedge* pEdge,unsigned int len, bool suffix)
{
  Q_ASSERT(!pEdge->GetMorphemes()->empty());

  QString firstMorph = pEdge->GetMorphemes()->front()->toStr(); 
  QString commonMorphStr = ( suffix ? firstMorph.right(len) : firstMorph.left(len) );

  FSAstate* pNewState = new FSAstate(this);
  FSAedge* pEdge1 = new FSAedge(this, pEdge->GetFromState(),pNewState);
  FSAedge* pEdge2 = new FSAedge(this, pNewState,pEdge->GetToState());

  FSAedge *commonEdge, *multiEdge;
  if(suffix) {
    multiEdge=pEdge1;
    commonEdge=pEdge2; 
  } else {
    commonEdge=pEdge1;
    multiEdge=pEdge2;
  }
  unsigned int totalCount = 0;

  FSAMorphemeList::iterator m_iter = pEdge->GetMorphemes()->begin();
  while(m_iter != pEdge->GetMorphemes()->end())
  {
    FSAMorpheme * pMorph = *m_iter;
    QString morphStr = pMorph->toStr();
    QString curCommon = ( suffix ? morphStr.right(len) : morphStr.left(len) );
    Q_ASSERT( curCommon == commonMorphStr );
    totalCount += pMorph->m_corpusCount;

    unsigned int len_other = morphStr.length()-len;
    QString cur_multi = ( suffix ? morphStr.left(len_other) : morphStr.right(len_other) );
    //add stem morphemes
    multiEdge->AddMorpheme(cur_multi,pMorph->m_corpusCount);
    pEdge->RemoveMorpheme(m_iter++);
  }
  //add affix morpheme
  Q_ASSERT(pEdge->GetMorphemes()->empty());
  commonEdge->AddMorpheme(commonMorphStr,totalCount);

  this->RemoveEdge(pEdge); 
  return pEdge1;
}


FSA::FSA()
	: m_States(), m_Edges(), m_StartStates(),
	m_FSAPathList(), m_searched(false), m_MaxPathLength(0),
	m_lexicon(), m_charCount(0),
	m_morphemes(),
   m_nextStartStateId(1), m_nextStateId(1) { }

FSA::FSA(const FSA& fsa)
	: m_States(fsa.m_States), m_Edges(fsa.m_Edges), m_StartStates(fsa.m_StartStates),
	m_FSAPathList(fsa.m_FSAPathList), m_searched(false), m_MaxPathLength(0),
	m_lexicon(), m_charCount(0),
	m_morphemes(fsa.m_morphemes),
   m_nextStartStateId(1), m_nextStateId(1) { }

FSA::FSA(CMiniLexicon* pMiniLexicon)
	: m_States(new FSAStateList),
	m_Edges(new FSAedgeList),
	m_StartStates(new FSAStateList),
	m_FSAPathList(), m_searched(false), m_MaxPathLength(0),
	m_lexicon(pMiniLexicon),
	m_charCount(pMiniLexicon->GetNumberOfCharacterTypes()),
	m_morphemes(),
   m_nextStartStateId(1), m_nextStateId(1)
{
//m_StartState->setMaxDepth(0);
// this->AddState(m_StartState);

  //int MinimumNumberOfStemsForDisplay = 2; 
  //if (pMiniLexicon->GetSignatures()->GetCount() < 20 ) MinimumNumberOfStemsForDisplay = 1;
  //int MinimumNumberOfAffixesForDisplay = 1; //make this adjustable by user @@@@

  for (int i = 0; i < pMiniLexicon->GetSignatures()->GetCount(); i++)
  {
    CSignature* pSig = pMiniLexicon->GetSignatures()->GetAt(i);

    //if (pSig->GetNumberOfStems() < MinimumNumberOfStemsForDisplay ) continue;
    //if (pSig->Size() < MinimumNumberOfAffixesForDisplay ) continue;

    AddSignature(pSig);
  }
}

FSA::~FSA()
{
  while(!m_Edges->empty()) 
  {
    FSAedge* pEdge = m_Edges->front();
    m_Edges->pop_front();
    while(!(pEdge->GetMorphemes()->empty()))
    { 
      delete pEdge->GetMorphemes()->front();
      pEdge->GetMorphemes()->pop_front();
    }
    delete pEdge;
  }

  while(!m_States->empty())
  {
    FSAstate* pState = m_States->front();
    m_States->pop_front();
    while(!pState->m_PathList.empty())
    {
      delete pState->m_PathList.front();
      pState->m_PathList.pop_front();
    }
    delete pState;
  }

  QMap<QString, FSAMorpheme*>::iterator i = m_morphemes.begin();
  while (i != m_morphemes.end()) {
    delete i.value();
    ++i;
  }

  delete this->m_States;
  delete this->m_Edges;
  delete this->m_StartStates;
}

void FSA::AddSignature(CSignature* pSig)
{
  //std::cout << "---- Signature " <<pSig->GetNumberOfStems()<<" "<< pSig->GetNumberOfAffixes()<< std::endl; 

   //add start state 
  FSAstate* pStart = new FSAstate(this);
  pStart->setMaxDepth(0);
  this->m_StartStates->push_back(pStart);

  FSAstate* pMiddle = new FSAstate(this);
  FSAstate* pEnd    = new FSAstate(this);

   
  CStringSurrogate ssStem;
  FSAedge* pEdge1 = new FSAedge(this, pStart, pMiddle);
  for (int i = 0; i < pSig->GetNumberOfStems(); i++)
  {
    CStem* pStem = pSig->GetStemPtrList()->at(i);
    QString str = pStem->Display();

    int corpusCount = pStem->GetCorpusCount();
    Q_ASSERT(corpusCount>0);
    if(str.size()>0)
    {
      //std::cout << "\tStem:"<<str.toStdString() <<"."<< corpusCount <<std::endl;
      pEdge1->AddMorpheme( str,corpusCount); //TODO
    }
  }

  FSAedge* pEdge2 = new FSAedge(this, pMiddle, pEnd);
  for (int i = 0; i < pSig->GetNumberOfAffixes(); i++)
  {
    CSuffix* pSuffix= pSig->GetSuffixPtrList()->at(i);
    QString suffixStr = pSuffix->Display();
    int corpusCount = 0;

    //for(int j=0;j<pEdge1->GetMorphemes()->size();j++)
    FSAMorphemeList::iterator j_iter = pEdge1->GetMorphemes()->begin();
    while(j_iter != pEdge1->GetMorphemes()->end())
    {
      FSAMorpheme *pMorph = *j_iter++;
      int stemCount = pMorph->m_corpusCount;
      Q_ASSERT(stemCount>0);
      QString analyzedWord = pMorph->toStr();
      if (suffixStr!="NULL") 
        analyzedWord.append(suffixStr);

      CWordCollection* wc = this->m_lexicon->GetWords();

      CStem* pStem = ( *wc^=analyzedWord);
      Q_ASSERT(pStem);

      corpusCount+=(pStem->GetCorpusCount());
    }

    if(suffixStr.size()>0)
    {
      //std::cout << "\t\tSuffix "<< suffixStr.toStdString() <<"."<<corpusCount<<std::endl;
      pEdge2->AddMorpheme(suffixStr, corpusCount);
    }
  }
}
//----------------------------------------------------------------// 


void FSA::AddState(FSAstate* pState)
{
  m_States->push_back(pState);
};

void FSA::AddEdge(FSAedge* pEdge)
{
  m_Edges->push_back(pEdge);
   pEdge->GetFromState()->AddEdgeOut(pEdge);
   pEdge->GetToState()->AddEdgeIn(pEdge);
};

namespace linguistica {
namespace fsa_helper {

  struct contains_edge : std::unary_function<const FSAedgeList*, bool> {
    FSAedge* const e;
    contains_edge(FSAedge* edge) : e(edge) { }
    bool operator()(const FSAedgeList* l) const
    {
      return std::find(l->begin(), l->end(), e) != l->end();
    }
  };
}
}

void FSA::RemoveEdge(FSAedge* pEdge)
{
   using std::find_if;

   // remove all morphemes from edge, this does bookkeeping at FSA level
   // currently unused, TestFunc only removes edge when Morph count is zero
   if (pEdge->GetMorphemes()->size() > 0)
   {
     FSAMorphemeList::iterator iter = pEdge->GetMorphemes()->begin();
     while(iter != pEdge->GetMorphemes()->end())
       pEdge->RemoveMorpheme(iter++); 
   }
 
   this->m_Edges->remove(pEdge);
   pEdge->GetFromState()->m_EdgesOut.remove(pEdge);
   pEdge->GetToState()->m_EdgesIn.remove(pEdge);

   //remove paths that contain this edge
   // currently not used -- paths will have been cleared by ResetDisplay
   const linguistica::fsa_helper::contains_edge contains_pEdge(pEdge);

   FSApathList::iterator path_it = this->m_FSAPathList.begin();
   const FSApathList::iterator paths_end = this->m_FSAPathList.end();

   while ((path_it = find_if(path_it,paths_end,contains_pEdge)) != paths_end)
   {
       FSAedgeList* const pPath = *path_it;
       path_it = this->m_FSAPathList.erase(path_it);
       delete pPath;
   }
   // end -- remove paths
   
   delete pEdge;
};

void FSA::FSATestFunc()
{
 CSuffixCollection* lexSuffixes = this->m_lexicon->GetSuffixes();
 lexSuffixes->Sort(COUNT);
 CStemCollection* lexStems = this->m_lexicon->GetStems();

 for (int suf_idx=0; suf_idx<lexSuffixes->GetCount(); ++suf_idx)
 {
  if(suf_idx>=TEST_FUNC_MAX) {
    //std::cout << "BREAK" << std::endl;
    break;
  }
  CSuffix* pSuffix = lexSuffixes->GetAtSort(suf_idx);
  //std::cout << "Suffix:" << pSuffix->Display().toStdString() << std::endl;
  QString suffixStr = pSuffix->Display();

  if(suffixStr == "NULL") continue;

  //make copy now, list will be altered in while-loop
  FSAedgeList edges = FSAedgeList(*this->m_Edges);

  FSAedgeList::iterator edge_it = edges.begin();
  while(edge_it != edges.end())
  {
    FSAedge* pEdge = *edge_it; 
    ++edge_it; //increment now, pEdge may be deleted

    FSAMorphemeList morphemesToReanalyze;
    FSAMorphemeList * morphemes = pEdge->GetMorphemes();
    //std::cout << "At edge: " << join(*morphemes,QString(' ')).toStdString() << std::endl;
    foreach (FSAMorpheme* pMorph, *morphemes)
    {
      QString edgeStr = pMorph->toStr();
      //std::cout<<" Morpheme:"<<edgeStr.toStdString()<<std::endl;

      if (edgeStr.endsWith(suffixStr) && edgeStr!=suffixStr)
      {
        QString stemStr = edgeStr.left(edgeStr.length() - suffixStr.length());

        CStem * pStem = (*lexStems^=stemStr);
        //if stemStr in Lexicon
        if(pStem)
        {
          //if stem takes this suffix
          CSignature * pSig = pStem->GetSuffixSignature();
          for (int k = 0;k<pSig->GetSuffixPtrList()->count();k++) 
          {
            CSuffix* pSuffix= pSig->GetSuffixPtrList()->at(k);
            if(pSuffix->Display() == suffixStr )
            {
              morphemesToReanalyze.push_back(pMorph);
              break; 
            }
          }
        }
      } //else skip this morpheme
    }//edge-morphemes
    if(!morphemesToReanalyze.empty()) {
      if( morphemes->size()==morphemesToReanalyze.size() ) {
        //just do series split
        this->DoSeriesSplit(pEdge,suffixStr.length(),true);
      } else {
        FSAedge* pNewEdge = this->DoParallelSplit(pEdge,morphemesToReanalyze);
        this->DoSeriesSplit(pNewEdge,suffixStr.length(),true);
      }
    }
  }//edges
 }//suffix morphemes
}

void FSA::AddPrefixes(CMiniLexicon* pMiniLexicon) //Should be Prefix MiniLex
{
 CPrefixCollection* lexPrefixes = pMiniLexicon->GetPrefixes();
 Q_ASSERT(lexPrefixes);
 lexPrefixes->Sort(COUNT);
 //CStemCollection* lexStems = this->m_lexicon->GetStems();
 CStemCollection* lexStems = pMiniLexicon->GetStems();

 for (int suf_idx=0; suf_idx<lexPrefixes->GetCount(); ++suf_idx)
 {
  //if(suf_idx>=TEST_FUNC_MAX) {  //Not used for prefixes
    //std::cout << "BREAK" << std::endl;
  //  break;
  //}

  CPrefix* pPrefix = lexPrefixes->GetAtSort(suf_idx);
  std::cout << "Prefix:" << pPrefix->Display().toStdString() << std::endl;
  QString prefixStr = pPrefix->Display();

  if(prefixStr == "NULL") continue;

  //make copy now, list will be altered in while-loop
  FSAedgeList edges = FSAedgeList(*this->m_Edges);

  FSAedgeList::iterator edge_it = edges.begin();
  while(edge_it != edges.end())
  {
    FSAedge* pEdge = *edge_it; 
    ++edge_it; //increment now, pEdge may be deleted

    FSAMorphemeList morphemesToReanalyze;
    FSAMorphemeList * morphemes = pEdge->GetMorphemes();
    //std::cout << "At edge: " << join(*morphemes,QString(' ')).toStdString() << std::endl;
    foreach (FSAMorpheme* pMorph, *morphemes)
    {
      QString edgeStr = pMorph->toStr();
      //std::cout<<" Morpheme:"<<edgeStr.toStdString()<<std::endl;

      if (edgeStr.startsWith(prefixStr) && edgeStr!=prefixStr)
      {
        QString stemStr = edgeStr.right(edgeStr.length() - prefixStr.length());
        std::cout<<" Morpheme:"<<edgeStr.toStdString()<<std::endl;

        CStem * pStem = (*lexStems^=stemStr);
        //if stemStr in Lexicon
        //if(pStem)
        if(pStem && pStem->GetPrefixSignature())
        {
          //if stem takes this prefix
          CSignature * pSig = pStem->GetPrefixSignature();
          Q_ASSERT(pSig->GetPrefixPtrList()->count());
          for (int k = 0;k<pSig->GetPrefixPtrList()->count();k++) 
          {
            CPrefix* pPrefix= pSig->GetPrefixPtrList()->at(k);
            if(pPrefix->Display() == prefixStr )
            {
              morphemesToReanalyze.push_back(pMorph);
              break; 
            }
          }
        }

      } //else skip this morpheme

    }//edge-morphemes
    if(!morphemesToReanalyze.empty()) {
      if( morphemes->size()==morphemesToReanalyze.size() ) {
        //just do series split
        this->DoSeriesSplit(pEdge,prefixStr.length(),false);
      } else {
        FSAedge* pNewEdge = this->DoParallelSplit(pEdge,morphemesToReanalyze);
        this->DoSeriesSplit(pNewEdge,prefixStr.length(),false);
      }
    }
  }//edges
 }//prefix morphemes
}

void FSA::DoSearch()
{
  if(this->m_searched)
    return;

  this->m_searched = true;

  // get 'depth' of FSA
  this-> m_MaxPathLength=0;
  FSAStateList queue;
  //queue.append(this->m_StartState);
  for ( FSAStateList::iterator i=this->m_StartStates->begin();
        i != this->m_StartStates->end(); ++i)
  {
    FSAstate* pStartState=*i;  //this->m_StartStates->at(i); 

    //add empty path
    FSAedgeList* pPath = new FSAedgeList();
    pStartState->addPath(pPath);

    queue.push_back(pStartState);

    while(!queue.empty())
    {
     //FSAstate* pState = queue.takeFirst();
     FSAstate* pState = queue.front();
     queue.pop_front();

     FSAedgeList* edgeList = pState->GetEdgesOut();

     //if at leaf, add paths to FSA and do not search outbound edges
     if(edgeList->size() == 0)
     {
       //this->m_FSAPathList += *(pState->GetPathList());
       for(FSApathList::iterator j=pState->GetPathList()->begin();
           j != pState->GetPathList()->end(); ++j)
         this->m_FSAPathList.push_back(*j);
     }
     else
     {
      //iter over outbound edges
      for (FSAedgeList::iterator j=edgeList->begin(); j!=edgeList->end(); ++j)
      {
        FSAedge*  pEdge = *j;
        FSAstate* pChildState = pEdge->GetToState();
        pChildState->m_DiscoverCount++;

        //add paths to pChildState
        for(FSApathList::iterator l=pState->GetPathList()->begin();
            l != pState->GetPathList()->end(); ++l)
        {
          FSAedgeList* pPath = *l;

          FSAedgeList* pNewPath = new FSAedgeList(*pPath);
          pNewPath->push_back(pEdge);
          pChildState->addPath(pNewPath);
        }

        int maxChildDepth = pChildState->getMaxDepth();

        //set length of max path to child
        if(maxChildDepth==-1 || pState->getMaxDepth()+1 > maxChildDepth)
        {
          pChildState->setMaxDepth(pState->getMaxDepth()+1);
          maxChildDepth = pChildState->getMaxDepth();
        }

        if (maxChildDepth > m_MaxPathLength) 
        {
          m_MaxPathLength=maxChildDepth; 
        }

        Q_ASSERT(pChildState->m_DiscoverCount  <= pChildState->GetEdgesIn()->size());

        if(pChildState->m_DiscoverCount == pChildState->GetEdgesIn()->size())
          queue.push_back(pChildState);       
       }
      }
    }
  }
  std::cout<< "FSA DL:"<<this->ComputeDL() << std::endl;
}

int FSA::GetRobustness(FSAedgeList * pPath)
{
  QStringList pathWordsWorking;
  int pathCharCount = 0;
  FSAMorphemeList* pEdge0Morphemes = pPath->front()->GetMorphemes();
  FSAMorphemeList::iterator j_iter = pEdge0Morphemes->begin();
  //for(int j=0;j<pEdge0Morphemes->size();j++)
  while(j_iter!=pEdge0Morphemes->end())
  {
    FSAMorpheme* pMorph = *j_iter++;
    pathCharCount+=pMorph->toStr().length();
    pathWordsWorking.push_back(pMorph->toStr());
  }

  FSAedgeList::iterator i = pPath->begin();
  ++i; //skip first edge, already got these morphemes (above)
  while( i != pPath->end() )
  {
    FSAedge* pEdge = *i++;
    QStringList newWords;
    FSAMorphemeList* mList = pEdge->GetMorphemes();
    FSAMorphemeList::iterator j_iter = mList->begin();
    //for(int j=0;j<mList->size();j++)
    while (j_iter!=mList->end())
    {
      QString morph = (*j_iter++)->toStr();
      //std::cout<< "At morpheme:"<< morph.toStdString() <<std::endl;
      pathCharCount += morph.length();
      for(int k=0;k<pathWordsWorking.size();k++)
      {
        QString newWord = pathWordsWorking.at(k) + morph;
        //std::cout<< "  created: "<< newWord.toStdString() <<std::endl;
        newWords.append(newWord);
      } 
    }
    pathWordsWorking.clear();
    for(int j=0;j<newWords.size();j++)
      pathWordsWorking.append(newWords.at(j));
  }//end edge

  int charsReplacedCount = 0;
  for(int i=0;i<pathWordsWorking.size();i++)
  {
    charsReplacedCount+=pathWordsWorking.at(i).length();
  } 

  //std::cout<< "  charsReplaced: "<< charsReplacedCount << " pathCharCount: "<< pathCharCount << std::endl;
  return charsReplacedCount-pathCharCount;
}

void FSAstate::OutputXfst (QTextStream& outf)
{
  FSAstate* pState=this;
  if ( pState->GetEdgesIn()->size() == 0)//start state
  {
    outf << "\ndefine "<< this->getStateName() << " 0; "<<endl; 
  }
  else
  {
         outf << "define " << this->getStateName() << " ";
         if (pState->GetEdgesIn()->size()>1) outf << " [ "; 
        
         for (FSAedgeList::iterator j_iter=pState->GetEdgesIn()->begin();
              j_iter != pState->GetEdgesIn()->end(); ++j_iter)
         {
           if (j_iter != pState->GetEdgesIn()->begin())
             outf << "|";
           FSAedge* pEdge = *j_iter;
           outf << pEdge->GetFromState()->getStateName();

           if (pEdge->GetMorphemes()->size()==1)
           {
            FSAMorpheme* pm  = pEdge->GetMorphemes()->front();
            const QString ss = pm->toStr();
            if (pm->toStr()=="NULL")
               outf << " 0 ";
            else  
               outf << " {" << ss.toAscii() << "} "; 
           }
           else
           {
             //outf << QString("S%1 ").arg(pEdge->GetFromState()->m_stateId);
             outf << " [ ";
             FSAMorphemeList::iterator k_iter = pEdge->GetMorphemes()->begin();
             //for(int k=0;k<pEdge->GetMorphemes()->size();k++)
             while(k_iter != pEdge->GetMorphemes()->end())
             {
               if (k_iter!=pEdge->GetMorphemes()->begin()) outf << "|";
               FSAMorpheme* pm  = *k_iter++;
               const QString ss = pm->toStr();
               if (pm->toStr()=="NULL")
                 outf << " 0 ";
               else  
                 outf << " {" << ss.toAscii() << "} "; 
             }
             outf << " ] ";
           }
         }
         if (pState->GetEdgesIn()->size()>1) outf << " ]"; 
         outf << ";" << endl;
  }
}

void FSAstate::SearchEdgeXfst(QTextStream& outf, std::set<FSAstate*>& discovered)
{
  for(FSAedgeList::iterator i=this->GetEdgesIn()->begin();
      i != this->GetEdgesIn()->end(); i++)
  {
    FSAstate* pStateIn = (*i)->GetFromState();
    if(discovered.count(pStateIn)==0) //if state not discovered yet
      pStateIn->SearchEdgeXfst(outf,discovered); 
  }
  this->OutputXfst(outf); 
}

void FSA::OutputFSAXfst ( const QString& filename )
{
   QFile file( filename );

   FSAStateList topoSortedStates;
	std::set<FSAstate*> discovered;

   //find states with no outbound edges
   FSAStateList endStates;
   for (FSAStateList::iterator i=this->GetStates()->begin();
        i != this->GetStates()->end();++i)
   {
     if ((*i)->GetEdgesOut()->size()==0)
       endStates.push_back(*i); 
   }

 
   if( file.open( IO_WriteOnly ) )
   {
      QTextStream outf( &file ); //Should be ascii file, not unicode

      outf << "# " << endl;
      outf << "# File: " << filename << endl;
      outf << "# " << endl;

      //for (int i=0;i<this->GetStates()->size();i++) 
      for (FSAStateList::iterator i = endStates.begin();
           i!=endStates.end(); ++i) 
      {
        (*i)->SearchEdgeXfst(outf,discovered);
      }

      outf << endl;
      //outf << "union net" << endl << endl;
      //outf << "print words" << endl << endl;
      for (FSAStateList::iterator i = endStates.begin();
           i!=endStates.end(); ++i)
      {
        FSAstate* pState = *i;
        outf << endl;
        outf << "push " <<  pState->getStateName() <<   endl;
        outf << "print words" << endl << endl;
        outf << "pop stack" << endl << endl;
        outf << endl;
      }

      file.close();
   }
}

void FSA::FSAListDisplay(Q3ListView* pView,
	QMap<class QString, class QString>* /* out filter not supported yet */,
	bool /* “analyzed words only” flag for future use */)
{
  pView->setRootIsDecorated( FALSE );
  pView->setSorting(1);
  // Remove all previous columns
  while( pView->columns() ) pView->removeColumn( 0 );
  pView->clear();

  // get 'depth' of FSA

  this->DoSearch();

  pView->setSorting(0); //sort on 0th column

  // Add Column headers
//std::cout<<"Graph Max: "<<m_MaxPathLength <<std::endl;
   QStringList headers;
   for (int i = 0;i<this->m_MaxPathLength;i++)
   {
     pView->addColumn( QString( "Edge %1" ).arg(i)); 
   }
   pView->addColumn( QString( "Robustness" ));
  //m_pLexicon->GetDocument()->setStatusBar1( "Displaying FSA" );

  //associate each start state with it's parent list view item (row).
  //  If start state only has one outbound path then there will 
  //  be only one lv item, otherwise lv items/paths will be associated
  //  with a placeholder parent path 
  QMap<FSAstate*,FSAListViewItem*> lvMap;
  //create placeholder lvItem for start states with >=1 path
  for(FSApathList::iterator i=this->m_FSAPathList.begin();
      i != this->m_FSAPathList.end(); ++i)
  {
    FSAedgeList * pPath= *i;
    FSAstate * pStartState = pPath->front()->GetFromState();
    if(!lvMap.contains(pStartState) && pStartState->GetEdgesOut()->size()>1)
    {
      FSAListViewItem * lvtop = new FSAListViewItem(pView,pPath);
      lvtop->setVisible(TRUE);
      lvtop->setOpen(TRUE);
      lvtop->setText(0,"All paths: ");
      lvMap.insert(pStartState,lvtop);
    }
  }

  //store totaled robustness values for subgraph starting at each start state
  QMap<FSAstate*,int> robustnessMap; 

  //Add Item for each start state
/*
  for (int i=0;i<this->m_StartStates->size();i++)
  {
    FSAstate* pStartState = m_StartStates->at(i);

    FSAListViewItem * pStartStateItem= new FSAListViewItem(pView, this->m_FSAPathList.at(0)); //LEAK
    pStartStateItem->setVisible(TRUE);
    pStartStateItem->setOpen(TRUE);

    lvMap.insert(pStartState,pStartStateItem); 
  }*/

 //add every path as row to list view
  for(FSApathList::iterator i=this->m_FSAPathList.begin();
      i != this->m_FSAPathList.end(); ++i)
  {
    FSAedgeList* pPath = *i;

    int robustness = FSA::GetRobustness(pPath);
    QString robustnessStr = QString("%1").arg(robustness,16);

    FSAListViewItem *lvItem;
    //get parent list view item
    FSAstate* pStartState=pPath->front()->GetFromState(); 
    FSAListViewItem * pParent=lvMap.value(pStartState);
    if(pParent==NULL)
    {
      lvItem = new FSAListViewItem(pView, pPath);
      lvMap.insert(pStartState,lvItem);
    }
    else
    {
      lvItem = new FSAListViewItem(pParent,pPath);

      int oldVal = robustnessMap.value(pStartState);
      robustnessMap.insert(pStartState,robustness+oldVal); //score of lvItem,pPath
    }

    lvItem->setVisible(TRUE);
    lvItem->setOpen(TRUE);

    int col_idx=0;
    // first column
    for(FSAedgeList::iterator i=pPath->begin(); i!=pPath->end(); ++i)
    {
      FSAedge* pEdge = *i;
      FSAMorphemeList* labelList = pEdge->GetMorphemes();
      QString label = join(*labelList, QString(' '));
      if(label.length() > 64) 
        label= QString("%1 ... %2 (%3)")
          .arg(labelList->front()->toStr())
          .arg(labelList->back()->toStr())
          .arg(labelList->size());

      //populate table
      lvItem->setText(col_idx,label+"  ");
 
      //if this edge spans multiple columns, 
      //then columns / use whitespace
      col_idx = pEdge->GetToState()->getMaxDepth();

      //iterate
      //FSAedgeList * edgeList = pEdge->GetToState()->GetEdgesOut();
      //pEdge = ((edgeList->isEmpty())? NULL : edgeList->first());
    }
    lvItem->setText(m_MaxPathLength,robustnessStr); 

  }
  QMap<FSAstate*,FSAListViewItem*>::iterator lvm_it = lvMap.begin();
  while (lvm_it != lvMap.end())
  {
    FSAstate* pState = lvm_it.key();
    if(pState->GetEdgesOut()->size() > 1)
    {
      int robVal = robustnessMap.value(pState);
      QString robustnessStr = QString("%1").arg(robVal,16);
      lvm_it.value()->setText(m_MaxPathLength,robustnessStr);

    }
    lvm_it++;
  }

}
//--------------------------------------------------------//

FSAListViewItem::~FSAListViewItem()
{
  if(m_pImage) delete m_pImage;
}

QPixmap* FSAListViewItem::GetQPixmap()
{
  if (m_pImage == 0)
    build_pixmap();

  return m_pImage;
}

#ifdef USE_GRAPHVIZ
void FSAListViewItem::build_pixmap()
{
	// Init graph
	const char outputName[] = "tmp.png";
	QMap<FSAstate*,Agnode_t*> nodeMap;

	// generate new image
	GVC_t* gvc = gvContext();
	graph_t* g = agopen(const_cast<char*>("tmpgraph"), AGDIGRAPHSTRICT);

	// left-to-right orientation
	agraphattr(g, const_cast<char*>("rankdir"), const_cast<char*>(""));
	agset(g, const_cast<char*>("rankdir"), const_cast<char*>("LR"));

	/*QString& start_state_name = this->GetLVStartState()->m_stateLabel;
	if (start_state_name.isEmpty())
     //start_state_name = "S";
     start_state_name = this->GetLVStartState()->getStateName();*/

	// create new Agnode_t for start state
	nodeMap.insert(this->GetLVStartState(),
		 agnode(g, this->GetLVStartState()->getStateName().toUtf8().data()));

	QList<FSAstate*> queue;
	queue.append(this->GetLVStartState());

	//now do search
	//int j=1;
	while (!queue.isEmpty()) {
		FSAstate* pCurState = queue.takeFirst();

		FSAedgeList& edges = *pCurState->GetEdgesOut();
		for (FSAedgeList::iterator i = edges.begin(); i != edges.end(); ++i) {
			FSAedge* pEdge = *i;
			FSAstate* pNextState = pEdge->GetToState();

			//label node, if does not already have a label
			/*if (pNextState->m_stateLabel.isEmpty())
         {
						//QString("N%1").arg(j++);
            ++j;
				pNextState->m_stateLabel = QString("S%1").arg(pNextState->m_stateId);
         }*/

			if (!nodeMap.contains(pNextState)) {
				//QString label_text = pNextState->m_stateLabel;
				QString label_text = pNextState->getStateName();
				nodeMap.insert(pNextState,
					agnode(g, label_text.toUtf8().data()));
				 queue.append(pNextState);
			}

			// make edge label
			FSAMorphemeList* labelList = pEdge->GetMorphemes();
			QString label = join(*labelList, QString(' '));
         if(label.length() > 64)
           label= QString("%1 ... %2 (%3)")
               .arg(labelList->front()->toStr())
               .arg(labelList->back()->toStr())
               .arg(labelList->size());

			// make edge
			Agedge_t* e1 = agedge(g, nodeMap.value(pCurState),
					nodeMap.value(pNextState));
			agedgeattr(g, const_cast<char*>("label"),
					const_cast<char*>(""));
			agset(e1, const_cast<char*>("label"),
					const_cast<char*>(label.toUtf8().data()));

			// make bold if:
			if (
				// start state has >=2 outbound paths
				GetLVStartState()->GetEdgesOut()->size() >= 2 &&
				// edge in path
				std::find(this->m_pPath->begin(),this->m_pPath->end(),pEdge)
               != this->m_pPath->end() 
            ) 
			{
				agedgeattr(g, const_cast<char*>("style"),
						const_cast<char*>(""));
				agset(e1, const_cast<char*>("style"),
						const_cast<char*>("bold"));
			}
		}
	}

	gvLayout(gvc, g, const_cast<char*>("dot"));
	gvRenderFilename(gvc, g, const_cast<char*>("png"),
			const_cast<char*>(outputName));

	// cleanup - free Layout
	gvFreeLayout(gvc, g);

	// free graph
	agclose(g);

	// close output file and free context
	gvFreeContext(gvc);

	// save image
	delete m_pImage;
	QPixmap * pp = new QPixmap();//"dog.jpg");//QPixmap*uoutputName);
	//std::cout<<" Null:"<<(pp->isNull()?"Yes":"No")<<std::endl;

	m_pImage = pp;
}
#endif

void FSAListViewItem::DisplayEdgePath(Q3TextEdit* m_commandLine)
{
  int i = 0;
  FSAedge* pEdge=this->m_pPath->front();//GetEdge();
  int tablength = 0; //FIX
  while( pEdge ){
    m_commandLine->insert( QString( "Edge %1:\n\n" ).arg(i++) );
    FSAMorphemeList* morphemes = pEdge->GetMorphemes();

    //display as table
    int nColumns = 5;
    FSAMorphemeList::iterator j_iter = morphemes->begin();
    //for (int  j = 0; j < morphemes->size(); )
    while(j_iter!=morphemes->end())
    {
      //read up to 5 morphemes and print to line
      for( int k=0; k < nColumns && j_iter != morphemes->end(); k++ )
      {
        //QString text = morphemes->at(j++)->toStr();//.stripWhiteSpace();
        QString text = (*j_iter++)->toStr();//.stripWhiteSpace();
        m_commandLine->insert( text + "\t" );

        // Make tab length depend on the longest
        // word and font size
        int wordlength = text.length() * m_commandLine->pointSize();
        if( tablength < wordlength ) tablength = wordlength;
      }
      m_commandLine->insert( "\n" );
    }
    m_commandLine->insert( "\n" );

    //m_commandLine->insert(join(*morphemes, QString(' ')));
    FSAedgeList * edgeList = pEdge->GetToState()->GetEdgesOut();
    if(!edgeList->empty())
      pEdge=edgeList->front();
    else
      pEdge=NULL;
    m_commandLine->insert("\n\n");
  }
  m_commandLine->setTabStopWidth( tablength );
}


void FSA::ResetDisplay()
{
  this->m_searched=false;
  this->m_FSAPathList.clear();

  for (FSAStateList::iterator i=this->GetStates()->begin();
       i != this->GetStates()->end(); ++i) 
  {
    FSAstate* pState = *i;
    pState->GetPathList()->clear();
    //CHEAP!!
    pState->m_DiscoverCount=0;
    pState->m_stateLabel=QString("");
  }

}


double FSA::ComputeDL()
{
  double morphemesDL=0;

  QMap<QString,FSAMorpheme*>::iterator m_it;
  int mSum=0; // sum morphCounter
  for (m_it = this->m_morphemes.begin(); m_it!=m_morphemes.end(); m_it++)
    mSum += m_it.value()->m_corpusCount;

  QMap<QString,double> morphPtrDLMap;
  for (m_it = this->m_morphemes.begin(); m_it!=m_morphemes.end(); m_it++)
  {
    FSAMorpheme* pMorph = m_it.value();

    morphemesDL += pMorph->GetDL(this->m_charCount);

    //lenght of pointer to morpheme
    double cnt = pMorph->m_corpusCount; // morphCounter.value(mi.key()); 
    double pDL = base2log(mSum/cnt);
    //std::cout << mi.key().toStdString() << " \tDL: " << pDL << std::endl;
    morphPtrDLMap.insert(pMorph->toStr(), pDL);
  }

  std::cout << "morphemes DL: " << morphemesDL << std::endl;

  double allStatesDL=0;
  
  QMap<FSAedge*,double> edgePtrDLMap;
  for (FSAStateList::iterator i = this->m_States->begin();
       i != this->m_States->end(); ++i)
  {
    FSAstate* pState = *i;
    double stateDL = 0;
    for (FSAedgeList::iterator j_iter=pState->m_EdgesOut.begin();
         j_iter != pState->m_EdgesOut.end(); ++j_iter)
    {
      FSAedge* pEdge = *j_iter; 
      int edgeWordCount = 0;

      for(FSAMorphemeList::iterator k_iter = pEdge->GetMorphemes()->begin();
          k_iter != pEdge->GetMorphemes()->end(); 
          k_iter++)
      {
        FSAMorpheme* pMorph = *k_iter;
        const QString & morphStr = pMorph->toStr();
        edgeWordCount += pMorph->m_corpusCount;
        stateDL += morphPtrDLMap[morphStr];
      }

      double edgePtrDL = base2log(mSum/edgeWordCount);
      stateDL+=edgePtrDL;
      edgePtrDLMap.insert(pEdge,edgePtrDL); 
    }
    allStatesDL+=stateDL;
  }
  std::cout << "states DL: " << allStatesDL << std::endl;

  return morphemesDL + allStatesDL;
}

/*  This vn based on constructor, not used, using other vn based on TestFunc

void FSA::AddPrefixes(CMiniLexicon* pMiniLexicon)
{
//m_StartState->setMaxDepth(0);
// this->AddState(m_StartState);

  //int MinimumNumberOfStemsForDisplay = 2; 
  //if (pMiniLexicon->GetSignatures()->GetCount() < 20 ) MinimumNumberOfStemsForDisplay = 1;
  //int MinimumNumberOfAffixesForDisplay = 1; //make this adjustable by user @@@@

  for (int i = 0; i < pMiniLexicon->GetSignatures()->GetCount(); i++)
  {
    CSignature* pSig = pMiniLexicon->GetSignatures()->GetAt(i);

    //if (pSig->GetNumberOfStems() < MinimumNumberOfStemsForDisplay ) continue;
    //if (pSig->Size() < MinimumNumberOfAffixesForDisplay ) continue;
    AddPrefixSignature(pSig);
  }
  return;
}*/

/*
void FSA::AddPrefixSignature(CSignature* pSig)
{
  CStringSurrogate ssStem;
  //FSAedge* pEdge1 = new FSAedge(this, pStart, pMiddle);
  for (int i = 0; i < pSig->GetNumberOfStems(); i++)
  {
    CStem* pStem = pSig->GetStemPtrList()->at(i);
    QString stemStr = pStem->Display();

    int corpusCount = pStem->GetCorpusCount();
    Q_ASSERT(corpusCount>0);
    if(stemStr.size()>0)
    {
      std::cout << "\tStem:"<<stemStr.toStdString() <<"."<< corpusCount <<std::endl;
      //pEdge1->AddMorpheme( stmStrr,corpusCount); //TODO
      Q_ASSERT(pSig->GetNumberOfAffixes());
      for (int j = 0; j < pSig->GetNumberOfAffixes(); j++)
      {
        CPrefix* pPrefix= pSig->GetPrefixPtrList()->at(j);
        QString prefixStr = pPrefix->Display();
        if(prefixStr=="NULL") continue; //skip for now

        QString analyzedWord = (prefixStr == "NULL" ? "":prefixStr)+stemStr;
        std::cout << "\t\tWord:"<<analyzedWord.toStdString() <<"."<< corpusCount <<std::endl;
        //QList<FSAedge*> edges= this->m_morphemeEdges.values(analyzedWord);
        foreach(FSAedge * pEdge,edges)
        {
          FSAMorphemeList * pEdgeMorphemes = pEdge->GetMorphemes();
          FSAMorphemeList::iterator m_it = pEdgeMorphemes->begin();
          while(m_it != pEdge->GetMorphemes()->end())
          {
             if((*m_it)->toStr() == analyzedWord) break;
             m_it++;
          }
          FSAMorphemeList morphemesToReanalyze;
          morphemesToReanalyze.push_back(*m_it);
          FSAedge* pNewEdge = this->DoParallelSplit(pEdge,morphemesToReanalyze);
          this->DoSeriesSplit(pNewEdge,prefixStr.length(),false);
        }
      }
    }
  }

} */
