// Analyzing words using discovered signatures
// Copyright © 2009 The University of Chicago
#include "MiniLexicon.h"

#include <Q3ValueList>
#include <QString>
#include "ui/Status.h"
#include "Lexicon.h"
#include "DLHistory.h"
#include "Signature.h"
#include "Stem.h"
#include "SignatureCollection.h"
#include "WordCollection.h"
#include "StringSurrogate.h"
#include "Parse.h"
#include "HTML.h"

// We accept any stem if it can match a good signature

void CMiniLexicon::TakeSignaturesFindStems(CSignatureCollection* Sigs)
{
	CLexicon& lex = *m_pLexicon;
	linguistica::ui::status_user_agent& status = lex.status_display();

	CStem* pWord;
	const int SizeThreshold = lex.GetIntParameter(
		"TakeSignaturesFindStems\\SizeThreshold", 2);
	const int StemCountThreshold = lex.GetIntParameter(
		"TakeSignaturesFindStems\\StemCountThreshold", 2);
	const int MinimumStemLength = lex.GetIntParameter(
		"Main\\MinimumStemLength", 10);

	CStringSurrogate ssAffix;
	CStringSurrogate ssStem;
	CSignature* pSig;
	QString msg, stem, word;
	QMap<QString, int> ParsableWords;
	QStringList TempStems;
	QStringList NewStemsFound;
	int AffixLength;
	bool FailureFlag;
	if (Sigs == 0)
		Sigs = m_pSignatures;

	status.major_operation =
		QString("Mini-Lexicon %1: Take signatures to find stems")
		.arg(m_Index + 1);
	status.progress.clear();

	LogFileLargeTitle("Phase: Take Signatures, Find Stems");

	const bool analyzingSuffixes = !is_initial(GetAffixLocation());

	for (int wordno = 0; wordno < (int)m_pWords->GetCount(); wordno++) {
		pWord = m_pWords->GetAt(wordno);
		if (pWord->MayBeParsed())
			ParsableWords.insert(pWord->Display(), 1);	// 1 is a dummy value.
	}

	// We loop through the good signatures and
	// then run through the words to see
	// if they could belong to the good signatures.
	// We have to be careful, because a word might
	// have belonged to a different signature and
	// still have the marks of those suffixes
	// in its factorization.
	LogFileStartTable();
	LogFileHeader("--", "Signature");

	// Go through signatures:
	Sigs->Sort(SIGS);

	status.progress.set_denominator(Sigs->GetCount());
	for (int signo = 0; signo < (int)Sigs->GetCount(); signo++) {
		status.progress = signo;
		pSig = Sigs->GetAtSort(signo);
		if (!pSig)
			continue;
		if (pSig->Size() < SizeThreshold)
			continue;
		if (pSig->GetNumberOfStems() < StemCountThreshold)
			continue;

		LogFileSmallTitle("Empirical: " + pSig->Display('.'));
		status.details = pSig->Display();

		// Choose the first suffix in pSig that isn't NULL.
		int suffixno = 1;
		if (pSig->GetPiece(1).IsNULL())
			suffixno = 2;

		TempStems.clear();
		NewStemsFound.clear();

		ssAffix = pSig->GetPiece(suffixno);
		AffixLength = ssAffix.GetLength();
		QMapIterator<QString, int> iter(ParsableWords);

		while (iter.hasNext()) {
			word = iter.next().key();
			if (analyzingSuffixes) {
				if (word.endsWith(ssAffix.Display())) {
					if (word.length() == AffixLength)
						continue;
					stem = word.left(word.length() - AffixLength);
					Q_ASSERT(stem.length() != 0);
					if ((int) stem.length() < MinimumStemLength)
						continue;
					// put into Temp Stems
					// all those stems from words which might
					// be analyzed as ending in ssAffix.
					TempStems.append(stem);
				}
			} else {	// analyzing prefixes
				if (word.startsWith(ssAffix.Display())) {
					if (word.length() == AffixLength)
						continue;
					stem = word.right(word.length() - AffixLength);
					Q_ASSERT(stem.length() != 0);
					if ((int) stem.length() < MinimumStemLength)
						continue;
					TempStems.append(stem);
				}
			}
		} // end of loop on words

		LogFileStartTable();
		int colno = 1;
		const int numberofcolumns = 8;
		for (int stemno = 0; stemno < TempStems.count(); stemno++) {
			FailureFlag = false;
			stem = TempStems.at(stemno);
//			LogFileSimpleString(stem);
			for (int affixno =1; affixno <= pSig->Size(); affixno++) {
				ssAffix = pSig->GetPiece(affixno);
				analyzingSuffixes ? word = stem + ssAffix.Display():
				word = ssAffix.Display() + stem;
				if (! ParsableWords.contains(word)) {
					FailureFlag = true;
					break;
				}
			}
			if (FailureFlag == false) {
				NewStemsFound.append(stem);
				if (colno == numberofcolumns) {
					LogFileEndRow();
					colno = 1;
				}
				if (colno == 1)
					LogFileStartRow();
				LogFileSimpleString(stem);
				colno++;
			}
		} // end of stemno loop
		LogFileEndTable();

		// Now start building up pSig again.
		LogFileSmallTitle("Reanalyzed words");
		LogFileStartTable();
		for (int stemno = 0; stemno < NewStemsFound.count(); stemno++) {
			stem = NewStemsFound.at(stemno);
			int colno = 1;
			const int numberofcolumns = 8;
			for (int affixno = 1; affixno <= pSig->Size(); affixno++) {
				ssAffix = pSig->GetPiece(affixno);
				if (ssAffix.IsNULL())
					continue;

				if (analyzingSuffixes)
					word = stem + ssAffix.Display();
				else
					word = ssAffix.Display() + stem;

				pWord = *m_pWords ^= CStringSurrogate(word);
				if (!pWord)
					break;

				if (pWord && analyzingSuffixes) {
					pWord->ClearRootSuffixSplit();
					pWord->CutRightBeforeHere(stem.length());
					pWord->SetStemLoc(1);
					pWord->SetSuffixLoc(2);
					m_pLexicon->UpdateWord(pWord);
				} else {
					pWord->ClearPrefixStemSplit();
					pWord->CutRightBeforeHere(ssAffix.GetLength());
					pWord->SetStemLoc(2);
					pWord->SetPrefixLoc(1);
					m_pLexicon->UpdateWord(pWord);
				}

				if (pWord->GetConfidence().length() == 0) {
					msg = "3: From sig find stem";
					pWord->AppendToConfidence(msg);
				}
				if (colno == numberofcolumns) {
					LogFileEndRow();
					colno = 1;
				}
				if (colno == 1)
					LogFileStartRow();
				LogFileSimpleString(pWord->Display());
				colno++;
			} // end of loop on affixno
			if (pSig->Size() > 0)
				LogFileEndRow();
		}// cycle through this set of Stems
		LogFileEndTable();
	}// end of loop on signo
	status.progress.clear();
	status.details.clear();

	LogFileEndTable();

	QString mini_name("Mini-Lexicon %1");
	msg = "From sigs find stems";
	CStringSurrogate ssRemark = msg;

	// Writes to status.details instead of status.major_operation.
	TakeSplitWords_ProduceStemsAndSigs(ssRemark);

	status.major_operation.clear();
	mini_name = mini_name.arg(GetIndex() + 1);
	QString remark = "From sigs: find stems";
	GetDLHistory()->append(mini_name, remark, this);
}
