// Implementation of CCompoundCollection methods
// Copyright © 2009 The University of Chicago
#include "CompoundCollection.h"

#include <QList>
#include "linguisticamainwindow.h"
#include "ui/Status.h"
#include "MiniLexicon.h"
#include "Lexicon.h"
#include "Linker.h"
#include "Edge.h"
#include "Stem.h"
#include "LinkerCollection.h"
#include "WordCollection.h"
#include "log2.h"

class CPhoneCollection;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCompoundCollection::CCompoundCollection( CMiniLexicon* Lex )
{
	m_pMiniLex = Lex;
	if( m_pMiniLex ) m_pLexicon = m_pMiniLex->GetLexicon();
	m_ComponentCount = 0.0;
	m_LinkerCount = 0.0;
}

CCompoundCollection::CCompoundCollection( CLexicon* Lex )
{
	m_pMiniLex = NULL;
	m_pLexicon = Lex;
	m_ComponentCount = 0.0;
	m_LinkerCount = 0.0;
}

CCompoundCollection::~CCompoundCollection()
{
	m_pMiniLex = NULL;
	m_pLexicon = NULL;
	m_ComponentCount = 0.0;
	m_LinkerCount = 0.0;
}

void CCompoundCollection::FindMostProbableParse()
{
        QList<CStem*>*	stemSet;
	CMiniLexicon*		mini; 
	mini = NULL;
	
	CCompound*			pCompound;
	CLinker*			pLinker;
	CEdge*				pEdge;
	CParse				oneParse;

	int j;

	linguistica::ui::status_user_agent status = m_pLexicon->status_display();

	status.major_operation = "Counting Components";
	status.progress.clear();
	status.progress.set_denominator(GetCount());
	Sort(KEY);
	for (int i = 0; i < GetCount(); i++) {
		status.progress = i;
		pCompound = GetAtSort(i);

		double component_count,
			   parse_log_prob,
			   smallest_plog = 0.0;

		uint most_probable_parse = 0;

                for( pEdge = pCompound->GetParses()->first(); pEdge; pEdge = pCompound->GetParses()->next() )
                //for (int z = 0; z < pCompound->GetParses()->size(); z++)
                { //      pEdge = pCompound->GetParses()->at(z);
			pEdge->GetParse( &oneParse );
			parse_log_prob = 0.0;
			for( j = 1; j <= oneParse.Size(); j++ )
			{
				stemSet = m_pLexicon->GetStemSet( oneParse.GetPiece(j) );				
				if( stemSet )
				{
					// TODO: we might be adding the same stem more than once, check and fix
					component_count = 0.0;
					if (stemSet->size() > 0) {
						for (int y = 0; y < stemSet->size(); y++) {
							CStem* pStem = stemSet->at(y);
							component_count += pStem->GetCompoundCount();
						}

						CStem* pStem = stemSet->at(stemSet->size() - 1);
						parse_log_prob -= base2log( pStem->GetCompoundCount() / m_ComponentCount + m_LinkerCount );
					}
				}
				else
				{
					// Since this is not in the list of stems, it could be an
					// unanalyzed word. We should only look at the last mini-
					// lexicon
					for( i = m_pLexicon->GetMiniSize(); i >= 0; i++ )
					{
						mini = m_pLexicon->GetMiniLexicon(i);
						if( mini )
						{
							if (CStem* pStem = *mini->GetWords() ^= oneParse.GetPiece(j)) {
								parse_log_prob -= base2log( pStem->GetCompoundCount() / m_ComponentCount + m_LinkerCount );
							}
							break;
						}
					}

					// Otherwise must be a linker
					if( !mini )
					{
						pLinker = *m_pLexicon->GetLinkers() ^= oneParse.GetPiece(j);
						Q_ASSERT( pLinker );
						if( pLinker )
						{
							parse_log_prob -= base2log( pLinker->GetCompoundCount() / m_ComponentCount + m_LinkerCount );
						}
					}
				}
			}

			if( parse_log_prob < smallest_plog || smallest_plog == 0.0 ) 
			{
				smallest_plog = parse_log_prob;
                                most_probable_parse = pCompound->GetParses()->find( pEdge );
			}
		}
		pCompound->SetBestParse( most_probable_parse );

		m_pLexicon->UpdateCompound( pCompound->Display() );
	}
	status.progress.clear();
	status.major_operation.clear();
}

void CCompoundCollection::CheckAndRecount()
{
	CCompound* pCompound;
	CEdge* pParse;

	CParse parse;

	StemSet* pStemSet;
	CStem* pStem;

        QList<CCompound*> invalidCmpds;

	// Reset all stem's compound counts to 0.0
	Q3DictIterator<StemSet> it( *m_pLexicon->GetAllStems() );
	for( ; it.current(); ++it )
	{
                //for( pStem = it.current()->first(); pStem; pStem = it.current()->next() )
                for (int z= 0; z < it.current()->size(); z++)
		{
                        pStem = it.current()->at(z);
			pStem->SetCompoundCount( 0.0 );
		}
	}

	it = Q3DictIterator<StemSet>( *m_pLexicon->GetAllWords() );
	for( ; it.current(); ++it )
	{
                //for( pStem = it.current()->first(); pStem; pStem = it.current()->next() )
                for (int z = 0; z < it.current()->size(); z++)
                {       pStem = it.current()->at(z);
			pStem->SetCompoundCount( 0.0 );
		}
	}

	// Count components and remove compounds with missing components
	for (int i = 0; i < GetCount(); ++i) {
		pCompound = GetAt(i);
		
                QList<CEdge*> invalidParses;

                for( pParse = pCompound->GetParses()->first(); pParse; pParse = pCompound->GetParses()->next() )
                //for (int z = 0; z < pCompound->GetParses()->size(); z++)
                {   //    pParse = pCompound->GetParses()->at(z);
			pParse->GetParse( &parse );

			for( int j = 1; j <= parse.Size(); j++ )
			{
				pStemSet = (*m_pLexicon->GetAllStems())[ parse.GetPiece(j).Display() ];
				if( !pStemSet ) pStemSet = (*m_pLexicon->GetAllWords())[ parse.GetPiece(j).Display() ];
				if( !pStemSet )
				{
					invalidParses.append( pParse );
					break;
				}	
				
                                //for( pStem = pStemSet->first(); pStem; pStem = pStemSet->next() )
                                for (int y = 0; y < pStemSet->size(); y++)
                                {       pStem = pStemSet->at(y);
					pStem->IncrementCompoundCount(
						double(pCompound->GetCorpusCount()) /
						pCompound->GetParses()->count());
				}
			}
		}

		// Delete invalid parses
                //for( pParse = invalidParses.first(); pParse; pParse = invalidParses.next() )
                for (int x=0; x < invalidParses.size(); x++)
                {       pParse=invalidParses.at(x);
			pCompound->RemoveParse( pParse );
			m_pLexicon->UpdateCompound( pCompound->Display() );
		}

		if( pCompound->GetParses()->count() == 0 ) invalidCmpds.append( pCompound );
	}

	// Delete invalid compounds
        //for( pCompound = invalidCmpds.first(); pCompound; pCompound = invalidCmpds.next() )
        for (int w = 0; w < invalidCmpds.size(); w++)
        {       pCompound = invalidCmpds.at(w);
		m_pLexicon->UpdateCompound( pCompound->Display() );
		RemoveMember( pCompound );
	}
}
