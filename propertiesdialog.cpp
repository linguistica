// Implementation of PropertiesDialog methods
// Copyright © 2009 The University of Chicago
#include "propertiesdialog.h"

#include <Q3CanvasItem>
#include <QDialog>
#include "GraphicView.h"

PropertiesDialog::PropertiesDialog() : QDialog(), Ui::PropertiesDialogBase()
{ setupUi(this); }

void PropertiesDialog::exec(Q3CanvasItem* item)
{
	if (DiagramBox* box = dynamic_cast<DiagramBox*>(item)) {
		CState* pState = box->m_MyState;
		if (pState == 0)
			return;
	}

	static_cast<void>(QDialog::exec());
}
