// Template list for string alignment-based morphology discovery and display
// Copyright © 2009 The University of Chicago
#ifndef TEMPLATECOLLECTION_H
#define TEMPLATECOLLECTION_H

#include "CollectionTemplate.h"

#include <QString>
#include <QMap>
namespace linguistica { namespace ui { class status_user_agent; } }
class CTemplate;

class CTemplateCollection : public TCollection<CTemplate> {
	class CLexicon* m_lexicon;
	QMap<QString, class CParse*>* m_AllWordsAndParses;	///< Parsed Result
	float m_TotalGlobalStickNess;
	// Paragmatic Graph.
	QMap<QString, QMap<QString, float>*> m_GlobalStickNess2;
	QMap<QString, float> m_GlobalNodeStickNess2;
	float m_TotalGlobalStickNess2;
	float m_TotalWord2;

	int m_NumberOfDeletedTemplates;
public:
	CTemplateCollection();
	~CTemplateCollection();

	CTemplate* AddToCollection(class CAlignment* alignment);
	CTemplate* AddTemplate(CTemplate* a_template);
	CTemplate* operator<<(class CAlignment* alignment);
	void ListDisplay(class Q3ListView* widget,
		linguistica::ui::status_user_agent& status_display);
	void CheckForConflations(CTemplateCollection* InputCollection);
	void OutputTemplatesForGoldStand();
	QMap<QString, CParse*>* GetParsedResult() {return m_AllWordsAndParses;}
	void FindAllEditDistances(CLexicon* lex, class CWordCollection* words);

	void UpdateGlobalStickNess2();
	void AdjustTemplatesByMovingCommonTailOrHead2(int loopi);
	bool CollapseAlgorithm1(int loopnumber);
	void SetSwitchOfSortingValue(bool value);
	void AbsorbWords1(int Loopi);
	bool OneParseContainAnother(class CParse* haystack, class CParse* needles);
	void FindMorphemePrefixOrSuffixWithParadigmaticGraph(int loopi);

	// interface to MT.
	void CutMtCorpusWithMorphologyAnalyses(
		QString in_filename, QString out_filename,
		QMap<QString, class CStem*>& morphology_cuts, int strategy);
};

#endif // TEMPLATECOLLECTION_H
