// Type for words read from DCN syllabification input file
// Copyright © 2009 The University of Chicago
#ifndef DCNWORD_H
#define DCNWORD_H

// See the corpussyl documentation in DCNcorpussyl.h for an overview.

#include <qstring.h>

class dcnword  
{
public:
	dcnword();
	virtual ~dcnword();
	
	QString		getText() const			{ return m_text; }
	QString		getMaxima()	const		{ return m_maxima; }

	void		setText(QString word);
	void		setMaxima(QString word);

private:
	QString		m_text;
	QString		m_maxima;
	int			m_numOfSegments;

};

#endif // DCNWORD_H
