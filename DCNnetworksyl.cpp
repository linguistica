// Implementation of networksyl methods
// Copyright © 2009 The University of Chicago
#include "DCNnetworksyl.h"
#include <QTextStream>
#include <QIODevice>
#include <QString>
#include <QLabel>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

networksyl::networksyl()
{
	converged = false;
}

networksyl::~networksyl()
{
//	delete node;
//	delete sonority;
}


void		networksyl::setGrammar(grammarsyl* theGrammar)
{
	this->theGrammar = theGrammar;
}

void		networksyl::setWord(QString word)
{
	this->word = word;
}


void		networksyl::equilibrium()
{
	//definitions of network values
	float alpha = theGrammar->getAlpha();
	float beta  = theGrammar->getBeta();
	int syl = syllable();
	converged	= false;

	// the following is the meat of DCNs

	// oldIteration lets us know when we've converged
	// also initialize node[] and sonority[]
	float* oldIteration = new float[syl];
	node = new float[syl];
	sonority = new float[syl];
	for (int i = 0; i < syl; i++)
	{
		oldIteration[i] = -2.0;
		node[i] = 0.0;
		sonority[i] = theGrammar->getSonority(word.at(i));
	}
	
	
	// the DCN goes for 255 cylces before giving up
	for (int cycle = 0; cycle < 256; cycle++)
	{
		//run the network
		for (int s = 0; s < syl; s++)
		{
			if (s == 0)
				node[s] = alpha * node[s+1] + sonority[s];
			else if (s == syl-1)
				node[s] = beta  * node[s-1] + sonority[s];
			else
				node[s] = beta * node[s-1] + alpha * node[s+1] + sonority[s];
		}

		//calculate the distance
		float distance = 0.0;
		for (int i = 0; i < syl; i++)
			distance += ((node[i] - oldIteration[i]) * (node[i] - oldIteration[i]));

		//has the network converged?
		float DELTA = 0.0001f;
		if (distance < DELTA)
		{
			converged = true;
			break;
		}

		//store this iteration as old iteration
		for (int j = 0; j < syl; j++)
			oldIteration[j] = node[j];

		//last cycle, the network has failed to converge
		if (cycle == 255)
			converged = false;
	}

	delete[] oldIteration;
	
}

void		networksyl::print(QLabel* label)
{
	//definitions of network values
	float alpha = theGrammar->getAlpha();
	float beta  = theGrammar->getBeta();
	int syl = syllable();

	QString totalString;
	QTextStream s(&totalString, QIODevice::Append);

	s << "The values of the grammar:\n";
	s << "\talpha:\t\t" << alpha << '\n';
	s << "\tbeta:\t\t" << beta << '\n';

	s << "The value of the nodes:\n";
	s << "\tletter\t\tinherant sonority\t\tderived sonority\n";

	for (int i = 0; i < syl; i++) {
		s << "\t" << word.at(i) << "\t\t";
		s << sonority[i] << "    \t\t\t";
		s << node[i] << "\n";
	}

	s << "\n\n";

	s << "The syllabification of the word is:\t\t";
	if (isConverged())
		s << sylPrintout();
	else
		s << "not converged!";

	label->setText(totalString);
}

// a complicated printing function
QString		networksyl::sylPrintout() {
	QString output;
	QTextStream out(&output, QIODevice::Append);

	out << "\t";

	int syl = syllable();
	
	//gold.smith
	bool b = false;
	for (int r = 1; r < syl; ++r) {
		if (node[r-1] < node[r]) {
			if (b)
				out << ".";
			b = false;
		} else
			b = true;
		out << word.at(r-1);
	}
	out << word.at(word.length()-1) << "\t\n\n";
	
	out << "phonemes:\t\t";
	//g  o  l  d  s  m  I  th
	for (int w = 1; w < syl; ++w)
		out << word.at(w-1) << "\t";

	out << word.at(word.length()-1) << "\t\n\n";
	
	out << "min/max/other:\t\t";
	//L  H  O  O  L  O  H  L 
	for (int s = 0; s < syl; ++s) {
		if (s == 0) {
			if (node[s] > node[s+1])
				out << "H\t";
			else if (node[s] < node[s+1])
				out << "L\t";
			else
				out << "O\t";
		} else if (s == syl-1) {
			if (node[s] > node[s-1])
				out << "H\t";
			else if (node[s] < node[s-1])
				out << "L\t";
			else
				out << "O\t";
		} else {
			if ((node[s] > node[s-1]) && (node[s] > node[s+1]))
				out << "H\t";
			else if ((node[s] < node[s-1]) && (node[s] < node[s+1]))
				out << "L\t";
			else
				out << "O\t";
		}
	}
	out << "\n\n";
	
	out << "upward/downward:\t";
	//U  D  D  D  U  U  D  d
	for (int t = 1; t < syl; ++t) {
		if (node[t-1] < node[t])
			out << "U\t";
		else
			out << "D\t";
	}
	out << "d\n\n";

	out << "onset/nucleus/coda:\t";
	//O  N  C  C  O  O  N  C
	bool inCoda = false;
	for (int q = 0; q < syl; ++q) {
		if (q == 0) {
			if (node[q] > node[q+1]) {
				out << "N\t";
				inCoda = true;
			} else /* if (node[q] < node[q+1])*/
				out << "O\t";
		} else if (q == syl-1) {
			if (node[q] > node[q-1]) {
				out << "N\t";
				inCoda = true;
			} else /* if (node[q] < node[q-1])*/
				out << "C\t";
		} else {
			if ((node[q] > node[q-1]) && (node[q] > node[q+1])) {
				out << "N\t";
				inCoda = true;
			} else if ((node[q] < node[q-1]) && (node[q] < node[q+1])) {
				out << "O\t";
				inCoda = false;
			} else {
				if (inCoda)
					out << "C\t";
				else
					out << "O\t";
			}
		}
	}
	out << "\n\n";

	return output;
}

QString	networksyl::getMaxima() const
{
	QString output;
	QTextStream out(&output, QIODevice::Append);

	//L  H  O  O  L  O  H  L 
	for (int s = 0; s < syllable(); ++s) {
		if (s == 0) {
			if (node[s] > node[s+1])
				out << "H";
			else if (node[s] < node[s+1])
				out << "L";
			else
				out << "O";
		} else if (s == syllable()-1) {
			if (node[s] > node[s-1])
				out << "H";
			else if (node[s] < node[s-1])
				out << "L";
			else
				out << "O";
		} else {
			if ((node[s] > node[s-1]) && (node[s] > node[s+1]))
				out << "H";
			else if ((node[s] < node[s-1]) && (node[s] < node[s+1]))
				out << "L";
			else
				out << "O";
		}
	}

	return output;
}

QString	networksyl::getSyllabifiedWord() const
{
	QString output;
	QTextStream out(&output, QIODevice::Append);

	//gold.smith
	bool b = false;
	for (int r = 1; r < syllable(); r++) {
		if (node[r-1] < node[r]) {
			if (b)
				out << ".";
			b = false;
		} else
			b = true;
		out << word.at(r-1);
	}
	out << word.at(word.length()-1);

	return output;
}
