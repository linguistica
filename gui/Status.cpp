// Status and progress bars as provided by Qt.
// Copyright © 2010 The University of Chicago

// Based on the setStatusBar1, etc methods from LinguisticaMainWindow.

#include <utility>
#include <QProgressBar>
#include <QLabel>
#include <QString>
#include "ui/Status.h"
#include "Status.h"

namespace linguistica { namespace ui {

namespace {
	typedef qt::status_interface status;
}

// XXX. Processing blocks normal repaint events.
void status::label::update() { w.repaint(); }
void status::label::clear() { w.clear(); update(); }
status::label& status::label::operator=(const QString& str)
	{ w.setText(str); update(); return *this; }

// XXX. Processing blocks normal repaint events, but
// we do not force a repaint for historical raisons.
void status::progressbar::update() { }
void status::progressbar::clear() { w.reset(); update(); }

void status::progressbar::set_denominator(int denom)
	{ incr = std::max(denom / 20, 1); w.setRange(0, denom); }

status::progressbar& status::progressbar::assign(int n)
{
	w.setValue(n);
	update();
	return *this;
}

} } // namespace linguistica::ui
