// Linguistica’s interface to Qt’s status and progress bars.
// Copyright © 2010 The University of Chicago
#ifndef LXA_GUI_STATUS_H
#define LXA_GUI_STATUS_H

namespace linguistica { namespace ui {
	struct status_user_agent;

	namespace qt {
		struct status_interface;
	}
} }

#include "ui/Status.h"
class QLabel;
class QProgressBar;

class linguistica::ui::qt::status_interface
		: public linguistica::ui::status_user_agent {
	struct label : public status_user_agent::string_type {
		QLabel& w;
		explicit label(QLabel& bar) : w(bar) { }
	private:
		// XXX. lxa processing blocks repaint events.
		void update();
	public:
		void clear();
		label& operator=(const QString& str);
		// caller is responsible for freeing the label.
	};

	struct progressbar : public status_user_agent::progress_type {
		QProgressBar& w;
		explicit progressbar(QProgressBar& bar) : w(bar) { }
	private:
		// XXX. lxa processing blocks repaint events.
		void update();
		progressbar& assign(int n);
	public:
		void clear();
		void set_denominator(int denom);
		// caller is responsible for progress bar.
	};

	label l;
	progressbar m;
	label r;
public:
	status_interface(QLabel& left, QLabel& right, QProgressBar& middle)
		: status_user_agent(l, r, m), l(left), m(middle), r(right) { }
};

#endif // LXA_GUI_STATUS_H
