// Suffix list for suffix/signature-based morphology discovery and display
// Copyright © 2009 The University of Chicago
#ifndef SUFFIXCOLLECTION_H
#define SUFFIXCOLLECTION_H

class CSuffixCollection;

// See the CMiniLexicon class in MiniLexicon.h for an overview of
// suffix/signature-based discovery of morphology.
// See also the very similar CPrefixCollection class.

#include "CollectionTemplate.h"
#include "generaldefinitions.h"
template<class K, class V> class QMap;
class QString;
namespace linguistica { namespace ui { class status_user_agent; } }

class CSuffixCollection : public TCollection<class CSuffix> {
	double m_DLofPointersToMyMembers;
public:
	CSuffixCollection();
	explicit CSuffixCollection(class CMiniLexicon* lex);
	~CSuffixCollection();

	// output to GUI.
	void ListDisplay(class Q3ListView* widget,
		QMap<QString, QString>* filter,
		linguistica::ui::status_user_agent& status_display,
		bool deletees = true);

	// long-term storage.
	void OutputSuffixes(QString filename);
	void ReadSuffixFile(QString filename);

	void SortByIndex();
        int GetTotalUseCount();
        void CalculateTotalUseCount();

	// insert.
	// XXX. PrefixCollection passes string surrogates by value
	CSuffix* operator<<(class CStringSurrogate& suffix_name);
	CSuffix* operator<<(class CParse* suffix_name);	///< deprecated
	CSuffix* operator<<(class CParse& suffix_name);
	CSuffix* operator<<(QString suffix_name);
	void AddPointer(CSuffix* suffix);
	CSuffix* AddToCollection(class CParse& suffix_name);
	CSuffix* AddToCollection(class CStringSurrogate& suffix_name);
	
	// clear.
	void Empty();
	void RemoveAll();

	// remove.
	bool Remove(CSuffix* suffix);	///< doesn't delete suffix
	bool RemoveMember(CSuffix* suffix);	///< deletes suffix
	/// deletes CSuffix*
	bool RemoveMember(class CStringSurrogate& suffix_name);
	bool RemoveMember(class CStringSurrogate& suffix_name, bool delete_it);
	void DeleteMarkedMembers();

	// look for suffix combinations.
	void FindCombinations(class CSignatureCollection* Signatures,
				class CMiniLexicon* lex);
	void FindFactorableSuffixes();
	
	// description length.
	double GetDL_PhonologicalContent();
	double CalculatePointersToMySuffixes(enum eMDL_STYLE style);
};

#endif // SUFFIXCOLLECTION_H
