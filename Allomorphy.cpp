// Implementation of AffixAlignment, SignatureAlignment methods
// Copyright © 2009 The University of Chicago
#include "Allomorphy.h"

#include <QTextStream>
#include <QStringList>
#include <QtAlgorithms>
#include "Signature.h"
#include "StringSurrogate.h"
#include "Parse.h"
#include "StringFunc.h"
#include "HTML.h"
#include "Typedefs.h"
//---------------------------------------------------------------------//
class LxStringList {
        QStringList m_stringlist;
public:         
        LxStringList(CParse*);
void    SortBySizeRightAlphabetical();
int     size() { return m_stringlist.size(); }
QString at(int stringno);
QString operator[](int i){ return m_stringlist[i];};
QString LongestCommonPrefix();
void    Remove(QString);
bool    contains (QString string) { return m_stringlist.contains(string); }
};


//---------------------------------------------------------------------//
LxStringList::LxStringList(CParse* parse)
{
    parse->CreateQStringList (m_stringlist);
    for (int affixno = 0; affixno < m_stringlist.size(); affixno++)
    {
        if (m_stringlist[affixno]=="NULL")
               m_stringlist[affixno].clear();
    }
}




//---------------------------------------------------------------------//
// str1 is ranked before str2 if str1 is longer than str2; if they are the same length, then if it is alphabetically prior,
// scanning both strings from right to left.
bool comparefunction1(QString str1, QString str2){
  if (str1.length() > str2.length() ) return TRUE;
  if (str2.length() > str2.length() ) return FALSE;
  for (int i = str1.length()-1; i >= 0 ; i--)
  {
    if (str1[i] < str2[i] ) return TRUE;
    if (str1[i] > str2[i] ) return FALSE;
  }
  return FALSE;
}
//---------------------------------------------------------------------//
void LxStringList::SortBySizeRightAlphabetical()
{
    qSort(m_stringlist.begin(), m_stringlist.end(), comparefunction1);
}
//---------------------------------------------------------------------//





//---------------------------------------------------------------------//
QString LxStringList::at(int stringno)
{
    return m_stringlist[stringno];

}
//---------------------------------------------------------------------//
bool StringSubtractFromRight(QString string1, QString string2, QString& difference)
{
    // If string2 is not a suffix of string1, return FALSE;
    // Else, difference = string1.left (string1.length - string2.length )

    if ( string1.endsWith(string2)    ){
        difference = string1.left( string1.length() - string2.length() );
        return TRUE;
    }
    difference.clear();
    return FALSE;

}
//---------------------------------------------------------------------//
QString LxStringList::LongestCommonPrefix()
{   int prefixlength;
    QString prefix = m_stringlist[0];
    prefixlength = prefix.length();

    for (int morphno = 1; morphno < m_stringlist.size(); morphno++)
    {
        QString morph = m_stringlist[morphno];
        int morphlength = morph.length();
        int letterno;
        for (letterno = 0; letterno < morph.length() && letterno < prefix.length(); letterno++)
        {
            if ( prefix[letterno] == morph[letterno] ) continue;
            if ( letterno == 0) { prefix.clear(); return prefix; }
        }
        if (letterno == morphlength-1) // so match is as good as it can get...
        {
            prefix = prefix.left(letterno+1);
            prefixlength = prefix.length();
        }
        else if (letterno == prefixlength-1) // also, match is as good as it can get
        {
            continue;
        }
        else // we have a non-zero match with this morph; we'll note that and go on to next morph.
        {
            prefix = prefix.left(letterno+1);
            prefixlength= prefix.length();
        }
    } // end of loop over morphs in StringList
    return prefix;
}
//---------------------------------------------------------------------//
void LxStringList::Remove(QString string)
{
    m_stringlist.remove(string);
}



//---------------------------------------------------------------------//
AffixAlignment::AffixAlignment(QString Affix1, QString Affix2)
	: m_OriginalAffix1(), m_OriginalAffix2(),
	m_Affix1(Affix1), m_Affix2(Affix2),
	m_Margin1(), m_Margin2(),
	m_Shift1(), m_Shift2(),
	m_Status(Affix1 == Affix2 ? IDENTICAL : DIFFERENT),
	m_Agreement_unigram(0.0),
	m_Agreement_bigram(0.0),
	m_Disagreement_unigram(0.0),
	m_Disagreement_bigram(0.0) { }

AffixAlignment::AffixAlignment(QString Margin1, QString Affix1,
		QString Margin2, QString Affix2)
	: m_OriginalAffix1(), m_OriginalAffix2(),
	m_Affix1(Affix1), m_Affix2(Affix2),
	m_Margin1(Margin1), m_Margin2(Margin2),
	m_Shift1(), m_Shift2(),
	m_Status(Affix1 == Affix2 ? IDENTICAL : DIFFERENT),
	m_Agreement_unigram(0.0),
	m_Agreement_bigram(0.0),
	m_Disagreement_unigram(0.0),
	m_Disagreement_bigram(0.0) { }
//---------------------------------------------------------------------//
//              Constructor
//---------------------------------------------------------------------//
CSignatureAlignment::CSignatureAlignment(CSignature* Sig1, CSignature* Sig2)
	: m_SigPtr1(Sig1), m_SigPtr2(Sig2),
	m_AffixAlignments(),
        m_Sig1AlignedAffixes(), m_Sig2AlignedAffixes()
{   LxStringList TempList1(Sig1), TempList2(Sig2);
    int smallersize = TempList1.size();
    if (TempList2.size() < smallersize)
    {
        smallersize = TempList2.size();
    }

    int sig1length=0, sig2length = 0;
    for (int affixno = 0; affixno < smallersize; affixno++)
    {
        sig1length += TempList1[affixno].length();
        sig2length += TempList2[affixno].length();
    }
    if (sig1length <= sig2length)
    {
        m_SigPtr1 = Sig1;
        m_SigPtr2 = Sig2;
    }else
    {
        m_SigPtr1 = Sig2;
        m_SigPtr2 = Sig1;
    }
}
//---------------------------------------------------------------------//
bool CSignatureAlignment::FindWhetherOneIsSuffixOfTheOther()
{
    // First, sort both by Size and Right-side alphabetical:
     
    // We don't need to keep these local variables here, now that the signatures are kept in the SigAlignment. Fix? JG.
    LxStringList* ShorterSig, *LongerSig;
    QString suffix1, suffix2, ShorterSuffix, LongerSuffix;

    LxStringList SigList1 (m_SigPtr1);
    LxStringList SigList2 (m_SigPtr2);
    SigList1.SortBySizeRightAlphabetical();
    SigList2.SortBySizeRightAlphabetical();
    suffix1 = SigList1.at(0);
    suffix2 = SigList2.at(0);
    int     size = SigList1.size();
    if (suffix1.length() == suffix2.length()) {return FALSE;}
    else if (suffix1.length() < suffix2.length()) {
        LongerSig  = &SigList2;
        ShorterSig = &SigList1;

        m_LongerSig = m_SigPtr2;
        m_ShorterSig= m_SigPtr1;

        LongerSuffix = suffix2;
        ShorterSuffix = suffix1;
    } else {
        LongerSig  = &SigList1;
        ShorterSig = &SigList2;

        m_LongerSig = m_SigPtr1;
        m_ShorterSig = m_SigPtr2;

        LongerSuffix = suffix1;
        ShorterSuffix = suffix2;       
    }


    bool b_diff = StringSubtractFromRight(LongerSuffix, ShorterSuffix,m_difference);
    if (b_diff == FALSE) {         
        return FALSE;
    }
    // this checks that the difference between the corresponding suffixes is identical across the signature's suffixes:
    for (int suffixno = 1; suffixno < size; suffixno++){
        if ( LongerSig->at(suffixno)   != m_difference +  ShorterSig->at(suffixno)  )
        {
            return FALSE;
        }
    }

    return TRUE;


}

void CSignatureAlignment::FindBestAlignment()
{

    QString Morph, OtherMorph, MarginPiece;
    AffixAlignment* pAlign;
	CParse Margins1, Margins2, Suffixes1, Suffixes2;
    int morphlength, othermorphlength;
    CSS ssMorph, ssOtherMorph;

    LxStringList SigList1 (m_SigPtr1);
    LxStringList SigList2 (m_SigPtr2);
    SigList1.SortBySizeRightAlphabetical();
    SigList2.SortBySizeRightAlphabetical();

    QMap<QString,int> AffixesMatched1, AffixesMatched2, MarginsFound1, MarginsFound2;


//-------------------------------- Step 1 --------------------------------------------------------//
    // first, look for identical affixes in the sigs;
    for (int i = 0; i < SigList1.size(); i++)
    {
        Morph = SigList1[i];
        if (AffixesMatched1.contains(Morph) ) continue;
        for (int j = 0; j < SigList2.size(); j++)
        {
            OtherMorph = SigList2[j];
            if ( AffixesMatched2.contains(OtherMorph) ) continue;
            if (Morph==OtherMorph)
            {
                if (Morph.length() == 0) { continue; }
                pAlign = new AffixAlignment (Morph, Morph);
                m_AffixAlignments.append( pAlign );                       
                AffixesMatched1 [Morph] = 1;
                AffixesMatched2 [Morph] = 1;
                break;
            } 
        }
    }

//-------------------------------- Step 2 --------------------------------------------------------//
    // now look for non-identical but end-matching affix pairs, like "es/s"
    // Don't consider any suffixes which have been identified in Step 1 above (where there was an identical homolog)

    for (int  i = 0; i < SigList1.size(); i++)
    {                
        Morph = SigList1[i];
        if (AffixesMatched1.contains(Morph) ) continue;
        morphlength = Morph.length();
        if (morphlength == 0) continue;
        for (int j = 0; j < SigList2.size(); j++)
        {
            OtherMorph = SigList2[j];
            if ( AffixesMatched2.contains(OtherMorph) ) continue;
            othermorphlength = OtherMorph.length();
            if (morphlength == 0) continue;
            if ( OtherMorph.right( morphlength ) == Morph )
            {
                    MarginPiece = OtherMorph.left(othermorphlength - morphlength);
                    pAlign = new AffixAlignment (TheStringNULL, Morph,
                                                 MarginPiece,   Morph );
                    m_AffixAlignments.append(pAlign);            
                    AffixesMatched1[Morph]      = 1;
                    AffixesMatched2[OtherMorph] = 1;
                    MarginsFound1[TheStringNULL] = 1;
                    MarginsFound2[MarginPiece] = 1;
                    break;
            }
            if (morphlength>0 && othermorphlength > 0 && Morph.right(othermorphlength) == OtherMorph  )
            {
                    MarginPiece = Morph.left( morphlength - othermorphlength );
                    pAlign = new AffixAlignment ( MarginPiece,   OtherMorph,
                                              TheStringNULL, OtherMorph );
                    m_AffixAlignments.append(pAlign);
                    AffixesMatched1[Morph]    = 1;
                    AffixesMatched2[OtherMorph] = 1;
                    MarginsFound1[MarginPiece] = 1;
                    MarginsFound2[TheStringNULL] =1 ;
                    break;
            }             
        }        
    }

//-------------------------------- Step 3 --------------------------------------------------------//
    // if one of the signatures has an X in its margin region, and it also has an X as an affix and the other
    // signature has a NULL, then we can align the X and the NULL:

        //if (m_SigPtr1->ContainsNULL())
        if (SigList1.contains("") )
	{
                for (int suffixno = 0; suffixno < SigList2.size(); suffixno++)
		{
                       Morph = SigList2[suffixno];

                        if (Morph.length() == 0)              continue;
                        if ( AffixesMatched2.contains(Morph) ) continue;
                        if ( MarginsFound2.contains( Morph ) )
			{
                            pAlign = new AffixAlignment ( TheStringNULL, TheStringNULL, Morph, TheStringNULL);
                            m_AffixAlignments.append(pAlign);                            
                            AffixesMatched1[""] = 1;
                            AffixesMatched2[Morph];
			}
		}
	}

        if (SigList2.contains("") && ! AffixesMatched2.contains("") )
        {            
            for (int suffixno = 0; suffixno < SigList1.size(); suffixno++)
		{
                        Morph = SigList1[suffixno] ;
                        if (Morph.length() == 0)                continue;
                        if (AffixesMatched1.contains(Morph) )   continue;
                        if ( MarginsFound1.contains( Morph ) )
                        {
                            pAlign = new AffixAlignment ( Morph, TheStringNULL, TheStringNULL, TheStringNULL);
                            m_AffixAlignments.append(pAlign);
                            AffixesMatched2[""] = 1;
                            AffixesMatched1[Morph] = 1;
                        }
                }
        }

//-------------------------------- Step 4 --------------------------------------------------------//

        // match NULL against NULL
        if (SigList1.contains("") && ! AffixesMatched1.contains("")  && SigList2.contains("") && !AffixesMatched2.contains("") )
        {
            pAlign = new AffixAlignment ( TheStringNULL, TheStringNULL, TheStringNULL, TheStringNULL);
            m_AffixAlignments.append(pAlign);
            //MarginsFound1[TheStringNULL] = 1 ;
            AffixesMatched1[TheStringNULL] = 1;
            AffixesMatched2[TheStringNULL] = 1;

        }


}

// Deprecated: (use following function)
void CSignatureAlignment::Display(QTextStream& LogStream)
{
    AffixAlignment* pAlign;
 
    if (m_AffixAlignments.count()  > 0) 
    {
        LogStream <<

            StartTable      << 
            StartTableRow <<
                TableData (m_SigPtr1->Display('-')) <<
                TableData (m_SigPtr2->Display('-')) << EndTableRow <<
            StartTableRow   <<
                MakeTableHeader ("Shift region 1")  <<
                MakeTableHeader ("Margin region 1") <<
                MakeTableHeader ("Affix 1")         <<
                MakeTableHeader ("Shift region 2")  <<
                MakeTableHeader ("Margin region 2") <<
                MakeTableHeader ("Affix 2") << 
            EndTableRow     <<  endl;
    
        for (int i = 0; i < m_AffixAlignments.size(); i++)
        {    
            pAlign = m_AffixAlignments.at(i);
            LogStream << 
                StartTableRow <<
                    TableData( pAlign->GetShift1() ) <<
                    TableData( pAlign->GetMargin1()) <<
                    TableData( pAlign->GetAffix1() ) <<
                    TableData( pAlign->GetShift2() ) <<
                    TableData( pAlign->GetMargin2()) <<
                    TableData( pAlign->GetAffix2() ) <<
                EndTableRow << endl;
        }
        
		 
		
		LogStream           << EndTable;
    }

}
void CSignatureAlignment::Display(CMiniLexicon* mini)
{
    AffixAlignment* pAlign;

    if (m_AffixAlignments.count()  > 0)
    {
       mini->LogFileStartTable();
       mini->LogFileStartRow();
            mini->LogFile (m_SigPtr1->Display('-'));
            mini->LogFile (m_SigPtr2->Display('-'));
       mini->LogFileEndRow();
       mini->LogFileStartRow();
                mini->LogFileHeader ("Shift region 1");
                mini->LogFileHeader ("Margin region 1");
                mini->LogFileHeader ("Affix 1");
                mini->LogFileHeader ("Shift region 2");
                mini->LogFileHeader ("Margin region 2");
                mini->LogFileHeader ("Affix 2");
       mini->LogFileEndRow();


        for (int i = 0; i < m_AffixAlignments.size(); i++)
        {
            pAlign = m_AffixAlignments.at(i);
              mini->LogFileStartRow();
                mini->LogFileSimpleString( pAlign->GetShift1() );
                mini->LogFileSimpleString( pAlign->GetMargin1() );
                mini->LogFileSimpleString( pAlign->GetAffix1()  );
                mini->LogFileSimpleString( pAlign->GetShift2()  );
                mini->LogFileSimpleString( pAlign->GetMargin2() );
                mini->LogFileSimpleString( pAlign->GetAffix2()  );
              mini->LogFileEndRow();
        }




    }

}
