// Implementation of CStem description length methods
// Copyright © 2009 The University of Chicago
#include "Stem.h"
#include "MiniLexicon.h"
#include "Lexicon.h"
#include "StemCollection.h"
#include "log2.h"

double CStem::GetLengthOfPointerToMe() const
{
	if (m_LengthOfPointerToMe > 0)
		return m_LengthOfPointerToMe;
	m_LengthOfPointerToMe = log2(m_pMyMini->GetStems()->GetCorpusCount() /
	                             GetCorpusCount());
	return m_LengthOfPointerToMe;
}

double CStem::GetLengthOfPointerToMe_2()
{ return GetLengthOfPointerToMe(); }

double CStem::GetPhonologicalInformationContent(CLexicon* MotherLexicon) const
{
	if (m_PhonologicalContent != 0.0)
		return m_PhonologicalContent;

	if (MotherLexicon != 0)
		CalculatePhonologicalInformationContent(MotherLexicon);
	return m_PhonologicalContent;
}

double CStem::CalculatePhonologicalInformationContent(CLexicon* Lexicon) const
{
	m_PhonologicalContent = m_BigramLogProb > 0.0 ?
		m_BigramLogProb :
		CParse::ComputeDL(Lexicon->GetNumberOfCharacterTypes());
	return m_PhonologicalContent;
}

// Calculate the description length
//
// Returns:
//    float - the descrition length
float CStem::CalculateDL() const
{
	static const float PointerLength = log2(float(27.0));
	return PointerLength * GetKeyLength();
}
