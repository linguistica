// Part of speech discovery
// Copyright © 2009 The University of Chicago
#include "MiniLexicon.h"

#include <memory>
#include <QTextStream>
#include <QIODevice>
#include <QFile>
#include <QString>
#include "SignatureCollection.h"
#include "Signature.h"
#include "POS.h"
#include "POSCollection.h"
#include "Parse.h"
#include "HTML.h"

QString HTMLfileStart("<html><head> "
	"<link rel=\"stylesheet\" type=\"text/css\" href=\"lxa.css\"> "
	"</head>"
	"<body><table>");

void CMiniLexicon::FindMajorSignatures()
{
	// XXX. add more options, for prefixes, etc.
	// XXX. Make this a user-defined parameter
	const int NumberOfTopSignatures = 50;
	const int MinimumSignatureSize = 2;

	if (m_pPOS == 0) m_pPOS = new LxPoSCollection;

	LogFileSmallTitle("Parts of speech");
	LogFileStartTable();
	LogFile("Signature");

	// For each signature: if it's good enough, make it a part of speech
	int TooBigToIgnoreRankCutoff = 10;
	m_pSignatures->FindDisplayOrdering();
	for (int signo = 0, count = 0;
			signo < m_pSignatures->GetCount() &&
			count < NumberOfTopSignatures;
			++signo) {
		CSignature* pSig = m_pSignatures->GetAtSort(signo);

		LogFileStartRow(pSig->Display('-'));

		// hack: simulate labelled continue statement
		struct not_eligible { };
		try {
			// a sig P is a "mentor" to sig Q if P is more robust
			// than Q and P contains Q
			if (pSig->GetMentor() != 0)
				// not a part of speech: subsumed under mentor
				throw not_eligible();

			if (pSig->Size() < MinimumSignatureSize)
				// not a part of speech: too simple
				throw not_eligible();

			if (pSig->GetMentorList()->count() < 2 && signo > TooBigToIgnoreRankCutoff)
				// it has no mentees, and it is not among the very biggest signatures
				throw not_eligible();


			for (LxPoS* qPOS = m_pPOS->first(); qPOS != 0; qPOS=m_pPOS->next()) {
				// If pSig has exactly one more affix than a
				// signature already in PoS, then pSig can't
				// be a PoS, and its extra affix is entered as a satellite.
				CSignature* qSig = qPOS->GetSignature();

				if (qSig->Size() + 1 == pSig->Size() &&
						qSig->Contains(pSig)) {
					CParse Suffix = qSig->Intersection(*pSig);
					qSig->AppendSatelliteAffix(Suffix);
					LogFileSimpleString(Suffix.Display());
					throw not_eligible();
				}
			}
		} catch (not_eligible) { continue; }

		std::auto_ptr<LxPoS> new_pos(new LxPoS(pSig, this));
		LxPoS* pPOS = new_pos.get();

		// Record affix frequencies
		for (int k = 1; k <= pSig->Size(); ++k) {
			pPOS->SetPieceValue(k, pSig->GetNumberOfStems());
			LogFileSimpleString(pSig->GetPiece(k).Display());
		}

		// Calculate robustness
		pPOS->AddRobustness(pSig->GetRobustness());

		// Save part-of-speech
		m_pPOS->append(new_pos.release());
		++count;

		LogFileSimpleInteger(pSig->GetNumberOfStems());
		LogFileEndRow();

		// For each mentee:
		for (int signo = 0; signo < pSig->GetMentorList()->size(); signo++) {
			CSignature* qSig = pSig->GetMentorList()->at(signo);

			LogFileStartRow(qSig->Display('-'));

			pPOS->AppendSignature(qSig);
			// Adjust robustness for mentee.
			pPOS->AddRobustness(qSig->GetRobustness());

			// Adjust stem count
			for (int stemno = 0; stemno < qSig->GetNumberOfStems(); stemno++)
				pPOS->AddStem(qSig->GetStem(stemno));

			// Adjust affix frequencies for mentee.
			for (int affixno = 1; affixno <= qSig->Size(); ++affixno) {
				pPOS->IncrementPieceValue(qSig->GetPiece(affixno), qSig->GetNumberOfStems());

				LogFileSimpleString(qSig->GetPiece(affixno).Display());
				LogFileSimpleInteger(qSig->GetNumberOfStems());
			}
			LogFileEndRow();
		} // end of signo loop

		// Write affix frequencies to log.
		for (int m = 1; m <= pPOS->Size(); m++) {
			LogFileStartRow(pPOS->GetPiece(m).Display());
			LogFileSimpleDouble(pPOS->GetPieceValue(m));
			LogFileEndRow();
		}
	}
	LogFileEndTable();
}
