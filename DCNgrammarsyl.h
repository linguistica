// Grammar type for syllable discovery with Dynamic Computational Networks
// Copyright © 2009 The University of Chicago
#ifndef GRAMMARSYL_H
#define GRAMMARSYL_H

// See the corpussyl documentation in DCNcorpussyl.h for an overview.
// See also the very similar grammar type in DCNgrammar.h

#include <qlabel.h>
#include <qmap.h>
#include <qstring.h>

typedef QMap<QChar, float> SonorityMap;

/**
 *	A grammar holds the DCN's parameters. It's like an array of six
 *  floats, but much smarter, because you can make deep copies and
 *  you can print it really easily. It's an easy way to think of
 *  what the internal grammar of a language-user might be in the 
 *  DCN model.
 */
class grammarsyl
{

private:
	float alpha;
	float beta;
	SonorityMap map;

public:
	grammarsyl();
	grammarsyl(grammarsyl &theGrammar);
	virtual				~grammarsyl();

	void				setValues(float alpha, float beta);
	void				setSonority(QChar c, float sonority);
	void				clearMap() { map.clear(); }

	float				getAlpha() const { return alpha; }
	float				getBeta() const { return beta; }
	float				getSonority(QChar c) const;
	SonorityMap			getMap() { return map; }

	void				print(QLabel *label);
	QString				print();
	bool				isInMap(QChar c) const;

};

#endif // GRAMMARSYL_H
