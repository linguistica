// Implementation of the cMT class
// Copyright © 2009 The University of Chicago
#include "cMT.h"

#include <QMessageBox>
#include <Q3TextStream>
#include <QFile>
#include <Q3SortedList>
#include <QMap>
#include "cMTModel2Norm.h"
#include "cMTModel1.h"
#include "mTVolca.h"
#include "Typedefs.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

cMT::cMT(LinguisticaMainWindow* parent, QString projectDirectory)
	: m_parent(parent),
	m_projectDirectory(projectDirectory),
	m_Volca(),
	m_Model1(),
	m_Model2Norm(),
	m_MTLog("LinguisticaMTLog.txt") { }

cMT::~cMT()
{
	delete m_Model2Norm;
	delete m_Model1;
	delete m_Volca;
}

void cMT::readTrainingCorpus()
{
	delete m_Volca;
	m_Volca = new mTVolca(this, m_projectDirectory); 

	m_Volca ->initVolList(); 

	m_Volca ->readSentences(); 

	m_Volca ->setFastSearchPairsForT(); 

	QMessageBox::information(NULL, "Status", QString("Words # in language 1 is %1!").arg(m_Volca ->m_language1TotalWords), "OK"); 
	QMessageBox::information(NULL, "Status", QString("Words # in language 2 is %1!").arg(m_Volca ->m_language2TotalWords), "OK"); 

}

void cMT::trainModel1(int model1Iterations)
{
	delete m_Model1;
	m_Model1 = new cMTModel1(this, model1Iterations);

	m_Model1 ->initT(); 

	m_Model1 ->EMLoops(m_Model1 ->m_Iterations); 

	QMessageBox::information(NULL, "Status", "Logging T After Model1...!", "OK"); 
	logTAfterModel1(); 
	QMessageBox::information(NULL, "Status", "Done logging T After Model1...!", "OK"); 

}


void cMT::logTAfterModel1()
{
	int									language1WordId;
	int									language2WordId;
	QString								language1Word;
	QString								language2Word;
	double								TValue; 
	IntToIntToDouble::iterator			IntToIntToDoubleIt; 
	IntToDouble*						oneList; 
	IntToDouble::iterator				IntToDoubleIt; 
	mTSortedList						sortPlatForm; 
	mTForSortingItem*					oneSortItem; 
	int									outputTopLimit = 20; 
	int									outputIndex; 
	QFile								file( m_MTLog );
    
	if ( !file.open( QIODevice::WriteOnly | QIODevice::Append ) ) 
	{
		QMessageBox::information(NULL, "Error", "Can't Open the MT Log file!", "OK"); 
		return;    
	}

	Q3TextStream outf( &file );

	outf << "******T Tables After Model1******" << endl <<endl; 


	sortPlatForm.setAutoDelete(TRUE); 

	for ( IntToIntToDoubleIt = (m_Model1 ->m_T).begin(); IntToIntToDoubleIt != (m_Model1 ->m_T).end(); IntToIntToDoubleIt++)
	{
		language1WordId = IntToIntToDoubleIt.key();
		language1Word = (m_Volca ->m_language1WordIndex)[language1WordId] ;
		outf << language1Word << "	:"<<endl ;

		oneList = IntToIntToDoubleIt.data(); 

		for (IntToDoubleIt = oneList ->begin(); IntToDoubleIt != oneList ->end(); IntToDoubleIt++)
		{
			language2WordId = IntToDoubleIt.key(); 
			language2Word = (m_Volca ->m_language2WordIndex)[language2WordId] ;

			TValue = IntToDoubleIt.data(); 

			oneSortItem = new mTForSortingItem(language2Word, TValue, 1); 
			
			sortPlatForm.append(oneSortItem);
		}

		sortPlatForm.sort();
			
		outputIndex = 0; 
		for ( oneSortItem=sortPlatForm.first(); oneSortItem != 0; oneSortItem=sortPlatForm.next())
		{
			outputIndex++; 

			language2Word = oneSortItem ->m_name; 
			TValue = oneSortItem ->m_doubleValue; 
			
			outf <<"	"<< outputIndex <<" : " <<language2Word << "	" << TValue <<endl ;
			
			if ( outputIndex >= outputTopLimit) break; 

		}

		sortPlatForm.clear(); 
	}

	
	file.close(); 

}


void cMT::trainModel2Norm(int model2Iterations, bool getTFromModel1)
{
	delete m_Model2Norm;
	m_Model2Norm = new cMTModel2Norm(this, model2Iterations, getTFromModel1);

	m_Model2Norm ->initTandA(); 

	m_Model2Norm ->EMLoops(m_Model2Norm ->m_Iterations); 

	QMessageBox::information(NULL, "Status", "Doing Viterbi for Model2Norm...", "OK"); 

	m_Model2Norm ->viterbiAll(); 

	QMessageBox::information(NULL, "Status", "Logging TandA After Model2Norm...!", "OK");
	
	logTandAAfterModel2Norm(); 
	
	QMessageBox::information(NULL, "Status", "Done logging TandA After Model2Norm...!", "OK"); 

}


void cMT::logTandAAfterModel2Norm()
{
	int									language1WordId;
	int									language2WordId;
	QString								language1Word;
	QString								language2Word;
	int									language1ChunkId;
	int									language2ChunkId;
	QString								language1ChunkStr;
	QString								language2ChunkStr;
	double								TValue; 
	double								AValue; 
	IntToIntToDouble::iterator			IntToIntToDoubleIt; 
	IntToDouble*						oneList; 
	IntToDouble::iterator				IntToDoubleIt; 
	mTSortedList						sortPlatForm; 
	mTForSortingItem*					oneSortItem; 
	int									outputTopLimit = 20; 
	int									outputIndex; 
	QFile								file( m_MTLog );
    
	if ( !file.open( QIODevice::WriteOnly | QIODevice::Append ) ) 
	{
		QMessageBox::information(NULL, "Error", "Can't Open the MT Log file!", "OK"); 
		return;    
	}

	Q3TextStream outf( &file );

	// Log T
	outf << "******T Tables After Model2Norm******" << endl <<endl; 


	sortPlatForm.setAutoDelete(TRUE); 

	for ( IntToIntToDoubleIt = (m_Model2Norm ->m_T).begin(); IntToIntToDoubleIt != (m_Model2Norm ->m_T).end(); IntToIntToDoubleIt++)
	{
		language1WordId = IntToIntToDoubleIt.key();
		language1Word = (m_Volca ->m_language1WordIndex)[language1WordId] ;
		outf << language1Word << "	:"<<endl ;

		oneList = IntToIntToDoubleIt.data(); 

		for (IntToDoubleIt = oneList ->begin(); IntToDoubleIt != oneList ->end(); IntToDoubleIt++)
		{
			language2WordId = IntToDoubleIt.key(); 
			language2Word = (m_Volca ->m_language2WordIndex)[language2WordId] ;

			TValue = IntToDoubleIt.data(); 

			oneSortItem = new mTForSortingItem(language2Word, TValue, 1); 
			
			sortPlatForm.append(oneSortItem);
		}

		sortPlatForm.sort();
			
		outputIndex = 0; 
		for ( oneSortItem=sortPlatForm.first(); oneSortItem != 0; oneSortItem=sortPlatForm.next())
		{
			outputIndex++; 

			language2Word = oneSortItem ->m_name; 
			TValue = oneSortItem ->m_doubleValue; 
			
			outf <<"	"<< outputIndex <<" : " <<language2Word << "	" << TValue <<endl ;
			
			if ( outputIndex >= outputTopLimit) break; 

		}

		sortPlatForm.clear(); 
	}

	// Log A
	outf << "******A Tables After Model2Norm******" << endl <<endl; 


	sortPlatForm.setAutoDelete(TRUE); 

	for ( IntToIntToDoubleIt = (m_Model2Norm ->m_A).begin(); IntToIntToDoubleIt != (m_Model2Norm ->m_A).end(); IntToIntToDoubleIt++)
	{
		language2ChunkId = IntToIntToDoubleIt.key();
		language2ChunkStr = QString("%1").arg(language2ChunkId); 
		outf << language2ChunkStr << "	:"<<endl ;

		oneList = IntToIntToDoubleIt.data(); 

		for (IntToDoubleIt = oneList ->begin(); IntToDoubleIt != oneList ->end(); IntToDoubleIt++)
		{
			language1ChunkId = IntToDoubleIt.key(); 
			language1ChunkStr = QString("%1").arg(language1ChunkId);

			AValue = IntToDoubleIt.data(); 

			oneSortItem = new mTForSortingItem(language1ChunkStr, AValue, 1); 
			
			sortPlatForm.append(oneSortItem);
		}

		sortPlatForm.sort();
			
		outputIndex = 0; 
		outputTopLimit = 100; 
		for ( oneSortItem=sortPlatForm.first(); oneSortItem != 0; oneSortItem=sortPlatForm.next())
		{
			outputIndex++; 

			language1ChunkStr = oneSortItem ->m_name; 
			AValue = oneSortItem ->m_doubleValue; 
			
			outf <<"	" <<language1ChunkStr << "	" << AValue <<endl ;
			
			if ( outputIndex >= outputTopLimit) break; 

		}

		sortPlatForm.clear(); 
	}
	

	// Log Viterbi Alignments after model 2
	outf << "******Viterbi Alignments After Model2Norm******" << endl <<endl; 

	mTVolca*						myVolca; 
	int								i; 
	int								l,m; 
	IntToInt*						oneLan1Sentence;
	IntToInt*						oneLan2Sentence;
	IntToInt*						oneAlignment;
	int								language1SentenceLen;
	int								language2SentenceLen;
	QString							oneWordStr; 
	QString							twoWordStr; 

	

	myVolca = m_Volca; 

	for ( i=0; i < myVolca ->m_countOfSentences; i++)
	{
		outf <<"Sentence " << i << " : " << endl;
		
		oneLan1Sentence = myVolca ->m_language1Sentences[i];
		oneLan2Sentence = myVolca ->m_language2Sentences[i];

		language1SentenceLen = oneLan1Sentence ->size();
		language2SentenceLen = oneLan2Sentence ->size();
	
		// Output sentence in lan1
		outf << "Sentence 1 -> ";
		for ( l =0; l < language1SentenceLen; l++)
		{
			language1WordId = (*oneLan1Sentence)[l];
			oneWordStr = (m_Volca ->m_language1WordIndex)[language1WordId] ;
			outf << l << " : " <<  oneWordStr << "	";
		}

		outf << endl; 

		// Output sentence in lan2
		outf << "Sentence 2 -> ";
		for ( m =0; m < language2SentenceLen; m++)
		{
			language2WordId = (*oneLan2Sentence)[m];
			oneWordStr = (m_Volca ->m_language2WordIndex)[language2WordId] ;
			outf << m << " : " <<  oneWordStr << "	";
		}

		outf << endl; 

		// Output the alignment
		outf << "Alignments are language2Word -> language1Word" << endl; 
		oneAlignment = (myVolca ->m_sentenceAlignments)[i];
		
		for ( m=0; m< language2SentenceLen; m++)
		{
			language2WordId = (*oneLan2Sentence)[m];
			twoWordStr = (m_Volca ->m_language2WordIndex)[language2WordId] ;

			language1WordId = (*oneLan1Sentence)[(*oneAlignment)[m]];
			oneWordStr = (m_Volca ->m_language1WordIndex)[language1WordId] ;
			
			outf << "	" << twoWordStr << " --> " << oneWordStr << endl;

		}

	}	

	file.close(); 

}

