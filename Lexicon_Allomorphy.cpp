// Some methods for discovering allomorphs of stems
// Copyright © 2009 The University of Chicago
#include "MiniLexicon.h"

#include <memory>
#include <QList>
#include <QMap>
#include "Signature.h"
#include "Compound.h"
#include "Suffix.h"
#include "Stem.h"
#include "SignatureCollection.h"
#include "CompoundCollection.h"
#include "SuffixCollection.h"
#include "WordCollection.h"
#include "StemCollection.h"
#include "CollectionTemplate.h"
#include "StringSurrogate.h"
#include "StringFunc.h"
#include "HTML.h"
#include "Typedefs.h"

typedef QMap<QString,QString>	StringToString;

/*

	Finding rules: first we find pairs of similar stems, and see HOW they differ.


	Suppose we find a lot of stems that differ by final -e. Then we look at the smaller stem's
	signature. If it contains a high incidence of
	(a) suffix -e;
	(b) one or two other suffixes F1, F2
	(c) F1 and F2 "go well"  with the signature of the larger stem(s);

	then:
	(d) Consider the hypothesis that F1 and F2 are of the form <e>F1 and <e>F2


*/

struct SigLetter {
	CSignature* m_SigPointer;
	CParse m_Letter;

	SigLetter(CSignature* Sig, const CStringSurrogate& SS)
		: m_SigPointer(Sig), m_Letter(SS) { }
};

void CMiniLexicon::RelateStems()
{
	const CStringSurrogate CSSofQNULL(TheStringNULL);
	const int MinimumSuffixCount = 5;

        LogFileLargeTitle( "Finding allomorphy (Version 2002)");
	m_pSuffixes->Sort(COUNT);

	// XXX. consider using boost::optional instead.
	std::auto_ptr<CParse> pPossibleDeletingSuffixes;

	// Major loop over deletable pseudo-suffixes
        QList<SigLetter*> SignaturesToFixList;
        //SignaturesToFixList.setAutoDelete(true);    //fix this -- so it doesn't become a memory leak. @@@
	CSuffixCollection SuffixesToEliminate;
        for (int suffixno = 0; suffixno < m_pSuffixes->GetCount(); ++suffixno) {
                CSuffix* qSuffix = m_pSuffixes->GetAtSort(suffixno);
		if (qSuffix->GetKeyLength() != 1)
			continue;
		if (qSuffix->GetUseCount() < MinimumSuffixCount)
			continue;

		/// possible suffix (like 'e' in English)
		CStringSurrogate ssDiffLetter = qSuffix->GetKey();
                LogFileSmallTitle(ssDiffLetter.Display());

		pPossibleDeletingSuffixes = std::auto_ptr<CParse>(new CParse);

		// Find all suffix candidates that MIGHT delete this DiffLetter
		// if DiffLetter == 'e', SuffixCandidates includes ing, ity.
                LogFileHeader("Suffixes that do not follow this suffix", "Count");
		{
			bool found = false;
                        for (int suffixno2 = 1; suffixno2 < m_pSuffixes->GetCount(); ++suffixno2) {
                                CSuffix* pSuffix = m_pSuffixes->GetAt(suffixno2);

				if (pSuffix->GetUseCount() < MinimumSuffixCount)
					continue;

				CStringSurrogate ssSuffix = pSuffix->GetKey();
				int TotalStemsWithSuffix;
				int HowManyEndWithThisLetter;
				HowManyStemsWithThisSuffixEndInThisLetter(
						ssSuffix,
						ssDiffLetter,
						TotalStemsWithSuffix,
						HowManyEndWithThisLetter);

				// XXX. Use “Threshold = 0.05” instead?

				const int MinimumStemCount = 5;
				const int MaximumExceptionCount = 5;

				// if suffix doesn’t begin with DiffLetter
				// and its count is very small, ignore it.
				if (TotalStemsWithSuffix <= MinimumStemCount &&
						ssSuffix.Left(ssDiffLetter.GetLength())
							!= ssDiffLetter)
					continue;
                                LogFile(ssSuffix.Display(), HowManyEndWithThisLetter);

				if (HowManyEndWithThisLetter <= MaximumExceptionCount) {
					pPossibleDeletingSuffixes->Append(ssSuffix);
					found = true;
				}
			}

                        if (!found) LogFileSmallTitle( "None found");
		}

		// Simple signatures (2 affixes)
		// We now have a set of candidates that might delete DiffLetter
		// We will look at each signature, and see whether it
		// erroneously has a suffix that should really be thought
		// of as deleting from the stem rather than as a suffix.

		// 1. In our first test, we will look for signatures of the
		//    form: Letter.OtherSuffix, where OtherSuffix does not
		//    appear after stems ending in Letter, and where
		//    NULL.OtherSuffix does occur as a signature.

                LogFileHeader("Suspicious signature","Paired signature" );
		{
			bool found = false;
                        for (int signo = 0; signo < m_pSignatures->GetCount(); ++signo) {
                                CSignature* pSig = m_pSignatures->GetAt(signo);

				if (pSig -> Size() != 2)
					continue;
				if (!pSig -> Contains(ssDiffLetter))
					continue;

				QString OtherSuffix;
				if (pSig -> GetPiece(1) == ssDiffLetter)
					OtherSuffix = pSig->GetPiece(2).Display();
				else
					OtherSuffix = pSig->GetPiece(1).Display();

				if (OtherSuffix == TheStringNULL)
					continue;
				if (!pPossibleDeletingSuffixes->Contains(OtherSuffix))
					// OtherSuffix doesn't appear after DiffLetter
					continue;
                                LogFile(pSig->Display());
				found = true;

				CParse TestSig;
				TestSig.Append(TheStringNULL);
				TestSig.Append(OtherSuffix);
				TestSig.Alphabetize();

				if (*m_pSignatures ^= TestSig) {
					SuffixesToEliminate << ssDiffLetter;

					std::auto_ptr<SigLetter> pSigLetter(
						new SigLetter(pSig, ssDiffLetter));
					SignaturesToFixList.append(
						pSigLetter.release());
                                        LogFile(TestSig.Display());
				} else {
                                    LogFile(TestSig.Display(), " not found.");
				}
			}

                        if (!found) LogFile("None found.");
                        LogFileEndTable();

		}
		// NB: We should do the preceding test in a more
		// general way, that is:
		// Look for pairs of signatures of the form
		// A.B and A.xB, where x is the deleting letter;
		// the previous case is like this and where B is NULL.
		// Then A deletes x, i.e., A is really <x>A.

		// 2. In our other test, we look at the signatures
		// that contain both DiffLetter and one of the deleters,
		// and see, first of all, if a suffix appears more often
		// with NULL than with DiffLetter;
		// or, the suffix actually is of the form aX,
		// where a = DiffLetter, X occurs with NULL more
		// than aX occurs with a.

		if (!(SuffixesToEliminate ^= ssDiffLetter))
			continue;

                for (int signo = 0; signo < m_pSignatures->GetCount(); ++signo) {
                        CSignature* pSig = m_pSignatures->GetAt(signo);
			if (!pSig -> Contains(ssDiffLetter))
				continue;

                        for (int affixno = 1; affixno <= pSig->Size(); ++affixno) {
                                CStringSurrogate ssSuffix = pSig->GetPiece(affixno);
				CSuffix* pSuffix = *m_pSuffixes ^= ssSuffix;

				if (pSuffix->GetDeletees()->Contains(ssDiffLetter))
					// we've already determined it deletes DiffLetter
					continue;

				if (StemsWithBothSuffixes(TheStringNULL, ssSuffix) >
						StemsWithBothSuffixes(
							ssDiffLetter, ssSuffix))
					// probably a deleter
					pSuffix->AddDeletee(ssDiffLetter);

				if (ssSuffix.Left(ssDiffLetter.GetLength()) ==
						ssDiffLetter) {
					// probably cut wrong, like "ement"
					CStringSurrogate ssTruncatedForm =
						ssSuffix.Mid(
						ssDiffLetter.GetLength());

					if (StemsWithBothSuffixes(TheStringNULL,
							ssTruncatedForm) >
							StemsWithBothSuffixes(
								ssDiffLetter,
								ssSuffix))
						pSuffix->AddDeletee(ssDiffLetter);
				}
			}
		}

		// Now put onto SignaturesToFixList all of the sigs that
		// are composed ONLY of deleting suffixes.
                LogFileHeader("Signatures to fix");
                for (int signo = 0; signo < m_pSignatures->GetCount(); ++signo) {
                        CSignature* pSig = m_pSignatures->GetAt(signo);

			if (!pSig->Contains(ssDiffLetter))
				continue;

			bool bFoundDeletee = false;
			bool bFoundDeleter = false;
                        for (int affixno = 1; affixno <= pSig->Size(); ++affixno) {
                                const CStringSurrogate ssSuffix = pSig->GetPiece(affixno);
				if (ssSuffix == ssDiffLetter) {
					bFoundDeletee = true;
					continue;
				}

				CSuffix* pSuffix = *m_pSuffixes ^= ssSuffix;
				Q_ASSERT(pSuffix != 0);
				if (!pSuffix->GetDeletees()
						->Contains(ssDiffLetter)) {
//					break;
				} else {
					bFoundDeleter = true;
				}
			}

			if (bFoundDeletee && bFoundDeleter) {
				std::auto_ptr<SigLetter> pSigLetter(new SigLetter(
						pSig, ssDiffLetter));
                                LogFile(pSigLetter->m_SigPointer->Display());
				SignaturesToFixList.append(pSigLetter.release());
			}
		}

                LogFileEndTable();
	} // end of checking this letter to see if it ever deletes.
	// End of Major loop over deletable pseudo-suffixes
        LogFileSmallTitle("Now we do the changing for the simple signatures.");


        foreach (SigLetter* pSigLetter, SignaturesToFixList ) {
		CSignature* pSig = pSigLetter->m_SigPointer;
		CStringSurrogate ssDiffLetter = pSigLetter->m_Letter;

		// XXX. log:
		//	"1. Moving material from suffixes to stems:
		//	    ${pSigLetter->m_Letter}, in signature:
		//	    ${*pSigLetter->m_SigPointer}

		// this is where the real work happens
//		pPossibleDeletingSuffixes =
//			SuffixesWhichMightDeleteKey.find(ssDiffLetter.Display()));

		// XXX. pPossibleDeletingSuffixes kept on getting recreated
		// before, so something is probably awry.
		if (pPossibleDeletingSuffixes.get())
			MoveWordsStemSuffixBoundaryToRight(pSig,
				ssDiffLetter, pPossibleDeletingSuffixes.get());
	}

	// XXX. log: Suffixes to eliminate: ${SuffixesToEliminate}

	m_pSignatures->CleanUp();
	SignaturesToFixList.clear();
	m_pSignatures->CleanUp();

	// go through all the signatures,
	// and identify those that should be fixed.
	// For example, the signature e.<e>ing.es
	// will be marked to become NULL.<e>ing.s.		/
        LogFileSmallTitle("Bigger signatures");
        LogFile ("How many suffixes to eliminate? ", SuffixesToEliminate.GetCount());

        for (int suffixno = 0; suffixno < SuffixesToEliminate.GetCount(); ++suffixno) {
                CSuffix* const pSuffixToEliminate = SuffixesToEliminate[suffixno];
		const CStringSurrogate ssSuffixToEliminate =
			pSuffixToEliminate->GetKey();

                LogFileSmallTitle(ssSuffixToEliminate.Display());
                LogFileHeader("Signature", "Suffix", "Disposition");

                for (int signo = 0; signo < m_pSignatures->GetCount(); ++signo) {
                        CSignature* const pSig = m_pSignatures->GetAt(signo);

			bool ThisSigContainsDeletingSuffix = false;
			if (pSig->Contains(ssSuffixToEliminate) &&
					pSig->Size() > 2) {
                                LogFile(pSig->Display());

                                for (int affixno = 1; affixno <= pSig->Size(); ++affixno) {
					CStringSurrogate ssSuffix =
                                                        pSig->GetPiece(affixno);
                                        LogFile(ssSuffix.Display());

					if (ssSuffix == ssSuffixToEliminate) {
						// 2. Suffix is the one we want
						// to make into NULL
                                                LogFile("Found the deleting (pseudo-) suffix ");
						continue;
					}

					if (pPossibleDeletingSuffixes.get() &&
							pPossibleDeletingSuffixes
							->Contains(ssSuffix)) {
						// 3. Suffix deletes the suffix
						// we want to make NULL --
						ThisSigContainsDeletingSuffix = true;

						// XXX. CStringSurrogate::StartsWith
						if (ssSuffix.Left(
							ssSuffixToEliminate.GetLength())
							== ssSuffixToEliminate) {
							// this is the "ement" case
							// -- we want to change the
							// suffix,
							// not make it an e-deleter

							// XXX. SuffixChangesToMake
							// map never populated
							QString NewSuffix = "";


                                                        LogFile("We found the pseudosuffix at the beginning of a poor suffix; will change to: ",NewSuffix);
							continue;
						}
                                                LogFile("We found the pseudosuffix deleting before ", ssSuffix.Display());						
						continue;
                                        } else if (ssSuffix.Left( ssSuffixToEliminate.GetLength()) ==	ssSuffixToEliminate)
                                        {
						// 4. Suffix starts with the suffix
						// we want to make NULL
                                                LogFile("We found ", ssSuffix.Display());
						continue;
					}    
                                        LogFile("Not a deleter:", (ssSuffix.Display()));
				} // end of checking each suffix in the signature
			}

			if (!ThisSigContainsDeletingSuffix)
				continue;

			std::auto_ptr<SigLetter> pSigLetter(new SigLetter(
					pSig, ssSuffixToEliminate));
			SignaturesToFixList.append(pSigLetter.release());
		} // end of signature loop

                LogFileEndTable();
	} // end of loop over suffixes being eliminated.

        LogFileSmallTitle("Now we do the changing for more complex signatures");

	foreach (SigLetter* pSigLetter, SignaturesToFixList) {
		CSignature* pSig = pSigLetter->m_SigPointer;
		CStringSurrogate ssDiffLetter = pSigLetter->m_Letter;

                LogFile (QString(" Moving material from suffixes to stems: "),pSigLetter->m_Letter.Display(), QString(", in signature: "),
                        pSigLetter->m_SigPointer->Display());

		// this is where the real work happens:

		MoveWordsStemSuffixBoundaryToRight(pSig,
				pSigLetter->m_Letter.Display(),
				pPossibleDeletingSuffixes.get());
	}
        LogFileHeader ("Suffixes to eliminate: ");
        for (int suffixno = 0; suffixno < SuffixesToEliminate.GetCount(); ++suffixno){
            LogFile( SuffixesToEliminate[suffixno]->Display() );}
        LogFileEndTable();

	m_pSignatures->CleanUp();
}




//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

/* The following function takes a stem, and adds FinalLetter to it, and then
	adds all the other suffixes in the stem's old SuffixList to the bigger
	stem -- whether that stem already existed or not.
	We should be sure to get rid of this stem only if ALL of its suffixes
	are shifted -- and many may not be.
*/

//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

void CMiniLexicon::ShiftFinalLetterToStem (CStem* pStem, QString& FinalLetter )
{
	QString				Word,
						Suffix,
						NewStem,
						Stem = pStem->GetKey().Display();
	CSS					ssWord,
						ssSuffix,
						ssStem;
	CStem*				pWord;
	CParse*				SuffixList = pStem->GetSuffixList();
	CSignature*			pNewSig;
	CSuffix*			pSuffix;


	ssStem = *pStem;



        for (int suffixno = 1; suffixno <= SuffixList->Size(); suffixno++)
	{
                Suffix = SuffixList->GetPiece(suffixno).Display();
		if ( Suffix == FinalLetter )
		{
			continue;
		}


		Word = Stem + Suffix;
		pWord = *m_pWords ^= Word;
		Suffix = "<" + FinalLetter + ">" + Suffix;
		pSuffix  = *m_pSuffixes << ssSuffix;

		NewStem = ssStem.Display() + FinalLetter;
		CStem* pBiggerStem = *m_pStems ^= NewStem;

		if (pBiggerStem != 0) {
			// the bigger stem already exists....
			pBiggerStem->AddSuffix( Suffix );
			pNewSig = *m_pSignatures << pBiggerStem->GetSuffixList();

			// ...get rid of OLD signature in Signatures;


			// Fix word structure



		}
		else
		{
			// the bigger stem is new...

		}

		pWord->AttachWordAndSuffixalStem(pBiggerStem);
	}


}


void CMiniLexicon::HowManyStemsWithThisSuffixEndInThisLetter (
													CStringSurrogate& Suffix,
													CStringSurrogate& Letter,
													int&		 TotalStemsWithSuffix,
													int&		 HowManyEndWithThisLetter )

{
	CSignature*			pSig;
	CSS					ssStem;
	CStem*              pStem;

	TotalStemsWithSuffix = 0;
	HowManyEndWithThisLetter = 0;

    Q_ASSERT (Letter.Display() != "e" || Suffix.Display() != "ing");

        for (int signo = 0; signo < m_pSignatures->GetCount(); signo++)
	{
                pSig = m_pSignatures->GetAt(signo);
		if ( ! pSig->Contains ( Suffix ) ) { continue; }

            for (int stemno= 0; stemno < pSig->GetStemPtrList()->size(); stemno++)
            {   pStem = pSig->GetStemPtrList()->at(stemno);
                ssStem = pStem->GetKey();
                TotalStemsWithSuffix++;
                if ( ssStem.Right (Letter.GetLength() ) == Letter )
                {
                        HowManyEndWithThisLetter++;
                }
            }
	}
}

int	CMiniLexicon::StemsWithBothSuffixes(CStringSurrogate& ssSuffix1, CStringSurrogate& ssSuffix2)
{
	int				count = 0;
	CSignature*		pSig;


        for (int signo = 0; signo < m_pSignatures->GetCount(); signo++)
	{
                pSig = m_pSignatures->GetAt(signo);
		if ( pSig->Contains (ssSuffix1) && pSig->Contains(ssSuffix2) )
		{
			count += pSig->GetNumberOfStems();
		}
	}
	return count;
}
int	CMiniLexicon::StemsWithBothSuffixes(QString  Suffix1, CStringSurrogate& ssSuffix2)
{
        int		count = 0;
        CSignature*	pSig;


        for (int signo = 0; signo < m_pSignatures->GetCount(); signo++)
	{
                pSig = m_pSignatures->GetAt(signo);
		if ( pSig->Contains (Suffix1) && pSig->Contains(ssSuffix2) )
		{
			count += pSig->GetNumberOfStems();
		}
	}
	return count;
}

/*
void CMiniLexicon::MoveWordsStemSuffixBoundaryToRight(CSignature* pThisSig,
													 CStringSurrogate& ssDeletee,
													 CTypedPtrMap<CMapStringToString, CString, CString>& Remapper
													)
{
	CParse				PNewSig,
						POldSig,
						PMergedSig,
						PNewStem,
						PDeletee = ssDeletee;

	POSITION			Pos;
	bool				bModifiedSigExisted		= FALSE;
	CSignature			*pLargerModifiedSig,
						*pModifiedSig			= NULL,
						*pOldSigBecameSig		= NULL,
						*pOlderSig				= NULL;

	CStringSurrogate	ssStem,
						ssSuffix,
						ssNewSuffix;
	CStem*				pOldStem;

	CStem*				pNewStem;
	CParse				DummyParse,
						Word,
						WhatSigWillBecome,
						SuffixChanges;
	CSuffix*			pSuffix;
	CStem *				pWord;
	bool				bStemShouldRemain		= FALSE;
	bool				bOldSigRemains			= FALSE;
	bool				bNewStemAlreadyExisted	= FALSE;
	bool				val;
	int					i;
	//----------------------------------------------------------------//





	int* bStatus = new int [ pThisSig->Size() + 1];

	if ( m_LogFile )
	{
				*m_LogFile <<	"\n\tF1: Creating new signature: "	<<
								PNewSig.Display() <<
								" Residue of old sig: "<<
								WhatSigWillBecome.Display();
	}


	PNewSig = CreateADeletingSignature(
										pThisSig,
										PDeletee,
										SuffixChanges,
										bStatus,
										WhatSigWillBecome,
										Remapper
										);
	if ( m_LogFile )
	{
				*m_LogFile <<	"\n\tF1: Creating new signature: "	<<
								PNewSig.Display() <<
								" Residue of old sig: "<<
								WhatSigWillBecome.Display();
	}

	// TODO: fix this later. Eg local:  NULL.e.ly, creates a signature just NULL with locale. Not useful.
	if ( PNewSig.Size() == 1 ) {return; }

	// TODO: fix this later. Problem is what's left over is just NULL: e.g. NULL.e.ed
	if ( CSS( WhatSigWillBecome ) == CSS ( CString("NULL") ) )  { return; }


	pModifiedSig = *Signatures ^= PNewSig;
	if (! pModifiedSig )
	{
		pModifiedSig = *Signatures <<  &PNewSig ;
	}

	pModifiedSig->SetRemark ( pThisSig->GetRemark() + " + allomorphy" );

	if ( WhatSigWillBecome.Size() > 0 )
	{
		bOldSigRemains = TRUE;
		pOldSigBecameSig = *Signatures << &WhatSigWillBecome;
		pOldSigBecameSig  ->SetRemark( CString("Allomorphy2") );
	}

	//----------------------------------------------------------------//
	//	Loop through stems:
	//----------------------------------------------------------------//

	Pos = pThisSig->GetStemPtrList()->GetHeadPosition();
	CTypedPtrList<CPtrList, CStem*>* Temp = pThisSig->GetStemPtrList();
	while (Pos)
	{
		pOldStem		= pThisSig->GetStemPtrList()->GetNext(Pos);
		PNewStem		= pOldStem->GetPiece()    +     ssDeletee;
		PNewStem		.SimplifyParseStructure();
		pNewStem		= *Stems_Suffixed ^= PNewStem;



		if ( pNewStem )		// -- if the larger stem ("love") already existed
		{
				if ( m_LogFile ) { *m_LogFile << "\t NewStem: "<< pNewStem->GetKey()  << " existed.";		}

				bNewStemAlreadyExisted = TRUE;
				pLargerModifiedSig =
							ModifyAnExistingSignatureWithANewStem (	pOldStem, pNewStem, pThisSig );
		}
		else
		{
				if ( m_LogFile ) { *m_LogFile << "\t NewStem: "<< PNewStem.Display()  << " did not exist; it does now.";		}

				bNewStemAlreadyExisted = FALSE;
				pNewStem = CreateANewSignatureForThisStem ( PNewStem, pModifiedSig );

				val = pThisSig		->RemoveStem		( pOldStem );
				Q_ASSERT (val);


				if ( bOldSigRemains )
				{
					pOldStem			->SetSuffixList			( &WhatSigWillBecome );
					pOldStem			->SetSuffixSignature	( pOldSigBecameSig  );
				}
		}


		if ( m_LogFile ) { *m_LogFile << "\n\t\tF1: Changing stem: "<< pOldStem->GetKey() ;		}

		// go through each word associated with the OldStem, and change its suffix pointer.
		for (  i = 1; i <= (int) pThisSig->Size(); i++)
		{
			if ( bStatus[i] == FALSE )
			{
				bStemShouldRemain = TRUE;
				ssSuffix = pThisSig->GetPiece(i);
				if ( ssSuffix.IsNULL() )
				{
					Word = *pOldStem;
				}
				else
				{
					Word = pOldStem->GetPiece() + ssSuffix ;
				}

				LinkThisWordToThisSignature ( Word, pOldSigBecameSig );
				pThisSig		->RemoveWord (pWord);

				if ( !pOldSigBecameSig->GetStemPtrList()->Find( pOldStem ) )
				{
					pOldSigBecameSig->GetStemPtrList()->AddTail (pOldStem);
				}

				continue;
			} // this means the suffix doesn't delete the deleting suffix or anything

			ssNewSuffix		= SuffixChanges.GetPiece(i);

			pSuffix			= *Suffixes ^= ssNewSuffix;
			if ( ssNewSuffix.IsNULL() )
			{
				Word = PNewStem;
			}
			else
			{
				Word = pOldStem->GetPiece() + pThisSig->GetPiece(i) ;
			}

			pWord			= *Words ^= CStringSurrogate( Word );
			pWord			->AppendToConfidence( CString ("Allomorphy 2") );
			pWord			->SetSuffixPtr ( pSuffix );
			val = pThisSig	->RemoveWord (pWord);
	//		Q_ASSERT (val);

			// case like hostilit-y becoming hostility
			if ( ssNewSuffix.IsNULL() )
			{
				pWord->DoNotParse();
			}

			if ( bNewStemAlreadyExisted )
			{
				pWord				->SetSuffixSignature ( pLargerModifiedSig );
				pWord				->SetStemPtr ( pNewStem );
				pLargerModifiedSig	->AttachToSuffixSig ( pNewStem );
			}
			else
			{
				if ( m_LogFile ) { *m_LogFile << "\t NewStem: "<< PNewStem.Display()  << " did not exist.";		}
				pModifiedSig->AttachToSuffixSig(
					pNewStem, CSignature::eDo_Not_Call_Words);
				pWord->AttachSuffixSignature(pModifiedSig);
				pWord->SetStemPtr(pNewStem);
			}


		}// cycle through the suffixes


		if ( bStemShouldRemain == FALSE )
		{
			Stems_Suffixed		->RemoveMember( pOldStem->GetPiece() );
		}


	}  // cycle of stems


}
*/
//		This is DEPRECATED! Not used in new code. The following version is used, the one underneath this.
void	CMiniLexicon::MoveWordsStemSuffixBoundaryToRight(CSignature* pThisSig,	 QString Deletee,  CParse* pSuffixCandidates )
{
	CSS ssDeletee;
	ssDeletee = Deletee;
	MoveWordsStemSuffixBoundaryToRight(pThisSig,  Deletee, pSuffixCandidates);
};

void	CMiniLexicon::MoveWordsStemSuffixBoundaryToRight(CSignature* pThisSig,
													 CStringSurrogate& ssDeletee,
													 CParse* pSuffixCandidates //3/04
													)
{
	CParse				PNewSig,
                                        POldSig,
                                        PMergedSig,
                                        PNewStem,
                                        PDeletee = ssDeletee;

        CSignature*                     pModifiedSig = NULL;
        CSignature*                     pOldSigBecameSig = NULL;
        CSignature*                     pOlderSig = NULL;

        CStringSurrogate                ssSuffix,
                                        ssNewSuffix;
	CStem*				pOldStem;
	CStem*				pNewStem;
        CParse				Word,
                                        WhatSigWillBecome,
                                        SuffixChanges;
	CSuffix*			pSuffix;
	CStem *				pWord;
	bool				bStemShouldRemain = FALSE;
	bool				bOldSigRemains = FALSE;
	bool				bNewStemAlreadyExisted = FALSE;
	bool				val;	
	//----------------------------------------------------------------//



	// this actually changes the signature itself:

	int* bStatus = new int [ pThisSig->Size() + 1];


	PNewSig = CreateADeletingSignature(
										pThisSig,
										PDeletee,
										SuffixChanges,
										bStatus,
										WhatSigWillBecome,
										pSuffixCandidates
										);
        LogFile(  pThisSig->Display(),
                  QString(" Creating new signature:"),
                  PNewSig.Display('-'),
                  QString(" Residue of old sig: "),
                  WhatSigWillBecome.Display() );	

	// TODO: fix this later. Eg local:  NULL.e.ly, creates a signature just NULL with locale. Not useful.
	if ( PNewSig.Size() == 1 ) {return; }

	// TODO: fix this later. Problem is what's left over is just NULL: e.g. NULL.e.ed
	if ( CSS( WhatSigWillBecome ) == CSS ( QString("NULL") ) )  { return; }

	pModifiedSig = *m_pSignatures ^= PNewSig;
	if (! pModifiedSig )
	{
		pModifiedSig = *m_pSignatures <<  &PNewSig ;
	}

	pModifiedSig->SetRemark ( pThisSig->GetRemark() + " + allomorphy" );

	if ( WhatSigWillBecome.Size() > 0 )
	{
		bOldSigRemains = TRUE;
		pOldSigBecameSig = *m_pSignatures << &WhatSigWillBecome;
		pOldSigBecameSig  ->SetRemark( QString("Allomorphy") );
	}

	//----------------------------------------------------------------//
	//	Loop through stems:
	//----------------------------------------------------------------//

     CStem* pStem;
    LogFileHeader("New Stems", "Already existed?");


     for (int stemno=0; stemno < pThisSig->GetNumberOfStems(); stemno++)
     {
        pStem = pThisSig->GetStem(stemno);
        pOldStem		= pStem;
        QString temp2 = pStem->Display();
        PNewStem		= *pOldStem    +     ssDeletee;
        PNewStem		.SimplifyParseStructure();
        QString temp4 = PNewStem.Display();
        pNewStem		= *m_pStems ^= PNewStem;
        QString temp3 = PNewStem.Display();

		// XXX. suppresses warning
		// `pLargerModifiedSig' may be used uninitialized
		// since g++ can’t figure out the control flow.
		CSignature* pLargerModifiedSig = 0;

		bNewStemAlreadyExisted = (pNewStem != 0);
		if (bNewStemAlreadyExisted) {
			// -- if the larger stem ("love") already existed
                        LogFile(pNewStem->Display(), "yes");
			pOlderSig = pNewStem->GetSuffixSignature();
			POldSig = *pNewStem->GetSuffixSignature();
			POldSig.MergeParse(PNewSig,  PMergedSig);

			pLargerModifiedSig = *m_pSignatures << &PMergedSig;
			pLargerModifiedSig->SetRemark(QString("Allomorphy1"));
			pNewStem->SetSuffixList(&PMergedSig);
			pNewStem->SetSuffixSignature( pLargerModifiedSig);
			pNewStem->SetConfidence(QString("Allomorphy1"));
			pThisSig->DetachStem(pOldStem,
			                     CSignature::eDo_Not_Call_Words);
                } else
                {
                    LogFile(PNewStem.Display(), QString("no"));
                    pNewStem			= *m_pStems	<< PNewStem;
                    pNewStem			->SetSuffixList		( pModifiedSig );
                    pNewStem			->SetSuffixSignature( pModifiedSig  );
                    pNewStem			->SetConfidence ( QString("Allomorphy2") );
                    val = pThisSig		->RemoveStem		( pOldStem );
                    Q_ASSERT (val);
                    if ( bOldSigRemains )
                    {
                        pOldStem			->SetSuffixList			( &WhatSigWillBecome );
                        pOldStem			->SetSuffixSignature	( pOldSigBecameSig  );
                    }
		}

		// go through each word associated with the OldStem, and change its suffix pointer.
                for (int affixno = 1; affixno <= pThisSig->Size(); ++affixno) {
                        if ( bStatus[affixno] == FALSE )
			{
				bStemShouldRemain = TRUE;
                                ssSuffix = pThisSig->GetPiece(affixno);
				if ( ssSuffix.IsNULL() )
				{
					Word = *pOldStem;
				}
				else
				{
					Word = CSS(pOldStem) + ssSuffix ;
				}

				pWord			= *m_pWords ^= CStringSurrogate( Word );
				pWord			->SetSuffixSignature (  pOldSigBecameSig );
				pWord			->AppendToConfidence(QString ("Allomorphy 1") );
				pOldSigBecameSig->AddWord( pWord );
                                if (  ! pOldSigBecameSig->StemListContains( pOldStem ) <  0 )
				{
                                        pOldSigBecameSig->AppendStemPtr(pOldStem);
				}
				pThisSig		->RemoveWord (pWord);
				continue;
			} // this means the suffix doesn't delete the deleting suffix or anything

                        ssNewSuffix		= SuffixChanges.GetPiece(affixno);

			pSuffix			= *m_pSuffixes ^= ssNewSuffix;
			if ( ssNewSuffix.IsNULL() )
			{
				Word = PNewStem;
			}
			else
			{
                                Word = CSS(pOldStem) + pThisSig->GetPiece(affixno);
			}


			pWord			= *m_pWords ^= CStringSurrogate( Word );
			pWord			->AppendToConfidence( QString ("Allomorphy 2") );
			pWord			->SetSuffixPtr ( pSuffix );
			val = pThisSig	->RemoveWord (pWord);
			Q_ASSERT (val);

			// case like hostilit-y becoming hostility
			if ( ssNewSuffix.IsNULL() )
			{
				pWord->DoNotParse();
			}

			if (bNewStemAlreadyExisted) {
				pWord->SetSuffixSignature(pLargerModifiedSig);
				pWord->SetStemPtr(pNewStem);
				pLargerModifiedSig->AttachToSuffixSig(pNewStem);
			} else {
//				LogFile(  PNewStem.Display(), QString( " did not exist.");
				pModifiedSig->AttachToSuffixSig(pNewStem,
				                                CSignature::eDo_Not_Call_Words);
				pWord->AttachSuffixSignature(pModifiedSig);
				pWord->SetStemPtr(pNewStem);
			}


		}// cycle through the suffixes
                LogFileEndRow();

		if ( bStemShouldRemain == FALSE )
		{
                        m_pStems	->RemoveMember( pOldStem );
		}

	}  // cycle of stems
        LogFileEndTable();

}

/**
Takes as input a signature, a Deletee (stem final material that will be deleted)

*/
CParse CMiniLexicon::CreateADeletingSignature ( CSignature*		pSig,
										    CSS				Deletee,
										    CParse&			ReplacingSuffixes,
											int*			bStatus,
											CParse&			WhatSigWillBecome,
											CParse*			pSuffixCandidatesThatMightDeleteDeletee) //3/04
{


	CStringSurrogate	ssSuffix, ssTruncatedSuffix;


        CParse			NewSig;
        CSuffix*		pSuffix,
                                *pTruncatedSuffix;
        int			DeleteeLength = Deletee.GetLength();
        QString			QNULL ("NULL");

	ReplacingSuffixes.ClearParse();
	WhatSigWillBecome.ClearParse();




        LogFile("Affix", "Status");
        for (int affixno = 1; affixno <= pSig->Size(); affixno++)
	{   // Consider the suffixes in this signature. If one is the deletee, replace it by NULL.
                ssSuffix = pSig->GetPiece(affixno);
		if ( ssSuffix == Deletee )
		{
			NewSig.Append (QNULL );
			ReplacingSuffixes.Append ( QNULL );
                        bStatus[affixno]		= TRUE;
                        LogFile(ssSuffix.Display(), QString(" is our deletee"));			
		}
		else
		{
                        pSuffix			= *m_pSuffixes ^= ssSuffix;
                        ssTruncatedSuffix	= ssSuffix.Mid( Deletee.GetLength() );
                        pTruncatedSuffix	= *m_pSuffixes ^= ssTruncatedSuffix;
			// 0: if the candidate doesn't delete the deleting suffix
			if ( ! pSuffixCandidatesThatMightDeleteDeletee->Contains( ssSuffix ) )
			{
				ReplacingSuffixes.Append ( QString ("***") );
				WhatSigWillBecome.Append( ssSuffix );
                                bStatus[affixno]		 = FALSE;
                                LogFile ( ssSuffix.Display(), QString(" does not delete." ));
            }
			// 1: this is the "ement" case
			else if (
					pTruncatedSuffix											&&
					pSuffix->GetKey().Left( DeleteeLength ) == Deletee		&&
					(  StemsWithBothSuffixes ( TheStringNULL,    ssTruncatedSuffix ) >
					   StemsWithBothSuffixes ( Deletee, ssSuffix          )
					)
			   )
			{
				NewSig				.Append( ssTruncatedSuffix );
				ReplacingSuffixes	.Append( ssTruncatedSuffix );
                                bStatus[affixno]			= TRUE;
                            LogFile (ssSuffix.Display(), QString(" the ement sort of case." ));
                        }
			else if (
						StemsWithBothSuffixes ( TheStringNULL,    ssSuffix ) >
						StemsWithBothSuffixes ( Deletee  , ssSuffix    )
					)
			// 2: this is the case of a suffix that tdeletes a stem-final Deletee

			{
                                pSuffix			->AddDeletee (Deletee);
                                NewSig			.Append( ssSuffix );
				ReplacingSuffixes	.Append( ssSuffix );
                                bStatus[affixno]	= TRUE;
                                LogFile (ssSuffix.Display(), QString( " is a deleter." ));
                        }
			else
			// 3: another case where we'll say, for now, the candidate doesn't delete the deleting suffix:
			{
				ReplacingSuffixes.Append ( QString ("***") );
				WhatSigWillBecome.Append( ssSuffix );
                                bStatus[affixno]		 = FALSE;
                                LogFile (ssSuffix.Display(), QString( " maybe does not delete Deletee." ));
			}
		}
	}
        LogFileEndTable();

	return NewSig;
}

/*
CParse CMiniLexicon::CreateADeletingSignature ( CSignature*		pSig,
										    CSS				Deletee,
										    CParse&			ReplacingSuffixes,
											int*			bStatus,
											CParse&			WhatSigWillBecome,
											StringToString& Remapper) //3/04
{


	CStringSurrogate	ssSuffix, ssTruncatedSuffix;

	CParse				PSuffix,
						NewSig,
						Suffix;
	int					DeleteeLength = Deletee.GetLength();
	QString				QNULL ("NULL"),
						Rewrite;

	ReplacingSuffixes.ClearParse();
	WhatSigWillBecome.ClearParse();



	for (int n = 1; n <= pSig->Size(); n++)
	{
		ssSuffix = pSig->GetPiece(n);
		if ( ssSuffix == Deletee )
		{
			NewSig.Append (QNULL) ;
			ReplacingSuffixes.Append (  QNULL);
			bStatus[n]		= TRUE;
		}
		else
		{	//if ( ! pSuffixCandidates->Contains( ssSuffix ) )

            QMap<QString,QString>::Iterator it;
            if ( Remapper.contains (ssSuffix.Display())   )
                //Remapper.Lookup( ssSuffix.SpellOut() ) )
			{
                Rewrite             =Remapper.find(ssSuffix.Display()).data();
				NewSig				.Append( Rewrite);
				ReplacingSuffixes	.Append( Rewrite);
				bStatus[n]			= TRUE;
			}
			else
			{
				ReplacingSuffixes.Append ( QString ("***") );
				WhatSigWillBecome.Append( ssSuffix );
				bStatus[n]		 = FALSE;
			}
		}
	}
	return NewSig;
}
*/
