// CSequencer, sentenceItem classes
// Copyright © 2009 The University of Chicago
#ifndef SEQUENCER_H
#define SEQUENCER_H

class CSequencer;
class sentenceItem;

#include <Q3SortedList>
#include <QString>
#include <QMap>
namespace linguistica { namespace ui { class status_user_agent; } }

class CSequencer {
	// parameters.
	int m_K;
	int m_resultK;
	int m_maxlineintrain;
	int m_maxlineintest;
private:
	QString separator;

	QMap<QString, int> m_bigrams;
	QMap<QString, int> m_trigrams;

	QMap<QString, int> m_bigramsbase;
	QMap<QString, int> m_trigramsbase;

	QMap<QString, double> m_bigramprob;
	QMap<QString, double> m_trigramprob;

	int m_totalbigrams;
	int m_totaltrigrams;
	int m_totalbigramsbase;
	int m_totaltrigramsbase;
public:
	CSequencer();

	void readCorpus(linguistica::ui::status_user_agent& status_out);
	void sequenceASentence(int& birank, int& trirank,
		QString inputSentence = QString());
	void sequenceASentence2(int& birank, int& trirank,
		QString inputSentence = QString());
	void sequenizeFromABigram(QString bigram, double bigram_value,
		QMap<QString, int>& words, int sentence_len, int K,
		Q3SortedList<sentenceItem>& result_K_sentences,
		int computetype);
	void sequenize2(QMap<QString, int> allwords, int sentence_len,
		int K, Q3SortedList<sentenceItem>& result_K_sentences,
		int computetype);
	void sequencerTestAFile(linguistica::ui::status_user_agent& status_out);
};

class sentenceItem
{
public:

	double					m_value;
	QString					m_key; 
	QMap<QString, int>		m_bagofwords;
	QMap<int, QString>		m_historystrings;
	QMap<int, double>		m_historyscores;
	int						m_stepnumber;
	int						m_numberofwordsinsentence; // only used for sequencerize2

public:
	sentenceItem(){};
	~sentenceItem(){};

	sentenceItem(double value, QString& key,
	             QMap<QString, int>& bagofwords,
	             int stepnumber,
	             QMap<int, QString>& historystrings,
	             QMap<int, double>& historyscores)
		{ m_value = value;
								m_key = key; m_bagofwords = bagofwords; m_stepnumber = stepnumber; m_historystrings = historystrings; m_historyscores = historyscores;\
								m_historystrings.insert(m_stepnumber, m_key); m_historyscores.insert(m_stepnumber, m_value); m_numberofwordsinsentence = 0; };


	sentenceItem(sentenceItem* other) {m_value = other ->m_value; \
								m_key = other ->m_key; m_bagofwords = other ->m_bagofwords; \
								m_stepnumber = other ->m_stepnumber; \
								m_historystrings = other ->m_historystrings; \
								m_historyscores = other ->m_historyscores;\
								m_numberofwordsinsentence = other ->m_numberofwordsinsentence;
								};



	bool	operator<(const sentenceItem& another) {return !(m_value > another.m_value); };
	bool	operator==(const sentenceItem& another) {return (m_value == another.m_value);};



};

#endif // SEQUENCER_H
