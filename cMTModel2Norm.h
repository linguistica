// Machine Translation model 2
// Copyright © 2009 The University of Chicago
#ifndef CMTMODEL2NORM_H
#define CMTMODEL2NORM_H

#include <QMap>

class mTVolca;
typedef QMap<int, double> IntToDouble;
typedef QMap<int, IntToDouble*> IntToIntToDouble;

class cMTModel2Norm {
public:
	class cMT* m_myMT;
	int m_Iterations;
	/// set as 10 now...
	int m_sentenceNormChunk;
	bool m_getTfromModel1;

	// alignment parameters.

	QMap<int, QMap<int, double>*> m_T;
	/// Here, we use different idea,
	/// we normalize the len of a sentence to m_sentenceNormChunk.
	/// Also, the A is [language2][language1],
	/// this is different from T !! Discuss to do ?
	QMap<int, QMap<int, double>*> m_A;
	QMap<int, QMap<int, double>*> m_softCountOfT;
	QMap<int, QMap<int, double>*> m_softCountOfA;
	QMap<int, double> m_lamdaT;
	QMap<int, double> m_lamdaA;
public:
	// construction/destruction.

	cMTModel2Norm(cMT* driver, int iterations, bool from_model1);
	virtual ~cMTModel2Norm();

	// disable default-construction, copy
private:
	cMTModel2Norm();
	cMTModel2Norm(const cMTModel2Norm& x);
	cMTModel2Norm& operator=(const cMTModel2Norm& x);
public:
	void initTandA();
	void addSoftCountOfTandA(int);
	void EMLoops(int);
	void clearSoftCounts();
	void EStep();
	void MStep();
	void releaseSoftCounts();
	void viterbi(int);
	void viterbiAll();
};

#endif // CMTMODEL2NORM_H
