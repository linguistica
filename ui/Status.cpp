// Shim to allow code that expects a status bar to work without one.
// Copyright © 2010 The University of Chicago

#include "ui/Status.h"

namespace linguistica {
namespace ui {

namespace {
struct null_agent : status_user_agent {
	typedef status_user_agent base;

	struct string_forgetter : base::string_type {
		void clear() { }
		string_forgetter& operator=(const QString&)
			{ return *this; }
	};
	struct progress_forgetter : base::progress_type {
		void clear() { }
		void set_denominator(int) { incr = 0; }
		progress_forgetter& assign(int) { return *this; }
	};
	string_forgetter string_sink;
	progress_forgetter progress_sink;
	null_agent() : base(string_sink, string_sink, progress_sink) { }
};
}

status_user_agent& ignore_status_updates()
{
	static null_agent sink;
	return sink;
}

} // namespace linguistica::ui
} // namespace linguistica
