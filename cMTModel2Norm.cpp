// Implementation of the cMTModel2Norm class
// Copyright © 2009 The University of Chicago
#include "cMTModel2Norm.h"

#include <QMessageBox>
#include "cMTModel1.h"
#include "mTVolca.h"
#include "cMT.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

cMTModel2Norm::cMTModel2Norm(cMT* myMT, int Iterations, bool fromModel1)
	: m_myMT(myMT),
	m_Iterations(Iterations),
	m_sentenceNormChunk(10),
	m_getTfromModel1(fromModel1),
	m_T(), m_A(),
	m_softCountOfT(), m_softCountOfA(),
	m_lamdaT(), m_lamdaA() { }

cMTModel2Norm::~cMTModel2Norm()
{
	// XXX. not clear who owns what, so there are definitely some
	// leaks here.
}

void		cMTModel2Norm::initTandA()
{
	mTVolca*								myVolca; 
	int										totalAssociatedLanguage2Words; 
	IntToIntToDouble::iterator				IntToIntToDoubleIt; 
	IntToDouble::iterator					IntToDoubleIt; 
	IntToDouble*							oneListForOneLanguage1Word; 
	double									uniformProb;
	int										key; 
	IntToDouble*							oneListForOneLanuage2Chunk; 
	IntToDouble*							oneSoftCountListForOneLanuage2Chunk; 
	int										i,j; 
	


	myVolca = m_myMT ->m_Volca; 


	// Init m_T
	if ( !m_getTfromModel1)
	{
		m_T = myVolca ->m_fastWordsPairs;
		
		for ( IntToIntToDoubleIt = m_T.begin(); IntToIntToDoubleIt != m_T.end();IntToIntToDoubleIt++)
		{
			oneListForOneLanguage1Word = IntToIntToDoubleIt.data(); 
			
			totalAssociatedLanguage2Words = oneListForOneLanguage1Word ->size(); 

			uniformProb = 1.0 / totalAssociatedLanguage2Words;

			for ( IntToDoubleIt = oneListForOneLanguage1Word ->begin(); IntToDoubleIt != oneListForOneLanguage1Word ->end();  IntToDoubleIt++)
			{
				key = IntToDoubleIt.key();
				(*oneListForOneLanguage1Word)[key] = uniformProb;
			}
		}
	}
	else
	{
		m_T = m_myMT ->m_Model1 ->m_T; 
	}
		
		
	// init m_A and m_softcountofA
	for ( j=0; j< m_sentenceNormChunk; j++)
	{
		oneListForOneLanuage2Chunk = new IntToDouble(); 
		oneSoftCountListForOneLanuage2Chunk = new IntToDouble(); 
		m_A.insert(j, oneListForOneLanuage2Chunk); 
		m_softCountOfA.insert(j, oneSoftCountListForOneLanuage2Chunk); 

		uniformProb = 1.0/ m_sentenceNormChunk; 

		for ( i=0; i< m_sentenceNormChunk; i++)
		{
			oneListForOneLanuage2Chunk ->insert(i, uniformProb); 
			oneSoftCountListForOneLanuage2Chunk ->insert(i, 0.0);
		}
	}


	// init m_softcountofT
	m_softCountOfT = myVolca ->m_fastWordsSoftCounts; 
	
	for ( IntToIntToDoubleIt = m_softCountOfT.begin(); IntToIntToDoubleIt != m_softCountOfT.end();IntToIntToDoubleIt++)
	{
		oneListForOneLanguage1Word = IntToIntToDoubleIt.data(); 
		
		for ( IntToDoubleIt = oneListForOneLanguage1Word ->begin(); IntToDoubleIt != oneListForOneLanguage1Word ->end();  IntToDoubleIt++)
		{
			IntToDoubleIt.data() = 0.0; 
		}
	}



	QMessageBox::information ( NULL, "Linguistica : MT Model2Norm", "Finished InitTandA", "OK" );
} 



void			cMTModel2Norm::EMLoops(int numberOfIterations)
{
	int							loopI; 

	for (loopI =0; loopI < numberOfIterations; loopI++)
	{
		// E step 
		EStep();

		//QMessageBox::information ( NULL, "Linguistica : MT Model1", "Finished E-Step", "OK" );

		// M step
		MStep();

		//QMessageBox::information ( NULL, "Linguistica : MT Model1", "Finished M-Step", "OK" );

		// Clear softcouts for T
		clearSoftCounts();
	}

	// Release softcountT memory
	releaseSoftCounts(); 
} 

void			cMTModel2Norm::clearSoftCounts()
{
	IntToIntToDouble::iterator				IntToIntToDoubleIt;
	IntToDouble*							oneList; 
	IntToDouble*							oneListForOneLanuage2Chunk; 
	IntToDouble::iterator					IntToDoubleIt; 
	int										key; 
	int										i, j; 

	for ( IntToIntToDoubleIt = m_softCountOfT.begin(); IntToIntToDoubleIt != m_softCountOfT.end(); IntToIntToDoubleIt++)
	{
		oneList = IntToIntToDoubleIt.data(); 

		for (IntToDoubleIt = oneList ->begin(); IntToDoubleIt != oneList ->end(); IntToDoubleIt++)
		{
			key = IntToDoubleIt.key(); 
			(*oneList)[key] = 0.0; 
		}
	}

	for ( j=0; j< m_sentenceNormChunk; j++)
	{
		oneListForOneLanuage2Chunk = m_softCountOfA[j];
		for ( i=0; i< m_sentenceNormChunk; i++)
		{
			(*oneListForOneLanuage2Chunk)[i] = 0.0; 
		}
	}


}

void			cMTModel2Norm::EStep()
{
	mTVolca*						myVolca; 
	int								i; 

	myVolca = m_myMT ->m_Volca; 

	m_lamdaT.clear(); 
	m_lamdaA.clear(); 

	for ( i=0; i < myVolca ->m_countOfSentences; i++)
	{
		addSoftCountOfTandA(i); 		
	}
}


void			cMTModel2Norm::MStep()
{
	IntToIntToDouble::iterator				IntToIntToDoubleIt;
	IntToDouble*							oneList; 
	IntToDouble::iterator					IntToDoubleIt; 
	int										language1Id;
	int										language2Id;
	int										language1chunkId;
	int										language2chunkId;
	double									oneTotalSoftCount; 
	double									oneLamda;


	// Mstep for T
	for ( IntToIntToDoubleIt = m_softCountOfT.begin(); IntToIntToDoubleIt != m_softCountOfT.end(); IntToIntToDoubleIt++)
	{
		language1Id = IntToIntToDoubleIt.key();
		oneLamda = m_lamdaT[language1Id] ;
		oneList = IntToIntToDoubleIt.data(); 


		for (IntToDoubleIt = oneList ->begin(); IntToDoubleIt != oneList ->end(); IntToDoubleIt++)
		{
			language2Id = IntToDoubleIt.key();
			oneTotalSoftCount = IntToDoubleIt.data(); 
			(*(m_T[language1Id]))[language2Id] = oneTotalSoftCount / oneLamda ;
		}
	}

	// Mstep for A
	for ( IntToIntToDoubleIt = m_softCountOfA.begin(); IntToIntToDoubleIt != m_softCountOfA.end(); IntToIntToDoubleIt++)
	{
		language2chunkId = IntToIntToDoubleIt.key();
		oneLamda = m_lamdaA[language2chunkId] ;
		oneList = IntToIntToDoubleIt.data(); 

		for (IntToDoubleIt = oneList ->begin(); IntToDoubleIt != oneList ->end(); IntToDoubleIt++)
		{
			language1chunkId = IntToDoubleIt.key();
			oneTotalSoftCount = IntToDoubleIt.data(); 
			(*(m_A[language2chunkId]))[language1chunkId] = oneTotalSoftCount / oneLamda ;
		}
	}

}

void			cMTModel2Norm::addSoftCountOfTandA(int sentenceId)
{
	mTVolca*						myVolca; 
	double							oneT; 
	double							oneA;
	double							oneCount; 
	int								language1SentenceLen;
	int								language2SentenceLen;
	int								language1ChunkId;
	int								language2ChunkId;
	double							deNumerator; 
	int								l,m; 
	int								language1WordId;
	int								language2WordId;
	IntToInt*						oneLan1Sentence;
	IntToInt*						oneLan2Sentence;


	// add softcount of T
	myVolca = m_myMT ->m_Volca; 

	oneLan1Sentence = myVolca ->m_language1Sentences[sentenceId];
	oneLan2Sentence = myVolca ->m_language2Sentences[sentenceId];
	
	language1SentenceLen = oneLan1Sentence ->size();
	language2SentenceLen = oneLan2Sentence ->size();
	
	for (m = 0; m < oneLan2Sentence->size(); ++m) {
		language2WordId = (*oneLan2Sentence)[m];
		language2ChunkId = static_cast<int>(
			double(m) / language2SentenceLen * m_sentenceNormChunk);

		Q_ASSERT(language2ChunkId >=0 && language2ChunkId <m_sentenceNormChunk);


		deNumerator =0; 

		for (l = 0; l < oneLan1Sentence->size(); ++l) {
			language1WordId = (*oneLan1Sentence)[l];
			language1ChunkId = static_cast<int>(
				double(l) / language1SentenceLen *
				m_sentenceNormChunk);

			Q_ASSERT(language1ChunkId >=0 && language1ChunkId <m_sentenceNormChunk);

			oneT = (*(m_T[language1WordId]))[language2WordId];
			oneA = (*(m_A[language2ChunkId]))[language1ChunkId];
			deNumerator += oneT*oneA; 
		}

		for (l = 0; l < oneLan1Sentence->size(); ++l) {
			language1WordId = (*oneLan1Sentence)[l];
			language1ChunkId = static_cast<int>(
				double(l) / language1SentenceLen *
				m_sentenceNormChunk);

			oneA = (*(m_A[language2ChunkId]))[language1ChunkId];
			oneT = (*(m_T[language1WordId]))[language2WordId] ;
			oneCount = oneT*oneA/ deNumerator;

			(*(m_softCountOfT[language1WordId]))[language2WordId] += oneCount;
			
			if ( m_lamdaT.contains(language1WordId))
			{
				m_lamdaT[language1WordId] += oneCount;
			}
			else
			{
				m_lamdaT.insert(language1WordId, oneCount); 
			}
		
			// add softcount of A
			(*(m_softCountOfA[language2ChunkId]))[language1ChunkId] += oneCount;
				
			if ( m_lamdaA.contains(language2ChunkId))
			{
				m_lamdaA[language2ChunkId] += oneCount;
			}
			else
			{
				m_lamdaA.insert(language2ChunkId, oneCount); 
			}
		}
	}
}

void			cMTModel2Norm::releaseSoftCounts()
{
	IntToIntToDouble::iterator				IntToIntToDoubleIt;
	IntToDouble*							oneList; 
	
	for ( IntToIntToDoubleIt = m_softCountOfT.begin(); IntToIntToDoubleIt != m_softCountOfT.end(); IntToIntToDoubleIt++)
	{
		oneList = IntToIntToDoubleIt.data(); 

		delete oneList; 
	}


	for ( IntToIntToDoubleIt = m_softCountOfA.begin(); IntToIntToDoubleIt != m_softCountOfA.end(); IntToIntToDoubleIt++)
	{
		oneList = IntToIntToDoubleIt.data(); 

		delete oneList; 
	}

}

void			cMTModel2Norm::viterbiAll()
{
	mTVolca*						myVolca; 
	int								i; 

	myVolca = m_myMT ->m_Volca; 

	myVolca ->clearSentenceViterbiAlignment();

	for ( i=0; i < myVolca ->m_countOfSentences; i++)
	{
		viterbi(i); 		
	}
	
} 

void			cMTModel2Norm::viterbi(int sentenceId)
{
	mTVolca*						myVolca; 
	double							oneT; 
	double							oneA;
	double							oneCount; 
	double							bestCount;
	int								bestAlignmentId;
	int								language1SentenceLen;
	int								language2SentenceLen;
	int								language1ChunkId;
	int								language2ChunkId;
	int								l,m; 
	int								language1WordId;
	int								language2WordId;
	IntToInt*						oneLan1Sentence;
	IntToInt*						oneLan2Sentence;
	IntToInt*						oneAlignment; 	


	// add softcount of T
	myVolca = m_myMT ->m_Volca; 
	
	oneAlignment = new IntToInt();
	myVolca ->m_sentenceAlignments.insert(sentenceId, oneAlignment); 

	oneLan1Sentence = myVolca ->m_language1Sentences[sentenceId];
	oneLan2Sentence = myVolca ->m_language2Sentences[sentenceId];
	
	language1SentenceLen = oneLan1Sentence ->size();
	language2SentenceLen = oneLan2Sentence ->size();
	
	for (m = 0; m < language2SentenceLen; ++m) {
		language2WordId = (*oneLan2Sentence)[m];
		language2ChunkId = static_cast<int>(
			double(m) / language2SentenceLen *
			m_sentenceNormChunk);
		
		Q_ASSERT(language2ChunkId >=0 && language2ChunkId <m_sentenceNormChunk);

		bestAlignmentId = -1;
		bestCount = 0.0; 

		for (l = 0; l < language1SentenceLen; ++l) {
			language1WordId = (*oneLan1Sentence)[l];
			language1ChunkId = static_cast<int>(
				double(l) / language1SentenceLen *
				m_sentenceNormChunk);

			oneA = (*(m_A[language2ChunkId]))[language1ChunkId];
			oneT = (*(m_T[language1WordId]))[language2WordId] ;
			oneCount = oneT*oneA;

			if ( oneCount > bestCount)
			{
				bestAlignmentId = l;
				bestCount = oneCount; 
			}
		}

		oneAlignment->insert(m, bestAlignmentId);
	}
}
