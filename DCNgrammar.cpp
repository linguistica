// Implementation of grammar methods
// Copyright © 2009 The University of Chicago
#include "DCNgrammar.h"

#include <qlabel.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

grammar::grammar(){}

grammar::~grammar(){}

grammar::grammar(grammar &theGrammar)
{
	alpha	= theGrammar.getAlpha();
	beta	= theGrammar.getBeta();
	I		= theGrammar.getI();
	F		= theGrammar.getF();
	P		= theGrammar.getP();
	bias	= theGrammar.getBias();
}


void	grammar::setValues(float alpha, float beta, float I, float F, float P, float bias)
{
	this->alpha = alpha;
	this->beta  = beta;
	this->I		= I;
	this->F		= F;
	this->P		= P;
	this->bias	= bias;
}

void	grammar::print(QLabel *label)
{
	QString totalString;
	QString partialString;

	partialString.setNum(alpha);
	totalString =  "alpha:\t\t"	+	partialString + '\n';
	partialString.setNum(beta);
	totalString += "beta:\t\t"	+	partialString + '\n';
	partialString.setNum(I);	
	totalString += "initial:\t\t" +	partialString + '\n';
	partialString.setNum(F);
	totalString += "final:\t\t"	+	partialString + '\n';
	partialString.setNum(P);
	totalString += "penult:\t\t"  +	partialString + '\n';
	partialString.setNum(bias);
	totalString += "bias:\t\t"	+	partialString + '\n';

	label->setText(totalString);
}
