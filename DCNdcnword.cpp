// Implementation of dcnword methods
// Copyright © 2009 The University of Chicago
#include "DCNdcnword.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

dcnword::dcnword()
{
}

dcnword::~dcnword()
{
}

void dcnword::setText(QString word)
{
	m_text = word;
	m_numOfSegments = word.length();
}

void dcnword::setMaxima(QString word)
{
	m_maxima = word;
}

