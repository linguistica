// Maintaining the small graphic display
// Copyright © 2009 The University of Chicago
#include "linguisticamainwindow.h"

#include <iostream>

#include <QList>
#include "MiniLexicon.h"
#include "Lexicon.h"
#include "StateEmitHMM.h"
#include "GraphicView.h"
#include "Signature.h"
#include "Compound.h"
#include "Stem.h"
#include "Edge.h"
#include "SignatureCollection.h"
#include "generaldefinitions.h"

void LinguisticaMainWindow::updateSmallGraphicDisplaySlot()
{
        CState*				pState;
        Q3PtrList<CEdge>                parses;
        CEdge*				pEdge;
	CCompoundListViewItem*		item;

	switch( m_docType )
	{

	case COMPOUNDS:
		if( !m_commandParse ) break;

		item = (CCompoundListViewItem*)m_collectionView->currentItem();
		if( item->GetParse() >= 0 )
		{
			pEdge = ((CCompound*)m_commandParse)->GetParses()->at( item->GetParse() );
			parses.append( pEdge );
			m_SmallGraphicDisplay->SetMyRoots( &parses );
		}
		else
		{
			m_SmallGraphicDisplay->SetMyRoots( ((CCompound*)m_commandParse)->GetParses() );
		}
		m_SmallGraphicDisplay->PlotStates();

		break;

	case FSA_DOC:
	{
		m_SmallGraphicDisplay->update();
	}
		break;
	default:
		pState = this->m_CommonState;

		if ( !pState) return; 

		m_SmallGraphicDisplay->SetMyState(pState); 

		m_SmallGraphicDisplay->PlotStates(); 

		break;
	}
}


void LinguisticaMainWindow::updateSmallGraphicDisplaySlotForPhonogy(CStem* selectedWord)
{

	CStem*						theWord; 

	theWord = selectedWord;

	if ( theWord ->m_donephonology)
	{
		m_SmallGraphicDisplay ->SetMyStem(selectedWord);
		m_SmallGraphicDisplay ->PlotStems(); 
	}

	return; 
}

void LinguisticaMainWindow::updateSmallGraphicDisplaySlotForMultiDimensionData
			(int numberOfDimension, 
			 int numberOfDataPoints, 
			 double** ptrData, 
			 IntToString listOfSymbols)
{
	
	
	m_SmallGraphicDisplay ->PlotNVectors(numberOfDimension,
										 numberOfDataPoints, 
										 ptrData, 
										 listOfSymbols); 
}

void LinguisticaMainWindow::updateSignatureGraphicDisplaySlot()
{

	QString					QstrSignature; 
 	CSignature				*pThisSig;
	CStem*					pStem;
 	IntToString				ListOfSymbols;

	if (m_docType != SIGNATURES && m_docType != SUFFIX_SIGNATURES )
	{ return;}
		
	int NumberOfSignaturesSelected = 0;
	Q3ListViewItem* selectedItem = NULL;
    
	Q3ListViewItemIterator it( m_collectionView );
    while ( it.current() ) 
	{
            Q3ListViewItem *item = it.current();
            if (item->isSelected() ) 
			{
				NumberOfSignaturesSelected++;
				selectedItem = item;
				QstrSignature = ((CSignatureListViewItem*)selectedItem)->text(0);
			}
            ++it;
    }

	//:TODO if NumberOfSignaturesSelected == 0, then send a message to user;


	pThisSig =   *m_lexicon->GetMiniLexicon( m_lexicon->GetActiveMiniIndex())
								->GetSignatures()  ^= QstrSignature ;

	if (!pThisSig) return;
	
        int         NumberOfSuffixes            = pThisSig->Size();
        int         NumberOfStems		= pThisSig->GetNumberOfStems();
        int         stemno;
        double**    ptrData;

         QString    temp;
                    temp                        = pThisSig->Display();
         int*       data			= NULL;
                    ptrData			=  new double* [ NumberOfStems ];
        for ( stemno =0; stemno <NumberOfStems; stemno++)
	{
                ptrData[stemno]		= new double   [ NumberOfSuffixes ] ;
	}

        for ( stemno = 0; stemno < pThisSig->GetStemPtrList()->size(); stemno++)
        {   pStem = pThisSig->GetStemPtrList()->at(stemno);
            ListOfSymbols.insert(stemno, pStem->Display() );
            for (int affixno= 0; affixno < pThisSig->Size(); affixno++)
            {
                ptrData[stemno][affixno] = data[stemno * NumberOfStems + affixno];
            }            
        }
  
	// Display on Graphic View
	updateSmallGraphicDisplaySlotForMultiDimensionData(
		NumberOfSuffixes, 
                NumberOfStems,
		ptrData, 
		ListOfSymbols); 
	delete data;
}


 
//////////////////////////////////////////////////////////////////////////////
//		Test
//////////////////////////////////////////////////////////////////////////////

void LinguisticaMainWindow::testSignatureDisplaySlot()
{
        int			NumberOfSuffixes;
        int			NumberOfStems;
	double**		ptrData;
        int			i;
	CStem*			pStem; 
	IntToString		ListOfSymbols;

	struct not_implemented { };
	throw not_implemented();

	// XXX. pThisSig has to be initialized for this
	// code to work.
	CSignature		*pThisSig; 
        NumberOfSuffixes	= pThisSig->Size();
	NumberOfStems		= pThisSig->GetNumberOfStems();


	 QString temp;		temp = pThisSig->Display();
	

	 ptrData			=  new double* [ NumberOfStems ];
	 for ( i=0; i<NumberOfStems; i++)
	{
		ptrData[i]		= new double   [ NumberOfSuffixes ] ;
	}

        for (int stemno = 0; stemno < NumberOfStems; stemno++)
        {
            pStem = pThisSig->GetStemPtrList()->at(stemno);
            ListOfSymbols.insert(stemno, pStem->Display() );
            for (int affixno= 0; affixno <= NumberOfSuffixes; affixno++)
            {
                ptrData[stemno][affixno] = pThisSig->GetWordCount( stemno, affixno );
            }
        }  
	// Display on Graphic View
	updateSmallGraphicDisplaySlotForMultiDimensionData(
		NumberOfSuffixes, 
		pThisSig->GetNumberOfStems(), 
		ptrData, 
		ListOfSymbols); 


    }

void LinguisticaMainWindow::DisplayVideoHMM()
{
	if (m_lexicon->GetHMM() && m_lexicon->GetHMM()->m_Video )
	{
		m_SmallGraphicDisplay ->PlotVideo (	m_lexicon->GetHMM()->m_Video );
	}

}
