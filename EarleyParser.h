// Earley parser
// Copyright © 2009 The University of Chicago
#ifndef EARLEYPARSER_H
#define EARLEYPARSER_H

class CEarleyParser;

#include <QMap>
template<class T> class Q3PtrList;

class CEarleyParser {
protected:
	QMap<unsigned int, Q3PtrList<class CEdge> > m_states;
	QMap<class QString, Q3PtrList<class CGrammarRule> >* m_grammar;
	class CParse* m_word;
	QMap<QString, class CTerminalRuleCollection*>* m_lexicon;
	/// Length of shortest non-wild-card terminal
	int m_shortest;

	Q3PtrList<class CEdge>* m_Roots;
	int m_CurrentLocation;
	unsigned int m_NumberOfStates;

	class QTextStream* m_logFile;

	QMap<class QString, int> m_minimumLengths;
	bool m_validGrammarFlag;
	int m_maximumParseDepth;
public:
	// construction/destruction.
	CEarleyParser(CParse* word,
		QMap<QString, Q3PtrList<CGrammarRule> >* grammar,
		QMap<QString, CTerminalRuleCollection*>* lexicon,
		QTextStream* log = 0,
		int maximumParseDepth = 5);
	virtual ~CEarleyParser();

	// the main event.
	Q3PtrList<CEdge>* Parse();

	bool Prediction(CEdge* edge);
	bool Scanning(CEdge* edge);
	bool Completion(CEdge* edge);
	bool isValidGrammar() { return m_validGrammarFlag; }
	void setMaximumParseDepth(int d) { m_maximumParseDepth = d; }
	int getMaximumParseDepth() const { return m_maximumParseDepth; }
};

#endif // EARLEYPARSER_H
