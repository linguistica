// Q3ListViewItem pointers to CStem in its two roles
// Copyright © 2009 The University of Chicago
#ifndef STEMLISTVIEWITEM_H
#define STEMLISTVIEWITEM_H

class CWordListViewItem;
class CStemListViewItem;

#include <Q3ListViewItem>
#include <QString>
class Q3ListView;
template<class K, class V> class QMap;
class CSignature;
class CStem;

/// A list view item for words.
/**
	Contains all the necessary variables needed when displaying a
	word and its data in a list view.
 */

class CWordListViewItem : public Q3ListViewItem {
public:
	enum display_mode {
		MiniLexicon_MorphologyStuffFirst,
		MiniLexicon_PhonologyStuffFirst,
		Lexicon_Word,
	};
private:
	CStem* m_pWord;
	int m_mini;
	QMap<QString, QString>* m_filter;
	enum display_mode m_DisplayMode;
	int m_char_count;
	double m_DL;
	QString m_prefix;
	QString m_stem;
	QString m_suffix;
	QString m_signature;
	QString m_tier1;
	QString m_tier1_skeleton;
	QString m_tier2;
	CSignature* m_pSig;
public:
	// construction/destruction.

	explicit CWordListViewItem(Q3ListView* parent = 0);

        CWordListViewItem(Q3ListView* parent, QString word_text,
            int mini_index = -1, CStem* word = 0,
            QMap<QString, QString>* filter = 0,
            enum display_mode format = MiniLexicon_MorphologyStuffFirst,
            int char_count = 27);

	CWordListViewItem(Q3ListViewItem* parent, QString word_text,
            int mini_index = -1, CStem* word = 0,
            QMap<QString, QString>* filter = 0,
            enum display_mode format = MiniLexicon_MorphologyStuffFirst,
            int char_count = 27);

	// copy.

	CWordListViewItem(const CWordListViewItem& x)
		: Q3ListViewItem(x),
		m_pWord(x.m_pWord),
		m_mini(x.m_mini),
		m_filter(x.m_filter),
		m_DisplayMode(x.m_DisplayMode),
		m_char_count(x.m_char_count),
		m_DL(x.m_DL),
		m_prefix(x.m_prefix),
		m_stem(x.m_stem),
		m_suffix(x.m_suffix),
		m_signature(x.m_signature),
		m_tier1(x.m_tier1),
		m_tier1_skeleton(x.m_tier1_skeleton),
		m_tier2(x.m_tier2),
		m_pSig(x.m_pSig) { }
	CWordListViewItem& operator=(const CWordListViewItem& x)
	{
		Q3ListViewItem::operator=(x);
		m_pWord = x.m_pWord;
		m_mini = x.m_mini;
		m_filter = x.m_filter;
		m_DisplayMode = x.m_DisplayMode;
		m_char_count = x.m_char_count;
		m_DL = x.m_DL;
		m_prefix = x.m_prefix;
		m_stem = x.m_stem;
		m_suffix = x.m_suffix;
		m_signature = x.m_signature;
		m_tier1 = x.m_tier1;
		m_tier1_skeleton = x.m_tier1_skeleton;
		m_tier2 = x.m_tier2;
		m_pSig = x.m_pSig;
		return *this;
	}

	// Qt3 list view item interface.

	virtual QString text(int column_index) const;
	int compare(Q3ListViewItem* other, int column, bool ascending) const;
	virtual QString key(int column, bool ascending) const;

	// underlying stem object.

	CStem* GetWord() { return m_pWord; }
	void SetWord(CStem* pWord) { m_pWord = pWord; }

	/// index of mini-lexicon containing this word (or -1 for none)
	int GetMini() { return m_mini; }

	/// description length
	double GetDL() { return m_DL; }

	bool isFullLex() { return false; }
};

/// A list view item for stems.
/**
	Contains all the necessary variables needed when displaying a
	stem and its data in a list view.
 */

class CStemListViewItem : public Q3ListViewItem {
	CStem* m_Stem;
	QMap<QString, QString>* m_filter;
	int m_char_count;
	int m_mini;
public:
	// construction/destruction.

	explicit CStemListViewItem(Q3ListView* parent = 0);
	CStemListViewItem(Q3ListView* parent, QString stem_text,
		int mini_index = -1, CStem* stem = 0,
		QMap<QString, QString>* filter = 0,
		int char_count = 27);
	CStemListViewItem(Q3ListViewItem* parent, QString stem_text,
		int mini_index = -1, CStem* stem = 0,
		QMap<QString, QString>* filter = 0,
		int char_count = 27);

	// copy.

	CStemListViewItem(const CStemListViewItem& x)
		: Q3ListViewItem(x),
		m_Stem(x.m_Stem),
		m_filter(x.m_filter),
		m_char_count(x.m_char_count),
		m_mini(x.m_mini) { }
	CStemListViewItem& operator=(const CStemListViewItem& x)
	{
		Q3ListViewItem::operator=(x);
		m_Stem = x.m_Stem;
		m_filter = x.m_filter;
		m_char_count = x.m_char_count;
		m_mini = x.m_mini;
		return *this;
	}

	// Qt3 list view item interface.

	virtual QString text(int column_index) const;
	int compare(Q3ListViewItem* other, int column, bool ascending) const;
	virtual QString key(int column, bool ascending) const;

	// underlying stem object.

	CStem* GetStem() { return m_Stem; }
	void SetStem(CStem* pStem) { m_Stem = pStem; }
};

#endif // STEMLISTVIEWITEM_H
