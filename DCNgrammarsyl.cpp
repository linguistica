// Implementation of grammarsyl methods
// Copyright © 2009 The University of Chicago
#include "DCNgrammarsyl.h"
#include <QLabel>


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

grammarsyl::grammarsyl(){}

grammarsyl::~grammarsyl(){}

grammarsyl::grammarsyl(grammarsyl &theGrammar)
{
	alpha	= theGrammar.getAlpha();
	beta	= theGrammar.getBeta();
	map		= theGrammar.getMap();
}


void	grammarsyl::setValues(float alpha, float beta)
{
	this->alpha = alpha;
	this->beta  = beta;
}

void	grammarsyl::print(QLabel *label)
{
	QString totalString;
	QString s;
	QString n;

	n.setNum(alpha);
	totalString =  "alpha:\t"	+	n + '\n';
	n.setNum(beta);
	totalString += "beta: \t"	+	n + '\n';

	SonorityMap::const_iterator it = map.begin();
	SonorityMap::const_iterator end = map.end();

	while (it != end)
	{
		n.setNum(it.data());
		totalString += QString (it.key()) + "\t" + n + '\n';

		it++;
	}

	label->setText(totalString);
}

QString	grammarsyl::print()
{
	QString totalString;
	QString s;
	QString n;

	n.setNum(alpha);
	totalString =  "alpha: "	+	n + "\n";
	n.setNum(beta);
	totalString += "beta:   "	+	n + "\n\n";

	SonorityMap::const_iterator it = map.begin();
	SonorityMap::const_iterator end = map.end();

	while (it != end)
	{
		n.setNum(it.data());
		totalString += QString (it.key()) + "\t" + n + '\n';

		it++;
	}

	return totalString;
}

void	grammarsyl::setSonority(QChar c, float sonority)
{
	map[c] = sonority;
}

float	grammarsyl::getSonority(QChar c) const
{
		return map[c];
}

bool grammarsyl::isInMap(QChar c) const
{
	return map.contains(c);
}

