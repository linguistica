// Graphic display properties dialog
// Copyright © 2009 The University of Chicago
#ifndef PROPERTIESDIALOG_H
#define PROPERTIESDIALOG_H

#include <QDialog>
#include "ui_propertiesdialogbase.h"

class PropertiesDialog : public QDialog, private Ui::PropertiesDialogBase
{
    Q_OBJECT

public:
	PropertiesDialog();

	void exec(Q3CanvasItem*);
};

#endif // PROPERTIESDIALOG_H
