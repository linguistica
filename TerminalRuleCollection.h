// CTerminalRuleCollection class
// Copyright © 2009 The University of Chicago
#ifndef TERMINALRULECOLLECTION_H
#define TERMINALRULECOLLECTION_H

class CTerminalRuleCollection;

#include "CollectionTemplate.h"
template<class T> class Q3PtrList;

class CTerminalRuleCollection : public TCollection<class CTerminalRule> {
public:
	CTerminalRuleCollection();
	virtual ~CTerminalRuleCollection();

	// insert.
	CTerminalRule* operator<<(CTerminalRule* rule);

	void FindSubstrings(CStringSurrogate& str,
		Q3PtrList<CTerminalRule>& out);
	int GetShortest();
};

#endif // TERMINALRULECOLLECTION_H
