// Implementation for CWordCollection methods
// Copyright © 2009 The University of Chicago
#include "WordCollection.h"

#include <vector>
#include <Q3TextStream>
#include "ui/Status.h"
#include "MiniLexicon.h"
#include "Lexicon.h"
#include "MonteCarlo.h"
#include "Alignment.h"
#include "Signature.h"
#include "Prefix.h"
#include "Suffix.h"
#include "Stem.h"
#include "SignatureCollection.h"
#include "PrefixCollection.h"
#include "SuffixCollection.h"
#include "BiphoneCollection.h"
#include "PhoneCollection.h"
#include "StemCollection.h"
#include "StringSurrogate.h"
#include "Typedefs.h"
#include "HTML.h"
#include "log2.h"
#include "DLHistory.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


CWordCollection::CWordCollection(CMiniLexicon* Lex)
	: TCollection<CStem>(Lex, "Words"),
	m_AffixLocation(WORD_FINAL),
	m_DisplayMode(CWordListViewItem::MiniLexicon_MorphologyStuffFirst),
	m_Phones_Tier1(new CPhoneCollection(this)),
	m_Phones_Tier2(new CPhoneCollection(this)),
	m_Phones_Tier1_Skeleton(new CPhoneCollection(this)),
	m_PhonologicalContent_Unigram(0.0),
	m_PhonologicalContent_Bigram(0.0),
	m_Tier2_LocalMI_Score(0.0),
	m_Tier2_DistantMI_Score(0.0),
	m_LocalMI_TotalBoltzmannScore(0.0),
	m_LocalMI_Plog(0.0),
	m_DistantMI_TotalBoltzmannScore(0.0),
	m_DistantMI_Plog(0.0),
	m_MyZ_Local(0.0),
	m_MyZ_Distant(0.0),
	m_Vowels(new CParse()) { }

CWordCollection::~CWordCollection()
{
	delete m_Phones_Tier1;
	delete m_Phones_Tier2;
	delete m_Phones_Tier1_Skeleton;
	delete m_Vowels;

	if (m_pLexicon != 0) {
		CLexicon& lex = *m_pLexicon;

		for (int i = 0; i < GetCount(); ++i)
			lex.RemoveWord(GetAt(i));
	}
}

CStem* CWordCollection::operator<< ( CStem* Word )
{
  CStem*	pWord;

  pWord =	AddToCollection (*Word);

  QChar*	pAlpha;

  pAlpha	= LxAlphabetizeString ( Word->GetKeyPointer(), Word->GetKeyLength() );
  pWord		->SetAlphabetizedForm ( QString( pAlpha, Word->GetKeyLength() ) );

  return pWord;
}

CStem* CWordCollection::operator<< ( CStem& Word )
{
  CStem*	pWord;

  pWord =	AddToCollection (Word);

  QChar*	pAlpha;

  pAlpha	= LxAlphabetizeString ( Word.GetKeyPointer(), Word.GetKeyLength() );
  pWord		->SetAlphabetizedForm ( QString( pAlpha, Word.GetKeyLength() ) );

  return pWord;
}
CStem* CWordCollection::operator<< ( CStringSurrogate& Word )
{
  CStem*  pWord;

  pWord = AddToCollection( Word );

  QChar*	pAlpha;
  pAlpha	= LxAlphabetizeString ( Word.GetKey(), Word.GetLength() );
  pWord		->SetAlphabetizedForm ( QString( pAlpha, Word.GetLength() ) );

  return pWord;
}


CStem* CWordCollection::operator<< ( CParse* Word )
{
  CStem*  pWord;

  pWord = AddToCollection (*Word);

  QChar* pAlpha;
  pAlpha = LxAlphabetizeString ( Word->GetKeyPointer(), Word->GetKeyLength() );
  pWord->SetAlphabetizedForm ( QString( pAlpha, Word->GetKeyLength() ) );

  return pWord;
}


CStem* CWordCollection::operator<< ( QString Word )
{
  CStem*  pWord;

  CStringSurrogate cssWord( Word );
  pWord = AddToCollection( cssWord );

  QChar* pAlpha;
  pAlpha = LxAlphabetizeString ( Word.unicode(), Word.length() );
  pWord->SetAlphabetizedForm ( QString( pAlpha, Word.length() ) );

  return pWord;
}


void CWordCollection::AddPointer( CStem* pWord )
{
	TCollection<CStem>::AddPointer( pWord );

	if( m_pLexicon ) m_pLexicon->InsertWord( pWord );
}


CStem* CWordCollection::AddToCollection( CParse& Word )
{
	CStem* pWord = TCollection<CStem>::AddToCollection( Word );

	if( m_pLexicon ) m_pLexicon->InsertWord( pWord );

	return pWord;
}


CStem* CWordCollection::AddToCollection( CStringSurrogate& Word )
{
	CStem* pWord = TCollection<CStem>::AddToCollection( Word );

	if( m_pLexicon ) m_pLexicon->InsertWord( pWord );

	return pWord;
}


void CWordCollection::Empty()
{
	if( m_pLexicon )
	{
		CStem* pWord;

		for( int i = 0; i < GetCount(); i++ )
		{
			pWord = GetAt(i);

			m_pLexicon->RemoveWord( pWord );
		}
	}

	TCollection<CStem>::Empty();
}


void CWordCollection::RemoveAll()
{
	if( m_pLexicon )
	{
		CStem* pWord;

		for( int i = 0; i < GetCount(); i++ )
		{
			pWord = GetAt(i);

			m_pLexicon->RemoveWord( pWord );
		}
	}

	TCollection<CStem>::RemoveAll();
}


bool CWordCollection::Remove( CStem* pWord )
{
	if( m_pLexicon ) m_pLexicon->RemoveWord( pWord );

	return TCollection<CStem>::Remove( pWord );
}


bool CWordCollection::RemoveMember( CStem* pWord )
{
	if( m_pLexicon ) m_pLexicon->RemoveWord( pWord );

	return TCollection<CStem>::RemoveMember( pWord );
}


bool CWordCollection::RemoveMember( CStringSurrogate& Word )
{
	CStem* pWord = *this ^= Word;

	if( m_pLexicon ) m_pLexicon->RemoveWord( pWord );

	return TCollection<CStem>::RemoveMember( Word );
}


bool CWordCollection::RemoveMember( CStringSurrogate& Word, bool b )
{
	CStem* pWord = *this ^= Word;

	if( m_pLexicon ) m_pLexicon->RemoveWord( pWord );

	return TCollection<CStem>::RemoveMember( Word, b );
}


void CWordCollection::DeleteMarkedMembers()
{
	if ( m_DeletionArray == NULL ) { return; }

	int count = GetCount();
	for (int i = 0; i < count; i++)
	{
		if ( m_DeletionArray[i] == 1 )
		{
			if( m_pLexicon ) m_pLexicon->RemoveWord( m_PointerArray[i] );
		}
	}

	TCollection<CStem>::DeleteMarkedMembers();
}


CPhoneCollection* CWordCollection::GetPhones()
{
	return m_Phones_Tier1;
}
CPhoneCollection* CWordCollection::GetPhones_Tier1_Skeleton()
{
	return m_Phones_Tier1_Skeleton;
}
CPhoneCollection* CWordCollection::GetPhones_Tier2()
{
	return m_Phones_Tier2;
}
 
void CWordCollection::AssignSignatureFromStemsAffixPointer(
	enum eAffixLocation AffixLoc)
{
	CLexicon& lex = *m_pLexicon;
	linguistica::ui::status_user_agent& status = lex.status_display();

	status.details = "Assign signature to word";
	status.progress.clear();
	status.progress.set_denominator(GetCount());
	for (int i = 0; i < GetCount(); i++) {
		CStem& word = *GetAt(i);
		status.progress = i;
		if (CStem* const stem = word.GetStemPtr()) {
			if (is_initial(AffixLoc))
				word.SetPrefixSignature(
					stem->GetPrefixSignature());
			else
				word.SetSuffixSignature(
					stem->GetSuffixSignature());
		}
	}
	status.progress.clear();
	status.details.clear();
}

/// TODO: this gets very slow when the total
/// requested amount is large (>300,000) we
/// should keep a running total during
/// analysis.  For now, I am adding count down
/// capabilities so the user doesn't think the
/// program is stalled
int CWordCollection::HowManyAreAnalyzed(int& HowManyNotAnalyzed,
	linguistica::ui::status_user_agent& status)
{
	status.major_operation = "Counting analyzed words";
	status.progress.clear();
	status.progress.set_denominator(GetCount());
	int count = 0;
	HowManyNotAnalyzed = 0;
	for (int i = 0; i < GetCount(); i++) {
		CStem& word = *GetAt(i);
		status.progress = i;
		if (word.GetSuffixSignature() != 0)
			count++;
		else
			HowManyNotAnalyzed++;
	}
	status.progress.clear();
	status.major_operation.clear();
	return count;
}

void CWordCollection::PredecessorFreq1(CStemCollection* Stems,
	CPrefixCollection* Prefixes,
	CSignatureCollection* Signatures,
	enum eSuccessorFrequencyMode PFM,
	int ToBeRemoved)
{
	CLexicon& lex = *m_pLexicon;
	linguistica::ui::status_user_agent& status = lex.status_display();
	QString Null = "NULL";

	const int MinimumNumberOfAppearancesOfPrefix = m_pLexicon->GetIntParameter(
		"PredecessorFrequency\\MinimumNumberOfAppearancesOfPrefix", 2);
	const int MinimumNumberOfStemsInSignature = m_pLexicon->GetIntParameter(
		"PredecessorFrequency\\MinimumNumberOfStemsInSignature", 2);
	const int MinimumLengthOfSignature = m_pLexicon->GetIntParameter(
		"PredecessorFrequency\\MinimumLengthOfSignature", 2);
	const int LengthOfAStrongSignature = m_pLexicon->GetIntParameter(
		"PredecessorFrequency\\LengthOfAStrongSignature", 5);
	const int LargeNumberOfStems = m_pLexicon->GetIntParameter(
		"PredecessorFreq1\\LargeNumberOfStems", 25);
	const int MaxNeighborPredecessorCount = m_pLexicon->GetIntParameter(
		"PredecessorFrequency\\MaxNeighborPredecessorCount", 1);
	const int MaximumPrefixLength = m_pLexicon->GetIntParameter(
		"Main\\MaximumPrefixLength", 5);
	const int MinimumStemLength = m_pLexicon->GetIntParameter(
		"Main\\MinimumStemLength", 3);

	m_AffixLocation = WORD_INITIAL;

	if (GetCount() == 0)
		return;

	status.major_operation = PFM == SF1 ?
		"Predecessor Freq 1" :
		"Predecessor Freq 2";
	status.progress.clear();
	if (PFM == SF1)
		Sort(KEY);
	T_PredecessorFrequency(PFM,
		PFM == SF1 ? 0 : m_pMiniLex->GetStems(),
		PFM == SF1 ? 0 : m_pMiniLex->GetPrefixes(),
		MaxNeighborPredecessorCount,
		MaximumPrefixLength,
		MinimumStemLength);

	// Use the breaks discovered by PredecessorFrequency.
	status.progress.clear();
	status.progress.set_denominator(GetCount());
	for (int wordno = 0; wordno < GetCount(); wordno++) {
		CStem& word = *GetAt(wordno);
		status.progress = wordno;
		if (!word.MayBeParsed())
			// it's a compound.
			continue;

		// Do not analyze unwanted types.
		// hack: simulate labelled continue
		struct unwanted { };
		try {
			for (enum CStem::type t = CStem::POLYWORD_PIECE;
			     t != CStem::type(0);
			     t = CStem::type(t >> 1)) {
				if (ToBeRemoved & t && word.GetWordType() == t)
					throw unwanted();
				ToBeRemoved &= ~t;
			}
		} catch (unwanted) { continue; }

		if (word.Size() <= 1)
			// no prefix or unanalyzed.
			continue;

		// XXX. suffix version calls Display() explicitly
		CPrefix& prefix = *(*Prefixes << word.GetPiece(1));
		// XXX. What about the stem pointer list?
		prefix.AppendToStemString(word.GetPiece(2));
		word.SetStemLoc(2);
		word.SetPrefixLoc(1);
		lex.UpdateWord(&word);
	}
	status.progress.clear();

	// If a prefix appears only rarely, get rid of it.
	for (int affixno = 0; affixno < Prefixes->GetCount(); affixno++) {
		CPrefix& prefix = *Prefixes->GetAt(affixno);
		if (prefix.GetCorpusCount() < MinimumNumberOfAppearancesOfPrefix)
			prefix.MergeMeWithMyStems(m_pMiniLex);
	}

	// Make temporary signatures.
	status.progress.clear();
	status.progress.set_denominator(GetCount());
	for (int wordno = 0; wordno < GetCount(); wordno++) {
		CStem& word = *GetAt(wordno);
		status.progress = wordno;
		if (!word.MayBeParsed())
			// it's a compound.
			continue;
		if (word.Size() <= 1)
			// no prefix or unanalyzed.
			continue;
		CStem& stem = *(*Stems << word.GetPiece(2));
		stem.AddPrefix(word.GetPiece(1));
	}
	status.progress.clear();

	for (int stemno = 0; stemno < Stems->GetCount(); stemno++) {
		CStem& stem = *Stems->GetAt(stemno);
		if (*this ^= stem.GetKey())
			stem.AddPrefix(Null);
		CSignature& sig = *(*Signatures << stem.GetPrefixList());
		sig.AttachToPrefixSig(&stem);
	}

	std::vector<CSignature*> BadSigs;
	for (int signo = 0; signo < Signatures->GetCount(); signo++) {
		CSignature& sig = *Signatures->GetAt(signo);
		int Length = 0;
		bool GoodFlag = true;

		for (int m = 1; m <= sig.Size(); m++) {
			if (sig.GetPiece(m).Display() == "NULL")
				Length += 1;
			else
				Length += sig.GetPiece(m).GetLength() - 1;
			// so NULL.b and b.p are both bad,
			// but NULL.br and br.pr are both good
		}

		if (Length < MinimumLengthOfSignature &&
		    sig.GetNumberOfStems() < LargeNumberOfStems) {
			BadSigs.push_back(&sig);
			GoodFlag = false;
		}

		// the idea in what follows
		// is that if a signature is strong enough,
		// even one example suffices:
		if (Length < LengthOfAStrongSignature &&
		    sig.GetNumberOfStems() < MinimumNumberOfStemsInSignature) {
			BadSigs.push_back(&sig);
			GoodFlag = false;
		}
	}

	// Undo the bad signatures.
	for (unsigned int signo = 0; signo < BadSigs.size(); signo++) {
		CSignature& sig = *BadSigs.at(signo);

		// XXX. suffix version uses GetStemPtrList()->size()
		for (int stemno = 0; stemno < sig.GetNumberOfStems(); stemno++) {
			CStem& stem = *sig.GetStem(stemno);
			CStringSurrogate ssStem = stem.GetKey();
			for (int affixno = 1; affixno <= sig.Size(); affixno++) {
				CStringSurrogate ssAffix = sig.GetPiece(affixno);
				if (ssAffix.IsNULL())
					continue;

				CParse word_string = ssAffix + ssStem;
				if (CStem* word = *this ^= word_string) {
					word->ClearPrefixStemSplit();
					m_pLexicon->UpdateWord(word);
				}
			}
		}
	}

	// Now we have good word splits.
	// XXX. suffix version factors out this section
	status.progress.clear();
	status.progress.set_denominator(GetCount());
	for(int i = 0; i < GetCount(); i++) {
		CStem& word = *GetAt(i);
		word.ClearPointers();
		status.progress = i;
		if (!word.MayBeParsed())
			// it's a compound.
			continue;
		if (word.Size() < 2)
			// Unanalyzed
			continue;
		CStem& stem = *(*Stems << word.GetPiece(2));
		CPrefix& prefix = *(*Prefixes << word.GetPiece(1));

		word.AttachWordStemAndPrefix(&stem, &prefix);
		// the above already increased the corpus counts by 1.
		stem.IncrementCorpusCount(word.GetCorpusCount() - 1);
		prefix.IncrementCorpusCount(word.GetCorpusCount() - 1);
		stem.IncrementWordCount();
		stem.SetConfidence("PF 1");
		word.SetStemLoc(2);
		word.SetPrefixLoc(1);
		lex.UpdateWord(&word);
		word.DoNotParse();
	}
	status.progress.clear();

	// XXX. suffix version calculates description length
	// and updates description length history.

	for (int i = 0; i < Stems->GetCount(); i++) {
		CStem& stem = *Stems->GetAt(i);
		if (*this ^= stem.GetKey()) {
			stem.AddPrefix(Null);
			CPrefix& prefix = *(
				*m_pMiniLex->GetPrefixes() <<
				CStringSurrogate(Null));
			prefix.IncrementUseCount();
			prefix.AddStem(&stem);
		}

		CSignature& sig = *(*Signatures << stem.GetPrefixList());
		sig.AttachToPrefixSig(&stem);
		sig.IncrementCorpusCount(stem.GetCorpusCount());

		for (int z = 0; z < stem.GetWordPtrList()->size(); z++) {
			CStem& word = *(stem.GetWordPtrList()->at(z));
			word.SetPrefixSignature(&sig);
		}

		sig.SetRemark(PFM == SF1 ? "PF1" : "PF2");
	}
	// XXX. not an operation
	status.major_operation = PFM == SF1 ?
		"End of Predecessor Freq 1" :
		"End of Predecessor Freq 2";
}

/// SuccessorFreq-1.
/// Suffixes/Successor frequencies
/// bootstrap heuristic
void CWordCollection::SuccessorFreq1(
	CStemCollection* Stems, CSuffixCollection* Suffixes,
	CSignatureCollection* Signatures,
	enum eSuccessorFrequencyMode SFM,
	int ToBeRemoved)
{
	CLexicon& lex = *m_pLexicon;
	linguistica::ui::status_user_agent& status = lex.status_display();
	QString Null = "NULL";

	const int MinimumNumberOfAppearancesOfAffix = m_pLexicon->GetIntParameter(
		"SuccessorFreq1\\MinimumNumberOfAppearancesOfSuffix", 3);
	const int MinimumNumberOfStemsInSignature = m_pLexicon->GetIntParameter(
		"SuccessorFreq1\\MinimumNumberOfStemsInSignature", 2);
	const int MinimumLengthOfSignature = m_pLexicon->GetIntParameter(
		"SuccessorFreq1\\MinimumLengthOfSignature", 2);
	const int LengthOfAStrongSignature = m_pLexicon->GetIntParameter(
		"SuccessorFreq1\\LengthOfAStrongSignature", 4);
	const int LargeNumberOfStems = m_pLexicon->GetIntParameter(
		"SuccessorFreq1\\LargeNumberOfStems", 25);
	const int MaxNeighborSuccessorCount = m_pLexicon->GetIntParameter(
		"SuccessorFreq1\\MaxNeighborSuccessorCount", 1);
	const int MaximumAffixLength = m_pLexicon->GetIntParameter(
		"Main\\MaximumSuffixLength", 5);
	const int MinimumStemLength = m_pLexicon->GetIntParameter(
		"Main\\MinimumStemLength", 3);

	m_AffixLocation = WORD_FINAL;

	if (GetCount() == 0)
		return;

	status.major_operation = SFM == SF1 ?
		"Successor Freq 1" :
		"Successor Freq 2";
	status.progress.clear();
	if (SFM == SF1)
		Sort(KEY);
	T_SuccessorFrequency(SFM,
		SFM == SF1 ? 0 : m_pMiniLex->GetStems(),
		SFM == SF1 ? 0 : m_pMiniLex->GetSuffixes(),
		MaxNeighborSuccessorCount,
		MaximumAffixLength,
		MinimumStemLength);

	// Use the breaks discovered by SuccessorFrequency.
	status.progress.clear();
	status.progress.set_denominator(GetCount());
	for (int wordno = 0; wordno < GetCount(); wordno++) {
		CStem& word = *GetAt(wordno);
		status.progress = wordno;
		if (!word.MayBeParsed())
			// it's a compound.
			continue;

		// Do not analyze unwanted types.
		// hack: simulate labelled continue
		struct unwanted { };
		try {
			for (enum CStem::type t = CStem::POLYWORD_PIECE;
			     t != CStem::type(0);
			     t = CStem::type(t >> 1)) {
				if (ToBeRemoved & t && word.GetWordType() == t)
					throw unwanted();
				ToBeRemoved &= ~t;
			}
		} catch (unwanted) { continue; }

		if (word.Size() <= 1)
			// no affix or unanalyzed.
			continue;

		CSuffix& suffix = *(*Suffixes << word.GetPiece(2).Display());
		// XXX. What about the stem pointer list?
		suffix.AppendToStemString(word.GetPiece(1));
		word.SetStemLoc(1);
		word.SetSuffixLoc(2);
		lex.UpdateWord(&word);
	}
	status.progress.clear();

	// If an affix appears only rarely, get rid of it.
	for (int affixno = 0; affixno < Suffixes->GetCount(); affixno++) {
		CSuffix& suffix = *Suffixes->GetAt(affixno);
		if (suffix.GetCorpusCount() < MinimumNumberOfAppearancesOfAffix)
			suffix.MergeMeWithMyStems(m_pMiniLex);
	}

	// Make temporary signatures.
	status.progress.clear();
	status.progress.set_denominator(GetCount());
	for (int wordno = 0; wordno < GetCount(); wordno++) {
		CStem& word = *GetAt(wordno);
		status.progress = wordno;
		if (!word.MayBeParsed())
			// it's a compound.
			continue;
		if (word.Size() <= 1)
			// no affix or unanalyzed.
			continue;
		CStem& stem = *(*Stems << word.GetPiece(1));
		stem.AddSuffix(word.GetPiece(2));
	}
	status.progress.clear();

	for (int stemno = 0; stemno < Stems->GetCount(); stemno++) {
		CStem& stem = *Stems->GetAt(stemno);
		if (*this ^= stem.GetKey())
			stem.AddSuffix(Null);
		CSignature& sig = *(*Signatures << stem.GetSuffixList());
		sig.AttachToSuffixSig(&stem);
	}

	std::vector<CSignature*> BadSigs;
	for (int signo = 0; signo < Signatures->GetCount(); signo++) {
		CSignature& sig = *Signatures->GetAt(signo);
		int Length = 0;
		bool GoodFlag = true;

		for (int m = 1; m <= sig.Size(); m++) {
			if (sig.GetPiece(m).Display() == "NULL")
				Length += 1;
			else
				Length += sig.GetPiece(m).GetLength() - 1;
			// so NULL.b and b.p are both bad,
			// but NULL.br and br.pr are both good
		}

		if (Length < MinimumLengthOfSignature &&
		    sig.GetNumberOfStems() < LargeNumberOfStems) {
			BadSigs.push_back(&sig);
			GoodFlag = false;
		}

		// the idea in what follows
		// is that if a signature is strong enough,
		// even one example suffices:
		if (Length < LengthOfAStrongSignature &&
		    sig.GetNumberOfStems() < MinimumNumberOfStemsInSignature) {
			BadSigs.push_back(&sig);
			GoodFlag = false;
		}
	}

	// Undo the bad signatures.
	for (unsigned int signo = 0; signo < BadSigs.size(); signo++) {
		CSignature& sig = *BadSigs.at(signo);

		for (int stemno = 0; stemno < sig.GetStemPtrList()->size(); stemno++) {
			CStem& stem = *sig.GetStem(stemno);
			CStringSurrogate ssStem = stem.GetKey();
			for (int affixno = 1; affixno <= sig.Size(); affixno++) {
				CStringSurrogate ssAffix = sig.GetPiece(affixno);
				if (ssAffix.IsNULL())
					continue;

				CParse word_string = ssStem + ssAffix;
				if (CStem* word = *this ^= word_string) {
					word->ClearRootSuffixSplit();
					m_pLexicon->UpdateWord(word);
				}
			}
		}
	}

	// Now we have good word splits.
	{
		CStringSurrogate Heuristic("Initial heuristic");
		m_pMiniLex->TakeSplitWords_ProduceStemsAndSigs(Heuristic);
	}

	GetMiniLexicon()->CalculateDescriptionLength();
	QString mini_name =
		QString("Mini-Lexicon %1").arg(m_pMiniLex->GetIndex() + 1);
	m_pLexicon->GetDLHistory()->append(mini_name, "SuccessorFreq", m_pMiniLex);

	// XXX. necessary?
	status.progress.clear();
}

void CWordCollection::OutputWords(QString FileName,
	QMap<QString, QString>* filter)
{
	QFile file( FileName );

	if( file.open( QIODevice::WriteOnly ) )
	{
		Q3TextStream outf( &file );
		outf.setEncoding( Q3TextStream::Unicode );

		outf << "# Word Count" << endl;
		outf << "# ----------" << endl;
		outf << "  " << GetCount() << endl << endl;

		outf << "# Index | Size | Pieces                                   | Corpus Count | Prefix Loc | Stem Loc | Suffix Loc | Signature " << endl;
		outf << "# ----------------------------------------------------------------------------------------------------------------------- " << endl;
		
		Sort(CORPUSCOUNT);
		for (int i = 0; i < GetCount(); ++i)
			GetAtSort(i)->OutputWord(outf, i, filter);
	}
}


void CWordCollection::OutputWordsForTesting (QString FileName)
{
  CStem* pWord;
  QFile  file( FileName );

  if( file.open(QIODevice::WriteOnly) )
  {
    Q3TextStream  outf( &file );
    outf.setEncoding ( Q3TextStream::Unicode );

    outf.setf(2);
    outf << GetCount() << endl;

    Sort (KEY);
    for (int i = 0; i< (int)GetCount(); i++)
    {

      pWord = GetAtSort(i);

      outf.width(20);
      outf <<  pWord->GetKey().Display( m_pLexicon->GetOutFilter() ) << " ";
      outf.width(2);
      outf << pWord->Size() << " ";

      if ( pWord->GetStemPtr() )
      {
        outf.width(20);
        outf << pWord->GetStemPtr()->GetKey().Display( m_pLexicon->GetOutFilter() ) << " ";
      }
      if ( pWord->GetSuffixPtr() )
      {
        outf.width(8);
        outf << pWord->GetSuffixPtr()->GetKey().Display( m_pLexicon->GetOutFilter() ) << " ";
      }
      outf << endl;
    }
    file.close();
  }
}

void CWordCollection::ReadWordFile(QString FileName)
{
	CLexicon& lex = *m_pLexicon;
	linguistica::ui::status_user_agent& status = lex.status_display();

	CStem* pWord;
	CSignature* pSig;
	QFile file(FileName);
	QString buffer, QstrSignature, source;
	int index, size, corpusCount, prefixLoc, stemLoc,
		suffixLoc, numberOfPieces, lineCount = 0;

	if (file.exists() && file.open(QIODevice::ReadOnly)) {
		Q3TextStream inf(&file);
		inf.setEncoding(Q3TextStream::Locale);

		buffer = inf.readLine();
		Q_ASSERT(buffer[0] == '#');

		buffer = inf.readLine();
		Q_ASSERT(buffer[0] == '#');

		inf >> size;

		buffer = inf.readLine();	// end of size line
		Q_ASSERT(buffer.length() == 0);

		buffer = inf.readLine();	// blank line
		Q_ASSERT(buffer.length() == 0);

		buffer = inf.readLine();
		Q_ASSERT( buffer[0] == '#' );

		buffer = inf.readLine();
		Q_ASSERT( buffer[0] == '#' );

		status.progress.clear();
		status.progress.set_denominator(size);
		while (!inf.atEnd() && lineCount < size) {
			status.progress = lineCount++;

			pWord = new CStem(m_pMiniLex);
			inf >> index >> numberOfPieces;
			if (numberOfPieces == 0)
				numberOfPieces = 1;

			for (int i = 1; i <= numberOfPieces; i++) {
				inf >> buffer;

				// Filter all sequences that should be
				// analyzed as one character
				buffer = Filter(m_pLexicon->GetInFilter(), buffer);

				pWord->Append(buffer);
			}

			inf >> corpusCount >>
				prefixLoc >>
				stemLoc >>
				suffixLoc >>
				QstrSignature;

			pWord->SetCorpusCount(corpusCount);
			m_CorpusCount += corpusCount;
			pWord->SetPrefixLoc(prefixLoc);
			pWord->SetStemLoc(stemLoc);
			pWord->SetSuffixLoc(suffixLoc);
			pWord->SetIndex(index);

			if (QstrSignature != "NONE") {
				// Filter all sequences that should be
				// analyzed as one character
				QstrSignature = Filter(m_pLexicon->GetInFilter(), QstrSignature);

				pSig = *m_pMiniLex->GetSignatures() ^= QstrSignature;

				if (suffixLoc != 0) {
					pWord->SetSuffixSignature(pSig);
					pWord->SetPrefixSignature(NULL);
				} else if (prefixLoc != 0) {
					pWord->SetPrefixSignature(pSig);
					pWord->SetSuffixSignature(NULL);
				}
			}

			// Insert the word into the trie
			CNode* pTerminal;
			pTerminal = Insert(pWord->GetKey());
			pTerminal->SetPointer(pWord);
		}
		status.progress.clear();
		file.close();
	}  
}

//=====================================================================
void CWordCollection::CreatePhonologyFromOrthography()
{
	int i;
	for (i = 0; i<GetSize(); i++)
	{
		GetAt(i)->CreatePhonologyFromOrthography();
	}
}

void CWordCollection::CountPhonesAndBiphones(enum eTier WhichTier)
{
	CLexicon& lex = *m_pLexicon;
	linguistica::ui::status_user_agent& status = lex.status_display();

	status.major_operation = "Counting phones and biphones:";
	status.progress.clear();

	CPhoneCollection* phones =
		WhichTier == TIER_1 ? m_Phones_Tier1 :
		WhichTier == TIER_2 ? m_Phones_Tier2 :
		WhichTier == TIER_1_SKELETON ? m_Phones_Tier1_Skeleton :
		0;

	phones->Empty();
	// XXX. probably a typo but kept to be safe
	if (WhichTier == TIER_1)
		phones->GetMyBiphones()->Empty();
	else
		phones->GetMyBiphones()->clear();

	status.progress.set_denominator(GetCount());
	for (int i = 0; i < GetCount(); i++) {
		status.progress = i;
		phones->CountPhonesAndBiphonesInWord(GetAt(i), WhichTier);
	}
	status.progress.clear();
	status.major_operation.clear();
}

void CWordCollection::DoPhonology()
{	 
//	m_Vowels->ClearParse();
//	m_Vowels->Collapse( CStringSurrogate (m_pLexicon->GetStringParameter( "Symbols\\Vowels" ) ) ); 	   

	const QChar vowels[] = {
		'a', 'e', 'i', 'o', 'u', 'y',
		'A', 'E', 'I', 'O', 'U', // XXX. but not Y in Yttrium?
		228, 246, '@' // ä, ö, @
	};
	const std::size_t num_vowels = sizeof(vowels)/sizeof(QChar);

	for (const QChar* vowel = &vowels[0];
			vowel != &vowels[num_vowels];
			++vowel)
		*m_Vowels << CSS(vowel, 0, 1);

	static bool AlreadyDone(false);

	if ( AlreadyDone == false && GetCount () > 0 )
	{
		CreatePhonologyFromOrthography();
 		 
		CountPhonesAndBiphones(TIER_1);
		// XXX. this should be made conditional on Tier2
		// having been already created
//		CountPhonesAndBiphones(TIER_2);

		GetPhones()->Normalize();
		GetPhones_Tier2()->Normalize();
		
		ComputeProbabilitiesOfWords();
		GetPhonologyTierInfoForGraphOfWords(); 
	}

	AlreadyDone = true;

	m_DisplayMode = CWordListViewItem::MiniLexicon_PhonologyStuffFirst;
}

//====================================================================
void CWordCollection::CreateCVTemplate()
{
	
	int i;
	static bool AlreadyDone(false);

	if (AlreadyDone == true ) return;

	for (i = 0; i<GetSize(); i++)
	{
		GetAt(i)->CreateCVTemplate( m_Vowels);
	}
	CountPhonesAndBiphones(TIER_1_SKELETON);
	m_Phones_Tier1_Skeleton->Normalize();

	AlreadyDone =  true;

}

void CWordCollection::SplitPhonologyToTiers(
	enum CStem::ePhonologySplitType SplitType)
{
	int i;
	static bool AlreadyDone = false;

	if ( AlreadyDone == true) return;

	for (i = 0; i<GetSize(); i++)
	{
		GetAt(i)->SplitPhonologyToTiers (SplitType, *m_Vowels);
	}
	CountPhonesAndBiphones(TIER_1);
	CountPhonesAndBiphones(TIER_2);

	GetPhones()->Normalize();
	GetPhones_Tier2()->Normalize();
	
	ComputeProbabilitiesOfWords();
	
	AlreadyDone = true;

}
 
void CWordCollection::ComputeProbabilitiesOfWords()
{
	CLexicon& lex = *m_pLexicon;
	linguistica::ui::status_user_agent& status = lex.status_display();

	m_PhonologicalContent_Unigram = 0;
	m_PhonologicalContent_Bigram = 0;
	m_Tier2_LocalMI_Score = 0;
	m_Tier2_DistantMI_Score = 0;
	m_MyZ_Local = 0;
	m_MyZ_Distant = 0;

	status.major_operation = "Computing word probabilities:";
	status.progress.clear();
	status.progress.set_denominator(GetSize());
	for (int i = 0; i < GetSize(); i++) {
		status.progress = i;
		GetAt(i)->ComputeProbabilities(this);
		m_PhonologicalContent_Unigram += GetAt(i)->GetUnigramLogProb();
		m_PhonologicalContent_Bigram += GetAt(i)->GetBigramLogProb();
		m_Tier2_LocalMI_Score += GetAt(i)->GetTier2_LocalMI_Score();
		m_Tier2_DistantMI_Score += GetAt(i)->GetTier2_DistantMI_Score();
		m_LocalMI_Plog += GetAt(i)->GetLocalMI_Plog();
		m_DistantMI_Plog += GetAt(i)->GetDistantMI_Plog();
	}
	status.progress.clear();
	status.major_operation.clear();

	m_pLexicon->AddToScreen(QString("Unigram content: %1	Bigram content: %2")
		.arg(m_PhonologicalContent_Unigram, 10)
		.arg(m_PhonologicalContent_Bigram, 10));
}

void CWordCollection::ComputeBoltzmannProbabilities()
{
	int i;
	m_Tier2_LocalMI_Score			=0;
	m_Tier2_DistantMI_Score			=0;
	m_LocalMI_TotalBoltzmannScore	=0; 
	m_LocalMI_Plog					=0;
	m_DistantMI_TotalBoltzmannScore	=0; 
	m_DistantMI_Plog				=0;

	

	for (i = 0; i<GetSize(); i++)
	{
		GetAt(i)->ComputeBoltzmannProbabilities(m_MyZ_Local, 
												m_MyZ_Distant);	
		m_Tier2_LocalMI_Score			+= GetAt(i)->GetTier2_LocalMI_Score();
		m_Tier2_DistantMI_Score			+= GetAt(i)->GetTier2_DistantMI_Score();
		m_LocalMI_TotalBoltzmannScore	+= GetAt(i)->GetLocalMI_TotalBoltzmannScore();
		m_LocalMI_Plog					+= GetAt(i)->GetLocalMI_Plog();
		m_DistantMI_TotalBoltzmannScore	+= GetAt(i)->GetDistantMI_TotalBoltzmannScore();
		m_DistantMI_Plog				+= GetAt(i)->GetDistantMI_Plog();

	}

}

double CWordCollection::ComputeZ()
{
	CLexicon& lex = *m_pLexicon;
	linguistica::ui::status_user_agent& status = lex.status_display();

	int NumberOfIterations = m_pLexicon->GetIntParameter(
		"Boltzmann\\NumberOfSamples", 100);
	int NumberOfPhones(m_Phones_Tier1->GetSize());
	int MinimumWordLength = 2;
	double TotalWeightedExponentiatedScore = 0;
	double TotalWeightOfSpaceSampled = 0;
	CWordCollection TestWords;

	MonteCarlo Dealer(NumberOfPhones);
	CStem Word;
	CParse prsWord;
	double prob, score, expoScore;

	status.major_operation = "Computing Z:";
	status.progress.clear();

	Dealer.m_ModelType = BIGRAM;
	m_Phones_Tier1->PopulateMonteCarlo(&Dealer);
	QFile outfile("c:\\deleteMeLxa.txt");
	outfile.open(QIODevice::WriteOnly);
	Q3TextStream outf(&outfile);

	outf.setEncoding(Q3TextStream::Unicode);
	outf << "Word \t GibbsScore \t ExpoScore \t BigramProb \t UnigramLogProb \t T1 MI \t T2 MI";

	status.progress.set_denominator(NumberOfIterations);
	for (int i = 1; i < NumberOfIterations; i++) {
		Dealer.ReturnString(prsWord);

		status.progress = i;
		if (prsWord.Size() <= MinimumWordLength)
			continue;
		QString temp = prsWord.Display();
		if (TestWords ^= temp) {
			// XXX. huh?  infinite loop, no?
			i--;
			continue;
		}
		TestWords << temp;
		Word = prsWord;
		Word.CreatePhonologyFromOrthography(CStem::NO_BOUNDARIES);
		Word.SplitPhonologyToTiers(CStem::Split_LeaveCopy, *m_Vowels);
		Word.ComputeProbabilities(this);
		score = Word.GetBigramLogProb() - Word.GetTier2_LocalMI_Score();
		prob = exp2(-1.0 * Word.GetBigramLogProb());
		//score2 = score * score/prob;
		expoScore = exp2(-1.0 * score);
		QString temp3 = Word.Display();
		double Tier1MI = Word.GetUnigramLogProb() - Word.GetBigramLogProb();
		outf << endl << temp << "\t" <<
				score << "\t" <<
				expoScore << "\t" <<
				prob  << "\t" <<
				Word.GetUnigramLogProb() << "\t" <<
				Tier1MI << "\t" <<
				Word.GetTier2_LocalMI_Score();
		TotalWeightedExponentiatedScore += expoScore;
		TotalWeightOfSpaceSampled += prob;
	}
	status.progress.clear();
	m_MyZ_Local = TotalWeightedExponentiatedScore/TotalWeightOfSpaceSampled;
	outfile.close();
	status.major_operation.clear();
	return m_MyZ_Local;
}

/// For "field" method on Tier 2
double CWordCollection::ComputeZStar()
{
	CLexicon& lex = *m_pLexicon;
	linguistica::ui::status_user_agent& status = lex.status_display();

	int NumberOfIterations = m_pLexicon->GetIntParameter(
		"Boltzmann\\NumberOfIterations", 10000);
	int NumberOfPhones(m_Phones_Tier1->GetSize());
	int MinimumWordLength = 2;
	double TotalWeightedExponentiatedScore = 0;
	double TotalWeightOfSpaceSampled = 0;
	MonteCarlo Dealer(NumberOfPhones);
	CStem Word;
	CParse prsWord;
	double prob, expoScore;

	status.major_operation = "Computing Z*:";
	status.progress.clear();

	Dealer.m_ModelType = BIGRAM;
	m_Phones_Tier1->PopulateMonteCarlo(&Dealer);

	status.progress.set_denominator(NumberOfIterations);
	for (int i = 1; i < NumberOfIterations; i++) {
		Dealer.ReturnString(prsWord);

		status.progress = i;
		if (prsWord.Size() <= MinimumWordLength)
			continue;
		Word = prsWord;
		Word.CreatePhonologyFromOrthography(CStem::NO_BOUNDARIES);
		Word.SplitPhonologyToTiers(CStem::Split_LeaveCopy, *m_Vowels);
		Word.ComputeProbabilities(this);
		prob = exp2(-Word.GetBigramLogProb());
		expoScore = exp2(-(Word.GetBigramLogProb() - Word.GetTier2_DistantMI_Score()));
		TotalWeightedExponentiatedScore += expoScore;
		TotalWeightOfSpaceSampled += prob;
	}
	status.progress.clear();
	m_MyZ_Distant = TotalWeightedExponentiatedScore / TotalWeightOfSpaceSampled;
	status.major_operation.clear();
	return m_MyZ_Distant;
}

void CWordCollection::GetPhonologyTierInfoForGraphOfWords()
{
	CLexicon& lex = *m_pLexicon;
	linguistica::ui::status_user_agent& status = lex.status_display();

	status.major_operation = "Get Tier1 Info For Graphic Display...:";
	status.progress.clear();
	status.progress.set_denominator(GetSize());
	for (int i = 0; i < GetSize(); i++) {
		status.progress = i;
		GetAt(i)->GetPhonogyTier1InfoForGraph(this);
	}
	status.progress.clear();
	status.major_operation.clear();
}

void CWordCollection::FindAllWordNeighbors(CLexicon* MyLexicon)
{
	CLexicon& lex = *m_pLexicon;
	linguistica::ui::status_user_agent& status = lex.status_display();

	int MinimumSize = 5;
	double ScoreThreshold = MyLexicon->GetIntParameter(
		"Neighbors\\DifferenceThreshold", 2.5);
	int MaximumNumberOfLetterDifferences = 9;
	int MinimumNumberOfCommonLetters = 5;
	CParse Substitution;
	CStem* pWord;
	CStem* qWord;
	CAlignment* pAlignment;
	double Score;
	QMap<QString, CTemplate*> Templates;
	int Counter = 1;
	int TotalNumber;
	int NumberOfWords = GetCount();
	TotalNumber = NumberOfWords - 1;
	bool MatchAlreadyFound;

	status.major_operation = "StringEdit:FindAllEditDistances";
	status.progress.clear();

	if (MyLexicon->LogFileOn() && MyLexicon->GetLogFileStream())
		*MyLexicon->GetLogFileStream() << endl <<
			"<h3 class=blue>" <<
			"Word neighbors" <<
			"</h3>" <<
			endl <<
			StartTable <<
			StartTableRow <<
			MakeTableHeader("String 1") <<
			MakeTableHeader("String 2") <<
			MakeTableHeader("Distance") <<
			EndTableRow;

	status.progress.set_denominator(TotalNumber);
	for (int i = 0; i < NumberOfWords - 1; i++) {
		pWord = GetAtSort(i);

		if (pWord->GetKeyLength() < MinimumSize)
			continue;
		MatchAlreadyFound = false;

		for (int j = i + 1; j < NumberOfWords; j++) {
			qWord = GetAtSort(j);

			if (qWord->GetKeyLength() < MinimumSize)
				continue;

			// Our tests to see if these two words,
			// pWord and qWord, are similar enough
			// to be worth testing with string edit distance.
			int Overlap = OverlapOfTwoAlphabetizedLists(
				pWord->GetAlphabetizedForm(), qWord->GetAlphabetizedForm());
			if (Overlap < MinimumNumberOfCommonLetters)
				continue;

			int Diff = DifferencesOfTwoAlphabetizedLists(
				pWord->GetAlphabetizedForm(), qWord->GetAlphabetizedForm());
			if (Diff > MaximumNumberOfLetterDifferences)
				continue;
			// end of tests.

			pAlignment = new CAlignment(pWord, qWord);
			Score = pAlignment->FindStringEditDistance();
			if (Score < ScoreThreshold) {
				if (MyLexicon->LogFileOn()) {
					if (MatchAlreadyFound == false) {
						*MyLexicon->GetLogFileStream() << endl <<
							StartTableRow <<
							TableData(pWord) <<
							TableData(qWord) <<
							TableData(Score);
						if (pAlignment->m_Slips == 1) {
							Substitution = pAlignment->FindSubstitution();
							*MyLexicon->GetLogFileStream() <<
								TableData(Substitution.GetPiece(1)) <<
								TableData(Substitution.GetPiece(2));
						}
						MatchAlreadyFound = true;
						*MyLexicon->GetLogFileStream() << EndTableRow;
					} else {
						*MyLexicon->GetLogFileStream() <<
							endl <<
							StartTableRow <<
							TableData("") <<
							TableData(qWord);
						*MyLexicon->GetLogFileStream() <<
							TableData(Score);
						if (pAlignment->m_Slips == 1) {
							Substitution = pAlignment->FindSubstitution();
							*MyLexicon->GetLogFileStream() <<
								TableData(Substitution.GetPiece(1)) <<
								TableData(Substitution.GetPiece(2));
						}
						*MyLexicon->GetLogFileStream() << EndTableRow;
					}
				}
			}
			delete pAlignment;
		}
		status.progress = Counter++;
	}
	status.progress.clear();
	status.major_operation.clear();
}
