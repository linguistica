// Small wrapper method
#include "MiniLexicon.h"
#include "SignatureCollection.h"

// XXX. move to Lexicon_Allomorphy.cpp

void CMiniLexicon::FindAllomorphy()
{
    m_pSignatures->CompareSignaturePairsForTotalOverlap(); //added July 16, 2010
    // -------------------------------------------------------------------------------- //
   // if (analyzingSuffixes) {
            QString Remark("When one signature is a complete suffix of the other");
            CStringSurrogate ssRemark(Remark);

            RebuildAffixesStemsAndSignaturesFromWordSplits(ssRemark);

          //  status.major_operation = "Mini-Lexicon " + QString("%1").arg( m_Index+1 ) + ": End -- from stems, find suffixes";
   // } else {	// Prefixes!

   //         QString Remark("When one signature is a complete suffix of the other");
   //         CStringSurrogate ssRemark(Remark);
   //         RebuildAffixesStemsAndSignaturesFromWordSplits(ssRemark);
          //  status.major_operation = "Mini-Lexicon " + QString("%1").arg( m_Index+1 ) + ": End -- from stems, find prefixes";
   // }
     // -------------------------------------------------------------------------------- //




    m_pSignatures->FindAllomorphy();

    //    RelateStems();


}
