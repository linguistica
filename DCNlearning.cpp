// Implementation of Dynamic Computational Network learning class
// Copyright © 2009 The University of Chicago
#include "DCNlearning.h"

#include <cstdlib>
#include <ctime>
#include <QString>
#include <QFile>
#include <Q3TextStream>

#include "DCNgrammar.h"
#include "DCNnetwork.h"
#include "DCNsnetwork.h"
#include "ui/Status.h"
#include "random.h"

namespace {
	using linguistica::ui::status_user_agent;
}

learning::learning()
{

}

learning::~learning()
{

}

void learning::runHelper(Q3TextStream& logstream, status_user_agent& status)
{
	using std::srand;
	using std::time;

	srand(static_cast<unsigned>(time(0)));

	successful = false;

	//careful, i'm specifying values
	float				T = TfromUser;
	int					cutoff = cutoffFromUser;
	int					corpusIndex = 0;

	int					maxCorpusIndex = numberOfWords;

	int					syl = 10;

	float				alpha;
	float				beta;
	float				I;		
	float				F;
	F = 0;
	I = 0;
	beta = 0;
	alpha = 0; 
	

	snetwork			snet(2, 15);

	theGrammar.setValues(startingAlpha,startingBeta, startingI, startingF, 0, 0);
	
	status.progress.clear();
	status.progress.set_denominator(cutoffFromUser);
	int updateNumber = 0;
	while (T > 0.01 && cutoff >= 0) {
		status.progress = updateNumber;
		logstream
			<< "T:  "		<< T						<< "\t" 
			<< "it:  "		<< cutoffFromUser - cutoff	<< "\t"
			<< "alpha:  "	<< theGrammar.getAlpha()	<< "\t"
			<< "beta:  "	<< theGrammar.getBeta()		<< "\t"
			<< "initial:  "	<< theGrammar.getI()		<< "\t"
			<< "final:  "	<< theGrammar.getF()		<< "\n";
		QString string = corpus[corpusIndex];

		syl = string.length();
		network theNetwork(syl);
		theNetwork.setGrammar(&theGrammar);
		theNetwork.equilibrium();

		QString word = theNetwork.getStress();

		if (word != corpus[corpusIndex]) {
			using linguistica::random_small_float;

			// random float between -1 and 1
			float deltaAlpha = T * random_small_float();
			alpha = theGrammar.getAlpha() + deltaAlpha;

			float deltaBeta = T * random_small_float();
			beta = theGrammar.getBeta() + deltaBeta;

			float deltaI = T * random_small_float();
			I = theGrammar.getI() + deltaI;

			float deltaF = T * random_small_float();
			F = theGrammar.getF() + deltaF;

			//theGrammar.setValues(alpha, beta, I, F, 0, 0);
			
			possibleGrammar.setValues(alpha, beta, I, F, 0, 0);
			snet.setGrammar(&possibleGrammar);
			snet.equilibrium();
			
			if (snet.isTotallyConverged())
			{
				// alpha * beta > .3 may be another thing to put in here
				if (alpha > 0)
					alpha *= -1;
				if (beta > 0)
					beta  *= -1;
				theGrammar.setValues(alpha, beta, I, F, 0, 0);
			}
			T = T + increaseWhenWrong;
			//T = T + sqrt( pow(deltaAlpha, 2.0) + pow(deltaBeta, 2.0)
			//	+ pow(deltaI, 2.0) + pow(deltaF, 2.0) );
			
		}
		else if (word == corpus[corpusIndex])
		{
			T = T * decreaseWhenRight;
		}
		//T = T - .01;
		cutoff--; updateNumber++;
		corpusIndex = (corpusIndex + 1)%maxCorpusIndex;
	}
	status.progress.clear();

	possibleGrammar.setValues(alpha, beta, I, F, 0, 0);
	snet.setGrammar(&possibleGrammar);
	snet.equilibrium();
	if (cutoff > 2 && snet.isTotallyConverged())
		successful = true;
}

void learning::run(status_user_agent& status_display)
{
	using std::time_t;
	using std::time;
	using std::ctime;

	QFile logfile( "DCNlog.txt" ); // added : -cs-
	if( logfile.open( QIODevice::WriteOnly | QIODevice::Append ) ) // added : -cs-
	{ // added : -cs-
		Q3TextStream logstream( &logfile ); // added : -cs-
		time_t rawtime;
	
		time ( &rawtime );
		logstream << "Learning Algorithm run at " << ctime(&rawtime);
		
		for (int i = 0; i < numberOfTries; i++)
		{
			logstream << "\n\tTRIAL NUMBER " << i+1 << "\n";
			runHelper(logstream, status_display);
			if (this->isSuccessful()) break;
		}
		logfile.close();
	} // added : -cs-
}

void learning::setCorpus(corpusType corpus, int numberOfWords)
{
	this->corpus = corpus;
	this->numberOfWords = numberOfWords;
}

grammar* learning::returnGrammar()
{
	grammar* returnGrammar = new grammar(theGrammar);
	return returnGrammar;
}

