// Grammar type for stress discovery with Dynamic Computational Networks
// Copyright © 2009 The University of Chicago
#ifndef GRAMMAR_H
#define GRAMMAR_H

// See the learning documentation in DCNlearning.h for an overview.

#include <QLabel>

class QLabel;

/**
 *	A grammar holds the DCN's parameters. It's like an array of six
 *  floats, but much smarter, because you can make deep copies and
 *  you can print it really easily. It's an easy way to think of
 *  what the internal grammar of a language-user might be in the 
 *  DCN model.
 */
class grammar
{

private:
	float alpha;
	float beta;
	float I;
	float F;
	float P;
	float bias;

public:
	grammar();
	grammar(grammar &theGrammar);
	virtual ~grammar();
	void	setValues(float alpha, float beta, float I, float F, float P, float bias);

	float	getAlpha() { return alpha; }
	float	getBeta() { return beta; }
	float	getI() { return I; }
	float	getF() { return F; }
	float	getP() { return P; }
	float	getBias() { return bias; }

	void	print(QLabel *label);

};

#endif // GRAMMAR_H
