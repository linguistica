// Machine Translation driver
// Copyright © 2009 The University of Chicago
#ifndef CMT_H
#define CMT_H

#include <QString>

class LinguisticaMainWindow; 
class mTVolca; 
class cMTModel1; 
class cMTModel2Norm; 
class mTForSortingItem;

template<class V> class Q3SortedList;
template<class K, class V> class QMap;

typedef Q3SortedList<mTForSortingItem> mTSortedList;
typedef QMap<int, double> IntToDouble;
typedef QMap<int, IntToDouble*> IntToIntToDouble;

class cMT {
public:
	LinguisticaMainWindow* m_parent;
	QString m_projectDirectory;

	mTVolca* m_Volca;

	/// Model 1
	cMTModel1* m_Model1;
	/// Model 2
	cMTModel2Norm* m_Model2Norm;

	/// Log parameter
	QString m_MTLog;
public:
	// construction/destruction.

	cMT(LinguisticaMainWindow*, QString);
	virtual ~cMT();

	// disable default-construction, copy
private:
	cMT();
	cMT(const cMT& x);
	cMT& operator=(const cMT& x);
public:
	// training.

	void readTrainingCorpus();
	/// Model 1
	void trainModel1(int);
	/// Model 2
	void trainModel2Norm(int, bool);

	// output to logs.

	/// Log Model1
	void logTAfterModel1();
	/// Log Model2
	void logTandAAfterModel2Norm();
};

class mTForSortingItem {
public:
	QString m_name;
	int m_type;
	double m_doubleValue;
public:
	// construction/destruction.

	mTForSortingItem() : m_name(), m_type(0), m_doubleValue(0.0) { }
	mTForSortingItem(QString name, double value, int type)
		: m_name(name), m_type(type),
		m_doubleValue(m_type == 1 ? value : 0.0) { }
	// destructor implicitly defined

	// copy construction/assignment implicitly defined

	// compare.

	bool operator<(const mTForSortingItem& x)
	{
		return m_type == 1 ?
			m_doubleValue > x.m_doubleValue :
			true;
	}
	bool operator==(const mTForSortingItem& x)
	{
		return m_type == 1 ?
			m_doubleValue == x.m_doubleValue :
			true;
	}
};

#endif // CMT_H
