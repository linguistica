// fast “slice-and-count” calculation of string similarity
// Copyright © 2009 The University of Chicago
#ifndef SLICE_H
#define SLICE_H

class snippet;
class StringInventory;
class StringPtrList;
class SliceCollection;

// for snippet:
#include <QString>
// for StringInventory:
#include <Q3Dict>
#include <QString>
namespace linguistica { namespace ui { class status_user_agent; } }
// for StringPtrList:
#include <QList>
#include <QString>
// for SliceCollection:
#include <QList>
namespace linguistica { namespace ui { class status_user_agent; } }

/**
 * A snippet is a representation of a string which also remembers the
 * number of times it is used.  The primary purpose of this object is
 * so that we can access a pointer to each one.
 */
class snippet {
	QString m_string;
	int m_count;
public:
	explicit snippet(QString);
	unsigned int getlength() const {return  m_string.length();}
	QString getkey() const {return m_string;}
};

extern bool lessThan(const snippet* s1, const snippet* s2);

/**
 * This is simply a dictionary that allows us to go from a QString to a pointer
 * to a snippet with the same key that is in the inventory.
 */
class StringInventory : public Q3Dict<snippet> {
public:
	StringInventory();

	void addwordcollection(class CWordCollection* words,
		SliceCollection& slices,
		linguistica::ui::status_user_agent& status_display);
	snippet* getsnippetpointer(QString key);
	snippet* operator<<(QString key);
	StringPtrList* addword(QString word);
};

/**
 * A slice is a list of pointers to a snippet for every substring in the key of
 * the slice.  It is the principal class in this file.
 */
class StringPtrList : public QList<snippet*> {	// a.k.a. Slice
public:
	QString m_key;
	StringInventory* m_MyStringInventory;
public:
	StringPtrList(QString key, StringInventory*);

	void sort();
	int getlength() {return m_key.length();}
	QString getkey() {return m_key;}
};
typedef StringPtrList Slice;

class SliceCollection : public QList<Slice*> {
public:
	void writetolog(class QTextStream& log);
	void FindWordPairs(linguistica::ui::status_user_agent& status_display,
		class CLexicon* lex, unsigned int minimumlength = 0);
};

extern int largestcommonsubstringlength(Slice* a, Slice* b,
	unsigned int minimum = 0);
extern snippet* largestcommonsubstring(Slice* a, Slice* b,
	unsigned int minimum = 0);

#endif // SLICE_H
