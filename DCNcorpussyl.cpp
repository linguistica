// Syllabification using Dynamic Computational Networks
// Copyright © 2009 The University of Chicago
#include "DCNcorpussyl.h"
#include <Q3TextStream>
#include <QLabel>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

corpussyl::corpussyl()
{

}

corpussyl::~corpussyl()
{

}

void corpussyl::readInput(QFile &file)
{
	if ( file.open( QIODevice::ReadOnly ) )
	{
        Q3TextStream stream( &file );
        QString line;
        m_numOfWords = 0;
        while ( !stream.atEnd() ) {
            line = stream.readLine(); // line of text excluding '\n'
			dcnword word;
			word.setText(line.section('\t', 0, 0));
			word.setMaxima(line.section('\t', 1, 1));
			
			m_wordVector.push_back(word);
			m_numOfWords++;
		}
    file.close();
	}
}

void corpussyl::printCorpus(QLabel *label, grammarsyl *gram)
{
	networksyl net;
	net.setGrammar(gram);

	QString corpus = gram->print() + "\n\n";

	Q3ValueVector<dcnword>::const_iterator it  = m_wordVector.begin();
	Q3ValueVector<dcnword>::const_iterator end = m_wordVector.end();

	while (it != end)
	{
		QString targText = it->getText();
		QString targMaxima = it->getMaxima();

		net.setWord(targText);
		net.equilibrium();
		QString expText = net.getSyllabifiedWord();
		QString expMaxima = net.getMaxima();
		corpus += targText + "\t" + targMaxima + "\t\t" + expText + "\t\t"
			+ expMaxima + "\n";
		
		it++;
	}
	label->setText(corpus);

}

QString corpussyl::printCorpus(grammarsyl *gram)
{
	networksyl net;
	net.setGrammar(gram);

	QString corpus;

	Q3ValueVector<dcnword>::const_iterator it  = m_wordVector.begin();
	Q3ValueVector<dcnword>::const_iterator end = m_wordVector.end();

	while (it != end)
	{
		QString targText = it->getText();
		QString targMaxima = it->getMaxima();

		net.setWord(targText);
		net.equilibrium();
		QString expText = net.getSyllabifiedWord();
		QString expMaxima = net.getMaxima();
		corpus += targText + "\t\t" + targMaxima + "\n" + expText + "\t\t"
			+ expMaxima + "\n\n";
		
		it++;
	}
	return corpus;
}

dcnword corpussyl::wordAt(uint index) const
{
		return m_wordVector.at(index);
}
