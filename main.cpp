// main() function
// Copyright © 2010 The University of Chicago

// Entry point (it all starts here)
int main(int argc, char** argv);

#include <memory>
#include <QApplication>
#include <QSplashScreen>
#include <QPixmap>
#include "linguisticamainwindow.h"

namespace {
typedef std::auto_ptr<LinguisticaMainWindow> main_window_ptr;

main_window_ptr init(int argc, char** argv, QApplication& app)
{
	return main_window_ptr(new LinguisticaMainWindow(argc, argv,
							&app, NULL, 0));
}

void init_gui(int argc, char** argv, QApplication& app,
				main_window_ptr& main_window)
{
	// The splash screen image is linked into the Linguistica
	// executable (see linguistica.qrc).
	QSplashScreen splash(QPixmap(":/splash.png"));
	splash.show();

	if (!main_window.get())
		main_window = init(argc, argv, app);
	app.setMainWidget(main_window.get());

	splash.finish(main_window.get());
}
}

int main( int argc, char** argv )
{
	QApplication app(argc, argv);
	main_window_ptr main_window;

	if (argc >= 2) {	// could be command-line mode.
		main_window = init(argc, argv, app);
		if (main_window->commandLineMode())
			// done!
			return 0;
	}
	init_gui(argc, argv, app, main_window);
	main_window->show();
	return app.exec();
}
