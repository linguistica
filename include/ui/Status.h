// Generalized status bar: interface for reporting status to the user.
// Copyright © 2010 The University of Chicago
#ifndef LXA_UI_STATUS_H
#define LXA_UI_STATUS_H

namespace linguistica {
namespace ui {
	struct status_user_agent;
	/// fake status bar widget that ignores all status updates.
	extern status_user_agent& ignore_status_updates();
}
}

class QString;

/// Traditionally, Linguistica included a “status bar” widget at the
/// bottom of the screen, divided into three segments:
///
/// From signatures find stems | ### 30%   | Rebuilding all signatures: stems
///
/// The string on the left represents the high-level operation that is
/// taking place.  It is not expected to change much.  This string is
/// represented by the major_operation member below.
///
/// The string on the right represents what task in particular the computer
/// is busy with.  The progress bar in the middle represents progress
/// toward completing that task.  The string is represented by the details
/// member below, progress bar by the progress member.
///
/// Scripted users and alternative user interfaces are expected to be able to
/// implement these methods using something other than a status bar, of
/// course.
struct linguistica::ui::status_user_agent {
	class string_type {
	public:
		/// reset status (i.e., indicate idle state)
		virtual void clear() = 0;
		/// report new status
		virtual string_type& operator=(const QString& str) = 0;
		virtual ~string_type() { }
	};
	class progress_type {
	public:
		/// disable progress bar
		virtual void clear() = 0;
		/// progress can only be set to a number from 0 to denom.
		/// implicitly sets progress to 0.
		virtual void set_denominator(int denom) = 0;
		/// report progress, return *this
		/// cannot be called before set_denominator is called for
		/// the first time
		inline progress_type& operator=(int progress)
		{
			return (progress % incr == 0) ?
				assign(progress) : *this;
		}

		virtual ~progress_type() { }
	protected:
		int incr;
	private:
		virtual progress_type& assign(int progress) = 0;
	};

	string_type& major_operation;
	string_type& details;
	progress_type& progress;
	status_user_agent(string_type& m, string_type& d, progress_type& p)
		: major_operation(m), details(d), progress(p) { }
};

#endif // LXA_UI_STATUS_H
