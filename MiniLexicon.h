// Sub-stem morphology model and driver
// Copyright © 2009 The University of Chicago
#ifndef MINILEXICON_H
#define MINILEXICON_H

class CMiniLexicon;

#include <QMap>
#include "generaldefinitions.h"
#include "AffixLocation.h"
class QString;
class CStringSurrogate;

/// All analyses at the sub-stem level (non-compound) are done in the
/// lexicon’s mini-lexica.
class CMiniLexicon {
public:
	enum which_words {
		WW_All,
		WW_AnalyzedOnly,
		WW_UnanalyzedOnly,
	};
private:
	/// The parent of this mini-lexicon
	class CLexicon* m_pLexicon;

	/// The index of this mini in its parent's mini-lexicon list.
	int m_Index;

	/// A flag designating the affix type (prefix|suffix). This is also
	/// noted by the signatures and their collection.
	enum eAffixLocation m_AffixLocation;

	/// This mini-lexicon's local copy of words will be the basis for all
	/// analyses within the mini-lexicon. The suffixes, prefixes, stems and
	/// signatures are all discovered locally in this mini-lexicon. The
	/// mini-lexicon has the responsibility to update the global collection.
        class CWordCollection*                  m_pWords;
        class CSuffixCollection*                m_pSuffixes;
        class CPrefixCollection*                m_pPrefixes;
        class CStemCollection*                  m_pStems;
        class CSignatureCollection*             m_pSignatures;
        class LxPoSCollection*                  m_pPOS;
        class CDescriptionLength*               m_DescriptionLength;
        double                                  m_CorpusCountOfUnanalyzedWords;
        double                                  m_PhonologicalInformationOfUnanalyzedWords;

	class GUIWordCollection* m_GUIWords;

	QMap<QString, class CDatum> m_DataMap; // for deMarcken JG

//	class FSA* m_pFSA;
public:
	// construction/destruction.

	CMiniLexicon(CLexicon* parent = 0, int index = -1,
		enum eAffixLocation affixLocation = STEM_FINAL);
	~CMiniLexicon();

	// disable copy.
private:
	CMiniLexicon(const CMiniLexicon& x);
	CMiniLexicon& operator=(const CMiniLexicon& x);
public:

	// assign.

	void ClearAll();

	// location in parent lexicon.

        int                 GetIndex() { return m_Index; }
        CLexicon*           GetLexicon() { return m_pLexicon; }
        class CCorpusWord*  FindAWord(class CStem* stem, class CSuffix* suffix);
	/// total number of (not necessarily unique) words in corpus
        int                 GetCorpusCount();
        int                 GetNumberOfCharacterTypes();
        int                 GetMiniCount();
        int                 GetMiniSize();
        CMiniLexicon*       GetMiniLexicon(int index);

	// parameters.

	enum eAffixLocation GetAffixLocation() { return m_AffixLocation; }
	/// Set affix type.
	/// If affix type has changed from initial to final or vice-versa,
	/// this entails forgetting any previously discovered affixes.
	/// If this further entails forgetting some discovered stems,
	/// do nothing; result is false.
	/// Result is true on success.
        bool                SetAffixLocation(enum eAffixLocation prefix_or_suffix);
        int                 GetIntParameter(QString key, int default_value);

	// analyzed and unanalyzed words.

        CWordCollection*    GetWords() { return m_pWords; }
        void                AddToWordCollection(CStemCollection* words);
        void                AddToWordCollection(CWordCollection* words,
                            enum which_words which = WW_UnanalyzedOnly);
        int                 GetNumberOfUnanalyzedWords();
        int                 GetCorpusCountOfUnanalyzedWords();
        int                 GetNumberOfAnalyzedWords(int& unanalyzed_count);
        CStem*              GetWordFromStemSuffix(CStem*, CSuffix*);
        CStem*              GetWordFromStemPrefix(CStem*, class CPrefix*);

	// stem/signature/suffix model of morphology.

        CPrefixCollection*      GetPrefixes() { return m_pPrefixes; }
        CSignatureCollection*   GetSignatures() { return m_pSignatures; }
        CStemCollection*        GetStems() { return m_pStems; }
        CSuffixCollection*      GetSuffixes() { return m_pSuffixes; }
        int                     GetNumberOfStems();
        int                     GetNumberOfSuffixes();

	// linguistic analysis (stem/signature/suffix discovery).

        CPrefixCollection*      FindPrefixes();
	/// If affix type is not suffix, do nothing.
	/// Otherwise, apply various heuristics to fill this mini-lexicon’s
	/// suffix collection.  Result points to the suffix collection.
        CSuffixCollection*      FindSuffixes();
	void FindSingletonSignatures();
	/// Analyze unanalyzed words using known stems and known affixes.
	/// XXX. If a word can be analyzed in two ways as stem + suffix,
	/// this function does not assess their relative merits.
	/// Instead, it just picks the version with the longer suffix.
	/// The situation is even worse for prefixes: the version with
	/// the stem earliest in alphabetical order is used.
        void                    ExtendKnownStemsToKnownAffixes();
	/// Rebuild signature list through knowledge of stems and their
	/// associated affixes.
	void FindAllSignatures();
	void FromStemsFindAffixes();
	/// Find new signatures using unanalyzed words and known affixes.
	/// More precisely:
	///
	/// For each word, if it ends with a known suffix (resp starts with
	/// a known prefix), consider the corresponding stem text.
	///
	/// If adding that new stem (and the corresponding signature from
	/// analyzing other words with it) decreases description length,
	/// record the new stem, signature, and analyzed words.
	void LooseFit();
	void RebuildAffixesStemsAndSignaturesFromWordSplits(
		CStringSurrogate& remark);
	void TakeSplitWords_ProduceStemsAndSigs(CStringSurrogate& Remark,
		CWordCollection* words = 0, CStemCollection* stems = 0,
		CPrefixCollection* prefixes = 0,
		CSuffixCollection* suffixes = 0);
	/// successor-frequency algorithm (bootstrapping step)
	void TakeSignaturesFindStems(CSignatureCollection* Sigs = 0);
	void CheckSignatures();

	// description length.

        CDescriptionLength*     GetDescriptionLength() 		{ return m_DescriptionLength; }
        double                  CalculateDescriptionLength();
        double                  CalculateSumOfPointersToMyUnanalyzedWords(enum eMDL_STYLE style);
        double                  CalculateUnanalyzedWordsTotalPhonologicalInformationContent();
        class                   CDLHistory* GetDLHistory();
        double                  CalculateCompressedLengthOfUnanalyzedWords();

	// part-of-speech discovery (does not affect description length).

        LxPoSCollection*        GetPOS() { return m_pPOS; }
	/// Build or update m_pPOS to reflect top discovered signatures
	void FindMajorSignatures();

	// output.

        //FSA*                    GetFSA() { return m_pLexicon->m_pFSA; }
        bool                    LogFileOn();
        class QTextStream*      GetLogFile();
	QMap<QString, QString>* GetOutFilter();

	// output to GUI.

	/// set text for “command line” pane
	void AddToScreen(QString info);
	/// handle for UI operations
	class LinguisticaMainWindow* GetDocument();
	GUIWordCollection* GetGUIWords();

	// CDatum* LookUp(CStringSurrogate&);

	// allomorphy.

	void FindAllomorphy();
	void RelateStems();
	void ShiftFinalLetterToStem(class CStem* pStem, QString& FinalLetter);
	void HowManyStemsWithThisSuffixEndInThisLetter(CStringSurrogate& Suffix,
		CStringSurrogate& Letter, int& TotalStemsWithSuffix,
		int& HowManyEndWithThisLetter);
	int StemsWithBothSuffixes(
		CStringSurrogate& suf1, CStringSurrogate& suf2);
	int StemsWithBothSuffixes(QString suf1, CStringSurrogate& suf2);
	void MoveWordsStemSuffixBoundaryToRight(class CSignature* pSigToChange,
		CStringSurrogate& Letters, class CParse* pSuffixCandidates);
	void MoveWordsStemSuffixBoundaryToRight(class CSignature* pSigToChange,
		QString	Letters, class CParse* pSuffixCandidates);
	class CParse CreateADeletingSignature(class CSignature* pSig,
		CStringSurrogate Deletee, class CParse& ReplacingSuffixes,
		int* bStatus, class CParse& WhatSigWillBecome,
		QMap<QString, QString>& Remapper);
	class CParse CreateADeletingSignature(class CSignature* pSig,
		CStringSurrogate Deletee, class CParse& ReplacingSuffixes,
		int* bStatus, class CParse& WhatSigWillBecome,
		class CParse* pSuffixCandidates);

        // LogFile functions
        void        LogFileSmallTitle(QString);       
        void        LogFileSmallTitle(QString, QString);
        void        LogFileSmallTitle(QString, QString, QString);
        void        LogFileLargeTitle(QString);
        void        LogFileStartTable();
        void        LogFileEndTable();      
        void        LogFileStartRow();
        void        LogFileStartRow(QString);
        void        LogFileEndRow();
        void        LogFile (QString);
        void        LogFile (double);
        void        LogFile1SimpleString(QString); // no row start or end
        void        LogFileSimpleString(QString); // no row start or end
        void        LogFileSimpleInteger(int); // no row start or end
        void        LogFileSimpleDouble(double); // no row start or end
        void        LogFile (QString, QString);
        void        LogFile (QString, QString, QString);
        void        LogFile (QString, QString, QString, QString);
        void        LogFile( QString, QString, QString, QString, QString);
        void        LogFile( QString, QString, QString, QString, QString, QString);
        void        LogFile (QString, int);
        void        LogFile(int, QString);
        void        LogFile(int, double, QString);
        void        LogFile (QString, double);
        void        LogFile(QString, int, double);
        void        LogFile(QString, int, int, double, double, double);
        void        LogFileHeader(QString);
        void        LogFileHeader ( QString, QString);
        void        LogFileHeader (QString, QString, QString);
        void        LogFileHeader( QString, QString, QString, QString, QString, QString);

};

#endif // MINILEXICON_H
