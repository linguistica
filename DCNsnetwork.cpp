// Implementation of snetwork methods
// Copyright © 2009 The University of Chicago
#include <qstring.h>
#include <q3textedit.h>
#include "DCNgrammar.h"
#include "DCNnetwork.h"

#include "DCNsnetwork.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

snetwork::snetwork(int leastNumberOfSyl, int greatestNumberOfSyl)
{
	this->leastNumberOfSyl		= leastNumberOfSyl;
	this->greatestNumberOfSyl	= greatestNumberOfSyl;
	int total = greatestNumberOfSyl - leastNumberOfSyl + 1;
	theNetworks.resize(total);

	for (int i = 0; i < total; i++)
	{
		network* theNetwork = new network(i + leastNumberOfSyl);
		theNetworks.insert(i, theNetwork);
	}

}

snetwork::~snetwork() {}

void snetwork::setGrammar(grammar* theGrammar)
{
	this->theGrammar = theGrammar;
}

void snetwork::equilibrium()
{
	// are all of the networks in the snetwork converged?
	// boolean logic will tell us so
	converged = true;
	for (uint i = 0; i < theNetworks.count(); i++)
	{
		theNetworks[i]->setGrammar(theGrammar);
		theNetworks[i]->equilibrium();
		converged = converged && theNetworks[i]->isConverged();
	}
}

void snetwork::print(Q3TextEdit *label)
{
	//label->setReadOnly(true);
	
	//definitions of network values
//	float alpha = theGrammar->getAlpha();	unused variables: alpha, beta, I, F
//	float beta  = theGrammar->getBeta();
//	float I     = theGrammar->getI();
//	float F     = theGrammar->getF();
	//float P     = theGrammar->getP();
	//float bias  = theGrammar->getBias();

	// partially ganked from grammar::print(label)
	QString totalString;
	QString partialString;
	/*
	totalString += "The values of the grammar:\n";
	totalString += "alpha\t\tbeta\t\tinitial\t\tfinal\n"; // + "\tpenult\tbias\n\t";
	partialString.setNum(alpha);	totalString += partialString + "\t";
	partialString.setNum(beta);		totalString += partialString + "\t";
	partialString.setNum(I);		totalString += partialString + "\t";	
	partialString.setNum(F);		totalString += partialString; //+ "\t";
	//partialString.setNum(P);		totalString += partialString + "\t";
	//partialString.setNum(bias);		totalString += partialString + "\t";
	totalString += "\n\n";

	totalString += "Stress:\n";
	*/
	for (uint i = 0; i < theNetworks.count(); i++)
	{
		//partialString.setNum(i + leastNumberOfSyl);
		//totalString += "\tWord of length " + partialString + ":\t";
		if (theNetworks[i]->isConverged())
			totalString += theNetworks[i]->getStress() + "\n";
		else
			totalString += "not converged!\n";
	}

	/*
	totalString += "\n\n";
	if (isTotallyConverged())
		totalString += "The grammar is stable for these word-lengths\n";
	else
		totalString += "The grammar is unstable for these word-lengths\n";
	*/

	label->setText(totalString);

}

