// Implementation of network methods
// Copyright © 2009 The University of Chicago
#include "DCNnetwork.h"
#include "DCNgrammar.h"
#include <qstring.h>
#include <qlabel.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

network::network(int syl)
{
	this->syl = syl;
	node = new float[syl];
	for (int i = 0; i < syl; i++)
		node[i] = 0.0;
	converged = false;
}

network::~network()
{
	delete[] node;
}


void		network::setGrammar(grammar* theGrammar)
{
	this->theGrammar = theGrammar;
}

void		network::equilibrium()
{
	//definitions of network values
	float alpha = theGrammar->getAlpha();
	float beta  = theGrammar->getBeta();
	float I     = theGrammar->getI();
	float F     = theGrammar->getF();
	float P     = theGrammar->getP();
	float bias  = theGrammar->getBias();
	converged	= false;


	// the following is the meat of DCNs

	// oldIteration lets us know when we've converged
	float* oldIteration = new float[syl];
	for (int i = 0; i < syl; i++)
		oldIteration[i] = -2.0;
	
	// the DCN goes for 255 cylces before giving up
	for (int cycle = 0; cycle < 256; cycle++)
	{
		//run the network
		for (int s = 0; s < syl; s++)
		{
			if (s == 0)
				node[s] = I + alpha * node[s+1] + bias;
			else if (s == syl-1)
				node[s] = F + beta * node[s-1] + bias;
			else if ((s == syl-2) && (syl != 2))
				node[s] = P + beta * node[s-1] + alpha * node[s+1] + bias;
			else
				node[s] = beta * node[s-1] + alpha * node[s+1] + bias;
		}

		//calculate the distance
		float distance = 0.0;
		for (int i = 0; i < syl; i++)
			distance += ((node[i] - oldIteration[i]) * (node[i] - oldIteration[i]));

		//has the network converged?
		float DELTA = 0.0001f;
		if (distance < DELTA)
		{
			converged = true;
			break;
		}

		//store this iteration as old iteration
		for (int j = 0; j < syl; j++)
			oldIteration[j] = node[j];

		//last cycle, the network has failed to converge
		if (cycle == 255)
			converged = false;
	}

	delete[] oldIteration;
}

void		network::print(QLabel* label)
{
	//definitions of network values
	float alpha = theGrammar->getAlpha();
	float beta  = theGrammar->getBeta();
	float I     = theGrammar->getI();
	float F     = theGrammar->getF();
	float P     = theGrammar->getP();
	float bias  = theGrammar->getBias();

	// partially ganked from grammar::print(label)
	QString totalString;
	QString partialString;

	totalString += "The values of the grammar:\n";
	partialString.setNum(alpha);
	totalString +=  "\talpha:\t\t"	+	partialString + '\n';
	partialString.setNum(beta);
	totalString += "\tbeta:\t\t"	+	partialString + '\n';
	partialString.setNum(I);	
	totalString += "\tinitial:\t\t" +	partialString + '\n';
	partialString.setNum(F);
	totalString += "\tfinal:\t\t"	+	partialString + '\n';
	partialString.setNum(P);
	totalString += "\tpenult:\t\t"  +	partialString + '\n';
	partialString.setNum(bias);
	totalString += "\tbias:\t\t"	+	partialString + '\n';
	totalString += "\n\n";

	totalString += "The value of the nodes:\n";
	for (int i = 0; i < syl; i++)
	{
		partialString.setNum(i);
		totalString += "\tnode # " + partialString + ":\t";
		partialString.setNum(node[i]);
		totalString += partialString + "\n";
	}
	totalString += "\n\n";

	totalString += "The stress of the word is:\n\t";
	if (isConverged())
		totalString += getStress();
	else
		totalString += "not converged!";

	label->setText(totalString);

}

// 1 means a stress, 0 means no stress
QString		network::getStress() {
	QString stress;
	for (int s = 0; s < syl; s++)
	{
		if (s == 0)
		{
			if (node[s] > node[s+1])	stress += "1";
			else						stress += "0";
		}
		else if (s == syl-1)
		{
			if (node[s] > node[s-1])	stress += "1";
			else						stress += "0";
		}
		else
		{
			if ((node[s] > node[s-1]) && (node[s] > node[s+1]))
										stress += "1";
			else						stress += "0";
		}
	}
	return stress;
}

