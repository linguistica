// Machine Translation model 1
// Copyright © 2009 The University of Chicago
#ifndef CMTMODEL1_H
#define CMTMODEL1_H

#include <QMap>

class mTVolca;
typedef QMap<int, double>			IntToDouble;
typedef QMap<int, IntToDouble*>		IntToIntToDouble;

class cMTModel1 {
public:
	class cMT* m_myMT;
	int m_Iterations;

	// alignment parameters.

	QMap<int, QMap<int, double>*> m_T;
	QMap<int, QMap<int, double>*> m_softCountOfT;
	QMap<int, double> m_lamdaT;
public:
	// construction/destruction.

	cMTModel1(cMT* driver, int iterations);
	virtual ~cMTModel1();

	// disable default-construction, copy
private:
	cMTModel1();
	cMTModel1(const cMTModel1& x);
	cMTModel1& operator=(const cMTModel1& x);
public:
	void initT();
	void addSoftCountOfT(int);
	void EMLoops(int);
	void clearSoftCounts();
	void EStep();
	void MStep();
	void releaseSoftCounts();
};

#endif // CMTMODEL1_H
