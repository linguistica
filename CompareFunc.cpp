// Implementation of comparison and sorting functions
// Copyright © 2009 The University of Chicago
#include "CompareFunc.h"

#include <algorithm>
#include <iterator>
#include <utility>
#include <QStringList>
#include <QString>
#include <QChar>
#include "MiniLexicon.h"
#include "Signature.h"
#include "Morpheme.h"
#include "LParse.h"
#include "Affix.h"
#include "Stem.h"
#include "StringFunc.h"

int CompareAlphabetically(const void *pA, const void *pB)
{
	CLParse* pS1 = *static_cast<CLParse* const*>(pA);
	CLParse* pS2 = *static_cast<CLParse* const*>(pB);

	Q_ASSERT(pS1);
	Q_ASSERT(pS2);

	if ( pS1->GetMyMini() && pS2->GetMyMini() )
	{
		QString str1 = pS1->Display( QChar(0),
					pS1->GetMyMini()->GetOutFilter() ),
			str2 = pS2->Display( QChar(0),
					pS2->GetMyMini()->GetOutFilter() );

		return LxStrCmp( str1, str2, str1.length(), str2.length(),
					0, 0 );
	}
	else
		return LxStrCmp( pS1->GetKeyPointer(),
					pS2->GetKeyPointer(),
					pS1->GetKeyLength(),
					pS2->GetKeyLength() );
}

int CompareReverseAlphabetically(const void *pA, const void *pB)
{
	CLParse* pS1 = *static_cast<CLParse* const*>(pA);
	CLParse* pS2 = *static_cast<CLParse* const*>(pB);

  Q_ASSERT(pS1);
  Q_ASSERT(pS2);


  QChar* S1 = new QChar[pS1->GetKeyLength()];
  QChar* S2 = new QChar[pS2->GetKeyLength()];
  LxStrCpy_R( pS1->GetKeyPointer(), S1, pS1->GetKeyLength() );
  LxStrCpy_R( pS2->GetKeyPointer(), S2, pS2->GetKeyLength() );
  int comp = LxStrCmp( S1, S2, pS1->GetKeyLength(), pS2->GetKeyLength() );

  delete [] S1;
  delete [] S2;

  return comp;
}

int CompareSortingQuantity (const void *pA, const void *pB)
{
	CLParse* pS1 = *static_cast<CLParse* const*>(pA);
	CLParse* pS2 = *static_cast<CLParse* const*>(pB);

  float  dif =   pS2->GetSortingQuantity() -  pS1->GetSortingQuantity();

  if (dif > 0) {return 1;} else if (dif < 0) {return -1;} else {return 0;}
}

int CompareSortingString (const void *pA, const void *pB)
{
	CLParse* pS1 = *static_cast<CLParse* const*>(pA);
	CLParse* pS2 = *static_cast<CLParse* const*>(pB);

  return LxStrCmp( pS1->GetSortingString(), pS2->GetSortingString(), pS1->GetSortingString().length(), pS2->GetSortingString().length() );

}

int CompareNumberOfStems (const void *pA, const void *pB)
{
	CSignature* pS1 = *static_cast<CSignature* const*>(pA);
	CSignature* pS2 = *static_cast<CSignature* const*>(pB);

	int dif = pS2->GetNumberOfStems() -  pS1->GetNumberOfStems();

  if (dif > 0) {return 1;} else if (dif < 0) {return -1;} else {return 0;}

}

int CompareSigRemark (const void *pA, const void *pB)
{
	CSignature* pS1 = *static_cast<CSignature* const*>(pA);
	CSignature* pS2 = *static_cast<CSignature* const*>(pB);
  return QString::compare( pS1->GetRemark(), pS2->GetRemark() );
}

int CompareStemSource (const void *pA, const void *pB)
{
	CStem* pS1 = *static_cast<CStem* const*>(pA);
	CStem* pS2 = *static_cast<CStem* const*>(pB);
  return QString::compare( pS1->GetConfidence(), pS2->GetConfidence() );
}

int CompareCorpusCount (const void *pM1,   const void *pM2)
{
	CLParse* pS1 = *static_cast<CLParse* const*>(pM1);
	CLParse* pS2 = *static_cast<CLParse* const*>(pM2);

	int dif = pS2->GetCorpusCount() -  pS1->GetCorpusCount();

  if (dif > 0) {return 1;} else if (dif < 0) {return -1;} else {return 0;}
}

int CompareSize (const void *pM1,   const void *pM2)
{
	CLParse* pS1 = *static_cast<CLParse* const*>(pM1);
	CLParse* pS2 = *static_cast<CLParse* const*>(pM2);

  int  dif =   pS2->Size() -  pS1->Size();

  if (dif > 0) {return 1;} else if (dif < 0) {return -1;} else {return 0;}
}

int CompareLength (const void *pM1,   const void *pM2)
{
	CLParse* pS1 = *static_cast<CLParse* const*>(pM1);
	CLParse* pS2 = *static_cast<CLParse* const*>(pM2);

  int  dif =   pS2->GetKeyLength() -  pS1->GetKeyLength();

  if (dif > 0) {return 1;} else if (dif < 0) {return -1;} else {return 0;}
}

int CompareDLSavings (const void *pM1,   const void *pM2)
{
	CMorpheme* pS1 = *static_cast<CMorpheme* const*>(pM1);
	CMorpheme* pS2 = *static_cast<CMorpheme* const*>(pM2);

  double  dif =   pS2->GetDLSavings() -  pS1->GetDLSavings();

  if (dif > 0) {return 1;} else if (dif < 0) {return -1;} else {return 0;}
}

int CompareFrequency (const void *pM1,   const void *pM2)
{
	CMorpheme* pS1 = *static_cast<CMorpheme* const*>(pM1);
	CMorpheme* pS2 = *static_cast<CMorpheme* const*>(pM2);

  double  dif =   pS2->GetFrequency() -  pS1->GetFrequency();

  if (dif > 0) {return 1;} else if (dif < 0) {return -1;} else {return 0;}
}

int CompareMorphemeCount (const void *pM1,   const void *pM2)
{
	CMorpheme* pS1 = *static_cast<CMorpheme* const*>(pM1);
	CMorpheme* pS2 = *static_cast<CMorpheme* const*>(pM2);

  double  dif =   pS2->GetMorphemeCount() -  pS1->GetMorphemeCount();

  if (dif > 0) {return 1;} else if (dif < 0) {return -1;} else {return 0;}
}

int CompareUseCount (const void *pM1,   const void *pM2)
{
	CAffix* pS1 = *static_cast<CAffix* const*>(pM1);
	CAffix* pS2 = *static_cast<CAffix* const*>(pM2);

  double  dif =   pS2->GetUseCount() -  pS1->GetUseCount();

  if (dif > 0) {return 1;} else if (dif < 0) {return -1;} else {return 0;}
}

// compare() for basic types
int MakeComparable(int a, int b)
{
	const int difference = a - b;

	if (difference < 0) return -1;
	else if (difference == 0) return 0;
	else return 1;
}

int MakeComparable(double a, double b)
{
	const double difference = a - b;

	if (difference < 0) return -1;
	else if (difference == 0) return 0;
	else return 1;
}
