// Signature type for suffix/signature-based morphology discovery
// Copyright © 2009 The University of Chicago
#ifndef SIGNATURE_H
#define SIGNATURE_H

class CSignatureListViewItem;
class CSignature;

// See the CMiniLexicon class in MiniLexicon.h for an overview of
// suffix/signature-based discovery of morphology.

// for CSignatureListViewItem:
#include <Q3ListViewItem>
#include <QString>
template<class K, class V> class QMap;

// for CSignature:
#include "LParse.h"
#include <QString>
#include <QChar>
#include "Parse.h"
#include "AffixLocation.h"
template<class K, class V> class QMap;
template<class T> class QList;

/// A list view item for signatures.
/**
	Contains all the necessary variables needed when displaying a
	signature and its data in a list view.
 */
class CSignatureListViewItem : public Q3ListViewItem {
	CSignature* m_signature;
	QMap<QString, QString>* m_filter;
	QString m_label;
	class Q3ListView* m_parentlist;
	int m_mini;
public:
	// construction/destruction.

	explicit CSignatureListViewItem(class Q3ListView* parent = 0,
		QString sig_text = QString(), int mini_index = -1,
		CSignature* sig = 0, QMap<QString, QString>* filter = 0);
	CSignatureListViewItem(Q3ListViewItem* parent,
		QString sig_text = QString(), int mini_index = -1,
		CSignature* sig = 0, QMap<QString, QString>* filter = 0);

	// copy.

	CSignatureListViewItem(const CSignatureListViewItem& x)
		: Q3ListViewItem(x),
		m_signature(x.m_signature),
		m_filter(x.m_filter),
		m_label(x.m_label),
		m_parentlist(x.m_parentlist),
		m_mini(x.m_mini) { }
	CSignatureListViewItem& operator=(const CSignatureListViewItem& x)
	{
		Q3ListViewItem::operator=(x);
		m_signature = x.m_signature;
		m_filter = x.m_filter;
		m_label = x.m_label;
		m_parentlist = x.m_parentlist;
		m_mini = x.m_mini;
		return *this;
	}

	// Qt3 list view item interface.

	virtual QString text(int column_index) const;
	int compare(Q3ListViewItem* other, int column, bool ascending) const;

	// underlying signature object.

        CSignature*                     GetSignature() { return m_signature; }
        void                            SetSignature(CSignature* pSig) { m_signature = pSig; }

	/// enclosing widget
        Q3ListView*                     GetParent() { return m_parentlist; }
};

/// A string implementation of signatures.
/**
	Signatures are simple statements of morphological patterns, which aid both
	in quantifying the Minimum Description Length (MDL) account and in constructively
	building a satisfactory morphological grammar (for MDL offers no guidance in the
	task of seeking the optimal analysis).
 */
class CSignature: public CLParse {
public:
	enum detachment_parameter {
		eDo_Not_Call_Words,
		eCall_Words,
	};
protected:
	enum eAffixLocation m_AffixLocation;
        QList<class CStem*>*            m_StemPtrList;
        QList<class CStem*>*            m_WordPtrList;
        QList<class CSuffix*>*          m_SuffixPtrList;
        QList<class CPrefix*>*          m_PrefixPtrList;

	// XXX. we may want to do this with a bit-base
	// vector someday to speed it up.
	/// points to the indexes of the affixes
//	class CSparseIntVector* m_Vector;

        /// Count-related members
        QVector<double>                 m_WordCounts;
        QVector<double>                 m_StemCounts;
        QVector<double>                 m_AffixCounts;
        QVector<double>                 m_StemFrequencies;
        QVector<double>                 m_AffixFrequencies;
        QVector<double>                 m_WordFrequencies;
        int                             m_TotalCount;
        QString                         m_Remark;

        // float* m_Frequencies;


	/// the number of letters saved by having this signature
	/// = ( Number of stems - 1 ) * ( Number of suffixes - 1);
	/// Or, if there is a more robust signature which is
	/// contained in this one.
	/// It really measures how good our knowledge of these stems is, qua stems.
        mutable double                  m_Robustness;
        CSignature*                     m_Mentor;
        QList<CSignature*>*             m_MentorList;
        CSignature*                     m_MyGeneralizer;
	/// without the <e> stuff prefixed
        CParse                          m_SimplifiedForm;
        class CSignatureCollection*     m_SignatureCollection;

	/// These affixes are "close" to this signature, in the sense that
	/// stems associated with this signature may also be marked to take
	/// that affix.
	CParse m_SatelliteAffixes;

	// description length.

        double                          m_DLofMyCorpus;
        double                          m_DLofMyStemPointers;
        double                          m_DLofMyAffixPointers;
        double                          m_LengthOfPointerToMe;
public:
	// construction/destruction.

	CSignature(class CMiniLexicon* mini);
	CSignature(enum eAffixLocation prefix_or_suffix,
		class CMiniLexicon* mini);
	CSignature(const CParse& affixes, class CMiniLexicon* mini);
	/// single-affix signature
	CSignature(const class CStringSurrogate& affix,
		class CMiniLexicon* mini);
	/// deprecated
	CSignature(const CParse* affixes_ptr, class CMiniLexicon* mini);
	~CSignature();

	// copy/assignment.

	CSignature(const CSignature& x);
	CSignature& operator=(const CSignature& x)
		{ *this = &x; return *this; }
	/// deprecated
	void operator=(const CSignature* x);
        ///  clear
        void                            Suicide();
        void                            ConsumeParse(CParse* sigs);

	// convert to string.

	/// includes deletees in affixes
        QString                         Express(bool bDisplayDeletees = true);
	// includes deletees in affixes
        CSignature&                     Express(CSignature& Express, bool bDisplayDeletees = true);
        QString                         Display(QChar sep, QMap<QString, QString>* filter = 0) const;
        QString                         Display(QMap<QString, QString>* filter) const;
        QString                         Display() const;

	// counts.

	/// number of occurences in corpus (corpus count).
	/// output is a vector of integers whose length is the number of stems
	/// times the number of suffixes.
        int                             GetTotalCount() const;
        int                             GetNumberOfWords() const;
        double                          GetWordCount(int wordno) const;
        double                          GetWordCount(int stemno, int affixno) const;
        void                            SetWordCount(int stemno, int affixno, double value);
        void                            CalculateWordCounts();
        double                          GetStemCount(int stemno) const;
        double                          GetAffixCount(int affixno) const;
        double                          GetStemFrequency (int stemno) const;
        double                          GetAffixFrequency(int affixno) const;
        double                          GetWordFrequency(int stemno, int affixno) const;
        int                             GetNumberOfAffixes() const;
        int                             GetNumberOfStems() const;
        void                            CalculateFrequencies(class CMiniLexicon* mini);
        double                          GetCorpusCount() const;
        void                            SetNewWordCounts (int size = 0);
        void                            SetNewStemCount (int size = 0);
        void                            SetNewAffixFrequencies(int size = 0);
        void                            SetNewStemFrequencies(int size = 0);

	// context (enclosing mini-lexicon).

	/// affix location (prefix or suffix)
        enum                            eAffixLocation GetAffixLocation() const;
        void                            SetAffixLocation(enum eAffixLocation prefix_or_suffix)   { m_AffixLocation = prefix_or_suffix; }
        CSignatureCollection*           GetSignatureCollection()                                 { return m_SignatureCollection; }
        void                            SetSignatureCollection(CSignatureCollection* list)       { m_SignatureCollection = list; }
        class CMiniLexicon*             GetLexicon() const                                       { return m_pMyMini; }
        void                            SetLexicon(class CMiniLexicon* lex)                      { m_pMyMini = lex; }

	/// human-readable string describing this signature’s discovery.
        QString                         GetRemark() const;
        void                            SetRemark(QString origin);

        CSignature*                     GetGeneralizer() const { return m_MyGeneralizer; }

	// contained affixes.

	/// indices of affixes in the affix collection that holds them.


        QList<CSuffix*>*                GetSuffixPtrList() const;
        QList<CPrefix*>*                GetPrefixPtrList() const;
        void                            SetNewStemPtrList ( QList< CStem* >& );
        void                            CreateNewStemPtrList (int size);
        void                            CreateNewSuffixPtrList (int size);
        void                            CreateNewPrefixPtrList (int size);
        CSuffix*                        GetSuffix (int n) const;
        CPrefix*                        GetPrefix (int n) const;
        void                            AppendSuffixPtr (CSuffix*);
        void                            AppendPrefixPtr (CPrefix*);
        void                            AppendStemPtr (CStem*) const;
        bool                            StemListContains(CStem* );
        void                            RemoveFromWordList(CStem* pWord);
        void                            RecalculateStemAndWordPointers(); //jan 2010

	// stems appearing with this sig.

        CParse                          GetStems();
        CStem*                          GetStem(int stemno) const;
        QList<CStem*>*                  GetStemPtrList() const;
        void                            ClearStemPtrList();

	// words using this sig. 
        CParse                          GetWords();
        CStem*                          GetWord(int stem, int suffix) const;
        void                            AppendWordPointer(CStem* const);

	// description length.

        double                          GetDLofMyStemPointers();
        double                          GetDLofMyAffixPointers();
        virtual double                  ComputeDLofModel(int char_count = 26);
        double                          FindCost(class CMiniLexicon* mini);
        double                          ComputeDLofMyCorpus();
        double                          GetLengthOfPointerToMe();
        void                            SetLengthOfPointerToMe(double);
        double                          GetSumOfDLofInternalPointers();

	// part-of-speech discovery.

        CSignature*                     GetMentor();
        void                            SetMentor(CSignature* sig);
        QList<CSignature*>*             GetMentorList();

	/// deprecated: index of a known suffix in the suffix list.
	int GetNumber(CSuffix* suffix);

	/// (#affixes - 1)(total length of stems) +
	/// (#stems - 1)(total length of affixes)
        double                          GetRobustness() const;
        void                            SetRobustness(double value);
	/// deprecated: synonym for GetRobustness
        float                            GetSortingQuantity() const;

	// insert.

	/// register a stem appearing with this signature.
	void operator<<(class CStem* stem);
	/// Attach to Suffix Sig.
	/**
	 * This is the function to use to take a stem from one
	 * signature to a new one, and deal with all of the bookkeeping
	 * that goes with that. First it detaches the Stem from its
	 * old signature by using DetachStem() (see below);
	 * then it adds this stem to the New Signature;
	 * then it shifts all of the stem's words from the old signature
	 * to the new signature.
	 */
        void                            AttachToSuffixSig(CStem* stem, bool bLookAtPreviousSig = true);
        void                            AttachToPrefixSig(CStem* stem, bool bLookAtPreviousSig = true);
        void                            AttachStemFromDifferentPrefixSignature(CStem* s)                   { AttachToPrefixSig(s); }
        void                            AttachStemFromDifferentSuffixSignature(CStem* s)                   { AttachToSuffixSig(s); }
        void                            AddWord(CStem* word);
	/// add a satellite affix
	/// (i.e. remember one which often occurs on stems that take
	/// this signature)
        void                            AppendSatelliteAffix(CParse& Affix);
        void                            TakeAllStems(CSignature* other);

	// remove.

        void                            DetachWord(CStem* word, enum detachment_parameter);
	/// remove a stem from a signature's Stem Ptr List, and if the
	/// Detachment Parameter is true, also remove all of the stem's
	/// WORDS from the signature's Word Ptr List.
	/// This is called by AttachToSuffixSig().
        void                            DetachStem(CStem* stem, enum detachment_parameter words_too);
        bool                            RemoveStem(CStem* stem);
        bool                            RemoveWord(CStem* word);

	// output to GUI.

	void BorrowedSigsDisplay(class Q3ListView* widget,
		QMap<QString, QString>* filter = 0);
	void ListDisplay(class Q3ListView* widget,
		QMap<QString, QString>* filter = 0,
		bool express_deletees = true);

	/// output to file
        void                            OutputSignatureXfst ( class QTextStream& outf, int count );
        void                            OutputSignature(class QTextStream& out);

	/// no empty affixes (but "NULL" as an affix is fine)
        bool                            IsValid();

	/// XXX. no-op
        void                            FindCorpusCount();

	/// XXX. unused
        void                            SetMyGeneralizer(CSignature* sig);

	// allomorphy and cut shifting helpers.

	/// delete Letter from the beginning of each suffix, and
	/// add it to each stem.
	/// add a <Letter> to suffixes
	/// Much like shifting boundary of stem/suffix split to the right.
        void                            RemoveLetter(CStringSurrogate& Letter,
		class CMiniLexicon* mini, CSignature* out);
	/// add Letter to the beginning of each suffix
        void                            AddLetter(const QString& Letter);
	/// whether adding a letter at the start of each suffix still yield
	/// known suffixes
        bool                            EachSuffixCanHaveThisLetterPrefixedToIt(const QString& Letter);
        void                            ShiftStemSuffixCutToTheLeft(int distance, const QString& Piece);
        void                            ShiftStemSuffixCutToTheLeft(int distance);
        bool                            Generalizes(CSignature* other);

	CParse CreateADeletingSignature(CParse& Deleter,
        class                           CMiniLexicon* mini);

        /// This will look at words claimed by this signature,
        /// and actively make cuts in them to match the affixes declared by the signature.
        void                            CutMyWordsAsIDeclare();

	/// entropy of final ngrams of stems
        double                          ComputeFinalNgramEntropyOfStems(int n);

	// real workers.

        int                             CheckOut(class CMiniLexicon* mini);
        void                            IterateThroughStems(int NumberOfLettersShifted,
                                            class CMiniLexicon* Lexicon, CLParse* pPiece,
                                            double& TotalDecreaseInDLDueToShorterStems,
                                            double LogTotalNumberOfAnalyzedWords,
                                            double& StemPointersToThisSig,
                                            double& SavingsBecauseStemAlreadyExisted,
                                            bool analyzingSuffixex);

        friend class QTextStream&       operator<<(class QTextStream& out,
                                            CSignature* sigp);
};

#endif // SIGNATURE_H
