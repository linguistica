// Implementation of CSuffixCollection methods
// Copyright © 2009 The University of Chicago
#include "SuffixCollection.h"

#include <Q3TextStream>
#include <QIODevice>
#include <QFile>
#include "ui/Status.h"
#include "MiniLexicon.h"
#include "Lexicon.h"
#include "Suffix.h"
#include "log2.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSuffixCollection::CSuffixCollection(CMiniLexicon* Lex)
{
  // do the creation of TCollection with ptr to Lexicon and name of class
	m_pMiniLex = Lex;
	if( m_pMiniLex ) m_pLexicon = m_pMiniLex->GetLexicon();
}


CSuffixCollection::CSuffixCollection( )
{
	m_pMiniLex = NULL;
	m_pLexicon = NULL;
}


CSuffixCollection::~CSuffixCollection()
{
	if( m_pLexicon )
	{
		CSuffix* pSuffix;
		for( int i = 0; i < GetCount(); i++ )
		{
			pSuffix = GetAt(i);

			m_pLexicon->RemoveSuffix( pSuffix );
		}
	}
}

void CSuffixCollection::ListDisplay(
	Q3ListView* pView, QMap<QString, QString>* filter,
	linguistica::ui::status_user_agent& status,
	bool ExpressDeletees)
{
	pView->setRootIsDecorated(false);
	pView->setSorting(1);

	// Clean it out first.
	while (pView->columns() != 0)
		pView->removeColumn(0);

	// Add Column headers
	pView->addColumn("Prefixes");
	pView->addColumn("Descr. Length");
	pView->addColumn("Length of Ptr");
	pView->addColumn("Corpus Count");
	pView->addColumn("Use Count");
	pView->addColumn("Stems");

	pView->setColumnAlignment(0, Qt::AlignCenter);
	pView->setColumnAlignment(1, Qt::AlignRight);
	pView->setColumnAlignment(2, Qt::AlignRight);
	pView->setColumnAlignment(3, Qt::AlignCenter);
	pView->setColumnAlignment(4, Qt::AlignCenter);
	pView->setColumnAlignment(5, Qt::AlignLeft);

	// Column three gets really wide, so
	// limit the width to 100.
	pView->setColumnWidthMode(3, Q3ListView::Manual);
	pView->setColumnWidth(3, 100);

	status.major_operation = "Creating suffix list for display";
	status.progress.clear();

	if (m_SortValidFlag == false)
		Sort(COUNT);

	// Display each item
	status.progress.set_denominator(GetCount());
	for (int i = 0; i < (int) GetCount(); i++) {
		GetAtSort(i)->ListDisplay(pView, filter, ExpressDeletees,
		                          m_pLexicon->GetNumberOfCharacterTypes());
		status.progress = i;
	}
	status.progress.clear();
	status.major_operation.clear();
}

CSuffix* CSuffixCollection::operator<< ( CStringSurrogate& Suffix )
{
  CSuffix*  pSuffix;

  pSuffix = AddToCollection( Suffix );

  return pSuffix;
}


CSuffix* CSuffixCollection::operator<< ( CParse* Suffix )
{
  CSuffix*  pSuffix;

  pSuffix = AddToCollection (*Suffix);

  return pSuffix;
}


CSuffix* CSuffixCollection::operator<< ( CParse& Suffix )
{
  CSuffix*  pSuffix;

  pSuffix = AddToCollection (Suffix);

  return pSuffix;
}

CSuffix* CSuffixCollection::operator<< ( QString Suffix )
{
  CSuffix*  pSuffix;

  CStringSurrogate cssSuffix( Suffix );

  pSuffix = AddToCollection( cssSuffix );

  return pSuffix;
}


void CSuffixCollection::AddPointer( CSuffix* pSuffix )
{
	TCollection<CSuffix>::AddPointer( pSuffix );

	if( m_pLexicon ) m_pLexicon->InsertSuffix( pSuffix );
}


CSuffix* CSuffixCollection::AddToCollection( CParse& Suffix )
{
	CSuffix* pSuffix = TCollection<CSuffix>::AddToCollection( Suffix );

	if( m_pLexicon ) m_pLexicon->InsertSuffix( pSuffix );

	return pSuffix;
}


CSuffix* CSuffixCollection::AddToCollection( CStringSurrogate& Suffix )
{
	CSuffix* pSuffix = TCollection<CSuffix>::AddToCollection( Suffix );

	if( m_pLexicon ) m_pLexicon->InsertSuffix( pSuffix );

	return pSuffix;
}


void CSuffixCollection::Empty()
{
	if( m_pLexicon )
	{
		CSuffix* pSuffix;
                for( int suffixno = 0; suffixno < GetCount(); suffixno++ )
		{
                        pSuffix = GetAt(suffixno);
			m_pLexicon->RemoveSuffix( pSuffix );
		}
	}
	TCollection<CSuffix>::Empty();
}


void CSuffixCollection::RemoveAll()
{
	if( m_pLexicon )
	{
		CSuffix* pSuffix;

                for( int suffixno = 0; suffixno < GetCount(); suffixno++ )
		{
                        pSuffix = GetAt(suffixno);
			m_pLexicon->RemoveSuffix( pSuffix );
		}
	}
	TCollection<CSuffix>::RemoveAll();
}


bool CSuffixCollection::Remove( CSuffix* pSuffix )
{
	if( m_pLexicon ) m_pLexicon->RemoveSuffix( pSuffix );

	return TCollection<CSuffix>::Remove( pSuffix );
}


bool CSuffixCollection::RemoveMember( CSuffix* pSuffix )
{
	if( m_pLexicon ) m_pLexicon->RemoveSuffix( pSuffix );

	return TCollection<CSuffix>::RemoveMember( pSuffix );
}


bool CSuffixCollection::RemoveMember( CStringSurrogate& Suffix )
{
	CSuffix* pSuffix = (CSuffix*)Find1( Suffix )->Get_T_Pointer();

	if( m_pLexicon ) m_pLexicon->RemoveSuffix( pSuffix );

	return TCollection<CSuffix>::RemoveMember( Suffix );
}


bool CSuffixCollection::RemoveMember( CStringSurrogate& Suffix, bool b )
{
	CSuffix* pSuffix = (CSuffix*)Find1( Suffix )->Get_T_Pointer();

	if( m_pLexicon ) m_pLexicon->RemoveSuffix( pSuffix );

	return TCollection<CSuffix>::RemoveMember( Suffix, b );
}

int CSuffixCollection::GetTotalUseCount() 
{  if (m_TotalUseCount <= 0) 
      CalculateTotalUseCount();
    return m_TotalUseCount;
}

void CSuffixCollection::CalculateTotalUseCount()
{
	m_TotalUseCount = 0;
       for (int suffixno = 0; suffixno < GetCount(); suffixno++)
       {	   m_TotalUseCount += GetAt(suffixno)->GetUseCount();
       }
}

void CSuffixCollection::DeleteMarkedMembers()
{
	if ( m_DeletionArray == NULL ) { return; }

	int count = GetCount();
        for (int suffixno = 0; suffixno < count; suffixno++)
	{
                if ( m_DeletionArray[suffixno] == 1 )
		{
                        if( m_pLexicon ) m_pLexicon->RemoveSuffix( m_PointerArray[suffixno] );
		}
	}
	TCollection<CSuffix>::DeleteMarkedMembers();
}


void CSuffixCollection::OutputSuffixes( QString FileName )
{
	QFile file( FileName );

	if( file.open( QIODevice::WriteOnly ) )
	{
		Q3TextStream outf( &file );
		outf.setEncoding( Q3TextStream::Unicode );

		outf << "# Suffix Count" << endl;
		outf << "# ------------" << endl;
		outf << "  " << GetCount() << endl << endl;

		outf << "# Suffix       | Use Count | Corpus Count | Index | " << endl;
		outf << "# ------------------------------------------------- " << endl;
		
		Sort( CORPUSCOUNT );
                for (int suffixno = 0; suffixno < (int)GetCount(); suffixno++)
		{
                        GetAtSort(suffixno)->OutputSuffix( outf );
		}

		file.close();
	}
}


void  CSuffixCollection::ReadSuffixFile (QString FileName)
{
	CSuffix*	pSuffix;
	QFile		file(FileName);
	QString	Buffer, Key;
	int		Index,
	size,
	UseCount,
	CorpusCount,
	LineCount = 0;

	if( file.exists() && file.open( QIODevice::ReadOnly ) )
	{
		Q3TextStream inf(&file);
		inf.setEncoding ( Q3TextStream::Locale );

		Buffer = inf.readLine();
		Q_ASSERT( Buffer[0] == '#' );

		Buffer = inf.readLine();
		Q_ASSERT( Buffer[0] == '#' );

		inf >> size;

		Buffer = inf.readLine(); // end of size line
		Q_ASSERT( Buffer.length() == 0 );

		Buffer = inf.readLine(); // blank line
		Q_ASSERT( Buffer.length() == 0 );

		Buffer = inf.readLine();
		Q_ASSERT( Buffer[0] == '#' );

		Buffer = inf.readLine();
		Q_ASSERT( Buffer[0] == '#' );

		while( !inf.atEnd() && LineCount < size )
		{
			LineCount++;

			inf >> Key;
			inf >> UseCount;
			inf >> CorpusCount;
			inf >> Index;

			// Filter all sequences that should be
			// analyzed as one character
			Key = Filter( m_pLexicon->GetInFilter(), Key );

			pSuffix = *this <<   Key ;
			pSuffix->IncrementUseCount( UseCount );
			pSuffix->IncrementCorpusCount( CorpusCount );
		}

		file.close();
	}  
}

double	CSuffixCollection::GetDL_PhonologicalContent()
{
	double affix_total_dl = 0;
	CSuffix* pAffix;
        for( int suffixno = 0; suffixno < (int)GetCount(); suffixno++ )
	{
                pAffix =  GetAt(suffixno);
		affix_total_dl += pAffix->ComputeDL( m_pMiniLex->GetNumberOfCharacterTypes() );
	}
	return affix_total_dl ;
}


double	CSuffixCollection::CalculatePointersToMySuffixes(eMDL_STYLE style )//sum of pointers to members; this should be removed, and replaced by "GetLengthOfPointerToMe"
{
	double			Denominator = 0;
	double			ptr;
                                m_DLofPointersToMyMembers = 0;
                                m_CorpusCount = 0;
 	
	if (style == CorpusCount )
	{
                for (int suffixno = 0; suffixno < GetCount(); suffixno++)
		{
                        m_CorpusCount += GetAt(suffixno)->GetCorpusCount();
		}
                for (int suffixno = 0; suffixno < GetCount(); suffixno++)
		{
                        ptr = base2log ( m_CorpusCount / GetAt(suffixno)->GetCorpusCount() );
			m_DLofPointersToMyMembers += ptr;
		}
	}
	else if (style  == GrammarCount )
	{
                for (int suffixno = 0; suffixno < GetCount(); suffixno++)
		{
                        Denominator += GetAt(suffixno)->GetUseCount();
		}
                for (int suffixno = 0; suffixno < GetCount(); suffixno++)
		{
                        ptr = base2log ( Denominator/ GetAt(suffixno)->GetUseCount() );
			m_DLofPointersToMyMembers += ptr;
		}
	}
	return m_DLofPointersToMyMembers;
}
