// Suffix/signature-based discovery of morphology: core
// Copyright © 2009 The University of Chicago
#include "MiniLexicon.h"

#include <memory>
#include <vector>
#include <map>
#include <QList>
#include "ui/Status.h"
#include "Lexicon.h"
#include "DLHistory.h"
#include "Signature.h"
#include "Suffix.h"
#include "Prefix.h"
#include "Stem.h"
#include "SignatureCollection.h"
#include "SuffixCollection.h"
#include "PrefixCollection.h"
#include "WordCollection.h"
#include "StemCollection.h"
#include "CompareFunc.h"
#include "StringFunc.h"
#include "HTML.h"
#include "AffixLocation.h"
#include "implicit_cast.h"
using linguistica::implicit_cast;

/// Writes to status.details instead of status.major_operation.
void CMiniLexicon::TakeSplitWords_ProduceStemsAndSigs( CStringSurrogate& Remark,
                                                       CWordCollection* pWords,
                                                       CStemCollection* pStems,
                                                       CPrefixCollection* pPrefixes,
                                                       CSuffixCollection* pSuffixes )

{
        if( pWords == NULL )    { pWords = m_pWords; };
        if( pStems == NULL )    { pStems = m_pStems; }
	if( pPrefixes == NULL ) { pPrefixes = m_pPrefixes; }
	if( pSuffixes == NULL ) { pSuffixes = m_pSuffixes; }

	RebuildAffixesStemsAndSignaturesFromWordSplits( Remark );
}

void CMiniLexicon::RebuildAffixesStemsAndSignaturesFromWordSplits(
		CStringSurrogate& Remark)
{
	int HowManySuffixes = 0, HowManyPrefixes = 0;
	CStemCollection* OldStems;
	CStringSurrogate ssPrefix, ssStem, ssSuffix;
	CStem* pWord = NULL;
	CSuffix* pSuffix = NULL;
	CStem* pStem;
	CStem* qStem;
	CPrefix* pPrefix = NULL;
	CSuffix* pNullSuffix = NULL;
	CPrefix* pNullPrefix = NULL;

	CLexicon& lex = *m_pLexicon;
	linguistica::ui::status_user_agent& status = lex.status_display();

	LogFileSmallTitle("Rebuilding all signature structures");
	status.details = "Rebuilding all signature structures.";
	status.progress.clear();

	bool analyzingSuffixes = true;
	if (m_AffixLocation == STEM_INITIAL || m_AffixLocation == WORD_INITIAL)
		analyzingSuffixes = false;
	if (analyzingSuffixes) {
		m_pSuffixes->RemoveAll();
		pNullSuffix = *m_pSuffixes << TheStringNULL;
	} else {
		m_pPrefixes->RemoveAll();
		pNullPrefix = *m_pPrefixes << TheStringNULL;
	}
	OldStems = m_pStems;
	m_pStems = new CStemCollection(this);
	status.details = "Rebuilding all signature structures: words.";
	status.progress.set_denominator(m_pWords->GetCount());
	for (int wordno = 0; wordno < (int)m_pWords->GetCount(); wordno++) {
		status.progress = wordno;

		pWord = m_pWords->GetAt(wordno);
		if (!pWord)
			continue;

		pWord->SetSuffixSignature(NULL);
		pWord->SetPrefixSignature(NULL);

		// commented out to avoid seg fault
		if (
#if 0
		    pWord->GetWordType() == CStem::BIWORD_COMPOUND ||
		    pWord->GetWordType() == CStem::MULTIPLE_COMPOUND ||
#endif
		    pWord->GetWordType() == CStem::NUMBER)
			continue;

		// rebuild stems, part 1.

		ssStem = pWord->GetStem();

		if (ssStem.GetLength() == 0) {
			pWord->SetStemLoc(1);
			pWord->SetSuffixLoc(0);
			pWord->SetPrefixLoc(0);
			continue;
		}

		// rebuild affixes.

		if (analyzingSuffixes) {
			ssSuffix = pWord->GetSuffix();
			if (ssSuffix.GetLength() < 1)
				continue;
			pSuffix = *m_pSuffixes << ssSuffix;
			if (!pSuffix)
				continue;
			pSuffix->IncrementCorpusCount(pWord->GetCorpusCount() - 1);
			pSuffix->IncrementUseCount();
			pWord->SetSuffixPtr(pSuffix);
			HowManySuffixes++;
		} else {
			ssPrefix = pWord->GetPrefix();
			if (ssPrefix.GetLength() < 1)
				continue;
			pPrefix = *m_pPrefixes << ssPrefix;
			pPrefix->IncrementCorpusCount(pWord->GetCorpusCount() - 1);
			pPrefix->IncrementUseCount();
			pWord->SetPrefixPtr(pPrefix);
			HowManyPrefixes++;
		}

		// attach affix to stem.

		if (analyzingSuffixes) {
			pStem = *m_pStems << ssStem ;
			if (!pStem)
				continue;
			pWord->AttachWordAndSuffixalStem(pStem);
			pSuffix = *m_pSuffixes ^= pWord->GetSuffix();
			if (!pSuffix)
				continue;
			pSuffix->AddStem(pStem);
		} else {
			pStem = *m_pStems << ssStem;
			pWord->AttachWordAndPrefixalStem(pStem);
			pPrefix = *m_pPrefixes ^= pWord->GetPrefix();
			pPrefix->AddStem(pStem);
		}

		pStem->SetConfidence(pWord->GetConfidence());
		qStem = *OldStems ^= pStem->GetKey();
		if (qStem)
			pStem->SetConfidence(qStem->GetConfidence());
		else
			pStem->SetConfidence(Remark.Display());

		Q_ASSERT(pWord->IsValid());

		if (analyzingSuffixes)
			pStem->AddSuffix(pSuffix);
		else
			pStem->AddPrefix(pPrefix);
	} // loop over wordno
	status.progress.clear();

	// in what follows,
	// if a stem is also a word, then we add NULL
	// to the stem's factorization.
	// Stems Loop.
	LogFileSmallTitle("Stem Loop");
	LogFileStartTable();
	status.details = "Rebuilding all signature structures:stems.";
	status.progress.set_denominator(m_pStems->GetCount());
	int colno = 1;
	const int numberofcolumns = 8;
	for (int stemno = 0; stemno < (int)m_pStems->GetCount(); stemno++) {
		pStem = m_pStems->GetAt(stemno);
		status.progress = stemno;

		if (colno == numberofcolumns) {
			LogFileEndRow();
			colno = 1;
		}
		if (colno == 1)
			LogFileStartRow();
		LogFileSimpleString(pStem->Display());
		colno++;
		if (analyzingSuffixes) {
			if (pStem->GetNumberOfSuffixes() < 1)
				continue;
		} else {
			if (pStem->GetNumberOfPrefixes() < 1)
				continue;
		}
		pWord = *m_pWords ^= pStem->GetKey();
		if (pWord) {
			if (analyzingSuffixes) {
				pStem->AddNULLSuffix();
				pStem->AddWord(pWord);
				pNullSuffix->IncrementUseCount();
				pNullSuffix->IncrementCorpusCount(pWord->GetCorpusCount());
				pStem->IncrementWordCount();
				pNullSuffix->AddStem(pStem);
			} else {
				pStem->AddNULLPrefix();
				pStem->AddWord(pWord);
				pNullPrefix->IncrementUseCount();
				pNullPrefix->IncrementCorpusCount(pWord->GetCorpusCount());
				pStem->IncrementWordCount();
				pNullPrefix->AddStem(pStem);
			}

			pStem->IncrementCorpusCount(pWord->GetCorpusCount());
		}
		pStem->GetSuffixList()->Alphabetize();
	} // end of stem loop
	LogFileEndTable();
	status.details.clear();
	delete OldStems;
	FindAllSignatures();
}

//      Find All Signatures (from each Stem signatures)

/*  This scans the Stems, forming entries in This
 *  for each Class signature (the concatenation of
 *  suffixes taken by a stem
 *  It also makes a CPtrList for each signature
 *  It also makes a pointer to the Sig for each Stem. <<<<
 */
void CMiniLexicon::FindAllSignatures()
{
	CLexicon& lex = *m_pLexicon;
	linguistica::ui::status_user_agent& status = lex.status_display();

	const int MinimumSignatureLength = m_pLexicon->GetIntParameter(
			"Main\\MinimumSignatureLength", 1);
	const bool analyzingSuffixes = !is_initial(m_AffixLocation);

	status.details = "Find all signatures.";

	std::auto_ptr<CSignatureCollection> OldSignatures(m_pSignatures);
	m_pSignatures = analyzingSuffixes ?
		new CSignatureCollection(this, m_pSuffixes, m_AffixLocation) :
		new CSignatureCollection(this, m_pPrefixes, m_AffixLocation);

        for (int wordno = 0; wordno < m_pWords->GetCount(); wordno++)
        {
            m_pWords->GetAt(wordno)->SetPrefixSignature(NULL);
            m_pWords->GetAt(wordno)->SetSuffixSignature(NULL);
        }

	// For each stem: generate signature from prefix/suffix list
	const int num_stems = m_pStems->GetCount();
        bool NewlyCreatedSignature = FALSE;
	status.progress.clear();
	status.progress.set_denominator(num_stems - 1);
        for (int stemno = num_stems - 1; stemno >= 0 ; --stemno) {
                CStem& stem = *m_pStems->GetAt(stemno);
		status.progress = num_stems-1 - stemno;

		CParse& affixes = analyzingSuffixes ?
				*stem.GetSuffixList() :
				*stem.GetPrefixList();

                QList<CSuffix*> suffixes;	// used if analyzingPrefixes
                QList<CPrefix*> prefixes;	// used if !analyzingPrefixes

		// Look up prefixes/suffixes for the new signature’s
		// prefix/suffix pointer list
		for (int j = 1; j <= affixes.Size(); ++j) {
			CStringSurrogate affix_text = affixes.GetPiece(j);

                        analyzingSuffixes ? suffixes.append(*m_pSuffixes ^= affix_text):
                                            prefixes.append(*m_pPrefixes ^= affix_text);
		}

		// If not enough affixes, clear this stem’s signature.
		// Otherwise, record the new signature.

		CSignature* new_sig;
		if (affixes.Size() < MinimumSignatureLength) new_sig = 0;

                new_sig = *m_pSignatures ^= &affixes;
                if ( new_sig  )
                {
                   NewlyCreatedSignature = FALSE;
                }
                else {
                    new_sig = *m_pSignatures << &affixes;
                    NewlyCreatedSignature = TRUE;
                }
		if (analyzingSuffixes) {
			if (new_sig != 0)
				new_sig->AttachToSuffixSig(&stem);
			stem.SetSuffixSignature(new_sig);
			foreach (CStem* word, *stem.GetWordPtrList())
				word->SetSuffixSignature(new_sig);
		} else {
			if (new_sig != 0)
				new_sig->AttachToPrefixSig(&stem);
			stem.SetPrefixSignature(new_sig);
			foreach (CStem* word, *stem.GetWordPtrList())
				word->SetPrefixSignature(new_sig);
		}
                if (NewlyCreatedSignature)
                {
                    // Record signature origin.
                    if (new_sig != 0)
                    {
                            if (CSignature* old_sig = *OldSignatures ^= new_sig)
                                new_sig->SetRemark(old_sig->GetRemark());
                            else
				new_sig->SetRemark(
                                    QString("From known stem and %1")
                                    .arg(analyzingSuffixes ?
                                    "suffix" : "prefix"));
                    }                     
                }

        } // loop over stemno
	status.progress.clear();

        for (int signo = 0; signo < m_pSignatures->GetCount(); signo++)
        {
            m_pSignatures      ->GetAt(signo)->RecalculateStemAndWordPointers();
        }

	status.details.clear();
}

// From Stems Find Suffixes
void CMiniLexicon::FromStemsFindAffixes()
{
	CStem* pStem;
	CStem* pWord;
	CSignature* pSig;
	int StemLength;
	int wordno = 0;
	CStringSurrogate ssStem, ssAffix, ssWord, ssTemp;
	CParse Word;
	const double RobustnessThreshold = m_pLexicon->GetIntParameter(
		"FromStemsFindSuffixes\\RobustnessThreshold", 10);
	const int MinimumNumberOfOccurrences = m_pLexicon->GetIntParameter(
		"FromStemsFindSuffixes\\MinimumNumberOfOccurrences", 3);
	const int MinimumStemLength = m_pLexicon->GetIntParameter(
		"Main\\MinimumStemLength", 3);
	bool analyzingSuffixes = true;

	if (m_AffixLocation == STEM_INITIAL || m_AffixLocation == WORD_INITIAL)
		analyzingSuffixes = false;
	
	if (m_pStems->GetCount() == 0)
		return;

	m_pStems->Sort(KEY);
	m_pWords->Sort(KEY);

	m_pSignatures->CheckRobustness();	// TODO: check this for prefix compatibility

	CLexicon& lex = *m_pLexicon;
	linguistica::ui::status_user_agent& status = lex.status_display();

	status.major_operation = QString("Mini-Lexicon %1: From stems, find %2")
		.arg(m_Index + 1).arg(analyzingSuffixes ?
			QString("suffixes") : QString("prefixes"));

	std::map<QString, int> proposed_affixes;

	status.details = "From known stems, find new affixes";
	status.progress.clear();
	status.progress.set_denominator(m_pStems->GetCount());
	LogFileLargeTitle("Phase: From known stems, find new affixes");
	LogFileStartTable();
	LogFileHeader("Stems", "Affix");
	for (int stemno = 0; stemno < (int)m_pStems->GetCount(); stemno++) {
		status.progress = stemno;

		pStem = m_pStems->GetAtSort(stemno);
		ssStem = pStem->GetKey();

		StemLength = ssStem.GetLength();
		if ((int) StemLength < MinimumStemLength)
			continue;

		// Here is where to test to see
		// if the stem's signature is really robust.
		if (analyzingSuffixes)
			pSig = pStem->GetSuffixSignature();
		else
			pSig = pStem->GetPrefixSignature();
		if (pSig->GetRobustness() < RobustnessThreshold)
			continue;

		// New formulation:
		CParse StemContinuations;
		if (analyzingSuffixes) {
			m_pWords->GetTrie()->CreateSuffixSignatures(
				pStem->GetKey(), &StemContinuations);
		} else {
			ssTemp = pStem->GetKey();
			ssTemp.SetBackwards(true);
			m_pWords->GetReverseTrie()->CreateSuffixSignatures(
				&ssTemp, &StemContinuations);
			ssTemp.SetBackwards(false);
		}

		const QString stem = ssStem.Display();
		bool bStemAlreadyUsed = false;
		for (int suffixno = 1; suffixno < (int) StemContinuations.Size(); suffixno++) {
			ssAffix  = StemContinuations.GetPiece(suffixno);
			if (analyzingSuffixes) {
				Word = ssStem + ssAffix;
				pWord = *m_pWords ^= Word;
				if (m_pWords->GetAtSort(wordno)->GetSuffixLoc() > 0)
					continue;
				if (m_pWords->GetAtSort(wordno)->GetStem2Loc() > 0) {
					wordno++;
					continue;
					// or use WordType
				}
				if (*m_pSuffixes ^= ssAffix)
					continue;
			} else {
				ssAffix.SetBackwards(true);
				Word = ssAffix + ssStem;
				pWord = *m_pWords ^= Word;
				if (m_pWords->GetAtSort(wordno)->GetPrefixLoc() > 0)
					continue;
				if (m_pWords->GetAtSort(wordno)->GetStem2Loc() > 0) {
					wordno++;
					continue;
					// or use WordType
				}
				if (*m_pPrefixes ^= ssAffix)
					continue;
			}

			const QString affix = ssAffix.Display();
			++proposed_affixes[affix];
			if (!bStemAlreadyUsed) {
				LogFileStartRow();
				LogFileSimpleString(stem);
				bStemAlreadyUsed = true;
			}
			LogFileSimpleString(analyzingSuffixes ?
						(stem + " " + affix) :
						(affix + " " + stem));
		} // end of suffixno loop
		if (bStemAlreadyUsed)
			LogFileEndRow();
	} // end of stemno loop
	status.progress.clear();
	status.details.clear();

	LogFileEndTable();

	status.details = "Testing proposed affixes";
	status.progress.clear();

	LogFileSmallTitle("Testing proposed affixes");
	LogFileStartTable();
	LogFileHeader("Affix", "Count", "Status");
	status.progress.set_denominator(proposed_affixes.size());
	int counter = 0;
	for (std::map<QString, int>::iterator i = proposed_affixes.begin();
	     i != proposed_affixes.end();
	     ++i) {
		while (i != proposed_affixes.end() && i->second < MinimumNumberOfOccurrences) {
			status.progress = counter++;

			LogFileStartRow();
			LogFileSimpleString(i->first);
			LogFileSimpleInteger(i->second);
			LogFileSimpleString("<font color=\"gray\">removed</font>");
			LogFileEndRow();

			proposed_affixes.erase(i++);
		}
		if (i == proposed_affixes.end())
			break;

		status.progress = counter++;
		const QString affix = i->first;
		const int count = i->second;

		LogFileStartRow();
		LogFileSimpleString(affix);
		LogFileSimpleInteger(count);
		LogFileSimpleString("<font color=\"blue\">kept</font>");
		LogFileEndRow();
	}
	LogFileEndTable();
	status.progress.clear();
	status.details.clear();

	// Now we'll look to see how often these acceptable suffixes are
	// used on real stems
	LogFileSmallTitle("How acceptable are these suffixes?");
	LogFileHeader("Stems", "Direction", "Word");
	status.details = "Cutting words using stems and proposed affixes";
	status.progress.clear();

	status.progress.set_denominator(m_pStems->GetCount());
	for (int stemno = 0; stemno < (int)m_pStems->GetCount(); stemno++) {
		status.progress = stemno;
		pStem = m_pStems->GetAtSort(stemno);
		const QString stem = pStem->Display();
		if ((int) stem.length() < MinimumStemLength)
			continue;

		// consider each of these new suffixes on each of the old stems:
		int colno = 1;
		const int numberofcolumns = 8;
		for (std::map<QString, int>::const_iterator i = proposed_affixes.begin();
		     i != proposed_affixes.end(); ++i) {
			const QString affix = i->first;
			const QString word = analyzingSuffixes ?
					(stem + affix) :
					(affix + stem);
			pWord = *GetWords() ^= word;
			if (!pWord) continue;

			if (analyzingSuffixes && pWord->GetSuffixLoc() > 0)
				// already has suffix.
				continue;
			if (!analyzingSuffixes && pWord->GetPrefixLoc() > 0)
				// already has prefix.
				continue;
			if (pWord->GetStem2Loc() > 0) 	// compound.
				continue;

			pWord->CutRightBeforeHere(analyzingSuffixes ?
					stem.length() : affix.length());
			pWord->AppendToConfidence("somewhat uncertain.");
			pWord->SetStemLoc(analyzingSuffixes ? 1 : 2);
			if (analyzingSuffixes)
				pWord->SetSuffixLoc(2);
			else
				pWord->SetPrefixLoc(1);
			m_pLexicon->UpdateWord(pWord);

			if (colno == numberofcolumns) {
				LogFileEndRow();
				colno = 1;
			}
			if (colno == 1)
				LogFileStartRow();
			LogFileSimpleString(analyzingSuffixes ?
					(stem + " + " + affix) :
					(affix + " + " + stem));
			colno++;
		}
	} // end of stem loop
	LogFileEndTable();
	status.progress.clear();
	status.details.clear();

	if (analyzingSuffixes) {
		QString Remark("From stems find suffixes");
		CStringSurrogate ssRemark(Remark);

		RebuildAffixesStemsAndSignaturesFromWordSplits(ssRemark);
		// XXX. not an operation.
		status.major_operation = "Mini-Lexicon " + QString("%1").arg( m_Index+1 ) + ": End -- from stems, find suffixes";
	} else {	// Prefixes!

		QString Remark("From stems find prefixes");
		CStringSurrogate ssRemark(Remark);
		RebuildAffixesStemsAndSignaturesFromWordSplits(ssRemark);
		status.major_operation = "Mini-Lexicon " + QString("%1").arg( m_Index+1 ) + ": End -- from stems, find prefixes";
	}

	QString mini_name("Mini-Lexicon %1");
	mini_name = mini_name.arg(GetIndex() + 1);
	QString remark = "From stems: find affixes";
	GetDLHistory()->append(mini_name, remark, this);
}

void CMiniLexicon::ExtendKnownStemsToKnownAffixes()
{
	CLexicon& lex = *m_pLexicon;
	linguistica::ui::status_user_agent& status = lex.status_display();

        int colno = 1, numberofcolumns = 8;
	// We will be iterating over stems and words, so sort them.
	m_pStems->Sort(KEY);
	m_pWords->Sort(KEY);

	// Give the user some text to stare at
	status.progress.clear();
	status.major_operation =
		QString("Mini-Lexicon %1: Extend known stems and suffixes")
		.arg(m_Index + 1);

	const bool analyzingSuffixes = !is_initial(m_AffixLocation);
        LogFileLargeTitle("Phase: Extending analysis \n based on known stems and known affixes.");
        LogFileStartTable();


	// For each stem:
	const int TopCount = m_pStems->GetCount();
	status.details = "Going through the stems ";
	status.progress.set_denominator(TopCount);
	for (int t = 0; t < TopCount; ++t) {
		CStem& stem = *m_pStems->GetAtSort(t);

		status.progress = t;

		CStringSurrogate affix_text = analyzingSuffixes ?
				stem.GetSuffixList()->GetPiece(1) :
				stem.GetPrefixList()->GetPiece(1);
		const int stem_length = stem.GetKeyLength();
		const CStringSurrogate stem_text = stem.GetKey();

		// Write "" instead of "NULL".
		affix_text.ConvertNULL();

		const CParse word = analyzingSuffixes ?
				stem_text + affix_text :
				affix_text + stem_text;

		int SortIndex = 0;
		CStem* found_word = *m_pWords ^= word;
		if (found_word != 0)
			SortIndex = found_word->GetSortIndex();

		// First word starting with (resp ending with) stem text
		int TopIndex = SortIndex;
		if (analyzingSuffixes) {
			while (TopIndex >= 1 &&
					m_pWords->GetAtSort(TopIndex - 1)
					->GetKey().Left(stem_length) == stem_text)
				--TopIndex;
		} else {
			while (TopIndex >= 1 &&
					m_pWords->GetAtSort(TopIndex - 1)
					->GetKey().Right(stem_length) == stem_text)
				--TopIndex;
		}

		// Last word starting with (resp ending with) stem text
		int BottomIndex = SortIndex;
		if (analyzingSuffixes) {
			while (BottomIndex+1 < m_pWords->GetCount() &&
					m_pWords->GetAtSort(BottomIndex+1)
					->GetKey().Left(stem_length) == stem_text)
				++BottomIndex;
		} else {
			while (BottomIndex+1 < m_pWords->GetCount() &&
					m_pWords->GetAtSort(BottomIndex+1)
					->GetKey().Right(stem_length) == stem_text)
				++BottomIndex;
		}

		// For each word starting with (resp ending with) stem text:
		for (int w = TopIndex; w <= BottomIndex; ++w) {
			CStem& word2 = *m_pWords->GetAtSort(w);

			// Ignore already analyzed words
			const enum CStem::type word2_type = word2.GetWordType();
			const int affix_loc = analyzingSuffixes ?
					word2.GetSuffixLoc() :
					word2.GetPrefixLoc();
			if (affix_loc != 0 ||
			    word2_type == CStem::BIWORD_COMPOUND ||
			    word2_type == CStem::MULTIPLE_COMPOUND ||
			    word2_type == CStem::POSSIBLE_COMPOUND)
				continue;

			// Investigate possible affix
			const CStringSurrogate word2_text = word2.GetKey();
			const int affixlen = word2_text.GetLength() - stem_length;
			const CStringSurrogate PossibleAffix =
					analyzingSuffixes ?
					word2_text.Right(affixlen) :
					word2_text.Left(affixlen);
			CAffix* found_affix;
                        analyzingSuffixes ?
                                found_affix = *m_pSuffixes ^= PossibleAffix:
                                found_affix = *m_pPrefixes ^= PossibleAffix;
                        if (found_affix == 0)
				continue;

			// Affix present.  Rewrite word2 as stem + affix.
			// (Do not update counts yet.)
			if (analyzingSuffixes) {
				word2.CutRightBeforeHere(stem_length);
				word2.SetStemLoc(1);
				word2.SetSuffixLoc(2);
				stem.AddSuffix(static_cast<CSuffix*>(found_affix));
				found_affix->AddStem(&stem);
			} else {
				word2.CutRightBeforeHere(affixlen);
				word2.SetStemLoc(2);
				word2.SetPrefixLoc(1);
				stem.AddPrefix(static_cast<CPrefix*>(found_affix));
				found_affix->AddStem(&stem);
			}
                        if (colno==numberofcolumns){LogFileEndRow(); colno=1;}
                        if (colno ==1){LogFileStartRow();}
                        if (analyzingSuffixes)  { LogFileSimpleString(stem.Display() + " + " +  found_affix->Display());}
                         else                    { LogFileSimpleString(found_affix->Display() + " + " + stem.Display());}
                         colno++;
			m_pLexicon->UpdateWord(&word2);
		}
	}
	status.progress.clear();
	status.details.clear();
        LogFileEndTable();

	// Redo Signatures.
	QString Remark("From known stems and suffixes");
	CStringSurrogate ssRemark(Remark);

        RebuildAffixesStemsAndSignaturesFromWordSplits(ssRemark);

	// XXX. not an operation
	status.major_operation = QString("Mini-Lexicon %1: "
			"End of using known stems and %2")
			.arg(m_Index + 1)
			.arg(analyzingSuffixes ? "suffixes" : "prefixes");

	GetDLHistory()->append(
		QString("Mini-Lexicon %1").arg(m_Index + 1),
		QString("Known stems and affixes"),
		this);
}

/*
 * At the moment, this function decides too early whether to
 * allow a stem to be used. It ought to build up the TempSignatures,
 * and after all stems have been tried, decide which way to split
 * a stem is the best.
 */

/* Take a word W.
 *    Consider all splits for W such that its suffix is already in Suffixes.
 *      Put the stem in TempStems, and attach the suffix to the stem.
 *      Look at all words that start like that stem;
 *           Attach the suffixes found in those words to that stem.
 *      Done?
 *      Now, if there are more than 2 suffixes on that stem now,
 *           put the stem's signature in a TempSignature collection.
 *      Find cost = cost (Sig).
 *      If cost is negative, go ahead and make all those splits.
 *  End of loop.
 */
void CMiniLexicon::LooseFit()
{
	// Cost of a sig =
	//    Sum over all of its stems :
	//	  log ( CorpusSize / Stem-count )      ( cost    )
	//	  length ( stem ) * cost of a letter   ( savings )
	//
	//    Sum over all of its suffixes:
	//	  log ( CorpusSize / suffix-count )    ( cost    )
	//	  length ( suffix ) * cost of a letter ( savings )
	using std::auto_ptr;
	using std::vector;

	CLexicon& lex = *m_pLexicon;
	linguistica::ui::status_user_agent& status = lex.status_display();
	const bool analyzingSuffixes = !is_initial(m_AffixLocation);
	const int MinimumAffixLength = m_pLexicon->GetIntParameter(
			analyzingSuffixes ?
			"Main\\MinimumSuffixLength" :
			"Main\\MinimumPrefixLength", 2);
	const int MaximumAffixLength = m_pLexicon->GetIntParameter(
			analyzingSuffixes ?
			"Main\\MaximumSuffixLength" :
			"Main\\MaximumPrefixLength", 7);
	int MinimumStemLength = 5;
	if (MinimumStemLength < 1)
		// do not allow empty stems
		// XXX. notify user that value was ignored
		MinimumStemLength = 1;

        LogFileLargeTitle("Phase: Loose fit of known signatures");
        LogFileStartTable();

        auto_ptr<CSignatureCollection> proposed_sigs(analyzingSuffixes ?
                new CSignatureCollection(this, m_pSuffixes, m_AffixLocation) :
                new CSignatureCollection(this, m_pPrefixes, m_AffixLocation));
        auto_ptr<CSignatureCollection> existing_sigs(analyzingSuffixes ?
                new CSignatureCollection(this, m_pSuffixes, m_AffixLocation) :
                new CSignatureCollection(this, m_pPrefixes, m_AffixLocation));
        auto_ptr<CSignatureCollection> novel_sigs(analyzingSuffixes ?
                new CSignatureCollection(this, m_pSuffixes, m_AffixLocation) :
                new CSignatureCollection(this, m_pPrefixes, m_AffixLocation));

        QList<CSignature*> ProposedSignatures;
        QList<CSignature*> ExistingSignatures;
        QList<CSignature*> NovelSignatures;

	if (m_pWords->GetCount() == 0)
		// Need to read corpus first.
		return;

	// We will  be iterating over words, so sort them.
	m_pWords->Sort(KEY);

	// Give the user something to look at.
	status.progress.clear();
	status.major_operation =
		QString("Mini-Lexicon %1: Loose fit.").arg(m_Index+1);

	// For each word: look for known prefixes/suffixes and consider them.
	// Fill proposed_sigs with candidates.
	CStemCollection potential_stems;
	const int num_words = m_pWords->GetCount();
	status.progress.set_denominator(num_words);
        for (int wordno = 0; wordno < num_words; ++wordno) {
                CStem& word = *m_pWords->GetAtSort(wordno);
		status.progress = wordno;

		if (word.Size() > 1)
			// already analyzed
			continue;

		if (word.GetKeyLength() < 2)
			// too small to analyze
			continue;

		CStringSurrogate word_text = word.GetKey();

		// For each textual prefix/suffix of word: if known, consider.
		for (int i = MaximumAffixLength; i >= MinimumAffixLength; --i) {
			Q_ASSERT(MinimumStemLength >= 1);
			if (word_text.GetLength() - i < MinimumStemLength)
				// corresponding stem is too short
				continue;

			CStringSurrogate affix_text = analyzingSuffixes ?
				word_text.Right(i) : word_text.Left(i);
			CAffix* affix = analyzingSuffixes ?
				implicit_cast<CAffix*>(*m_pSuffixes ^= affix_text) :
				implicit_cast<CAffix*>(*m_pPrefixes ^= affix_text);

			if (affix == 0)
				// suffix not known
				continue;

			Q_ASSERT(word_text.GetLength() > i);
			CStringSurrogate stem_text = analyzingSuffixes ?
				word_text.Left(word_text.GetLength()-i) :
				word_text.Right(word_text.GetLength()-i);

			if (*m_pStems ^= stem_text)
				// stem already known
				continue;

			if (potential_stems ^= stem_text)
				// stem already under consideration
				continue;

			CStem* stem = potential_stems << stem_text;

			// build signature from remaining words
			CParse empirical_sig;
			if (analyzingSuffixes) {
				m_pWords->CreateSuffixSignatures(
						&stem_text, &empirical_sig);
			} else {
				if (!m_pWords->GetReverseTrie())
					m_pWords->CreateReverseTrie();

				// build list of reversed prefixes
				CParse backwards_sig;
				stem_text.SetBackwards();
				m_pWords->GetReverseTrie()->CreateSuffixSignatures(
						&stem_text, &backwards_sig);
				stem_text.SetBackwards(false);

				// reverse them to get actual prefixes
				for (int n = 1; n <= backwards_sig.Size(); ++n) {
					CStringSurrogate prefix =
						backwards_sig.GetPiece(n);

					prefix.SetBackwards();
					empirical_sig.Append(prefix);
					prefix.SetBackwards(false);
				}
			}

			// for each prefix/suffix that appears with stem:
			//  throw it out if it's obviously bad, but
			//  otherwise record it in *stem.
			for (int n = 1; n <= empirical_sig.Size(); ++n) {
				CStringSurrogate affix2 = empirical_sig.GetPiece(n);

				// word this affix was taken from
				CParse word2_text = analyzingSuffixes ?
						stem_text + affix2 :
						affix2 + stem_text;
				CStem* word2 = *m_pWords ^= word2_text;
				Q_ASSERT(word2 != 0);

				if (word2->Size() > 1)
					// previously analyzed
					continue;

				// If affix is of reasonable size or is
				// known from elsewhere, add it to stem's list.
				const int affix2_len = affix2.GetLength();
				const bool reasonable_size =
					affix2_len >= MinimumAffixLength &&
					affix2_len <= MaximumAffixLength;

				if (reasonable_size || (analyzingSuffixes ?
						(*m_pSuffixes ^= affix2) != 0:
						(*m_pPrefixes ^= affix2) != 0)) {
					if (analyzingSuffixes)
						stem->AddSuffix(affix2);
					else
						stem->AddPrefix(affix2);
				}
			}
			// special case: stem appears with null affix
			if (CStem* word2 = *m_pWords ^= stem_text) {
				if (word2->Size() > 1) {
					// previously analyzed
				} else {
					if (analyzingSuffixes)
						stem->AddNULLSuffix();
					else
						stem->AddNULLPrefix();
				}
			}

			// fetch recorded affixes
			CParse& affixes = analyzingSuffixes ?
				*stem->GetSuffixList() :
				*stem->GetPrefixList();

			// At least the affix that prompted this search
			// should be among them.
			Q_ASSERT(affixes.Contains(affix_text));
			Q_ASSERT(affixes.Size() > 0);

			// build new signature
			CSignature* new_sig = *proposed_sigs << &affixes;

			if (analyzingSuffixes)
				new_sig->AttachToSuffixSig(stem, false);
			else
				new_sig->AttachToPrefixSig(stem, false);
			new_sig->SetAffixLocation(m_AffixLocation);
			LogFile(stem_text.Display(), new_sig->Display());
		} // for each textual prefix/suffix
        } // end of wordno loop
	LogFileEndTable();
	status.progress.clear();

	// Now we have, or might have, a set of candidate splittings,
	// so let's compare them.
	// If any of the corresponding signatures are already real,
	// then we take the stem corresponding to the sig with the
	// highest value for Robustness.
	// Otherwise we use the Cost function for signatures.
        LogFileSmallTitle("Proposed signatures", "Signature", "status");


	// First find the set of existing signatures (won't be many).

        for (int signo = 0; signo < ProposedSignatures.count(); signo++)
        {
                CSignature* sig = ProposedSignatures.at(signo);

                if (sig->Size() < 2 && sig->GetKeyLength() < 4) {// total length < 4!  Way too small.
                    LogFile("too small: ",  sig->Display() );
			continue;
		}

		// known signature?
                if (*GetSignatures() ^= sig) ExistingSignatures.append(sig);
                else NovelSignatures.append(sig);
	}
        LogFileEndTable();

	{
		// Sort existing signatures by robustness
		const int size = existing_sigs->GetCount();

                LogFileStartTable(); LogFileHeader("Pre-existing signature", "Robustnes" );


		// calculate each signature’s robustness
		vector<double> Robustness;
		Robustness.reserve(size);
                for (int signo = 0; signo < size; ++signo) {
                        CSignature* sig = (*existing_sigs)[signo];

			Robustness.push_back(sig->GetRobustness());
                        LogFile(sig->Display(), Robustness[signo]);

		}
                 LogFileHeader("Signature", "Stem");

		// For each existing signature (sorted by robustness):
		// analyze (cut) unanalyzed words according to this signature.
		vector<int> shuffle(size);
		SortVector(&shuffle[0], &Robustness[0], size);
		for (int j = 0; j < size; ++j) {
			CSignature* sig = (*existing_sigs)[shuffle[j]];
                        LogFile(sig->Display());


			// Analyze (cut) unanalyzed words according to signature.
                        sig->CutMyWordsAsIDeclare();

		}
                LogFileEndTable();
	}

	{
		// Consider novel signatures.
		const int size = novel_sigs->GetCount();
                LogFileSmallTitle ("Novel signatures: "); LogFileStartTable();


		// for each signature: calculate cost function
		vector<double> Cost;
		Cost.reserve(size);
		for (int i = 0; i < size; ++i)
			Cost.push_back((*novel_sigs)[i]->FindCost(this));

		// for each novel signature, in least-cost-first order:
		// Analyze (cut) unanalyzed words according to this signature.
		// Stop when the cost becomes positive.
		vector<int> shuffle(size);
		SortVector(&shuffle[0], &Cost[0], size);

                for (int signo = size-1; signo >= 0 && Cost[shuffle[signo]] < 0; --signo) {
                        CSignature* sig = (*novel_sigs)[shuffle[signo]];
                        LogFile(sig->Display(), Cost[shuffle[signo]]);
                       // Analyze (cut) unanalyzed words according to signature.
                       sig->CutMyWordsAsIDeclare();

                } // end of signo loop
		// XXX. report cost of first rejected signature to log
	}
        // what had followed has been eliminated, and replaced by:
	CStringSurrogate remark(QString("Loose fit"));
	RebuildAffixesStemsAndSignaturesFromWordSplits (remark);

	// XXX. not an operation
	status.major_operation = QString("Mini-Lexicon %1: End of loose fit.")
			.arg(m_Index+1);
	GetDLHistory()->append(
			QString("Mini-Lexicon %1").arg(m_Index+1),
			QString("Loose Fit"),
			this);
}

int CMiniLexicon::GetNumberOfStems()
{
	return m_pStems->GetCount();
}
