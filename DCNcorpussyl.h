// Driver for syllable discovery using Dynamic Computational Networks
// Copyright © 2009 The University of Chicago
#ifndef CORPUSSYL_H
#define CORPUSSYL_H

// See http://linguistica.uchicago.edu/dcnsyl.htm for history and
// documentation.
// Also see the learningsyl class in DCNlearningsyl.h, which does most
// of the work.

#include <qstring.h>
#include <q3valuevector.h>
#include <qfile.h>
#include <q3textstream.h>
#include <qlabel.h>
#include "DCNdcnword.h"
#include "DCNnetworksyl.h"
#include "DCNgrammarsyl.h"

class corpussyl  
{
public:
	corpussyl();
	virtual ~corpussyl();

	void		readInput(QFile &file);
	void		printCorpus(QLabel *label, grammarsyl *gram);
	QString		printCorpus(grammarsyl *gram);

	uint		numberOfWords() const	{ return m_numOfWords; }
	dcnword		wordAt(uint index) const;

private:
	uint					m_numOfWords;
	Q3ValueVector<dcnword>	m_wordVector;
};

#endif
