// Implementation of CStem methods
// Copyright © 2009 The University of Chicago
#include "Stem.h"

// See also Stem_Phonology.cpp for phonology methods,
// Stem_EncodingLength.cpp for description length calculations,
// GUIclasses.cpp for methods pertaining to GUI output,
// and Word.cpp for methods concerning stems-qua-words.

#include <Q3TextStream>
#include <QList>
#include "EarleyParser.h"
#include "Signature.h"
#include "Prefix.h"
#include "Suffix.h"

CLexicon* CStem::m_Lexicon;  // assign value !  :TODO

// construction/destruction.

CStem::CStem(CMiniLexicon* mini)
	: CLParse(mini),
	m_WordCount(0),
	m_BrokenForm(),
	m_SuffixList(),	// initialized below
        m_pSuffixSignature(NULL), m_pPrefixSignature(NULL),
	m_PrefixList(),	// initialized below
	m_Regular(0),
	m_SimpleFlag(false),
	m_StemType(UNKNOWN),
	m_StemLoc(0),
	m_Stem2Loc(0),
	m_NumberOfStems(0),
	m_PrefixLoc(0), m_SuffixLoc(0),
	m_Confidence(QString()),
        m_pStem(NULL),
	m_strStem(),
	m_strSuffix(), m_strPrefix(),
        m_pSuffix(NULL), m_pPrefix(NULL),
	m_LengthOfPointerToMe(0.0),
	m_WordPtrList(new QList<CStem*>),
	m_LeftNeighbors(),
	m_RightNeighbors(),
	// compounding.
	m_MyEarleyParser(),
	m_CompoundCount(0.0),
	m_Affixness(0.0),
	// phonology.
	m_Phonology_Tier1(),
	m_Phonology_Tier2(),
	m_Phonology_Tier1_Skeleton(),
	m_UnigramLogProb(0.0),
	m_BigramLogProb(0.0),
	m_BigramComplexity(0.0),	// average
	m_UnigramComplexity(0.0),	// average
	m_PhonologicalContent(0.0),
	m_HMM_LogProbability(0.0),
	// first Boltzmann model.
	m_Tier2_LocalMI_Score(0.0),
	m_LocalMI_TotalBoltzmannScore(0.0),
	m_LocalMI_Plog(0.0),
	// second Boltzmann model.
	m_Tier2_DistantMI_Score(0.0),
	m_DistantMI_TotalBoltzmannScore(0.0),
	m_DistantMI_Plog(0.0),
	// tier-1 phonology info for graphical display.
	m_phonologies(), m_unigrams(), m_mis(),
	m_countofunigrams(0),
	m_countofmis(0),
	m_maxpositive(0.0),
	m_maxnegative(0.0),
	m_donephonology(false)
{
	m_SuffixList.Alphabetize();
	m_PrefixList.Alphabetize();
}

CStem::CStem(const CStringSurrogate& stem, CMiniLexicon* mini)
	: CLParse(stem, mini),
        m_WordCount         (0),
        m_BrokenForm        (),
        m_SuffixList        (),	// initialized below
        m_pSuffixSignature  (NULL),
        m_pPrefixSignature  (NULL),
        m_PrefixList        (),	// initialized below
        m_Regular           (0),
        m_SimpleFlag        (false),
        m_StemType          (UNKNOWN),
        m_StemLoc(          0),
        m_Stem2Loc          (0),
        m_NumberOfStems     (0),
        m_PrefixLoc         (0),
        m_SuffixLoc         (0),
        m_Confidence        (QString()),
        m_pStem             (NULL),
        m_strStem           (),
        m_strSuffix         (),
        m_strPrefix         (),
        m_pSuffix           (NULL),
        m_pPrefix           (NULL),
	m_LengthOfPointerToMe(0.0),
	m_WordPtrList(new QList<CStem*>),
        m_LeftNeighbors     (),
        m_RightNeighbors    (),
	// compounding.
        m_MyEarleyParser    (),
        m_CompoundCount     (0.0),
        m_Affixness (0.0),
	// phonology.
        m_Phonology_Tier1   (),
        m_Phonology_Tier2   (),
	m_Phonology_Tier1_Skeleton(),
        m_UnigramLogProb    (0.0),
        m_BigramLogProb     (0.0),
        m_BigramComplexity  (0.0),	// average
	m_UnigramComplexity(0.0),	// average
	m_PhonologicalContent(0.0),
	m_HMM_LogProbability(0.0),
	// first Boltzmann model.
	m_Tier2_LocalMI_Score(0.0),
	m_LocalMI_TotalBoltzmannScore(0.0),
	m_LocalMI_Plog(0.0),
	// second Boltzmann model.
	m_Tier2_DistantMI_Score(0.0),
	m_DistantMI_TotalBoltzmannScore(0.0),
	m_DistantMI_Plog(0.0),
	// tier-1 phonology info for graphical display.
	m_phonologies(), m_unigrams(), m_mis(),
	m_countofunigrams(0),
	m_countofmis(0),
	m_maxpositive(0.0),
	m_maxnegative(0.0),
	m_donephonology(false)
{
	m_SuffixList.Alphabetize();
	m_PrefixList.Alphabetize();
}

CStem::CStem(const CStem& x)
	: CLParse(x),
	m_WordCount(x.m_WordCount),
	m_BrokenForm(),	// XXX. copy?
	m_SuffixList(),	// initialized below. XXX. copy?
        m_pSuffixSignature      (x.m_pSuffixSignature),
        m_pPrefixSignature      (x.m_pPrefixSignature),
        m_PrefixList            (),	// initialized below. XXX. copy?
        m_Regular               (x.m_Regular),
        m_SimpleFlag            (x.m_SimpleFlag),
        m_StemType              (x.m_StemType),
        m_StemLoc               (x.m_StemLoc),
        m_Stem2Loc              (x.m_Stem2Loc),
        m_NumberOfStems         (x.m_NumberOfStems),
        m_PrefixLoc             (x.m_PrefixLoc),
        m_SuffixLoc             (x.m_SuffixLoc),
        m_Confidence            (x.m_Confidence),
        m_pStem                 (x.m_pStem),
        m_strStem               (x.m_strStem),
        m_strSuffix(),
        m_strPrefix(),	// XXX. copy?
	m_pSuffix(x.m_pSuffix),
	m_pPrefix(x.m_pPrefix),
	m_LengthOfPointerToMe(x.m_LengthOfPointerToMe),
	m_WordPtrList(new QList<CStem*>(*x.m_WordPtrList)),
	m_LeftNeighbors(), m_RightNeighbors(),	// XXX. copy?
	// compounding.
	m_MyEarleyParser(),	// XXX. copy?
	m_CompoundCount(x.m_CompoundCount),
	m_Affixness(x.m_Affixness),
	// phonology.
	m_Phonology_Tier1(x.m_Phonology_Tier1),
	m_Phonology_Tier2(x.m_Phonology_Tier2),
	m_Phonology_Tier1_Skeleton(x.m_Phonology_Tier1_Skeleton),
	m_UnigramLogProb(x.m_UnigramLogProb),
	m_BigramLogProb(x.m_BigramLogProb),
	m_BigramComplexity(x.m_BigramComplexity),
	m_UnigramComplexity(x.m_UnigramComplexity),
	m_PhonologicalContent(x.m_PhonologicalContent),
	m_HMM_LogProbability(0.0),	// XXX. copy?
	// first Boltzmann model.
	m_Tier2_LocalMI_Score(x.m_Tier2_LocalMI_Score),
	m_LocalMI_TotalBoltzmannScore(0.0),	// XXX. copy?
	m_LocalMI_Plog(0.0),	// XXX. copy?
	// second Boltzmann model.
	m_Tier2_DistantMI_Score(x.m_Tier2_DistantMI_Score),
	m_DistantMI_TotalBoltzmannScore(0.0),	// XXX. copy?
	m_DistantMI_Plog(0.0),	// XXX. copy?
	// tier-1 phonology info for graphical display.
	m_phonologies(), m_unigrams(), m_mis(),	// XXX. copy?
	m_countofunigrams(0),	// XXX. copy?
	m_countofmis(0),	// XXX. copy?
	m_maxpositive(0.0),	// XXX. copy?
	m_maxnegative(0.0),	// XXX. copy?
	m_donephonology(x.m_donephonology)
{
	m_SuffixList.Alphabetize();
	m_PrefixList.Alphabetize();
}

CStem::CStem(const CLParse& text_in_corpus)
	: CLParse(text_in_corpus),
	m_WordCount(0),
	m_BrokenForm(),
	m_SuffixList(),	// initialized below
        m_pSuffixSignature(NULL), m_pPrefixSignature(NULL),
	m_PrefixList(),	// initialized below
	m_Regular(0),
	m_SimpleFlag(false),
	m_StemType(UNKNOWN),
	m_StemLoc(0),
	m_Stem2Loc(0),
	m_NumberOfStems(0),
	m_PrefixLoc(0), m_SuffixLoc(0),
	m_Confidence(QString()),
        m_pStem(NULL),
	m_strStem(),
	m_strSuffix(), m_strPrefix(),
        m_pSuffix(NULL), m_pPrefix(NULL),
	m_LengthOfPointerToMe(0.0),
	m_WordPtrList(new QList<CStem*>),
	m_LeftNeighbors(),
	m_RightNeighbors(),
	// compounding.
	m_MyEarleyParser(),
	m_CompoundCount(0.0),
	m_Affixness(0.0),
	// phonology.
	m_Phonology_Tier1(),
	m_Phonology_Tier2(),
	m_Phonology_Tier1_Skeleton(),
	m_UnigramLogProb(0.0),
	m_BigramLogProb(0.0),
	m_BigramComplexity(0.0),	// average
	m_UnigramComplexity(0.0),	// average
	m_PhonologicalContent(0.0),
	m_HMM_LogProbability(0.0),
	// first Boltzmann model.
	m_Tier2_LocalMI_Score(0.0),
	m_LocalMI_TotalBoltzmannScore(0.0),
	m_LocalMI_Plog(0.0),
	// second Boltzmann model.
	m_Tier2_DistantMI_Score(0.0),
	m_DistantMI_TotalBoltzmannScore(0.0),
	m_DistantMI_Plog(0.0),
	// tier-1 phonology info for graphical display.
	m_phonologies(), m_unigrams(), m_mis(),
	m_countofunigrams(0),
	m_countofmis(0),
	m_maxpositive(0.0),
	m_maxnegative(0.0),
	m_donephonology(false)
{
	m_SuffixList.Alphabetize();
	m_PrefixList.Alphabetize();
}

CStem::CStem(const CParse& text, CMiniLexicon* lex)
	: CLParse(text, lex),
	m_WordCount(0),
	m_BrokenForm(),
	m_SuffixList(),	// initialized below
        m_pSuffixSignature(NULL), m_pPrefixSignature(NULL),
	m_PrefixList(),	// initialized below
	m_Regular(0),
	m_SimpleFlag(false),
	m_StemType(UNKNOWN),
	m_StemLoc(0),
	m_Stem2Loc(0),
	m_NumberOfStems(0),
	m_PrefixLoc(0), m_SuffixLoc(0),
	m_Confidence(QString()),
        m_pStem(NULL),
	m_strStem(),
	m_strSuffix(), m_strPrefix(),
        m_pSuffix(NULL), m_pPrefix(NULL),
	m_LengthOfPointerToMe(0.0),
	m_WordPtrList(new QList<CStem*>),
	m_LeftNeighbors(),
	m_RightNeighbors(),
	// compounding.
	m_MyEarleyParser(),
	m_CompoundCount(0.0),
	m_Affixness(0.0),
	// phonology.
	m_Phonology_Tier1(),
	m_Phonology_Tier2(),
	m_Phonology_Tier1_Skeleton(),
	m_UnigramLogProb(0.0),
	m_BigramLogProb(0.0),
	m_BigramComplexity(0.0),	// average
	m_UnigramComplexity(0.0),	// average
	m_PhonologicalContent(0.0),
	m_HMM_LogProbability(0.0),
	// first Boltzmann model.
	m_Tier2_LocalMI_Score(0.0),
	m_LocalMI_TotalBoltzmannScore(0.0),
	m_LocalMI_Plog(0.0),
	// second Boltzmann model.
	m_Tier2_DistantMI_Score(0.0),
	m_DistantMI_TotalBoltzmannScore(0.0),
	m_DistantMI_Plog(0.0),
	// tier-1 phonology info for graphical display.
	m_phonologies(), m_unigrams(), m_mis(),
	m_countofunigrams(0),
	m_countofmis(0),
	m_maxpositive(0.0),
	m_maxnegative(0.0),
	m_donephonology(false)
{
	m_SuffixList.Alphabetize();
	m_PrefixList.Alphabetize();
}

CStem::~CStem()
{
	delete m_BrokenForm;
	delete m_WordPtrList;
	delete m_MyEarleyParser;
}

//-----------------------------------------------------------------
// Overloaded operators
//-----------------------------------------------------------------

void CStem::operator= (const CStem& RHS)
{
  CopyParse(RHS);

  m_BrokenForm					= NULL;
  m_Confidence					= RHS.GetConfidence();
  m_NumberOfStems				= RHS.GetNumberOfStems();
  m_pPrefix					= RHS.GetPrefixPtr();
  m_pPrefixSignature                            = RHS.GetPrefixSignature();
  m_PrefixLoc					= RHS.GetPrefixLoc();
  m_pStem					= RHS.GetStemPtr();
  m_pSuffix					= RHS.GetSuffixPtr();
  m_pSuffixSignature                            = RHS.GetSuffixSignature();
  m_Regular					= RHS.GetRegular();
  m_SimpleFlag					= RHS.GetSimpleFlag();
  m_Stem2Loc					= RHS.GetStem2Loc();
  m_StemLoc					= RHS.GetStemLoc();
  m_StemType					= RHS.GetStemType();
  m_SuffixLoc					= RHS.GetSuffixLoc();
  m_WordCount					= RHS.GetWordCount();
  m_Phonology_Tier1				= RHS.m_Phonology_Tier1;
  m_Phonology_Tier2				= RHS.m_Phonology_Tier2;
  m_Phonology_Tier1_Skeleton                    = RHS.m_Phonology_Tier1_Skeleton;
  m_CompoundCount				= RHS.GetCompoundCount();
  m_Affixness					= RHS.GetAffixness();
  m_MyEarleyParser                              = RHS.GetMyEarleyParser();
//  m_LengthOfPointerToMe			= RHS.GetLengthOfPointerToMe();

//  m_SuffixList.SetAlphabetical();
//  m_PrefixList.SetAlphabetical();

}


void CStem::Copy (CStem& RHS)
{
  CopyParse(RHS);

  m_BrokenForm                                  = NULL;
  m_Confidence                                  = RHS.GetConfidence();
  m_NumberOfStems                               = RHS.GetNumberOfStems();
  m_pPrefix                                     = RHS.GetPrefixPtr();
  m_pPrefixSignature                            = RHS.GetPrefixSignature();
  m_PrefixLoc                                   = RHS.GetPrefixLoc();
  m_pStem                                       = RHS.GetStemPtr();
  m_pSuffix                                     = RHS.GetSuffixPtr();
  m_pSuffixSignature                            = RHS.GetSuffixSignature();
  m_Regular                                     = RHS.GetRegular();
  m_SimpleFlag                                  = RHS.GetSimpleFlag();
  m_Stem2Loc                                    = RHS.GetStem2Loc();
  m_StemLoc                                     = RHS.GetStemLoc();
  m_StemType                                    = RHS.GetStemType();
  m_SuffixLoc                                   = RHS.GetSuffixLoc();
  m_WordCount                                   = RHS.GetWordCount();
  m_MyEarleyParser                              = RHS.GetMyEarleyParser();
//  m_SuffixList.SetAlphabetical();
//  m_PrefixList.SetAlphabetical();

  m_CompoundCount  = RHS.GetCompoundCount();
  m_Affixness = RHS.GetAffixness();
  	m_LengthOfPointerToMe = RHS.GetLengthOfPointerToMe();
}

 

//-----------------------------------------------------------------
// Other methods
//-----------------------------------------------------------------


// Copy utility for stems
//
// Parameters:
//    RHS - the stem to be copied

void CStem::CopyStemInformation(CStem* RHS)
{
  m_Confidence = RHS->GetConfidence();
  SetCorpusCount(RHS->GetCorpusCount());
  m_NumberOfStems  = RHS->GetNumberOfStems();
  m_pPrefix = RHS->GetPrefixPtr();

  if ( RHS->GetPrefixList() )
  {
    m_PrefixList = RHS->GetPrefixList();
  }

  m_PrefixLoc = RHS->GetPrefixLoc();

  if ( RHS->GetPrefixSignature() )
  {
    m_pPrefixSignature = RHS->GetPrefixSignature();
  }

  m_pStem  = RHS->GetStemPtr();
  m_pSuffix = RHS->GetSuffixPtr();
  m_Regular = RHS->GetRegular();
  m_SimpleFlag = RHS->GetSimpleFlag();
  m_Stem2Loc = RHS->GetStem2Loc();
  m_StemLoc = RHS->GetStemLoc();
  m_StemType = RHS->GetStemType();
  m_SuffixLoc  = RHS->GetSuffixLoc();

  if ( RHS->GetSuffixList() )
  {
    m_SuffixList = RHS->GetSuffixList();
  }

  if ( RHS->GetSuffixSignature() )
  {
    m_pSuffixSignature = RHS->GetSuffixSignature();
  }

  m_WordCount = RHS->GetWordCount();

  CStem* word;
  for (int wordno = 0; wordno < RHS->GetWordPtrList()->size(); wordno++)
  {     word = RHS->GetWordPtrList()->at(wordno);
        m_WordPtrList->append( word );
    }

  m_CompoundCount  = RHS->GetCompoundCount();
  m_Affixness = RHS->GetAffixness();
  	m_LengthOfPointerToMe =  RHS->GetLengthOfPointerToMe();
    m_MyEarleyParser = RHS->GetMyEarleyParser();
}
//-----------------------------------------------------------------------------------//
// Add the prefix 'NULL' to the list of prefixes
void CStem::AddNULLPrefix()
//-----------------------------------------------------------------------------------//
{
  m_WordCount++;
  QString Null = "NULL";
  if ( ! m_PrefixList.ContainsNULL() )
  {
    m_PrefixList.Append (CStringSurrogate(Null.unicode(),0,Null.length()));
  }
}

//-----------------------------------------------------------------------------------//
// Add the suffix 'NULL' to the list of suffixes
void CStem::AddNULLSuffix()
//-----------------------------------------------------------------------------------//
{
  m_WordCount++;
  QString Null = "NULL";
  if ( ! m_SuffixList.ContainsNULL() )
  {
    m_SuffixList.Append (CStringSurrogate(Null.unicode(),0,Null.length()));
  }
}

//-----------------------------------------------------------------------------------//
// Add a word to the word list
bool CStem::AddWord (CStem* pWord)
//-----------------------------------------------------------------------------------//
{
        if ( m_WordPtrList->indexOf(pWord) < 0 )
	{
		m_WordPtrList->append (pWord);
		return TRUE;
	}
	return FALSE;
}


// Add a prefix to the prefix list
//
// Parameters:
//    pPrefix - pointer to the prefix to
//    be added

void CStem::AddPrefix (CPrefix* pPrefix)
{
  if (! ContainsPrefix (pPrefix)) {
//    if(!m_PrefixList.Alphabetical()) m_PrefixList.Alphabetize();
    m_PrefixList.Append (pPrefix->GetKey());
  }
}


// Add a suffix to the suffix list
//
// Parameters:
//    pSuffix - pointer to the suffix to
//    be added

void CStem::AddSuffix(CSuffix* pSuffix)
{
  if ( !ContainsSuffix (pSuffix) ) {
//    if(!m_SuffixList.Alphabetical()) m_SuffixList.Alphabetize();
    m_SuffixList.Append (pSuffix->GetKey());
  }
}

 


// Add a suffix to the suffix list
//
// Parameters:
//    key - surrogate string of the suffix to
//    be added

void CStem::AddSuffix(const CStringSurrogate& key)
{
  if ( !m_SuffixList.Contains (key) ) {
//    if(!m_SuffixList.Alphabetical()) m_SuffixList.Alphabetize();
    m_SuffixList.Append (key);
  }
}


// Copy a list of suffixes into the suffix list
//
// Parameters:
//    pParse - the list of new suffixes

void CStem::CopySuffixList(CParse* pParse)
{
  for (int i = 1; i <= (int)pParse->Size(); i++)
  {
    AddSuffix ( pParse->GetPiece(i) );
  }
}


// Find out if the prefix list contains a specific
// prefix
//
// Parameters:
//    Prefix - the prefix in question
//
// Returns:
//    bool - true if the prefix is in our list

bool  CStem::ContainsPrefix(CPrefix* Prefix) const
{
  if ( m_PrefixList.Contains (Prefix->GetKey()) ) {
    return true;
  } else {
    return false;
  }
}

bool  CStem::ContainsPrefix(const CStringSurrogate& Prefix) const
{
  if ( m_PrefixList.Contains (Prefix) ) {
    return true;
  } else {
    return false;
  }
}

QString	CStem::GetSortingString	()	
{ 
	QString sortString = GetSuffixList()->Display(); return sortString; 
}
// Add a prefix to the prefix list
//
// Parameters:
//    Prefix - prefix surrogate to be added be added

void CStem::AddPrefix(const CStringSurrogate& Prefix)
{
  if ( !ContainsPrefix (Prefix) )
  {
    m_PrefixList.Append (Prefix);
  }
}


// Find out if the suffix list contains a specific
// suffix
//
// Parameters:
//    Suffix - the suffix to look for
//
// Returns:
//    bool - true if the suffix is in the list

bool  CStem::ContainsSuffix(CSuffix* Suffix) const
{
  if (m_SuffixList.Contains (Suffix->GetKey() ) ){
    return true;
  } else {
    return false;
  }
}


// Increment the word count
//
// Parameters:
//    n - amount to increment, default = 1

void CStem::IncrementWordCount (int n )
{
  m_WordCount += n;
  Q_ASSERT (m_WordCount > 0);
  Q_ASSERT (m_WordCount < 1000000);
}


// Get the prefix
//
// Parameters:
//    Prefix - the parse to put the prefix in

void    CStem::GetPrefix ( CParse& Prefix ) const
{
    if (m_strPrefix.GetKeyLength() > 0) {
        Prefix =  m_strPrefix;
        return;
    }
    Prefix = GetPiece( m_PrefixLoc );
}


// Get the suffix
//
// Parameters:
//    Output - the parse to put the suffix in

void    CStem::GetSuffix(CParse& Output ) const
{
  if (m_strSuffix.GetKeyLength() > 0) {
        Output =  m_strSuffix;
        return;
    }
  Output = GetPiece( m_SuffixLoc );
}


// Get the stem
//
// Parameters:
//    Output - the parse to put the stem in

void    CStem::GetStem(CParse& Output) const
{
    if (m_strStem.GetKeyLength() > 0) {
        Output =  m_strStem;
        return;
    }
    Output = GetPiece( m_StemLoc );
}




// Display the type of this stem
//
// Returns:
//    QString - the type of this stem

QString CStem::DisplayStemType() const
{
  switch (m_StemType)
  {
  case NORMAL:
    { return ""; } // return "Normal":
  case BIWORD_COMPOUND:
    { return "2 word compound"; }
  case MULTIPLE_COMPOUND:
    { return "Multiple-word compound"; }
  case POSSIBLE_COMPOUND:
    { return "Possible compound"; };
  case NUMBER:
    { return "Number"; }
  case UNKNOWN:
    { return "??"; }
  case ENDS_IN_HYPHEN:
    { return "Ends in hyphen"; }
  case STEM_COMPOUND:
    { return "Compound"; }
  case STEM_NORMAL:
    { return "Stem"; }
  case STEM_PLUS_SUFFIX:
    { return "Stem & Suffix"; }
  case POLYWORD_PIECE:
    { return "Polyword piece"; }
  default:
    { return "???"; }
  }
}


// Merge the prefix and stem

void CStem::ClearPrefixStemSplit()
{
  if ( m_StemLoc && m_PrefixLoc)
  {
    MergePieces (m_PrefixLoc);
  }

  m_PrefixLoc = 0;
  m_StemLoc = 1;
}



 
// Merge the root and suffix

void CStem::ClearRootSuffixSplit()
{
  if ( m_StemLoc && HasASuffix()  )
  {
    MergePieces (m_StemLoc);
  }
  if ( m_StemLoc == 1)
  {
    m_StemLoc  = 0;
  }
}

// TODO : define this function
// Get the sorting quantity
//
// Returns:
//    float - the sorting quantity

float CStem::GetSortingQuantity() const
{
  Q_ASSERT (0);
  return 0;
}

/// used in allomorphy code.
void CStem::RepairSuffixList(const CMiniLexicon* Lexicon)
{
	struct not_implemented { };
	throw not_implemented();
	static_cast<void>(Lexicon);

//	QString NewSuffix;
//	CSuffix* pNewSuffix;

//	for (int i = 1; i <= m_SuffixList.Size(); ++i) {
		// TODO: Get John's help to fix, I don't understand.
//		CStringSurrogate ssSuffix = m_SuffixList.GetPiece(i);
//		CSuffix* pOldSuffix = *Lexicon->GetSuffixes() ^= ssSuffix;

//		SuffixStringTranslation.GetPiece(ssSuffix.SpellOut(), NewSuffix);
//		pNewSuffix = *Lexicon->GetSuffixes() ^=
//	}
}

// Detach a specific suffix from the list
//
// Parameters:
//    pSuffix - the suffix to detach

void CStem::DetachSuffix(CSuffix* pSuffix)
{
  Q_ASSERT (pSuffix);
  m_SuffixList.Remove ( pSuffix->GetKey() );
  pSuffix->RemoveFromStemPtrList ( this );
  pSuffix->RemoveStemString ( GetKey() );
}



// Detach a specific prefix from the list
//
// Parameters:
//    pPrefix - the prefix to detach

void CStem::DetachPrefix(CPrefix* pPrefix)
{
  Q_ASSERT (pPrefix);
  m_PrefixList.Remove ( pPrefix->GetKey() );
  pPrefix->RemoveFromStemPtrList ( this );
  pPrefix->RemoveStemString ( GetKey() );
}


// Remove a word from the word list
//
// Parameters:
//    pWord - pointer to the word to be removed

void CStem::RemoveWordFromWordPtrList(CStem* pWord)
{

  m_WordPtrList->remove(pWord);

}

// Replace the old suffix signature and return it
//
// Parameters:
//    pNewSig - the new signature
//
// Returns:
//    CSignature* - the old signature

CSignature* CStem::ChangeSuffixSignature(CSignature* pNewSig)
{
	CSignature* pOldSig = m_pSuffixSignature;

	if (pOldSig != 0)
		pOldSig->DetachStem(this, CSignature::eCall_Words);

	m_pSuffixSignature = pNewSig;
	return pOldSig;
}

void CStem::OutputStem(Q3TextStream& outf, int index,
	QMap<QString, QString>* filter)
{
	QString confidence;
        // "# Index | Stem                 | Confidence           | Corpus Count | # of Words  | Affixes |  Words"

	outf << "  ";

	outf.setf(2);
	outf.width(5);
	outf << index + 1;
	outf << "   ";

	outf.width(20);     
	outf << Display();
	outf << "   ";

	outf.width(20);
	confidence = GetConfidence();
	if( confidence == "" ) confidence = "NONE";
	outf << confidence.replace( " ", "_" );
	outf << "   ";

	outf.unsetf(2); 
	outf.width(12);
	outf << GetCorpusCount();
	outf << "   ";

        //outf.width(11);
	if( GetSuffixSignature() )            
	{ 
                outf.width(12);
                outf << m_WordPtrList->size();
		outf << "   ";

		outf.setf(2);
		outf << GetSuffixSignature()->Display(' ', filter);
		outf << " ";
	}
	else if( GetPrefixSignature() )
	{
                outf.width(12);
		outf << GetPrefixSignature()->Size();
		outf << "   ";

		outf.setf(2);
		outf << GetPrefixSignature()->Display(' ', filter);
		outf << " ";
	}
	else
	{
		outf << 0;
		outf << "   ";

		outf.setf(2);
		outf << "NONE";
		outf << " ";
	}

	outf << endl;
}
