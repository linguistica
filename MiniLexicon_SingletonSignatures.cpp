// Fixing false signatures like -t-ion
// Copyright © 2009 The University of Chicago
#include "MiniLexicon.h"

#include "ui/Status.h"
#include "Lexicon.h"
#include "DLHistory.h"
#include "Signature.h"
#include "Suffix.h"
#include "Prefix.h"
#include "Affix.h"
#include "Stem.h"
#include "SignatureCollection.h"
#include "PrefixCollection.h"
#include "SuffixCollection.h"
#include "WordCollection.h"
#include "StemCollection.h"
#include "HTML.h"

void CMiniLexicon::FindSingletonSignatures()
{
	CLexicon& lex = *m_pLexicon;
	linguistica::ui::status_user_agent& status = lex.status_display();

	int MinimumAffixCount = 30;
	int MinimumAffixLength = 2;
	int MinimumStemLength = 3;

	CStem* pStem;
	CParse NewSig;
	CStringSurrogate ssSubAffix;
	CAffix* pSubAffix;
	bool analyzingSuffixes = true;
	if (m_AffixLocation == WORD_INITIAL || m_AffixLocation == STEM_INITIAL)
		analyzingSuffixes = false;
	QString msg = "Mini-Lexicon " + QString("%1").arg( m_Index+1 ) + ": Finding singleton signatures";
	status.major_operation = msg;
	status.progress.clear();

	// loop through suffixes

	// Some of the singleton signatures that were created during SF1
	// can now be checked and seen to be faulty.
	// For example, there is a strong tendency
	// in English to choose -tion in SF1,
	// whereas now we know this is not a smart choice.
	// Maybe this should be a general check of suffixes, though.
	//
	// XXX. This strategy works poorly for words that end in -ies,
	// since es gets chosen over -ies.

	if (analyzingSuffixes) {
		m_pSuffixes->Sort(LENGTH);
		LogFileLargeTitle("Phase: Finding singleton suffixes");

		LogFileStartTable();
		status.progress.set_denominator(m_pSuffixes->GetCount());
		for (int suffixno = 0; suffixno < (int)m_pSuffixes->GetCount(); suffixno++) {
			CSuffix* pSuffix = m_pSuffixes->GetAtSort(suffixno);
			int SuffixLength = pSuffix->GetKeyLength();
			int ThisUseCount = pSuffix->GetUseCount();

			status.progress = suffixno;

			if (ThisUseCount < MinimumAffixCount)
				continue;
			if (SuffixLength < MinimumAffixLength)
				continue;

			CStringSurrogate ssSuffix = pSuffix->GetKey();

			// if there's a shorter subsuffix of length 2 or more
			// which has a higher count,
			// then don't use this suffix; use that one.
			bool bIgnoreThisSuffixFlag = false;
			for (int length = SuffixLength - 1; length >= 2; length--) {
				ssSubAffix = ssSuffix.Right(length);
				pSubAffix = *m_pSuffixes ^= ssSubAffix;
				if (pSubAffix && pSubAffix->GetUseCount() > ThisUseCount)
					bIgnoreThisSuffixFlag = true;
			}
			if (bIgnoreThisSuffixFlag)
				continue;

			LogFile(ssSuffix.Display());
			int colno = 1;
			const int numberofcolumns = 8;
			for (int wordno = 0; wordno < (int)m_pWords->GetCount(); wordno++) {
				CStem* pWord = m_pWords->GetAt(wordno);

				if (pWord->Size() > 1)
					continue;
				if (pWord->GetKeyLength() < SuffixLength + MinimumStemLength)
					continue;

				if (pWord->GetKey().Right(SuffixLength) == ssSuffix) {
					if (colno == numberofcolumns) {
						LogFileEndRow();
						colno = 1;
					}
					if (colno == 1)
						LogFileStartRow();
					LogFileSimpleString(pWord->Display());
					colno++;

					pWord->CutNFromTheRight(SuffixLength);
					pStem = *m_pStems << pWord->GetPiece(1);

					pWord->SetStemLoc(1);
					pWord->SetSuffixLoc(2);
				}
			}
		}
		status.progress.clear();
		status.major_operation.clear();
		LogFileEndTable();
	} else {
		// XXX. combine cases, get log file output for free

		m_pPrefixes->Sort(LENGTH);
		status.progress.set_denominator(m_pPrefixes->GetCount());
		for (int i = 0; i < (int)m_pPrefixes->GetCount(); i++) {
			CPrefix* pPrefix = m_pPrefixes->GetAtSort(i);
			int PrefixLength = pPrefix->GetKeyLength();
			int ThisUseCount = pPrefix->GetUseCount();

			status.progress = i;

			if (ThisUseCount < MinimumAffixCount)
				continue;
			if (PrefixLength < MinimumAffixLength)
				continue;

			CStringSurrogate ssPrefix = pPrefix->GetKey();
			bool bIgnoreThisPrefixFlag = false;
			for (int length = PrefixLength - 1; length >= 2; length--) {
				ssSubAffix = ssPrefix.Right(length);
				pSubAffix = *m_pPrefixes ^= ssSubAffix;
				if (pSubAffix && pSubAffix->GetUseCount() > ThisUseCount) {
					bIgnoreThisPrefixFlag = true;
					LogFile(QString("Ignore "), ssPrefix.Display(), ssSubAffix.Display());
				}
			}

			if (bIgnoreThisPrefixFlag)
				continue;

			for (int w = 0; w < (int)m_pWords->GetCount(); w++) {
				CStem* pWord = m_pWords->GetAt(w);

				if (pWord->Size() > 1)
					continue;
				if (pWord->GetKeyLength() < PrefixLength + MinimumStemLength)
					continue;

				if (pWord->GetKey().Left(PrefixLength) == ssPrefix) {
					if (LogFileOn())
						*GetLogFile() <<
							"\n" <<
							ssPrefix.Display() <<
							"\t" <<
							pWord->Display();

					pWord->CutNFromTheLeft(PrefixLength);
					pStem = *m_pStems << pWord->GetPiece(2);

					pWord->SetStemLoc(2);
					pWord->SetPrefixLoc(1);
				}
			}
		}
		status.progress.clear();
		status.major_operation.clear();
	}
	CStringSurrogate("Singleton signatures");
	QString mini_name("Mini-Lexicon %1");

	mini_name = mini_name.arg(GetIndex() + 1);
	QString remark = "Find singleton signatures";
	GetDLHistory()->append(mini_name, remark, this);
}
