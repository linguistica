// Implementation of CPhoneCollection methods
// Copyright © 2009 The University of Chicago
#include "PhoneCollection.h"

#include <Q3TextStream>
#include <QIODevice>
#include <QFile>
#include "ui/Status.h"
#include "MonteCarlo.h"
#include "Phone.h"
#include "Stem.h"
#include "BiphoneCollection.h"
#include "WordCollection.h"
#include "log2.h"

CPhoneCollection::CPhoneCollection(CWordCollection* words)
	: m_MyWords(words),
	m_MyBiphones(words),
	m_expMI(),
	m_expMIFromBoundary(),
	m_expMIToBoundary() { }

double CPhoneCollection::GetSumOfMyMIs()
	{ return m_MyBiphones.GetSumOfMyMIs(); }

void CPhoneCollection::CountPhonesAndBiphonesInWord( CStem* pStem, eTier WhichTier)
{

	CPhone*			pPhone, *prevPhone; 
	prevPhone = NULL;
	CParse*			pThisString;
	QString			Biphone;
        switch (WhichTier)
        {
        case (TIER_1):
                pThisString = pStem->GetPhonology_Tier1();
                break;
        case (TIER_1_SKELETON):
                pThisString = pStem->GetPhonology_Tier1_Skeleton();
                break;
        default:
                pThisString = pStem->GetPhonology_Tier2();
	}



        if (pThisString->Size()==0) return;
  
        for (int phoneno =1; phoneno <=  pThisString->Size(); phoneno++)
	{
                QString temp2 = pThisString->GetPiece(phoneno).Display();
		
                if ( phoneno  == pThisString->Size() &&		// these conditions define a # at the end of a string like #dog#
			 pThisString->GetPiece(1).Display() == QString("#")					&& 
                         pThisString->GetPiece(phoneno).Display() == QString("#")
			)
			{ 
                                pPhone = *this ^= pThisString->GetPiece(phoneno);
			}
			else
			{
                                pPhone = *this << pThisString->GetPiece(phoneno);
			}		
			if (phoneno > 1)
				m_MyBiphones.Insert(prevPhone, pPhone);
			prevPhone = pPhone;
	} 
}

void CPhoneCollection::Normalize()
{	 
	CPhone*			pPhone;
	CBiphone*		pBiphone;
	double			MI, Freq;

        for (int phoneno = 0; phoneno < GetCount(); phoneno++)
	{
                pPhone = GetAt(phoneno);
		pPhone->m_Frequency = pPhone->GetCorpusCount() / (double) m_CorpusCount;
		pPhone->m_LogFreq = -1 * base2log( pPhone->m_Frequency );
	}
	

	Q3DictIterator<CBiphone> it(m_MyBiphones);
	for ( ; it.current(); ++it)
	{	
 		pBiphone = it.current();
		Freq = (double) pBiphone->GetCorpusCount() / m_MyBiphones.GetTotalCount();
		pBiphone->m_Freq = Freq;
 		pBiphone->m_LogFreq =  -1 * base2log ( Freq );
		pBiphone->m_CondProb = Freq / (double) pBiphone->m_MyFirst->m_Frequency;
		pBiphone->m_LogCondProb = -1 * base2log ( pBiphone->m_CondProb );
		
		MI = base2log ( Freq / ( pBiphone->m_MyFirst->m_Frequency * pBiphone->m_MySecond->m_Frequency ) );
		
		pBiphone->m_MI = MI;
		pBiphone->m_WMI = MI * pBiphone->GetCorpusCount();
		pBiphone->m_NormalizedMI = pBiphone->m_MI - m_MyBiphones.m_Z_biphones;
	}
	m_MyBiphones.ComputeZ_MI();
 	
	for (it.toFirst() ; it.current(); ++it)
	{	
 		pBiphone = it.current();
		pBiphone->m_NormalizedMI =
			m_MyBiphones.m_Z_biphones - pBiphone->m_MI;
	}
}

void CPhoneCollection::ListDisplay(Q3ListView* pView,
	linguistica::ui::status_user_agent& status)
{
	CPhone* pPhone;

	pView->setRootIsDecorated(false);
	pView->setSorting(1);
	// Remove all previous columns
	while (pView->columns() != 0)
		pView->removeColumn(0);

	// Add Column headers
	pView->addColumn("Phone");
	pView->addColumn("Count");
	pView->addColumn("+LogProb");
	pView->addColumn("Prob");

	if (m_SortValidFlag == false)
		Sort(KEY);
 
	// Display all items
	status.major_operation = "Creating phone list for display";
	status.progress.clear();
	status.progress.set_denominator(GetCount());
	for (int phoneno = 0; phoneno < (int) GetCount(); phoneno++) {
		pPhone = GetAtSort(phoneno);
		pPhone->PhoneListDisplay(pView);
		status.progress = phoneno;
	}
	status.progress.clear();
	status.major_operation.clear();
}

void CPhoneCollection::PopulateMonteCarlo( MonteCarlo* pMyMonteCarlo)
{	
	QString				FirstPhone;
	MonteCarlo*			qMonteCarlo;
	int					Size;
	int					NumberOfBigramsFound = 0;
	CPhone*				pPhone, *qPhone;
	CBiphone*			pBiphone;
	int					j;
	// first, Unigram frequencies:		
	for (int i = 0; i < GetSize(); i++)
	{
		pPhone = GetAt(i);
		pMyMonteCarlo->StockDictionary( pPhone->Display(), pPhone->m_Frequency, i );	
	}

	static const QString FileName = "c:\\LxaDeleteMe.txt";
	QFile file( FileName );
	Q3TextStream outf( &file );
	outf.setEncoding( Q3TextStream::Unicode );

	// Now Bigram frequencies:
	if (pMyMonteCarlo->m_ModelType == BIGRAM)
	{		
		for (int i = 0; i < GetSize(); i++) // iterate throught the phonemes...
		{
			pPhone			= GetAt(i);
			FirstPhone		= pPhone->Display();
			Size			= GetSize();
			qMonteCarlo		= new MonteCarlo ( Size, FirstPhone );
			pMyMonteCarlo	->GetMyBigrams()->insert ( FirstPhone, qMonteCarlo ); // this is a QDict within the MonteCarlo, for subMonteCarlos for each phone
			NumberOfBigramsFound = 0;
			
			outf << endl << FirstPhone << endl;

			for (j = 0; j < Size; j++)  // iterate through the phonemes, for the second phoneme of this bigram
			{				 
				qPhone		= GetAt(j);
				pBiphone	= m_MyBiphones.GetBiphone(pPhone, qPhone);
				if (pBiphone)
				{
					qMonteCarlo->StockDictionary( qPhone->Display(), pBiphone->m_CondProb, j);
					NumberOfBigramsFound++;
				}			 
			}
			qMonteCarlo->SetSize ( NumberOfBigramsFound );
			qMonteCarlo->Normalize();				
			//qMonteCarlo->Dump( &outf );
			 
			


			

		}
	}

	pMyMonteCarlo->Normalize();



	


	file.close();


}

void CPhoneCollection::ComputeStringAgreementAndDisagreement(
	CLParse* string1, CLParse* string2,
	double& agreement_unigram, double& agreement_bigram,
	double& disagreement_unigram, double& disagreement_bigram)
{
	struct not_implemented { };
	throw not_implemented();

	static_cast<void>(string1);
	static_cast<void>(string2);
	static_cast<void>(agreement_unigram);
	static_cast<void>(agreement_bigram);
	static_cast<void>(disagreement_unigram);
	static_cast<void>(disagreement_bigram);

//	QString string1_alpha = string1->GetAlphabetizedForm();
//	QString string2_alpha = string2->GetAlphabetizedForm();
//	int i,j;
//	j = 1;
//	// first calculate unigram overlap and non-overlap costs.
//	for (i=1; i <= string1->Size(); i++)
//	{
//		if (string1_alpha[i] == string2_alpha[j] )
//		{
//			agreement_unigram += ;
//		}
//		else if (string1_alpha[i] < string2_alpha[j])
//		{
//			disagreement_unigram += ;
//			if (i==string1->Size()){
//				break;
//			}
//			i++;
//		}
//		else
//		{
//			disagreement_unigram += ;
//			if (j==string1->Size()){
//				break;
//			}
//			j++;
//		}
//	}
//	if (i < string1->Size()){
//		for (; i <= string1->Size(); i++){
//			disagreement_unigram +=  ;
//		}
//	}
//	if (j < string1->Size()){
//		for (; j <= string1->Size(); j++){
//			disagreement_unigram +=  ;
//		}
//	} // end of calculation of unigram figures.
}
