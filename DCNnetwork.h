// Network type for stress discovery with Dynamic Computational Networks
// Copyright © 2009 The University of Chicago
#include <QLabel>

// See the learning documentation in DCNlearning.h for an overview.

#ifndef NETWORK_H
#define NETWORK_H

class	grammar;
class	QString;
class	QLabel;

/**
 *	A network is a DCN that takes a grammar and puts it
 *	in equilibrium. The network is represented as an array
 *  of nodes, each node being type float. The number of nodes
 *  correspondes to the number of syllables in the word (syl).
 *  A network can output the stress of the word with getStress().
 */
class network  
{

private:
	int			syl;
	bool		converged;
	float*		node;
	grammar*	theGrammar;


public:
	network(int syl);
	virtual ~network();

	void		setGrammar(grammar* theGrammar);
	void		equilibrium();
	void		print(QLabel* label);
	QString		getStress();
	bool		isConverged() { return converged; }

};

#endif // NETWORK_H
